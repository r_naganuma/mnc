﻿<%--
=========================================================================================================
  Module      : トップ画面(Default.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%@ Register TagPrefix="uc" TagName="BodyProductCategoryTree" Src="~/Form/Common/Product/BodyProductCategoryTree.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductRanking" Src="~/Form/Common/Product/BodyProductRanking.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductRecommendAdvanced" Src="~/Form/Common/Product/BodyProductRecommendAdvanced.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductHistory" Src="~/Form/Common/Product/BodyProductHistory.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyNews" Src="~/Form/Common/BodyNews.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductRecommendByRecommendEngine" Src="~/Form/Common/Product/BodyProductRecommendByRecommendEngine.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyCategoryRecommendByRecommendEngine" Src="~/Form/Common/Product/BodyCategoryRecommendByRecommendEngine.ascx" %>
<%@ Register TagPrefix="uc" TagName="Parts010RCMD_001" Src="~/Page/Parts/Parts010RCMD_001.ascx" %>
<%@ Register TagPrefix="uc" TagName="Parts010RCMD_002" Src="~/Page/Parts/Parts010RCMD_002.ascx" %>
<%@ Register TagPrefix="uc" TagName="TopMv" Src="~/Page/Parts/Parts000TMPL_001.ascx" %>
<%@ Register TagPrefix="uc" TagName="Promotions" Src="~/Page/Parts/Parts000TMPL_002.ascx" %>
<%@ Register TagPrefix="uc" TagName="Link" Src="~/Page/Parts/Parts000TMPL_003.ascx" %>
<%@ Register TagPrefix="uc" TagName="Keywords" Src="~/Page/Parts/Parts000TMPL_004.ascx" %>
<%@ Register TagPrefix="uc" TagName="Lineup" Src="~/Page/Parts/Parts000TMPL_005.ascx" %>
<%@ Register TagPrefix="uc" TagName="Journal" Src="~/Page/Parts/Parts000TMPL_006.ascx" %>
<%@ Register TagPrefix="uc" TagName="Message" Src="~/Page/Parts/Parts000TMPL_007.ascx" %>
<%@ Register TagPrefix="uc" TagName="Info" Src="~/Page/Parts/Parts000TMPL_008.ascx" %>
<%@ Register TagPrefix="uc" TagName="Insta" Src="~/Page/Parts/Parts000TMPL_009.ascx" %>
<%@ Register TagPrefix="uc" TagName="Brand" Src="~/Page/Parts/Parts000TMPL_010.ascx" %>
<%@ Register TagPrefix="uc" TagName="Pickup" Src="~/Page/Parts/Parts000TMPL_012.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductColorSearchBox" Src="~/Form/Common/Product/ProductColorSearchBox.ascx" %>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Register TagPrefix="uc" TagName="Criteo" Src="~/Form/Common/Criteo.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/DefaultPage.master" autoeventwireup="true" inherits="Default, App_Web_default.aspx.cdcab7d2" title="シンプリス 公式サイト | SIMPLISSE produced by 山本未奈子" MetaDescription="シンプリス公式オンラインストア。サプリメント、スキンケアを展開するビューティーブランド。「自分らしい美しさ」を叶え、女性の人生を心地よく、もっと豊かに。" %>
<%--

下記は保持用のダミー情報です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="ｗ２ユーザー" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<% if (Constants.MOBILEOPTION_ENABLED){%>
	<link rel="Alternate" media="handheld" href="<%= GetMobileUrl() %>" />
<% } %>
<%= this.BrandAdditionalDsignTag %>
<script type="text/javascript" src="<%= Constants.PATH_ROOT %>Js/instagram.js"></script>
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%-- ▽編集可能領域：コンテンツ▽ --%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<article id="top">
  
  <!-- modal -->
  <% if ((((this.Request.Url.Query.Contains("param")) ? (DateTime.ParseExact(this.Request.QueryString["param"], "yyyyMMddHHmmss", null)) : DateTime.Now) > (DateTime.ParseExact("20220518100000", "yyyyMMddHHmmss", null))) && (((this.Request.Url.Query.Contains("param")) ? (DateTime.ParseExact(this.Request.QueryString["param"], "yyyyMMddHHmmss", null)) : DateTime.Now) < (DateTime.ParseExact("20220630150000", "yyyyMMddHHmmss", null)))){ %>
  <section class="modalArea">
    <div class="closeIcn sp_contents">
      <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/icn_close.png" />
    </div>
    <!-- 縦長の場合、クラスに「tate」を入れる -->
    <div class="modalArea_inner tate" style="padding: 0;">
      <div class="closeIcn pc_contents">
        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/icn_close.png" />
      </div>
      <!-- <p class="modalTtl">SIMPLISSE Online Store <br class="sp_contents">パスワード再設定のお願い</p>
      <p class="modalTxt">
        2021年1月28日のオンラインストアリニューアル<br class="sp_contents">以降、ログインをされていないお客さまには、<br>
        パスワードの再設定をお願いしております。<br>
        誠に恐れ入りますが、パスワード再設定の<br class="sp_contents">お手続きをお願いいたします。<br>
        <br>
        <span class="fwBold">パスワードの再設定はこちら</span><br>
        <a href="<%= Constants.PATH_ROOT %>Form/User/PasswordReminderInput.aspx">https://www.simplisse.jp/Form/User/PasswordReminderInput.aspx</a><br>
        <br>
        ご登録のメールアドレス宛に、再設定用の<br class="sp_contents">リンクを記載したメールをお送りいたします。<br>
        必要事項と新しいパスワードをご入力の上、再設定を実施いただくとログインが可能となります。<br>
        お手数をおかけしますが、何卒お願いいたします。<br>
        <br>
        ご不明な点は、下記までお問い合わせください。<br>
        <br>
        メール：<a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_INQUIRY_INPUT) %>">お問い合わせフォーム</a><br>
        電話：0120-370-063（通話料無料）<br class="sp_contents">平日 10:00-12:00 / 13:30-16:00
      </p> -->
      <a href="https://www.simplisse.jp/Page/welcome/">
        <picture>
          <source media="(max-width: 769px)" srcset="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/popup_20220518.jpg">
          <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/popup_20220518.jpg" alt="">
        </picture>
      </a>
    </div>
  </section>
	<% } %>
  <!-- // modal -->

    <!-- MV -->
    <uc:TopMv runat="server" />
    <!-- // MV -->
    <!-- PROMOTIONS -->
    <uc:Promotions runat="server" />
    <!-- // PROMOTIONS -->
    <!-- PICK UP -->
    <uc:Pickup runat="server" />
    <!-- // PICK UP -->
    <!-- LINK -->
    <uc:Link runat="server" />
    <!-- // LINK -->
    <!-- KEYWORDS -->
    <uc:Keywords runat="server" />
    <!-- // KEYWORDS -->
    <!-- Lineup -->
    <uc:Lineup runat="server" />
    <!-- // Lineup -->
    <!-- JOURNAL -->
    <uc:Journal runat="server" />
    <!-- // JOURNAL -->
    <!-- Message -->
    <uc:Message runat="server" />
    <!-- // Message -->
    <!-- info / media -->
    <uc:Info runat="server" />
    <!-- // info / media -->
    <!-- instagram -->
    <uc:Insta runat="server" />
    <!-- // instagram -->
    <!-- BRAND -->
    <uc:Brand runat="server" />
    <!-- // BRAND -->
</article>

<%-- △編集可能領域△ --%>

<script runat="server">
public new void Page_Load(Object sender, EventArgs e)
{
base.Page_Load(sender, e);

var recommendEngineUserControls = WebControlUtility.GetRecommendEngineUserControls(this.Form.FindControl("ContentPlaceHolder1"));
var lProductRecommendByRecommendEngineUserControls = recommendEngineUserControls.Item1;
var lCategoryRecommendByRecommendEngineUserControls = recommendEngineUserControls.Item2;

<%-- ▽編集可能領域：プロパティ設定▽ --%>
// 外部レコメンド連携パーツ設定
// 1つ目の商品レコメンド
if (lProductRecommendByRecommendEngineUserControls.Count > 0)
{
	// レコメンドコードを設定します
	lProductRecommendByRecommendEngineUserControls[0].RecommendCode = "pc111";
	// レコメンドタイトルを設定します
	lProductRecommendByRecommendEngineUserControls[0].RecommendTitle = "おすすめ商品一覧";
	// 商品最大表示件数を設定します
	lProductRecommendByRecommendEngineUserControls[0].MaxDispCount = 5;
	// レコメンド対象にするカテゴリIDを設定します（複数選択時はカンマ区切りで指定）
	lProductRecommendByRecommendEngineUserControls[0].DispCategoryId = "";
	// レコメンド非対象にするカテゴリIDを設定します（複数選択時はカンマ区切りで指定）
	lProductRecommendByRecommendEngineUserControls[0].NotDispCategoryId = "";
	// レコメンド非対象にするアイテムIDを設定します（複数選択時はカンマ区切りで指定）
	lProductRecommendByRecommendEngineUserControls[0].NotDispRecommendProductId = "";
}

// 2つ目の商品レコメンド
if (lProductRecommendByRecommendEngineUserControls.Count > 1)
{
	// レコメンドコードを設定します
	lProductRecommendByRecommendEngineUserControls[1].RecommendCode = "pc112";
	// レコメンドタイトルを設定します
	lProductRecommendByRecommendEngineUserControls[1].RecommendTitle = "おすすめ商品一覧";
	// 商品最大表示件数を設定します
	lProductRecommendByRecommendEngineUserControls[1].MaxDispCount = 5;
	// レコメンド対象にするカテゴリIDを設定します（複数選択時はカンマ区切りで指定）
	lProductRecommendByRecommendEngineUserControls[1].DispCategoryId = "";
	// レコメンド非対象にするカテゴリIDを設定します（複数選択時はカンマ区切りで指定）
	lProductRecommendByRecommendEngineUserControls[1].NotDispCategoryId = "";
	// レコメンド非対象にするアイテムIDを設定します（複数選択時はカンマ区切りで指定）
	lProductRecommendByRecommendEngineUserControls[1].NotDispRecommendProductId = "";
}
<%-- △編集可能領域△ --%>
}

</script>

<%-- CRITEOタグ --%>
<uc:Criteo ID="criteo" runat="server" Datas="<%# null %>" />
</asp:Content>