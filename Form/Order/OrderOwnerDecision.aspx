﻿<%--
=========================================================================================================
  Module      : 注文者決定画面(OrderOwnerDecision.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ page language="C#" masterpagefile="~/Form/Common/OrderPage.master" autoeventwireup="true" inherits="Form_Order_OrderOwnerDecision, App_Web_orderownerdecision.aspx.bf558b1b" title="注文者決定ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" maintainscrollpositiononpostback="true" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="最終更新者" %>

--%>
<%-- ▽▽Amazonペイメントを使う場合はウィジェットを配置するページは必ずSSLでなければいけない▽▽ --%>
<script runat="server">
	public override PageAccessTypes PageAccessType { get { return PageAccessTypes.Https; } }
</script>
<%-- △△Amazonペイメントを使う場合はウィジェットを配置するページは必ずSSLでなければいけない△△ --%>
<%@ Register TagPrefix="uc" TagName="PaypalScriptsForm" Src="~/Form/Common/PayPalScriptsForm.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />
<%-- △編集可能領域△ --%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<article id="cart" class="cartListPage">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!--▽ パンくず ▽-->
<div class="breadArea pc_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				ログイン
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

<!-- main -->
<section class="cartTtl second">
	<h2>
		ご注文手続きへ
	</h2>
</section>
<!-- // main -->

<div class="loginBox">
	<div class="loginBox_member">
		<h3>会員のお客様<span>ログインして、ご購入手続きへお進みください。</span></h3>
		<div class="loginBox_member--wrap">
			<dl class="cartInput">
			<%if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED) { %>
			<dd>
				<p class="adTtl"><%: ReplaceTag("@@User.mail_addr.name@@") %></p>
			</dd>
			<dd class="input"><asp:TextBox ID="tbLoginIdInMailAddr" runat="server" CssClass="input_widthC input_border" PlaceHolder="xxx@simplisse.jp" MaxLength="256" Type="email"></asp:TextBox></dd>
			<%} else { %>
			<dd>
				<p class="adTtl"><%: ReplaceTag("@@User.login_id.name@@") %></p>
			</dd>
			<dd class="input"><asp:TextBox ID="tbLoginId" runat="server" CssClass="input_widthC input_border" MaxLength="15" Type="email"></asp:TextBox></dd>
			<%} %>
			<dd>
				<p class="adTtl"><%: ReplaceTag("@@User.password.name@@") %></p>
			</dd>
			<dd class="input"><asp:TextBox ID="tbPassword" TextMode="Password" autocomplete="off" runat="server" CssClass="input_widthC input_border" MaxLength="15"></asp:TextBox></dd>
			</dl>
			<div><small id="dLoginErrorMessage" class="icnKome" runat="server"></small></div>
			<asp:CheckBox ID="cbAutoCompleteLoginIdFlg" runat="server" CssClass="radioBtn" Text="ログインIDを記憶する" />
			<asp:LinkButton ID="lbLogin" runat="server" onclick="lbLogin_Click" class="loginBtn">ログイン</asp:LinkButton>
			<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_PASSWORD_REMINDER_INPUT) %>" class="loginLink">パスワードをお忘れのお客様</a>
		</div>
	</div><!--loginBox_member-->
	<div class="loginBox_other">
		<h3>新規会員登録はこちら<span>会員さまだけの特典をご用意しています。</span></h3>
		<p class="loginBox_other--txt">
			・会員登録で即日使える500円クーポン進呈中！<br>
			・いつでも送料無料 ＋ 会員ランクに応じて最大10％OFF<br>
			・会員限定セール・特別クーポン・誕生日プレゼント<br>
			・ご購入履歴、ご配送先やお気に入り商品の登録　など
		</p>
		<a href="<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_REGIST_INPUT + "?" + Constants.REQUEST_KEY_NEXT_URL + "=" + Server.UrlEncode(Request[Constants.REQUEST_KEY_NEXT_URL]) %>" class="loginBtn">新規会員登録</a>
		<div style="display: none;" class="alignC" style="width:450px">
			<asp:LinkButton ID="lbUserEasyRegist" runat="server" OnClick="lbUserEasyRegist_Click" class="btn-org btn-large btn-org-blk">かんたん会員登録する</asp:LinkButton>
		</div>
		<%if (this.CartList.HasFixedPurchase == false) { %>
			<asp:LinkButton ID="lbOrderShipping" runat="server" 
				onclick="lbOrderShipping_Click" class="guestBtn">非会員のまま購入</asp:LinkButton>
		</div>
		<%} %>
		<% if (Constants.COMMON_SOCIAL_LOGIN_ENABLED) { %>
		<div style="display: none;" class="dvSocialLoginCooperation">
		<h3>ソーシャルログイン</h3>
			<p>ソーシャルログイン連携によるログインは、こちらから行ってください。</p>
			<ul style="display:flex;margin-bottom:20px;flex-wrap:wrap">
				<% if (Constants.SOCIAL_LOGIN_ENABLED) { %>
				<%-- Facebook --%>
				<li style="padding: 0 0.5em;">
					<a style="width: 170px;background-color: #305097;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 12px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
						href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
								w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Facebook,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
								true,
								Request.Url.Authority) %>">Facebookで新規登録/ログイン</a>
				</li>
				<%-- Twitter --%>
				<li style="padding: 0 0.5em;">
					<a style="width: 170px;background-color: #1da1f2;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 12px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
						href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
								w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Twitter,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
								true,
								Request.Url.Authority) %>">Twitterで新規登録/ログイン</a>
				</li>
				<%-- Yahoo --%>
				<li style="padding: 0 0.5em;">
					<a style="width: 170px;background-color:#ff0020;border:none;border-radius:5px;color:white;padding:1em 2em;text-align:center;text-decoration:none;display: inline-block;font-size:12px;font-family:'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
						href="<%= w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
								w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Yahoo,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
								true,
								Request.Url.Authority) %>">Yahoo!で新規登録/ログイン</a>
				</li>
				<%-- LINE --%>
				<li style="padding: 0 0.5em;">
					<a style="width: 170px;background-color: #00c300;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 12px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
						href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
								w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Line,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
								true,
								Request.Url.Authority) %>">LINEで新規登録/ログイン</a>
					<p style="margin:3px 0 4px;">※LINE連携時に友だち追加します</p>
				</li>
				<% } %>
				<%-- Amazon --%>
				<% if (this.IsVisibleAmazonPayButton || this.IsVisibleAmazonLoginButton) { %>
				<li style="padding: 0 0.5em;">
					<%-- ▼▼Amazonボタンウィジェット▼▼ --%>
					<div id="AmazonPayButton" style="margin-bottom:5px;"></div>
					<%-- ▲▲Amazonボタンウィジェット▲▲ --%>
				</li>
				<% } %>
				<%-- PayPal --%>
				<%if (Constants.PAYPAL_LOGINPAYMENT_ENABLED) {%>
					<li>
						<%
							ucPaypalScriptsForm.LogoDesign = "Login";
							ucPaypalScriptsForm.AuthCompleteActionControl = lbPayPalAuthComplete;
							ucPaypalScriptsForm.GetShippingAddress = (this.IsLoggedIn == false);
						%>
						<div style="width: 218px;margin: 0px 8px 0px 6px">
						<uc:PaypalScriptsForm ID="ucPaypalScriptsForm" runat="server" />
						<div id="paypal-button"></div>
						<% if (SessionManager.PayPalCooperationInfo != null) {%>
							※<%: SessionManager.PayPalCooperationInfo.AccountEMail %> 連携済
						<% } else {%>
							※PayPalで新規登録/ログインします<br />
						<%} %>
						</div>
						<asp:LinkButton ID="lbPayPalAuthComplete" runat="server" OnClick="lbPayPalAuthComplete_Click"></asp:LinkButton>
					</li>
				<% } %>
				<%-- 楽天Connect --%>
				<% if (Constants.RAKUTEN_LOGIN_ENABLED) { %>
					<li style="padding: 0 0.5em;">
						<asp:LinkButton ID="lbRakutenIdConnectRequestAuthForUserRegister" runat="server" OnClick="lbRakutenIdConnectRequestAuth_Click">
							<img src="https://checkout.rakuten.co.jp/p/common/img/btn_idconnect_03.gif" style="width: 218px;"/></asp:LinkButton>
						<p style="margin: 4px 0;">
							楽天会員のお客様は、楽天IDに登録している情報を利用して、<br/>
							「新規会員登録/ログイン」が可能です。
						</p>
					</li>
				<% } %>
			</ul>
		</div>
		<% } %>
	
</div>

<div class="reminderArea">
	<p class="reminderArea_ttl">SIMPLISSE Online Store <br class="sp_contents">パスワード再設定のお願い</p>
	<p class="reminderArea_txt">
		2021年1月28日のオンラインストアリニューアル以降、ログインをされていないお客さまには、パスワードの再設定をお願いしております。<br>
		誠に恐れ入りますが、パスワード再設定のお手続きをお願いいたします。<br>
		<br>
		パスワードの再設定はこちら：<br class="sp_contents"><a href="<%= Constants.PATH_ROOT %>Form/User/PasswordReminderInput.aspx">https://www.simplisse.jp/Form/User/PasswordReminderInput.aspx</a><br>
		<br>
		ご登録のメールアドレス宛に、再設定用のリンクを記載したメールをお送りいたします。<br>
		必要事項と新しいパスワードをご入力の上、再設定を実施いただくとログインが可能となります。<br>
		お手数をおかけしますが、何卒お願いいたします。<br>
		<br>
		ご不明な点は、下記までお問い合わせください。<br>
		<br>
		メール：<a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_INQUIRY_INPUT) %>">お問い合わせフォーム</a><br>
		電話：0120-370-063（通話料無料）平日 <br class="sp_contents">10:00-12:00 / 13:30-16:00
	</p>
</div>

<%--▼▼Amazonウィジェット用スクリプト▼▼--%>
<script type="text/javascript">
	
	window.onAmazonLoginReady = function () {
		amazon.Login.setClientId('<%=Constants.PAYMENT_AMAZON_CLIENTID %>');
	};
	window.onAmazonPaymentsReady = function () {
		if ($('#AmazonPayButton').length) showButton();
	};

	<%-- Amazonボタン表示ウィジェット --%>
	function showButton() {
		var authRequest;
		OffAmazonPayments.Button("AmazonPayButton", "<%=Constants.PAYMENT_AMAZON_SELLERID %>", {
			type: "<%= (this.IsVisibleAmazonPayButton) ? "PwA" : ((this.IsVisibleAmazonLoginButton) ? "LwA" : "") %>",
			color: "Gold",
			size: "large",
			authorization: function () {
				loginOptions = {
					scope: "payments:widget payments:shipping_address profile",
					popup: true,
					state: '<%= Request.RawUrl %>'
				};
				authRequest = amazon.Login.authorize(loginOptions, '<%= this.AmazonCallBackUrl %>');
			},
			onError: function (error) {
				alert(error.getErrorMessage());
			}
		});
		$('#OffAmazonPaymentsWidgets0').css({ 'height': '44px', 'width': '218px' });
	};
</script>
<script async="async" type="text/javascript" charset="utf-8" src="<%=Constants.PAYMENT_AMAZON_WIDGETSSCRIPT %>"></script>
<%-- ▲▲Amazonウィジェット用スクリプト▲▲ --%>
<%-- △編集可能領域△ --%>

</article>

<!--▽ パンくず ▽-->
<div class="breadArea sp_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				ログイン
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->
</asp:Content>