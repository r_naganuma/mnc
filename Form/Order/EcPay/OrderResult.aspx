﻿<%--
=========================================================================================================
  Module      : Order Result(OrderResult.aspx)
  ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2020 All Rights Reserved.
=========================================================================================================
--%>
<%@ page masterpagefile="~/Form/Common/UserPage.master" language="C#" autoeventwireup="true" inherits="EcPay_OrderResult, App_Web_orderresult.aspx.c2a417bf" title="Order Result" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<div id="dvUserRegistRegulation" class="unit">
		<div id="divError" class="dvContentsInfo" style="text-align: left" runat="server" visible="false">
			<br />
			<span>
				ECPayでお支払いが済の場合、現在処理中のため、今しばらくお待ちください。後ほどメールでお知らせします。
				<br />
				<br />
				ECPayでお支払いが失敗した場合、お手数ですが、再注文を行ってください。
			</span>
		</div>
		<div id="divButton" class="dvUserBtnBox" runat="server" visible="false">
			<p>
				<a class="btn btn-large btn-inverse" onclick="RedirectTopPage();">OK</a>
			</p>
		</div>
	</div>
	<script type="text/javascript">
		function RedirectTopPage() {
			window.location = '<%= Constants.PATH_ROOT %>';
		}
	</script>
</asp:Content>