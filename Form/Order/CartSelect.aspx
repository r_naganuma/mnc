﻿<%--
=========================================================================================================
  Module      : ログイン後カート選択画面(CartSelect.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Register TagPrefix="uc" TagName="CartSelect_ProductItem" Src="~/Form/Order/CartSelect_ProductItem.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/OrderPage.master" autoeventwireup="true" inherits="Form_Order_CartSelect, App_Web_cartselect.aspx.bf558b1b" title="カート商品選択ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="ｗ２ユーザー" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta http-equiv="pragma" content="no-cache">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />
<style>
	body{
		padding-top: 0 !important;
	}
	
	header{
		display: none;
	}
</style>
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<article id="cart" class="cartListPage">

<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="cartHead">
	<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/logo.svg" alt="SIMPLISSE" />
</section>

<div class="cartSelectWrap">
	<div>
		<!-- main -->
		<section class="cartTtl">
			<h2>
				カート選択
			</h2>
		</section>
		<!-- // main -->

		<div class="cartLead">
			<p>
				お客様の以前のカート情報が残っております。カート内の商品を選択後次へ進んでください。
			</p>
		</div>

		<div class="cartSelectArea">

			<h3>現在カートに入っている商品</h3>
			<!--▽ 現在カート選択 ▽-->
			<asp:Repeater id="rProductList" runat="server">
			<HeaderTemplate>
				<div class="cartSelectBox">
					<div class="cartSelectBox_ttl">
						<div class="cartSelectBox_ttl--name name">
							<p>商品名</p>
						</div>
						<div class="cartSelectBox_ttl--name price">
							<p>単価</p>
						</div>
						<div class="cartSelectBox_ttl--name check">
							<p>注文する</p>
						</div>
					</div>
					<div class="cartSelectBox_cts">
			</HeaderTemplate>
			<ItemTemplate>
			
				<%-- 現在カートに入っている商品 --%>
				<uc:CartSelect_ProductItem id="ucCartSelect_ProductItem" CartProduct="<%# Container.DataItem %>" DefaultChecked="true" runat="server"></uc:CartSelect_ProductItem>
				
				<%-- 隠し値 --%>
				<asp:HiddenField ID="hfIsSetItem" runat="server" Value="<%# ((CartProduct)Container.DataItem).IsSetItem %>" />
				<asp:HiddenField ID="hfShopId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ShopId %>" />
				<asp:HiddenField ID="hfProductId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductId %>" />
				<asp:HiddenField ID="hfVariationId" runat="server" Value="<%# ((CartProduct)Container.DataItem).VariationId %>" />
				<asp:HiddenField ID="hfIsFixedPurchase" runat="server" Value="<%# ((CartProduct)Container.DataItem).IsFixedPurchase %>" />
				<asp:HiddenField ID="hfAddCartKbn" runat="server" Value="<%# ((CartProduct)Container.DataItem).AddCartKbn %>" />
				<asp:HiddenField ID="hfProductSaleId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductSaleId %>" />
				<asp:HiddenField ID="hfProductSetId" runat="server" Value="<%# OrderPage.GetProductSetId(((CartProduct)Container.DataItem)) %>" />
				<asp:HiddenField ID="hfProductSetNo" runat="server" Value="<%# OrderPage.GetProductSetNo(((CartProduct)Container.DataItem)) %>" />
				<asp:HiddenField ID="hfProductSetItemNo" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductSetItemNo %>" />
				<asp:HiddenField ID="hfProductSetName" runat="server" Value="<%# OrderPage.GetProductSetName(((CartProduct)Container.DataItem)) %>" />
				<asp:HiddenField ID="hfProductOptionSettingList" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductOptionSettingList.GetDisplayProductOptionSettingSelectValues() %>" />

			</ItemTemplate>
			<FooterTemplate>
					</div>
				</div>
			</FooterTemplate>
			</asp:Repeater>
			<!--△ 現在カート選択 △-->

			<div class="beforeCart">
				<h3>以前カートに入れた商品</h3>
				<!--▽ 前回カート選択 ▽-->
				<asp:Repeater id="rProductListBefore" runat="server">
				<HeaderTemplate>
					<div class="cartSelectBox">
						<div class="cartSelectBox_ttl">
							<div class="cartSelectBox_ttl--name name">
								<p>商品名</p>
							</div>
							<div class="cartSelectBox_ttl--name price">
								<p>単価</p>
							</div>
							<div class="cartSelectBox_ttl--name check">
								<p>注文する</p>
							</div>
						</div>
						<div class="cartSelectBox_cts">
				</HeaderTemplate>
				<ItemTemplate>
	
					<%-- 以前カートに入れた商品 --%>
					<uc:CartSelect_ProductItem id="ucCartSelect_ProductItem" CartProduct="<%# Container.DataItem %>" DefaultChecked="false" runat="server"></uc:CartSelect_ProductItem>
					
					<%-- 隠し値 --%>
					<asp:HiddenField ID="hfIsSetItem" runat="server" Value="<%# ((CartProduct)Container.DataItem).IsSetItem %>" />
					<%if (Constants.CARTCOPY_OPTION_ENABLED){ %>
					<asp:HiddenField ID="hfCartId" runat="server" Value="<%# ((CartProduct)Container.DataItem).CartId %>" />
					<%} %>
					<asp:HiddenField ID="hfShopId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ShopId %>" />
					<asp:HiddenField ID="hfProductId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductId %>" />
					<asp:HiddenField ID="hfVariationId" runat="server" Value="<%# ((CartProduct)Container.DataItem).VariationId %>" />
					<asp:HiddenField ID="hfIsFixedPurchase" runat="server" Value="<%# ((CartProduct)Container.DataItem).IsFixedPurchase %>" />
					<asp:HiddenField ID="hfAddCartKbn" runat="server" Value="<%# ((CartProduct)Container.DataItem).AddCartKbn %>" />
					<asp:HiddenField ID="hfProductSaleId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductSaleId %>" />
					<asp:HiddenField ID="hfProductSetId" runat="server" Value="<%# OrderPage.GetProductSetId(((CartProduct)Container.DataItem)) %>" />
					<asp:HiddenField ID="hfProductSetNo" runat="server" Value="<%# OrderPage.GetProductSetNo(((CartProduct)Container.DataItem)) %>" />
					<asp:HiddenField ID="hfProductSetItemNo" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductSetItemNo %>" />
					<asp:HiddenField ID="hfProductSetName" runat="server" Value="<%# OrderPage.GetProductSetName(((CartProduct)Container.DataItem)) %>" />
					<asp:HiddenField ID="hfProductOptionSettingList" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductOptionSettingList.GetDisplayProductOptionSettingSelectValues() %>" />
	
				</ItemTemplate>
				<FooterTemplate>
						</div>
					</div>
				</FooterTemplate>
				</asp:Repeater>
				<!--△ 前回カート選択 △-->
			</div>
		</div>
	</div>

	<div class="cartNextbtn cartListBtn">
		<ul>
			<li class="prevBtn" style="display: none;"><a href="">買い物を続ける</a></li>
			<li class="nextBtn"><asp:LinkButton ID="lbNext" runat="server" OnClick="lbNext_Click">次へ進む</asp:LinkButton></li>
		</ul>
	</div>
</div>
<script>
	function controlCheckAllBeforeProduct(flg) {
		$("input[name*=rProductListBefore]input[type=checkbox]").each(function () {
			$(this).prop("checked", flg);
		});
	}

	function bodyPageLoad() {
		//エンマーク削除
		function word_assassin(target,word){
			if(target.length){
				target.each(function(){
				var txt = $(this).html();
				$(this).html(
					txt.replace(word,'')//unicode escape sequence
				);
				});
			}
		}
		word_assassin($('span.price'),'¥');
	}

</script>
</script>
<%-- △編集可能領域△ --%>

</article>
</asp:Content>