﻿<%--
=========================================================================================================
  Module      : Receiving Store For Response(ReceivingStoreForResponse.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2020 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" autoeventwireup="true" inherits="Form_Order_ReceivingStoreForResponse, App_Web_receivingstoreforresponse.aspx.bf558b1b" %>

<script>
	// Get URL parameter value
	function getUrlParameterValue(name) {
		name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
		var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
		var results = regex.exec(location.search);
		return (results === null) ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	};
	
	window.opener.setConvenienceStoreData(
		getUrlParameterValue('cvsspot'),
		getUrlParameterValue('name'),
		getUrlParameterValue('addr'),
		getUrlParameterValue('tel'));

	window.close();
</script>