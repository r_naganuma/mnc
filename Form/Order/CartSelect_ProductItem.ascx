﻿<%--
=========================================================================================================
  Module      : ログイン後カート選択画面：商品表示ユーザーコントロール(CartSelect_ProductItem.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ control language="C#" autoeventwireup="true" inherits="Form_Order_CartSelect_ProductItem, App_Web_cartselect_productitem.ascx.bf558b1b" %>
<%-- セット商品でない場合 --%>
<div class="cartSelectBox_cts--list" visible="<%# this.CartProduct.IsSetItem == false %>" runat="server">
	<div class="productImg">
		<!-- リンク・一覧画像 -->
		<a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(this.CartProduct, (string)GetKeyValue(this.CartProduct, Constants.FIELD_PRODUCTVARIATION_VARIATION_ID))) %>' runat="server" Visible="<%# this.CartProduct.IsProductDetailLinkValid() %>">
			<w2c:ProductImage ImageTagId="picture" ImageSize="L" ProductMaster="<%# this.CartProduct %>" IsVariation="true" runat="server" /></a>
		<w2c:ProductImage ImageTagId="picture" ImageSize="L" ProductMaster="<%# this.CartProduct %>" IsVariation="true" runat="server" Visible="<%# this.CartProduct.IsProductDetailLinkValid() == false %>" />
	</div>
	<div class="productCts">
		<div class="productName">
			<!-- 商品名 -->
			<a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(this.CartProduct, (string)GetKeyValue(this.CartProduct, Constants.FIELD_PRODUCTVARIATION_VARIATION_ID))) %>' runat="server" Visible="<%# this.CartProduct.IsProductDetailLinkValid() %>">
				<%# WebSanitizer.HtmlEncode(this.CartProduct.ProductJointName) %></a>
		</div>
		<div class="productPrice">
			<p>
				<!-- 商品価格 -->
				<span class="sp_inline">単価(税込)：</span><%# WebSanitizer.HtmlEncode(ValueText.GetValueText(Constants.TABLE_PRODUCT, Constants.FIELD_PRODUCT_TAX_INCLUDED_FLG, this.CartProduct.TaxIncludedFlg)) %> <span class="price"><%#: CurrencyManager.ToPrice(this.CartProduct.Price) %></span>円
			</p>
		</div>
		<div class="remark">
			<span class="sp_inline">注文する：</span><asp:CheckBox id="cbAddToCart" Text=" " Checked="<%# this.DefaultChecked %>" CssClass="radioBtn" runat="server" />
		</div>
	</div>
</div>

<%-- セット商品の場合 --%>
<div class="cartSelectBox_cts--list" visible="<%# this.CartProduct.IsSetItem %>" runat="server">
	<div class="productImg">
		<!-- リンク・一覧画像 -->
		<a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(this.CartProduct, (string)GetKeyValue(this.CartProduct, Constants.FIELD_PRODUCTVARIATION_VARIATION_ID))) %>' runat="server" Visible="<%# this.CartProduct.IsProductDetailLinkValid() %>">
			<w2c:ProductImage ImageTagId="picture" ImageSize="L" ProductMaster="<%# this.CartProduct %>" IsVariation="true" runat="server" /></a>
		<w2c:ProductImage ImageTagId="picture" ImageSize="L" ProductMaster="<%# this.CartProduct %>" IsVariation="true" runat="server" Visible="<%# this.CartProduct.IsProductDetailLinkValid() == false %>" />
	</div>
	<div class="productCts">
		<div class="productName">
			<!-- 商品名 -->
			<a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(this.CartProduct, (string)GetKeyValue(this.CartProduct, Constants.FIELD_PRODUCTVARIATION_VARIATION_ID))) %>' runat="server" Visible="<%# this.CartProduct.IsProductDetailLinkValid() %>">
				<%# WebSanitizer.HtmlEncode(this.CartProduct.ProductJointName) %></a>
		</div>
		<div class="productPrice">
			<p><span class="sp_inline">単価(税込)：</span><%# WebSanitizer.HtmlEncode(ValueText.GetValueText(Constants.TABLE_PRODUCT, Constants.FIELD_PRODUCT_TAX_INCLUDED_FLG, this.CartProduct.TaxIncludedFlg)) %> <span class="price"><%#: CurrencyManager.ToPrice(this.CartProduct.Price) %></span>x
				<%# WebSanitizer.HtmlEncode(this.CartProduct.CountSingle) %><br>
			<!-- 加算ポイント -->
			<%# WebSanitizer.HtmlEncode(StringUtility.AddHeaderFooter("ポイント", GetProductAddPointString(this.ShopId, this.CartProduct.PointKbn1, this.CartProduct.Point1, this.CartProduct.Price), "")) %></p>
		</div>
		<div class="remark" rowspan="<%# OrderPage.GetProductSetRowspan(this.CartProduct) %>" runat="server" visible="<%# this.CartProduct.ProductSetItemNo == 1 %>">
			<!-- チェックボックス -->
			<span class="sp_inline">注文する：</span><asp:CheckBox id="cbAddSetToCart" Text=" " Checked="<%# this.DefaultChecked %>" CssClass="radioBtn" runat="server" />
		</div>
	</div>
</div>