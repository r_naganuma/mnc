﻿<%--
=========================================================================================================
  Module      : 定期購入情報一覧画面(FixedPurchaseList.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_FixedPurchase_FixedPurchaseList, App_Web_fixedpurchaselist.aspx.1601e934" title="定期購入情報一覧ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" language="javascript">
	function bodyPageLoad() {
		// 検索結果数
		var total = $("span.total_counts").html();
		$(".leadTxt .all").html(total);
		var totalList = $("span.pagination_page").html();
		$(".leadTxt .list").html(totalList);
	}
</script>
<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					定期購入情報一覧
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<h2>定期購入情報一覧</h2>

	<div class="mypageBox">
		<div class="mypageBox_cts">
			<p class="leadTxt"><span class="all">0</span>件の商品がリストに入っています（<span class="list">0件表示</span>中）</p>
			<div class="ohAreaBoxWrap">
				
				<%-- 定期購入情報一覧 --%>
				<asp:Repeater ID="rList" ItemType="UserFixedPurchaseListSearchResult" Runat="server">
					<HeaderTemplate>
					</HeaderTemplate>
					<ItemTemplate>
						<div class="ohArea_box fixed">
							<div class="ohArea_box--item">
								<asp:Repeater ID="rFixedPurchaseItemList" DataSource="<%# GetFixedPurchaseItemContainerList(Item.FixedPurchaseId) %>" ItemType="w2.Domain.FixedPurchase.Helper.FixedPurchaseItemContainer" Runat="server">
									<ItemTemplate>
										<a href='<%#: PageUrlCreatorUtility.CreateFixedPurchaseDetailUrl(Item.FixedPurchaseId) %>' class="orderItem">
											<div class="orderItem_pic">
												<w2c:ProductImage ID="ProductImage1" ImageSize="L" IsVariation="<%# String.IsNullOrEmpty(Item.VariationId) == false %>" ProductMaster="<%# Item.DataSource %>" runat="server" Visible="true" />
											</div>
											<div class="orderItem_txt">
												<p><%#: Item.Name %></p>
											</div>
										</a>
									</ItemTemplate>
								</asp:Repeater>
							</div>
							<div class="ohArea_box--detail">
								<dl>
									<dt>
										<p>定期購入ID</p>
									</dt>
									<dd>
										<p><%#: Item.FixedPurchaseId %></p>
									</dd>
								</dl>
								<dl>
									<dt>
										<p>定期購入ステータス</p>
									</dt>
									<dd>
										<p><%#: Item.FixedPurchaseStatusText %></p>
									</dd>
								</dl>
								<dl>
									<dt>
										<p>定期購入設定</p>
									</dt>
									<dd>
										<p><%#: OrderCommon.CreateFixedPurchaseSettingMessage(Item) %></p>
									</dd>
								</dl>
								<dl>
									<dt>
										<p>未出荷注文配送希望日</p>
									</dt>
									<dd>
										<p><%#: GetFixedPurchaseShippingDateNearestValue(Item.FixedPurchaseShippingDateNearest) %></p>
									</dd>
								</dl>
								<dl>
									<dt>
										<p>次回配送日</p>
									</dt>
									<dd>
										<p><%#: (Item.IsCancelFixedPurchaseStatus == false) ? DateTimeUtility.ToStringFromRegion(Item.NextShippingDate, DateTimeUtility.FormatType.ShortDateWeekOfDay1Letter) : "-" %></p>
									</dd>
								</dl>
								<dl>
									<dt>
										<p>購入回数</p>
									</dt>
									<dd>
										<p><%#: StringUtility.ToNumeric(Item.OrderCount) %></p>
									</dd>
								</dl>
							</div>
						</div>
				</ItemTemplate>
					<FooterTemplate>
					</FooterTemplate>
			</asp:Repeater>
		</div>
			
			<%--購入履歴なし --%>
			<% if (StringUtility.ToEmpty(this.AlertMessage) != ""){ %>
				<%= this.AlertMessage%>
			<%} %>
			
			<%--ページャ --%>
			<div class="pagerArea" style="display: none;"><%= this.PagerHtml %></div>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					定期購入情報一覧
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
</asp:Content>