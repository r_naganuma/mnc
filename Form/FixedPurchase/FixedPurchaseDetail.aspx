﻿<%--
=========================================================================================================
  Module      : 定期購入情報詳細画面(FixedPurchaseDetail.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_FixedPurchase_FixedPurchaseDetail, App_Web_fixedpurchasedetail.aspx.1601e934" title="定期購入情報詳細ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<%-- ▼削除禁止：クレジットカードTokenコントロール▼ --%>
<%@ Register TagPrefix="uc" TagName="CreditToken" Src="~/Form/Common/CreditToken.ascx" %>
<%-- ▲削除禁止：クレジットカードTokenコントロール▲ --%>
<%-- ▽▽Amazonペイメントを使う場合はウィジェットを配置するページは必ずSSLでなければいけない▽▽ --%>
<script runat="server">
	public override PageAccessTypes PageAccessType { get { return PageAccessTypes.Https; } }
</script>
<%-- △△Amazonペイメントを使う場合はウィジェットを配置するページは必ずSSLでなければいけない△△ --%>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ Register TagPrefix="uc" TagName="Layer" Src="~/Form/Common/Layer/SearchResultLayer.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaymentDescriptionCvsDef" Src="~/Form/Common/Order/PaymentDescriptionCvsDef.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaymentDescriptionPayPal" Src="~/Form/Common/Order/PaymentDescriptionPayPal.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaypalScriptsForm" Src="~/Form/Common/PayPalScriptsForm.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaymentDescriptionTriLinkAfterPay" Src="~/Form/Common/Order/PaymentDescriptionTriLinkAfterPay.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaidyCheckoutScript" Src="~/Form/Common/Order/PaidyCheckoutScript.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaymentDescriptionAtone" Src="~/Form/Common/Order/PaymentDescriptionAtone.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaidyCheckoutControl" Src="~/Form/Common/Order/PaidyCheckoutControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaymentDescriptionNPAfterPay" Src="~/Form/Common/Order/PaymentDescriptionNPAfterPay.ascx" %>
<%@ Register TagPrefix="uc" TagName="EcPayScript" Src="~/Form/Common/ECPay/EcPayScript.ascx" %>
<%@ Import Namespace="w2.App.Common.Order.Payment.Paidy" %>
<%@ Import Namespace="w2.Domain.UserShipping" %>
<%@ Import Namespace="w2.Domain.User.Helper" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<% if (Constants.RECEIVINGSTORE_TWECPAY_CVSOPTION_ENABLED) { %>
<uc:EcPayScript runat="server" ID="ucECPayScript" />
<% } %>
<%-- UpdatePanel開始 --%>
<asp:UpdatePanel ID="upUpdatePanel" UpdateMode ="Conditional" runat="server">
<ContentTemplate>

<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />

<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					定期購入情報詳細
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
	<h2>定期購入情報詳細</h2>

	<div class="mypageBox">
		<div class="mypageBox_cts">
			<div class="ohDetail">

			<%-- 解約理由 --%>
			<% if (this.FixedPurchaseContainer.IsCancelFixedPurchaseStatus) { %>
			<div class="reasonArea">
				<h3>購入解約</h3>
				<dl class="ohDetail_box">
					<% if (this.FixedPurchaseContainer.CancelReasonId != "") { %>
					<dt>解約理由</dt>
					<dd>
						<%# WebSanitizer.HtmlEncode(this.CancelReason.CancelReasonName) %>
					</dd>
					<% } %>
					<dt style="display: none;">解約メモ</dt>
					<dd style="display: none;">
						<%# WebSanitizer.HtmlEncodeChangeToBr(this.FixedPurchaseContainer.CancelMemo) %>
					</dd>
				</dl>
			</div>
			<% } %>

			<%-- 休止理由 --%>
			<% if (this.FixedPurchaseContainer.IsSuspendFixedPurchaseStatus) { %>
				<div class="reasonArea">
					<h3>購入一時休止</h3>
					<dl class="ohDetail_box">
						<dt>再開予定日</dt>
						<dd>
							<%: (this.FixedPurchaseContainer.ResumeDate != null)
								? DateTimeUtility.ToStringFromRegion(this.FixedPurchaseContainer.ResumeDate, DateTimeUtility.FormatType.ShortDateWeekOfDay1Letter)
								: "指定なし" %>
						</dd>
						<dt>休止理由</dt>
						<dd>
							<%# WebSanitizer.HtmlEncodeChangeToBr(this.FixedPurchaseContainer.SuspendReason) %>
						</dd>
					</dl>
				</div>
			<% } %>

			<%-- 注文情報 --%>
			<div class="dvFixedPurchaseDetail">
				<div runat="server" visible="<%# ((string.IsNullOrEmpty(this.OrderNowMessagesHtmlEncoded) == false) || (this.RegisteredOrderIds.Count > 0)) %>" style="color: red; margin:5px 0px 5px 5px" >
					<asp:Label ID="lbOrderNowMessages" runat="server" />
					<%if ((this.RegisteredOrderIds.Count > 0) && (string.IsNullOrEmpty(this.OrderNowMessagesHtmlEncoded) == false)) { %> <br /> <%} %>
					<asp:Repeater ID="rOrderSuccess" runat="server" Visible="<%#  (this.RegisteredOrderIds.Count > 0) %>" ItemType="System.string">
						<HeaderTemplate>定期注文(</HeaderTemplate>
						<ItemTemplate>
							<span runat="server" visible ="<%# (Container.ItemIndex > 0) %>">&nbsp;</span>
							<a href="<%# WebSanitizer.UrlAttrHtmlEncode(CreateOrderHistoryDetailUrl(Item)) %>"> <%#: Item %> </a>
						</ItemTemplate>
						<FooterTemplate>)が登録されました。</FooterTemplate>
					</asp:Repeater>
				</div>
				<div class="dvContentsInfo" runat="server" Visible="<%# string.IsNullOrEmpty(this.ResumeFixedPurchaseMessageHtmlEncoded) == false %>">
					<asp:Literal runat="server" ID="lResumeFixedPurchaseMessage" />
				</div>
				<h3>定期購入情報</h3>
				<dl class="ohDetail_box">
						<dt>定期購入ID</dt>
						<dd>
							<%: this.FixedPurchaseContainer.FixedPurchaseId%><br />
							<% if (this.IsCancelable == false) { %>
								<span style="color:red">※出荷回数が<%: this.FixedPurchaseCancelableCount %>回以上から定期購入キャンセル、一時休止が可能になります。</span>
							<% } %>
							
							<%-- 定期キャンセル --%>
							<asp:LinkButton style="display: none;" Text="定期購入キャンセル" runat="server" Visible="<%# this.CanCancelFixedPurchase && this.IsCancelable %>" OnClick="btnCancelFixedPurchase_Click" class="btn" />
							<%-- 定期キャンセル（解約理由登録） --%>
							<asp:LinkButton style="display: none;" Text="定期購入キャンセル（解約理由）" runat="server" Visible="<%# this.CanCancelFixedPurchase && this.IsCancelable %>" OnClick="btnCancelFixedPurchaseReason_Click" class="btn" />
							<%-- 定期休止 --%>
							<asp:LinkButton style="display: none;" Text="定期購入一時休止" runat="server" Visible="<%# this.CanSuspendFixedPurchase && this.IsCancelable %>" OnClick="btnSuspendFixedPurchase_Click" class="btn" />
						</dd>
						<dt>定期購入設定</dt>
						<dd>
							<%: OrderCommon.CreateFixedPurchaseSettingMessage(this.FixedPurchaseContainer)%>
							&nbsp;
							<div class="changeBtn">
								<asp:LinkButton Visible="<%# this.DisplayFixedPurchaseShipping %>" Text="サイクル変更" runat="server" OnClick="lbDisplayShippingPatternInfoForm_Click" />
							</div>
						</dd>
					
					<%-- 配送パターン --%>
					<div class="creditWrap" id="dvFixedPurchaseDetailShippingPattern" visible="false" runat="server">
						<dl class="cartInput">
							<dt id="dtMonthlyDate" runat="server">
								<asp:RadioButton ID="rbFixedPurchaseDays" Text="月間隔日付指定" GroupName="FixedPurchaseShippingPattern" OnCheckedChanged="rbFixedPurchaseDays_OnCheckedChanged" AutoPostBack="true" cssClass="radioBtn" runat="server" />
							</dt>
							<dd id="ddMonthlyDate" runat="server">
								<div class="birthdayFlex">
									<div class="birthdayFlex_day">
										<asp:DropDownList ID="ddlFixedPurchaseMonth" runat="server"></asp:DropDownList>
									</div>
									<p class="birthdayFlex_txt">ヶ月ごと</p>
									<div class="birthdayFlex_day">
										<asp:DropDownList ID="ddlFixedPurchaseMonthlyDate" runat="server"></asp:DropDownList>
									</div>
									<p class="birthdayFlex_txt">に届ける</p>
								</div>
								<small><asp:CustomValidator runat="Server" ControlToValidate="ddlFixedPurchaseMonth" ValidationGroup="OrderShipping" ValidateEmptyText="true" SetFocusOnError="true" ClientValidationFunction="ClientValidate" CssClass="error_inline" /></small>
								<small><asp:CustomValidator runat="Server" ControlToValidate="ddlFixedPurchaseMonthlyDate" ValidationGroup="OrderShipping" ValidateEmptyText="true" SetFocusOnError="true" ClientValidationFunction="ClientValidate" CssClass="error_inline" /></small>
							</dd>
							<dt id="dtWeekAndDay" runat="server">
								<asp:RadioButton ID="rbFixedPurchaseWeekAndDay" Text="月間隔・週・曜日指定" GroupName="FixedPurchaseShippingPattern" OnCheckedChanged="rbFixedPurchaseWeekAndDay_OnCheckedChanged" cssClass="radioBtn" AutoPostBack="true" runat="server" />
							</dt>
							<dd id="ddWeekAndDay" runat="server">
								<div class="birthdayFlex">
									<div class="birthdayFlex_day">
										<asp:DropDownList ID="ddlFixedPurchaseIntervalMonths" runat="server" />
									</div>
									<p class="birthdayFlex_txt">ヶ月ごと</p>
									<div class="birthdayFlex_month">
										<asp:DropDownList ID="ddlFixedPurchaseWeekOfMonth" runat="server"></asp:DropDownList>
									</div>
									<div class="birthdayFlex_day">
										<asp:DropDownList ID="ddlFixedPurchaseDayOfWeek" runat="server"></asp:DropDownList>
									</div>
									<p class="birthdayFlex_txt">に届ける</p>
								</div>
								<small><asp:CustomValidator runat="Server" ControlToValidate="ddlFixedPurchaseIntervalMonths" ValidationGroup="OrderShipping" ValidateEmptyText="true" SetFocusOnError="true" ClientValidationFunction="ClientValidate" CssClass="error_inline" /></small>
								<small><asp:CustomValidator runat="Server" ControlToValidate="ddlFixedPurchaseWeekOfMonth" ValidationGroup="OrderShipping" ValidateEmptyText="true" SetFocusOnError="true" ClientValidationFunction="ClientValidate" CssClass="error_inline" /></small>
								<small><asp:CustomValidator runat="Server" ControlToValidate="ddlFixedPurchaseDayOfWeek" ValidationGroup="OrderShipping" ValidateEmptyText="true" SetFocusOnError="true" ClientValidationFunction="ClientValidate" CssClass="error_inline" /></small>
							</dd>
							<dt id="dtIntervalDays" runat="server">
								<asp:RadioButton ID="rbFixedPurchaseIntervalDays" Text="配送日間隔指定" GroupName="FixedPurchaseShippingPattern" OnCheckedChanged="rbFixedPurchaseIntervalDays_OnCheckedChanged" AutoPostBack="true" cssClass="radioBtn" runat="server" />
							</dt>
							<dd id="ddIntervalDays" runat="server">
								<div class="birthdayFlex">
									<div class="birthdayFlex_day">
										<asp:DropDownList ID="ddlFixedPurchaseIntervalDays" runat="server"></asp:DropDownList>
									</div>
									<p class="birthdayFlex_txt">日ごとに届ける</p>
								</div>
								<small><asp:CustomValidator runat="Server" ControlToValidate="ddlFixedPurchaseIntervalDays" ValidationGroup="OrderShipping" ValidateEmptyText="true" SetFocusOnError="true" ClientValidationFunction="ClientValidate" CssClass="error_inline" /></small>
							</dd>
						</dl>
						<div class="updateLink">
							<asp:Button Text="配送パターン更新" runat="server" ValidationGroup="OrderShipping" OnClientClick="return confirm('配送パターンを変更します。\n本当によろしいですか？')" OnClick="btnUpdateShippingPatternInfo_Click" />
							<asp:Button Text="キャンセル" runat="server" OnClick="btnCloseShippingPatternInfo_Click" />
						</div>
						<small ID="sErrorMessage" class="error" runat="server"></small>
					</div>
						<dt>最終購入日</dt>
						<dd>
							<%: DateTimeUtility.ToStringFromRegion(this.FixedPurchaseContainer.LastOrderDate, DateTimeUtility.FormatType.ShortDateWeekOfDay1Letter)%>
						</dd>
						<dt>購入回数</dt>
						<dd>
							<%: this.FixedPurchaseContainer.OrderCount%>
						</dd>

						<dt>定期購入ステータス</dt>
						<dd>
							<span id="spFixedPurchaseStatus" runat="server">
								<%: this.FixedPurchaseContainer.FixedPurchaseStatusText %></span>
							<asp:CheckBox runat="server" ID ="cbResumeFixedPurchase" Visible="<%# this.CanResumeFixedPurchase %>" autoPostBack="true" Text="定期購入を再開" cssClass="radioBtn" />
						</dd>
						<% lbResumeFixedPurchase.OnClientClick = "return confirm('"
							+ "定期購入を再開します。よろしいですか？\\n\\n"
							+ "■注意点\\n"
							+ "' + getResumeShippingMesasge() + '"
							+ "');"; %>
					<% if(cbResumeFixedPurchase.Checked == true) {%>
						<dt>配送再開日</dt>
						<dd>
							次回配送日：<asp:DropDownList ID="ddlResumeFixedPurchaseDate" runat="server" ></asp:DropDownList>
							<asp:LinkButton runat="server" ID="lbResumeFixedPurchase" OnClick="lbResumeFixedPurchase_Click" CssClass="btn" Text="  定期購入を再開する  "></asp:LinkButton>
							<asp:LinkButton runat="server" ID="lbResumeFixedPurchaseCancel" OnClick="lbResumeFixedPurchaseCancel_Click" CssClass="btn" Text="  キャンセル  "></asp:LinkButton>
							<br/>
							<div id="dvResumeFixedPurchaseErr" class="error" visible = "false" runat="server"></div>
						</dd>
					<%} %>
						<dt>決済ステータス</dt>
						<dd>
							<span id="spPaymentStatus" runat="server">
								<%: this.FixedPurchaseContainer.PaymentStatusText%></span>
							&nbsp;
						</dd>

						<dt>お支払い方法</dt>
						<dd>
							<%: this.Payment.PaymentName %>
							<%if (string.IsNullOrEmpty(this.FixedPurchaseContainer.CardInstallmentsCode) == false) { %>
							（<%: ValueText.GetValueText(Constants.TABLE_ORDER, OrderCommon.CreditInstallmentsValueTextFieldName, this.FixedPurchaseContainer.CardInstallmentsCode)%>払い）
							<%} %>
							<% if (this.Payment.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_AMAZON_PAYMENT) { %>
								<div style="margin: 10px 0;">
									<small style="line-height: 1.83;">※現在のAmazon Payでの配送先情報、お支払い方法を表示しています。<br>※配送先のご変更もこちらからお願いします。</small>
								</div>
								<iframe id="AmazonDetailWidget" src="<%: PageUrlCreatorUtility.CreateAmazonPayWidgetUrl(true, fixedPurchaseId: this.FixedPurchaseContainer.FixedPurchaseId) %>" style="width:100%;border:none;"></iframe>
							<% } %>
							<div class="changeBtn">
								<asp:LinkButton ID="lbDisplayInputOrderPaymentKbn"
									Text="お支払い方法変更" runat="server"
									OnClick="lbDisplayInputOrderPaymentKbn_Click"
									Visible="<%# this.CanCancelFixedPurchase %>"
									Enabled="<%# this.IsDisplayInputOrderPaymentKbn %>" />
							</div>
							<%-- ▼PayPalログインここから▼ --%>
							<div style="display: <%= dvOrderPaymentPattern.Visible ? "block" : "none"%>">
								<%if (Constants.PAYPAL_LOGINPAYMENT_ENABLED) {%>
									<%
										ucPaypalScriptsForm.LogoDesign = "Payment";
										ucPaypalScriptsForm.AuthCompleteActionControl = lbPayPalAuthComplete;
									%>
									<uc:PaypalScriptsForm ID="ucPaypalScriptsForm" runat="server" />
									<div id="paypal-button"></div>
									<%if (SessionManager.PayPalCooperationInfo != null) {%>
										<%: (SessionManager.PayPalCooperationInfo != null) ? SessionManager.PayPalCooperationInfo.AccountEMail : "" %> 連携済<br/>
									<%} %>
									<asp:LinkButton ID="lbPayPalAuthComplete" runat="server" OnClick="lbPayPalAuthComplete_Click"></asp:LinkButton>
								<%} %>
							</div>
							<%-- ▲PayPalログインここまで▲ --%>
						</dd>
						<!--Update payment pattern-->
						<asp:HiddenField ID="hfPaidyTokenId" runat="server" />
						<asp:HiddenField ID="hfPaidyPaySelected" runat="server" />
						<div id="dvOrderPaymentPattern" class="creditWrap" visible="false" runat="server">
									<div>
										<div>
											<span style="color:red" runat="server" visible="<%# (string.IsNullOrEmpty(StringUtility.ToEmpty(this.DispLimitedPaymentMessages[0])) == false) %>">
												<%# StringUtility.ToEmpty(this.DispLimitedPaymentMessages[0]) %><br/>
											</span>
											<dl class="cartInput">
												<asp:Repeater ID="rPayment" DataSource="<%# this.ValidPayments[0] %>" ItemType="w2.Domain.Payment.PaymentModel" runat="server" >
												<ItemTemplate>
												<asp:HiddenField ID="hfPaymentId" Value='<%# Item.PaymentId %>' runat="server" />
												<asp:HiddenField ID="hfPaymentName" Value='<%# Item.PaymentName %>' runat="server" />
												<dt><w2c:RadioButtonGroup ID="rbgPayment" GroupName='Payment' Checked="<%# this.FixedPurchaseContainer.OrderPaymentKbn == (string)Item.PaymentId %>" Text="<%#: Item.PaymentName %>" OnCheckedChanged="rbgPayment_OnCheckedChanged" AutoPostBack="true" CssClass="radioBtn" runat="server" /></dt>

												<%-- クレジット --%>
												<dd id="ddCredit" visible="<%# ((string)Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_CREDIT) %>" runat="server">
													<div class="creditPic">
														<img src="../../Contents/ImagesPkg/cart/credit.png" alt="クレジットカード">
													</div>
													<asp:DropDownList style="display: none;" ID="ddlUserCreditCard" runat="server" AutoPostBack="true" DataTextField="text" DataValueField="value" OnSelectedIndexChanged="ddlUserCreditCard_OnSelectedIndexChanged"></asp:DropDownList>
													
													<%-- ▽新規カード▽ --%>
													<% if (IsNewCreditCard()){ %>
													
													<% if (this.IsCreditCardLinkPayment() == false) { %>
													<%--▼▼ カード情報取得用 ▼▼--%>
													<input type="hidden" id="hidCinfo" name="hidCinfo" value="<%# CreateGetCardInfoJsScriptForCreditToken(Container) %>" />
													<%--▲▲ カード情報取得用 ▲▲--%>

													<%--▼▼ クレジット Token保持用 ▼▼--%>
													<asp:HiddenField ID="hfCreditToken" Value="" runat="server" />
													<%--▲▲ クレジット Token保持用 ▲▲--%>

													<%--▼▼ カード情報入力（トークン未取得・利用なし） ▼▼--%>
													<dl id="divCreditCardNoToken" runat="server" class="cartInput">
														<%if (OrderCommon.CreditCompanySelectable) {%>
														<strong>カード会社</strong>
														<p><asp:DropDownList id="ddlCreditCardCompany" runat="server" DataTextField="Text" DataValueField="Value" CssClass="input_widthG input_border"></asp:DropDownList></p>
														<%} %>
														<p class="adTtl">カード番号&nbsp;<span class="icnKome">※</span></p>
														<dd class="creditDD">
															<w2c:ExtendedTextBox id="tbCreditCardNo1" Type="tel" runat="server" CssClass="tel" MaxLength="16" autocomplete="off"></w2c:ExtendedTextBox>
															<small class="fred">
																<asp:CustomValidator ID="cvCreditCardNo1" runat="Server"
																	ControlToValidate="tbCreditCardNo1"
																	ValidationGroup="OrderPayment"
																	ValidateEmptyText="true"
																	SetFocusOnError="true"
																	ClientValidationFunction="ClientValidate"
																	CssClass="error_inline" />
															</small>
															<p class="inputNotice">
																カードの表記のとおりご入力ください。<br />
																例：1234567890123456（ハイフンなし）
															</p>
														</dd>
														<p class="adTtl">有効期限&nbsp;<span class="icnKome">※</span></p>
														<dd class="creditDD">
															<div class="creditLimitedFlex">
																<div class="creditLimitedFlex_list">
																	<asp:DropDownList id="ddlCreditExpireMonth" runat="server" CssClass="expMonth"></asp:DropDownList>
																</div>
																<div class="creditLimitedFlex_txt">
																	<p>月 /</p>
																</div>
																<div class="creditLimitedFlex_list">
																	<asp:DropDownList id="ddlCreditExpireYear" runat="server" CssClass="expYear"></asp:DropDownList>
																</div>
																<div class="creditLimitedFlex_txt">
																	<p>年</p>
																</div>
															</div>
														</dd>
														<p class="adTtl">カード名義人&nbsp;<span class="icnKome">※</span></p>
														<dd class="creditDD">
															<asp:TextBox id="tbCreditAuthorName" runat="server" CssClass="nameFull" MaxLength="50" autocomplete="off"></asp:TextBox><br />
															<small class="fred">
																<asp:CustomValidator ID="cvCreditAuthorName" runat="Server"
																	ControlToValidate="tbCreditAuthorName"
																	ValidationGroup="OrderPayment"
																	ValidateEmptyText="true"
																	SetFocusOnError="true"
																	ClientValidationFunction="ClientValidate"
																	CssClass="error_inline" />
															</small>
														</dd>
														<div id="trSecurityCode" visible="<%# OrderCommon.CreditSecurityCodeEnable %>" runat="server">
														<p class="adTtl">セキュリティコード&nbsp;<span class="icnKome">※</span></p>
														<dd class="inputCode creditDD">
															<div class="inputCodeBox">
																<asp:TextBox id="tbCreditSecurityCode" runat="server" CssClass="securityCode" MaxLength="4" autocomplete="off"></asp:TextBox>
															</div>
															<small class="fred">
															<asp:CustomValidator ID="cvCreditSecurityCode" runat="Server"
																ControlToValidate="tbCreditSecurityCode"
																ValidationGroup="OrderPayment"
																ValidateEmptyText="true"
																SetFocusOnError="true"
																ClientValidationFunction="ClientValidate"
																CssClass="error_inline" />
															</small>
														</dd>
														</div>
													</dl>
													<%--▲▲ カード情報入力（トークン未取得・利用なし） ▲▲--%>

													<%--▼▼ カード情報入力（トークン取得済） ▼▼--%>
													<div id="divCreditCardForTokenAcquired" runat="server">
														<%if (OrderCommon.CreditCompanySelectable) {%>
														<p class="adTtl">カード会社</p>
														<dd class="creditDD"><asp:Literal ID="lCreditCardCompanyNameForTokenAcquired" runat="server"></asp:Literal></dd>
														<%} %>
														<p class="adTtl">カード番号</p>
														<dd class="creditDD">XXXXXXXXXXXX<asp:Literal ID="lLastFourDigitForTokenAcquired" runat="server"></asp:Literal>
														<asp:LinkButton id="lbEditCreditCardNoForToken" OnClick="lbEditCreditCardNoForToken_Click" runat="server">再入力</asp:LinkButton></dd>
														<p class="adTtl">有効期限</p>
														<dd class="creditDD"><asp:Literal ID="lExpirationMonthForTokenAcquired" runat="server"></asp:Literal>
															/
															<asp:Literal ID="lExpirationYearForTokenAcquired" runat="server"></asp:Literal>
														(月/年)</dd>
														<p class="adTtl">カード名義人</p>
														<dd class="creditDD"><asp:Literal ID="lCreditAuthorNameForTokenAcquired" runat="server"></asp:Literal></dd>
													</div>
													<%--▲▲ カード情報入力（トークン取得済） ▲▲ --%>

													<dl id="Div3" visible="<%# OrderCommon.CreditInstallmentsSelectable %>" runat="server" class="cartInput">
														<p class="adTtl">支払い回数</p>
														<dd class="creditDD">
															<div class="inputCodeBox">
																<asp:DropDownList id="dllCreditInstallments" runat="server" DataTextField="Text" DataValueField="Value" CssClass="input_border" autocomplete="off"></asp:DropDownList>
															</div>
															<span style="display: none;" class="fgray">※AMEX/DINERSは一括のみとなります。</span>
														</dd>
													</dl>
													<% } else { %>
														<div>遷移する外部サイトでカード番号を入力してください。</div>
													<% } %>

													<asp:CheckBox style="display: none;" ID="cbRegistCreditCard" runat="server" Checked="false" OnCheckedChanged="cbRegistCreditCard_OnCheckedChanged" Text="登録する" autocomplete="off" AutoPostBack="true" />
													<div id="divUserCreditCardName" visible="false" runat="server">
														<p>クレジットカードを保存する場合は、以下をご入力ください。</p>
														<strong>クレジットカード登録名&nbsp;<span class="fred">※</span></strong>
														<p>
															<asp:TextBox ID="tbUserCreditCardName" Text="" MaxLength="100" CssClass="input_widthD input_border" runat="server" autocomplete="off"></asp:TextBox><br />
															<small class="fred">
															<asp:CustomValidator ID="cvUserCreditCardName" runat="Server"
																ControlToValidate="tbUserCreditCardName"
																ValidationGroup="OrderPayment"
																ValidateEmptyText="true"
																SetFocusOnError="true"
																ClientValidationFunction="ClientValidate"
																CssClass="error_inline" />
															</small>
														</p>
													</div>
													<span id="spanErrorMessageForCreditCard" style="color: red; display: none" runat="server"></span>
													<%-- △新規カード△ --%>

													<%-- ▽登録済みカード▽ --%>
													<% }else{ %>
													<div id="divCreditCardDisp" runat="server">
														<%if (OrderCommon.CreditCompanySelectable) {%>
														<strong>カード会社</strong>
														<p><%: this.CreditCardCompanyName %><br /></p>
														<%} %>
														<strong>カード番号</strong>
														<p>XXXXXXXXXXXX<%: this.LastFourDigit %><br /></p>
														<strong>有効期限</strong>
														<p><%: this.ExpirationMonth %>/<%: this.ExpirationYear %> (月/年)</p>
														<strong>カード名義人</strong>
														<p><%: this.CreditAuthorName %></p>
														<asp:HiddenField ID="hfCreditCardId" runat="server" />

														<div visible="<%# OrderCommon.CreditInstallmentsSelectable %>" runat="server">
														<strong>支払い回数</strong>
															<p>
																<asp:DropDownList id="dllCreditInstallments2" runat="server" CssClass="input_border"></asp:DropDownList>
																<br/>
																<span class="fgray">※AMEX/DINERSは一括のみとなります。</span>
															</p>
														</div>
													</div>
													<% } %>
													<%-- △登録済みカード△ --%>
												</dd>

												<%-- コンビニ(後払い) --%>
												<dd id="ddCvsDef" visible="<%# ((string)Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_CVS_DEF) %>" runat="server">
													<uc:PaymentDescriptionCvsDef runat="server" id="ucPaymentDescriptionCvsDef" />
												</dd>
													
												<%-- 後付款(TriLink後払い) --%>
												<dd id="ddTriLinkAfterPayPayment" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_TRILINK_AFTERPAY) %>" runat="server">
													<uc:PaymentDescriptionTriLinkAfterPay runat="server" id="ucPaymentDescriptionTryLinkAfterPay" />
												</dd>

												<%-- Amazon Pay --%>
												<dd id="ddAmazonPay" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_AMAZON_PAYMENT) %>" runat="server">
													<div style="display: none;">
														<small>※配送先情報、または、お支払い方法の変更を希望される方は「アドレス帳」→「お支払い方法」の順で選択してください。</small>
													</div>
													<iframe id="AmazonInputWidget" src="<%: PageUrlCreatorUtility.CreateAmazonPayWidgetUrl(false, fixedPurchaseId: this.FixedPurchaseContainer.FixedPurchaseId) %>" style="width:100%;border:none;"></iframe>
													<div id="constraintErrorMessage" style="color:red;padding:5px" ClientIDMode="Static" runat="server"></div>
													<asp:HiddenField ID="hfAmazonBillingAgreementId" ClientIDMode="Static" runat="server" />
													<dl style="display: none;" class="cartInput">
															<dt>配送方法</dt>
															<dd class="creditDD">
																<asp:DropDownList ID="ddlShippingMethodForAmazonPay" OnSelectedIndexChanged="ddlShippingMethod_OnSelectedIndexChanged" runat="server" AutoPostBack="true"></asp:DropDownList>
															</dd>
														<% if (this.DeliveryCompany.IsValidShippingTimeSetFlg && (this.WddlShippingMethodForAmazonPay.SelectedValue == Constants.FLG_SHOPSHIPPINGSHIPPINGCOMPANY_SHIPPING_KBN_EXPRESS)) { %>
															<dt>配送希望時間帯</dt>
															<dd class="creditDD">
																<asp:DropDownList ID="ddlShippingTimeForAmazonPay" runat="server"></asp:DropDownList>
															</dd>
														<% } %>
													</dl>
												</dd>
												<%-- Amazon Pay(CV2) --%>
												<dd id="ddAmazonPayCv2" visible="<%# Item.IsPaymentIdAmazonPayCv2 %>" runat="server">
												<%--▼▼ Amazon Pay(CV2)ボタン ▼▼--%>
												<div id="AmazonPayCv2Button2" style="display: inline-block"></div>
												<%--▲▲ Amazon Pay(CV2)ボタン ▲▲--%>
												</dd>
												
												<%-- 代金引換 --%>
												<dd id="ddCollect" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_COLLECT) %>" runat="server">
												</dd>
												
												<%-- PayPal --%>
												<dd id="ddPayPal" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_PAYPAL) %>" runat="server">
													<%if (SessionManager.PayPalCooperationInfo != null) {%>
														ご利用のPayPal アカウント：<br/>
														<b><%: SessionManager.PayPalCooperationInfo.AccountEMail %></b>
													<%} else {%>
														ご利用にはPayPalログインが必要です。
													<%} %>
													<uc:PaymentDescriptionPayPal runat="server" id="PaymentDescriptionPayPal" />
												</dd>

												<%-- Paidy --%>
												<dd id="ddPaidy" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_PAIDY) %>" runat="server">
													<uc:PaidyCheckoutControl ID="ucPaidyCheckoutControl" runat="server" />
												</dd>

												<%-- atone翌月払い --%>
												<dd id="ddPaymentAtone" class="Atone_0" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_ATONE) %>" runat="server">
													<span id="spanErrorMessageForAtone" class="spanErrorMessageForAtone" style="color: red; display: none" runat="server"></span>
													<uc:PaymentDescriptionAtone runat="server" id="PaymentDescriptionAtone" />
												</dd>

												<%-- aftee決済設定 --%>
												<dd id="ddPaymentAftee" class="Aftee_0" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_AFTEE) %>" runat="server">
													<span id="spanErrorMessageForAftee" class="spanErrorMessageForAftee" style="color: red; display: none" runat="server"></span>
												</dd>

												<!-- NP後払い -->
												<dd id="ddNpAfterPay" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_NP_AFTERPAY) %>" runat="server">
													<uc:PaymentDescriptionNPAfterPay runat="server" id="PaymentDescriptionNPAfterPay" />
												</dd>

											</ItemTemplate>
											</asp:Repeater>
											<%--▼▼Amazon支払契約ID格納▼▼--%>
											<asp:HiddenField runat="server" value="<%# this.FixedPurchaseContainer.ExternalPaymentAgreementId %>" ID="hfAmazonBillingAgreementId" />
											</dl>
										</div><!--list-->
									</div>
									<div id="divOrderPaymentUpdateButtons" style="display: block"> 
									<asp:HiddenField ID="hfPaymentNameSelected" runat="server" />
									<asp:HiddenField ID="hfPaymentIdSelected" runat="server" />
									<asp:HiddenField ID="hfPaymentPriceSelected" runat="server" />
									<div class="updateLink">
										<asp:LinkButton ID="lbUpdatePayment" Text="情報更新" runat="server" ValidationGroup="OrderPayment"
											OnClientClick="doPostbackEvenIfCardAuthFailed=false;return AlertPaymentChange();"
											OnClick="btnUpdatePaymentPatternInfo_Click" ></asp:LinkButton>
										<asp:LinkButton ID="btnClosePaymentPatternInfo" Text="キャンセル" runat="server" OnClick="btnClosePaymentPatternInfo_Click"></asp:LinkButton>
									</div>
									</div>
									<div id="divOrderPaymentUpdateExecFroms" style="display: none"> 
										更新中です...
									</div>
									<small id="sErrorMessagePayment" class="error" runat="server"></small>
						</div>
						<!--End Update payment pattern-->
						<dt>注文メモ</dt>
						<dd>
							<%#: (string.IsNullOrEmpty(this.FixedPurchaseContainer.Memo) ? "指定なし" : this.FixedPurchaseContainer.Memo) %>
						</dd>
					<% if ((this.FixedPurchaseContainer.OrderPaymentKbn == Constants.FLG_PAYMENT_PAYMENT_ID_CREDIT) && (this.UserCreditCardInfo != null)){ %>
						<dt>利用クレジットカード情報</dt>
						<dd>
							<% if (this.UserCreditCardInfo.DispFlg == Constants.FLG_USERCREDITCARD_DISP_FLG_ON) { %>
							カード登録名: <%:this.UserCreditCardInfo.CardDispName %><%: this.UserCreditCardInfo.DispFlag ? "" : " (削除済)" %><br />
							<% } %>
							<%if (OrderCommon.CreditCompanySelectable && (this.UserCreditCardInfo.CompanyName != string.Empty)) {%>
							カード会社: <%: this.UserCreditCardInfo.CompanyName %><br />
							<%} %>
							カード番号: XXXXXXXXXXXX<%: this.UserCreditCardInfo.LastFourDigit %><br />
							有効期限: <%: this.UserCreditCardInfo.ExpirationMonth + "/" + this.UserCreditCardInfo.ExpirationYear + " (月/年)" %><br />
							カード名義人: <%: this.UserCreditCardInfo.AuthorName %>
							<span style="font-weight: bold; display: block; margin-top: 2px;"><%= this.CompleteMessage %></span>
						</dd>
					<%} %>

					<% if (Constants.W2MP_POINT_OPTION_ENABLED) { %>
						<dt>次回購入の利用ポイント</dt>
						<dd>
							<%: GetNumeric(this.FixedPurchaseContainer.NextShippingUsePoint) %><%: Constants.CONST_UNIT_POINT_PT %>&nbsp;
							<asp:LinkButton ID="lbDisplayUpdateNextShippingUsePointForm" Visible="<%# this.CanCancelFixedPurchase %>" Enabled="<%# this.BeforeCancelDeadline %>" Text="  利用ポイント変更  " runat="server" OnClick="lbDisplayUpdateNextShippingUsePointForm_Click" class="btn" />&nbsp;
							（利用可能ポイント：<%: GetNumeric(this.LoginUserPointUsableForFixedPurchase) %><%: Constants.CONST_UNIT_POINT_PT %>）<br />
							<div id="dvNextShippingUsePoint" visible="false" runat="server">
								<asp:TextBox ID="tbNextShippingUsePoint" style="margin-top: 10px;" runat="server"/><br />
								<asp:CustomValidator runat="server" ControlToValidate="tbNextShippingUsePoint" ValidationGroup="FixedPurchaseModifyInput" ValidateEmptyText="true" SetFocusOnError="true" ClientValidationFunction="ClientValidate" CssClass="error_inline" />
								<span style='color: red;'><%= this.NextShippingUsePointErrorMessage %><br /></span>
								<asp:LinkButton ID="lbUpdateNextShippingUsePoint" Text="  利用ポイント更新  " OnClick="lbUpdateNextShippingUsePoint_Click" OnClientClick="return confirm('ご利用ポイントを変更します。よろしいですか？');" runat="server" class="btn" />&nbsp;
								<asp:LinkButton ID="lbCloseUpdateNextShippingUsePointForm" Text="  キャンセル  " OnClick="lbCloseUpdateNextShippingUsePointForm_Click" runat="server" class="btn" />&nbsp;
							</div>
							<small>
	<pre>
	※入力した利用ポイントは次回購入時に適用されます。次回配送日の <%# this.ShopShipping.FixedPurchaseCancelDeadline %>日前 まで変更可能です。
	　（一度入力済みの利用ポイントを減らす際、入力済みポイントが有効期限切れの場合には、
	　ポイントが消滅しますのでご注意ください。）
	※次回注文生成時、ポイント数が注文時の利用可能ポイント数より大きい場合、
	　適用されなかった分のポイントはお客様のポイントに戻されます。
	　（ポイント有効期限切れの場合は、ポイントは戻らず消滅します。）
	※定期購入に期間限定ポイントを利用することはできません。
	</pre>
							</small>
						</dd>
					<% } %>
					<% if (Constants.W2MP_COUPON_OPTION_ENABLED) { %>
						<dt style="display: none;">次回購入の<br>利用クーポン</dt>
						<dd style="display: none;">
							<%: (this.FixedPurchaseContainer.NextShippingUseCouponDetail == null) ? "利用なし" : this.FixedPurchaseContainer.NextShippingUseCouponDetail.DisplayName %>
							<div class="changeBtn">
								<asp:LinkButton ID="lbDisplayUpdateNextShippingUseCouponForm" Visible="<%# this.CanAddNextShippingUseCoupon %>" Enabled="<%# this.BeforeCancelDeadline %>" Text="  利用クーポン変更  " runat="server" OnClick="lbDisplayUpdateNextShippingUseCouponForm_Click" />
								<asp:LinkButton ID="lbCancelNextShippingUseCoupon" Visible="<%# this.CanCancelNextShippingUseCoupon %>" Enabled="<%# this.BeforeCancelDeadline %>" Text="  キャンセル  " runat="server" OnClick="lbCancelNextShippingUseCoupon_Click" />
							</div>
							<div class="noticeArea">
								<p>
									※入力した利用クーポンは次回購入時に適用されます。次回お届け予定日の <%#: this.ShopShipping.FixedPurchaseCancelDeadline %>日前 まで変更可能です。
								</p>
								<p>
									※次回注文生成時、クーポンが本注文に適用不可となる場合、クーポンが戻されます。
								</p>
							</div>
						</dd>
						<div id="dvNextShippingUseCoupon" visible="false" class="creditWrap" runat="server">
							<div id="divCouponInputMethod" runat="server" Visible="<%# this.UsableCoupons.Length > 0 %>" style="margin: 30px 0;">
							<asp:RadioButtonList ID="rblCouponInputMethod" runat="server" AutoPostBack="true"
								OnSelectedIndexChanged="rblCouponInputMethod_SelectedIndexChanged" OnDataBinding="rblCouponInputMethod_DataBinding"
								DataSource="<%# GetCouponInputMethod() %>" DataTextField="Text" DataValueField="Value" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" cssClass="radioBtn" />
							</div>
							<div id="dvCouponSelectArea" runat="server"  Visible="<%# this.UsableCoupons.Length > 0 %>">
								<asp:DropDownList CssClass="input_border" style="width: 240px" ID="ddlNextShippingUseCouponList" runat="server" DataSource="<%# GetUsableCouponListItems() %>" DataTextField="Text" DataValueField="Value" AutoPostBack="true"></asp:DropDownList>
							</div>
							<div id="dvCouponCodeInputArea" runat="server">
								<p class="adTtl">クーポンコード</p>
								<asp:TextBox ID="tbNextShippingUseCouponCode" runat="server" MaxLength="30" autocomplete="off" />
							</div>
							<div style="display: none;"><asp:LinkButton runat="server" ID="lbShowCouponBox" Text="クーポンBOX" OnClick="lbShowCouponBox_Click" Visible="<%# this.UsableCoupons.Length > 0 %>"
								style="color: #ffffff !important; background-color: #000 !important;
									border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25); text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25); display: inline-block;
									padding: 4px 10px 4px; margin-bottom: 0; font-size: 13px; line-height: 18px; text-align: center; vertical-align: middle; cursor: pointer;
									border: 1px solid #cccccc; border-radius: 4px; box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05); white-space: nowrap;"/></div>

							<div style='color: red; margin-top:5px; margin-bottom:5px;'><%: this.NextShippingUseCouponErrorMessage %></div>
							<div class="updateLink">
								<asp:LinkButton ID="lbUseCoupon" Text="クーポンを適用" OnClick="btnUpdateUseCoupon_Click"  OnClientClick="return confirm('ご利用クーポンを変更します。よろしいですか？');" runat="server"/>
								<asp:LinkButton ID="lbCancel" Text="キャンセル" OnClick="lbCloseUpdateNextShippingUseCouponForm_Click" runat="server"/>
							</div>
							<!--▽クーポンBOX▽-->
							<div runat="server" id="dvCouponBox" style="z-index: 1; top: 0; left: 0; width: 100%; height: 120%; position: fixed; background-color: rgba(128, 128, 128, 0.75);" visible="false">
							<div id="dvCouponList" style="width: 800px; height: 500px; top: 50%; left: 50%; text-align: center; border: 2px solid #aaa; background: #fff; position: fixed; z-index: 2; margin:-250px 0 0 -400px;">
							<h2 style="height: 20px; color: #fff; background-color: #000; font-size: 16px; padding: 3px 0px; border-bottom: solid 1px #ccc; ">クーポンBOX</h2>
							<div style="height: 400px; overflow: auto;">
							<asp:Repeater ID="rCouponList" ItemType="w2.Domain.Coupon.Helper.UserCouponDetailInfo" runat="server" DataSource="<%# this.UsableCoupons %>">
								<HeaderTemplate>
								<table>
										<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:150px;">クーポンコード</th>
										<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:230px;">クーポン名</th>
										<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:100px;">割引金額<br />/割引率</th>
										<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:70px;">利用可能回数</th>
										<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:150px;">有効期限</th>
										<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:100px;"></th>
								</HeaderTemplate>
								<ItemTemplate>
										<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:150px; background-color: white;">
											<%#: Item.CouponCode %><br />
											<asp:HiddenField runat="server" ID="hfCouponBoxCouponCode" Value="<%# Item.CouponCode %>" />
										</td>
										<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:230px; background-color: white;"
											title="<%#: Item.CouponDispDiscription %>">
											<%#: Item.CouponDispName %>
										</td>
										<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:70px; background-color: white;">
											<%#: (StringUtility.ToEmpty(Item.DiscountPrice) != "")
													? CurrencyManager.ToPrice(Item.DiscountPrice)
													: (StringUtility.ToEmpty(Item.DiscountRate) != "")
														? StringUtility.ToEmpty(Item.DiscountRate) + "%"
														: "-" %>
										</td>
										<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:70px; background-color: white;">
											<%#: GetCouponCount(Item) %>
										</td>
										<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:150px; background-color: white;">
											<%#:DateTimeUtility.ToStringFromRegion(Item.ExpireEnd, DateTimeUtility.FormatType.LongDateHourMinute1Letter) %>
										</td>
										<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:100px; background-color: white;">
											<asp:LinkButton runat="server" id="lbCouponSelect" OnClick="lbCouponSelect_Click" style="color: #ffffff !important; background-color: #000 !important;
												border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25); text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25); display: inline-block;
												padding: 4px 10px 4px; margin-bottom: 0; font-size: 13px; line-height: 18px; text-align: center; vertical-align: middle; cursor: pointer;
												border: 1px solid #cccccc; border-radius: 4px; box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05); white-space: nowrap;">このクーポンを使う</asp:LinkButton>
										</td>
								</ItemTemplate>
								<FooterTemplate>
								</table>
								</FooterTemplate>
							</asp:Repeater>
							</div>
							<div style="width: 100%; height: 50px; display: block; z-index: 3">
								<asp:LinkButton ID="lbCouponBoxClose" OnClick="lbCouponBoxClose_Click" runat="server"
									style="padding: 8px 12px; font-size: 14px; color: #333; text-decoration: none; border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
									display: inline-block; line-height: 18px; color: #333333; text-align: center; vertical-align: middle; border-radius: 5px; cursor: pointer; background-color: #f5f5f5;
									border: 1px solid #cccccc; box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05); text-decoration: none; background-image: none; margin: 5px auto">クーポンを利用しない</asp:LinkButton>
							</div>
							</div>
							</div>
							<!--△クーポンBOX△-->
						</div>
					<% } %>

					<% if ((this.FixedPurchaseShippingContainer.ShippingReceivingStoreFlg == Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_OFF)
						|| (Constants.RECEIVINGSTORE_TWECPAY_CVSOPTION_ENABLED == false)) { %>
						<dt>次回配送日</dt>
						<dd>
							<div class="nextWrap">
								<p>
									<%: DateTimeUtility.ToStringFromRegion(this.FixedPurchaseContainer.NextShippingDate, DateTimeUtility.FormatType.ShortDateWeekOfDay1Letter) %>
								</p>
								<div class="skipWrap" visible="<%# this.CanCancelFixedPurchase %>" runat="server">
								<% if (this.DisplaySkipNextShipping) { %>
									<div class="skipBox">
									<% if (this.HasClickSkipNextShipping) { %>
									<p runat="server" Visible="<%# this.IsCancelable %>">次回配送日の <%# this.ShopShipping.FixedPurchaseCancelDeadline %>日前 までスキップ可能です。</p>
									<p runat="server" Visible="<%# this.IsCancelable == false %>">出荷回数が<%# this.FixedPurchaseCancelableCount %>回以上からスキップ可能です。</p>
									<% } else { %>
									<p>定期購入スキップ制限回数を超えました。</p>
									<% } %>
									<asp:Button Text="次回配送スキップ" runat="server" Enabled="<%# this.BeforeCancelDeadline && this.IsCancelable && this.HasClickSkipNextShipping %>" OnClientClick="return confirm('次回配送をスキップします。よろしいですか？')" OnClick="btnSkipNextShipping_Click" class="skipBox_btn" />
									</div>
								<% } %>
								<div class="skipBox">
									<p runat="server">次回配送日の <%# this.ShopShipping.FixedPurchaseCancelDeadline %>日前 まで変更可能です。</p>
									<asp:Button ID="btnChangeNextShippingDate" runat="server" OnClick="btnChangeNextShippingDate_Click" Text="次回配送日変更"
									Enabled="<%# this.BeforeCancelDeadline %>" class="skipBox_btn" />
								</div>
								</div>
							</div>
						</dd>
					<% } %>
					<div id="dvChangeNextShippingDate" class="creditWrap" visible="false" runat="server">
						<dl class="cartInput">
							<dd>
								<div class="birthdayFlex change">
									<div class="birthdayFlex_year">
										<asp:DropDownList ID="ddlNextShippingDate" runat="server" DataSource="<%# GetChangeNextShippingDateList() %>"
									DataTextField="text" DataValueField="value" SelectedValue ='<%# this.FixedPurchaseContainer.NextShippingDate.Value.ToString("yyyy/MM/dd") %>'
									OnSelectedIndexChanged="ddlNextShippingDate_SelectedIndexChanged" AutoPostBack="true" />
									</div>
									<p class="birthdayFlex_txt">次回配送日を変更後、次々回配送日は自動変更されません</p>
								</div>
								<p><asp:Label runat="server" ID="lNextShippingDateErrorMessage" CssClass="fred" /></p>
								<div class="updateLink">
								<asp:Button ID="btnUpdateNextShippingDate" Text="次回配送日更新" runat="server" OnClientClick="return confirm('次回配送予定日を変更します。\n本当によろしいですか？')" OnClick="btnUpdateNextShippingDate_Click" />
								<asp:Button ID="btnCancelNextShippingDate" Text="キャンセル" runat="server" OnClick="btnCancelNextShippingDate_Click" />
								</div>
							</dd>
						</dl>
					</div>

					<dt>次々回配送日</dt>
					<dd>
						<%: DateTimeUtility.ToStringFromRegion(this.FixedPurchaseContainer.NextNextShippingDate.Value, DateTimeUtility.FormatType.ShortDateWeekOfDay1Letter) %>
					</dd>
					<dt runat="server" visible="<%# this.FixedPurchaseContainer.IsOrderRegister %>">今すぐ注文</dt>
					<dd runat="server" visible="<%# this.FixedPurchaseContainer.IsOrderRegister %>">
						<div class="changeBtn noPosition">
							<asp:Button ID="btnFixedPurchaseOrder" Text="今すぐ注文" runat="server" OnClientClick = "return confirm('今すぐ注文を登録します。よろしいですか？')" OnClick="btnFixedPurchaseOrder_Click" />
						</div>
						<asp:CheckBox ID="cbUpdateNextShippingDate" Text="次回配送日を更新する" CssClass="radioBtn" runat="server" Checked="<%# Constants.FIXEDPURCHASEORDERNOW_NEXT_SHIPPING_DATE_UPDATE_DEFAULT %>" />
						<br />
						<div class="noticeArea">
							<p style="display: none;">
								※設定された次回購入の利用ポイント数が本注文に適用されます。<br />
								　適用後、次回購入の利用ポイント数が「0」にリセットされます
							</p>
							<p>
								※今すぐ注文の配送日は「<%# ReplaceTag("@@DispText.shipping_date_list.none@@") %>」で登録されます。
							</p>
							<p>
								※「次回配送日を更新する」にチェックを入れた場合には、定期購入情報の次回・次々回配送日が変更されます。
							</p>
							<p>
								※「次回配送日を更新する」にチェックをしない場合には、
								定期購入情報の次回・次々回配送日は変更されません。
							</p>
						</div>
					</dd>
				</dl>
			</div>
			<%--▽領収書情報▽--%>
			<% if (Constants.RECEIPT_OPTION_ENABLED) { %>
			<div class="ohShipping">
				<h3>領収書情報</h3>
				<% if (this.IsReceiptInfoModify == false) { %>
				<dl class="ohDetail_box borderB0">
					<dt>領収書希望</dt>
					<dd>
						<%: ValueText.GetValueText(Constants.TABLE_FIXEDPURCHASE, Constants.FIELD_FIXEDPURCHASE_RECEIPT_FLG, this.FixedPurchaseContainer.ReceiptFlg) %>
						<div class="changeBtn"><asp:LinkButton Text="領収書情報変更" runat="server" Visible="<%# this.CanModifyReceiptInfo %>" OnClick="lbDisplayReceiptInfoForm_Click" /></div>
					</dd>
				</dl>
				<dl class="ohDetail_box borderT0 borderB0" runat="server" visible="<%# this.FixedPurchaseContainer.ReceiptFlg == Constants.FLG_ORDER_RECEIPT_FLG_ON %>">
					<dt>宛名</dt>
					<dd><%: this.FixedPurchaseContainer.ReceiptAddress %></dd>
				</dl>
				<dl class="ohDetail_box borderT0" runat="server" visible="<%# this.FixedPurchaseContainer.ReceiptFlg == Constants.FLG_ORDER_RECEIPT_FLG_ON %>">
					<dt>但し書き</dt>
					<dd><%: this.FixedPurchaseContainer.ReceiptProviso %></dd>
				</dl>

				<%--<div class="receiptDL">
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Receipt/ReceiptDownload.aspx") %>" class="receiptDL_btn">領収書をダウンロード</a>
					<div class="receiptDL_note">
						<p>
							※Web領収書の発行（表示）は、1回のご注文につき1回のみです。
						</p>
						<p>
							※ご注文いただいた商品が出荷されてから発行が可能となります。
						</p>
					</div>
				</div>--%>
				<% } else { %>
				<dl class="cartInput">
					<tr>
						<dt>領収書希望</dt>
						<dd>
							<asp:DropDownList ID="ddlReceiptFlg" runat="server" DataTextField="Text" DataValueField="Value"
								OnSelectedIndexChanged="ddlReceiptFlg_OnSelectedIndexChanged" AutoPostBack="true" DataSource="<%# ValueText.GetValueItemList(Constants.TABLE_FIXEDPURCHASE, Constants.FIELD_FIXEDPURCHASE_RECEIPT_FLG) %>" />
						</dd>
					</tr>
					<tr id="trReceiptAddressInput" runat="server">
						<th>
							<dt>宛名<span class="icnKome">※</span></dt>
						</th>
						<td>
							<dd>
								<asp:TextBox ID="tbReceiptAddress" runat="server" />
								<asp:CustomValidator runat="Server"
									ControlToValidate="tbReceiptAddress"
									ValidationGroup="ReceiptRegisterModify"
									ClientValidationFunction="ClientValidate"
									ValidateEmptyText="true"
									SetFocusOnError="true"
									CssClass="error_inline" />
							</dd>
						</td>
					</tr>
					<tr id="trReceiptProvisoInput" runat="server">
						<th>
							<dt>但し書き<span class="icnKome">※</span></dt>
						</th>
						<td>
							<dd>
								<asp:TextBox ID="tbReceiptProviso" runat="server" />
								<asp:CustomValidator runat="Server"
									ControlToValidate="tbReceiptProviso"
									ValidationGroup="ReceiptRegisterModify"
									ClientValidationFunction="ClientValidate"
									ValidateEmptyText="true"
									SetFocusOnError="true"
									CssClass="error_inline" />
							</dd>
						</td>
					</tr>
					<div style="display: block" class="updateLink">
						<asp:Button Text="領収書情報更新" runat="server" OnClientClick="return confirm('領収書情報を変更してもよろしいですか？')" OnClick="btnUpdateReceiptInfo_Click" />
						<asp:Button Text="キャンセル" runat="server" OnClick="lbDisplayReceiptInfoForm_Click" />
						<br />&nbsp<small>※注文済みの領収書情報は変更されませんのでご注意ください。</small>
						<br /><span style="color: red;"><%: this.ReceiptInfoModifyErrorMessage %></span>
					</div>
				</dl>
				<% } %>
			</div>
			<br />
			<% } %>
			<%--△領収書情報△--%>
			<div class="ohShipping">
				<%-- お届け先情報 --%>
				<h3>お届け先情報</h3>
				<% if (this.IsUserShippingModify == false) { %>
				<dl class="ohDetail_box borderB0">
					<% if (this.UseShippingAddress) { %>
						<dt>住所</dt>
						<dd>
							<div class="changeBtn">
								<% if (this.Payment.PaymentId != Constants.FLG_PAYMENT_PAYMENT_ID_AMAZON_PAYMENT_CV2) { %>
								<asp:LinkButton ID="lbDisplayUserShippingInfoForm" Visible="<%# this.CanCancelFixedPurchase && this.BeforeCancelDeadline && (this.Payment.PaymentId != Constants.FLG_PAYMENT_PAYMENT_ID_AMAZON_PAYMENT) %>" Text="お届け先情報変更" CssClass="btn" runat="server" OnClick="lbDisplayUserShippingInfoForm_Click" />
								<% } else if(this.CanCancelFixedPurchase && this.BeforeCancelDeadline) { %>
								<%--▼▼ Amazon Pay(CV2)ボタン ▼▼--%>
								<div id="AmazonPayCv2Button" style="display: inline-block"></div>
								<%--▲▲ Amazon Pay(CV2)ボタン ▲▲--%>
								<% } %>
							</div>
								<%= IsCountryJp(this.FixedPurchaseShippingContainer.ShippingCountryIsoCode)
										? "〒" + WebSanitizer.HtmlEncode(this.FixedPurchaseShippingContainer.ShippingZip) + "<br />"
										: "" %>
							<%: this.FixedPurchaseShippingContainer.ShippingAddr1%>
							<%: this.FixedPurchaseShippingContainer.ShippingAddr2%>
								<%: this.FixedPurchaseShippingContainer.ShippingAddr3%><br />
								<%: this.FixedPurchaseShippingContainer.ShippingAddr4%>
								<%: this.FixedPurchaseShippingContainer.ShippingAddr5%><br />
								<%= (IsCountryJp(this.FixedPurchaseShippingContainer.ShippingCountryIsoCode) == false)
										? WebSanitizer.HtmlEncode(this.FixedPurchaseShippingContainer.ShippingZip) + "<br />"
										: "" %>
								<%: this.FixedPurchaseShippingContainer.ShippingCountryName%>
						</dd>
					<% if (Constants.DISPLAY_CORPORATION_ENABLED) { %>
						<%-- 企業名・部署名 --%>
						<dt><%: ReplaceTag("@@User.company_name.name@@") %>・
							<%: ReplaceTag("@@User.company_post_name.name@@") %></dt>
						<dd>
							<%: this.FixedPurchaseShippingContainer.ShippingCompanyName%><br />
							<%: this.FixedPurchaseShippingContainer.ShippingCompanyPostName%>
						</dd>
					<% } %>
						<%-- 氏名 --%>
						<dt><%: ReplaceTag("@@User.name.name@@", this.ShippingAddrCountryIsoCode) %></dt>
						<dd>
							<%: this.FixedPurchaseShippingContainer.ShippingName1%><%: this.FixedPurchaseShippingContainer.ShippingName2%>&nbsp;様
							<% if (IsCountryJp(this.FixedPurchaseShippingContainer.ShippingCountryIsoCode)) { %>
							（<%: this.FixedPurchaseShippingContainer.ShippingNameKana1%><%: this.FixedPurchaseShippingContainer.ShippingNameKana2%>&nbsp;さま）
							<% } %>
						</dd>
						<%-- 電話番号 --%>
						<dt><%: ReplaceTag("@@User.tel1.name@@", this.ShippingAddrCountryIsoCode) %></dt>
						<dd>
							<%: this.FixedPurchaseShippingContainer.ShippingTel1%>
						</dd>
					<% } %>
					<% if (this.UseShippingReceivingStore) { %>
							<dt>店舗ID</dt>
							<dd>
								<div style="float:right"><asp:LinkButton Text="お届け先情報変更" runat="server" Visible="<%# this.CanCancelFixedPurchase && this.BeforeCancelDeadline && (this.Payment.PaymentId != Constants.FLG_PAYMENT_PAYMENT_ID_AMAZON_PAYMENT) %>" OnClick="lbDisplayUserShippingInfoForm_Click" class="btn" /></div>
								<%: this.FixedPurchaseShippingContainer.ShippingReceivingStoreId %>
							</dd>
							<dt>店舗名称</dt>
							<dd>
								<%: this.FixedPurchaseShippingContainer.ShippingName %>
							</dd>
							<dt>店舗住所</dt>
							<dd>
								<%: this.FixedPurchaseShippingContainer.ShippingAddr4 %>
							</dd>
							<dt>店舗電話番号</dt>
							<dd>
								<%: this.FixedPurchaseShippingContainer.ShippingTel1 %>
							</dd>
					<% } %>
						<dt>配送方法</dt>
						<dd>
							<%: ValueText.GetValueText(Constants.TABLE_ORDERSHIPPING, Constants.FIELD_ORDERSHIPPING_SHIPPING_METHOD, this.FixedPurchaseShippingContainer.ShippingMethod) %>
						</dd>
						<dt>配送サービス</dt>
						<dd>
							<%: this.DeliveryCompany.DeliveryCompanyName %>
						</dd>
					<% if (this.DeliveryCompany.IsValidShippingTimeSetFlg && (this.FixedPurchaseShippingContainer.ShippingMethod == Constants.FLG_SHOPSHIPPINGSHIPPINGCOMPANY_SHIPPING_KBN_EXPRESS) && (this.UseShippingAddress))
		{ %>
						<dt>配送希望時間帯</dt>
						<dd>
							<%#: this.HasShippingTime %>
						</dd>
					<%} %>
				</dl>
				<% }else{ %>
				<asp:HiddenField ID="hfCvsShopFlg" runat="server" Value='<%# this.FixedPurchaseShippingContainer.ShippingReceivingStoreFlg %>' />
				<asp:HiddenField ID="hfSelectedShopId" runat="server" Value='<%# this.FixedPurchaseShippingContainer.ShippingReceivingStoreId %>' />
				<dl class="cartInput pdg">
						<dd>
							<span style="color: red; display: block;">
								<asp:Literal ID="lShippingCountryErrorMessage" runat="server"></asp:Literal>
							</span>
							<asp:DropDownList
								DataTextField="Text"
								DataValueField="Value"
								SelectedValue='<%# IsDisplayButtonConvenienceStore(this.FixedPurchaseContainer.Shippings[0].ShippingReceivingStoreFlg)
									? CartShipping.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE
									: null %>'
								ID="ddlShippingType"
								AutoPostBack="true"
								OnSelectedIndexChanged="ddlShippingType_SelectedIndexChanged"
								DataSource='<%# GetPossibleShippingType(this.FixedPurchaseContainer.Shippings[0].ShippingReceivingStoreFlg) %>'
								runat="server"
								CssClass="UserShippingAddress">
							</asp:DropDownList>
							<% if (Constants.RECEIVINGSTORE_TWECPAY_CVSOPTION_ENABLED) { %>
							<asp:DropDownList
								ID="ddlShippingReceivingStoreType"
								DataTextField="Text"
								DataValueField="Value"
								Visible='<%# IsDisplayButtonConvenienceStore(this.FixedPurchaseContainer.Shippings[0].ShippingReceivingStoreFlg) %>'
								AutoPostBack="true"
								OnSelectedIndexChanged="ddlShippingReceivingStoreType_SelectedIndexChanged"
								DataSource='<%# ShippingReceivingStoreType() %>'
								runat="server"
								CssClass="UserShippingAddress">
							</asp:DropDownList>
							<% } %>
							<% if (Constants.RECEIVINGSTORE_TWPELICAN_CVSOPTION_ENABLED) { %>
							<span id="spConvenienceStoreSelect" visible="<%# this.UseShippingReceivingStore && (Constants.RECEIVINGSTORE_TWECPAY_CVSOPTION_ENABLED == false) %>" runat="server">
								<a href="javascript:openConvenienceStoreMapPopup();" class="btn btn-success convenience-store-button">Family/OK/Hi-Life</a>
							</span>
							<span id="spConvenienceStoreEcPaySelect" runat="server" visible='<%# ((this.FixedPurchaseContainer.Shippings[0].ShippingReceivingStoreFlg == Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_ON) && Constants.RECEIVINGSTORE_TWECPAY_CVSOPTION_ENABLED) %>'>
								<asp:LinkButton
									ID="lbOpenEcPay"
									runat="server"
									class="btn btn-success convenience-store-button"
									OnClick="lbOpenEcPay_Click"
									Text="  電子マップ  " />
							</span>
							<div id="dvErrorShippingConvenience" style="display: none;" runat="server">
								<span class="error_inline"><%#: WebMessages.GetMessages(WebMessages.ERRMSG_FRONT_GROCERY_STORE) %></span>
							</div>
							<div id="dvErrorPaymentAndShippingConvenience" runat="server" visible="false">
								<span class="error_inline"><%#: WebMessages.GetMessages(WebMessages.ERRMSG_FRONT_PAYMENT_METHOD_CHANGED_TO_CONVENIENCE_STORE) %></span>
							</div>
							<% } %>
						</dd>
					<%-- ▽コンビニ▽ --%>
					<div id="divShippingInputFormConvenience" visible="<%# this.UseShippingReceivingStore %>" runat="server" class="shipping_convenience">
								<dt><%: ReplaceTag("@@DispText.shipping_convenience_store.shopId@@") %></dt>
								<dd id="ddCvsShopId">
									<span style="font-weight:normal;">
										<asp:Literal ID="lCvsShopId" runat="server" Text="<%# this.FixedPurchaseShippingContainer.ShippingReceivingStoreId %>"></asp:Literal>
									</span>
									<asp:HiddenField ID="hfCvsShopId" runat="server" Value="<%# this.FixedPurchaseShippingContainer.ShippingReceivingStoreId %>" />
								</dd>
								<dt><%: ReplaceTag("@@DispText.shipping_convenience_store.shopName@@") %></dt>
								<dd id="ddCvsShopName">
									<span style="font-weight:normal;">
										<asp:Literal ID="lCvsShopName" runat="server" Text="<%# this.FixedPurchaseShippingContainer.ShippingName %>"></asp:Literal>
									</span>
									<asp:HiddenField ID="hfCvsShopName" runat="server" Value="<%# this.FixedPurchaseShippingContainer.ShippingName %>" />
								</dd>
								<dt><%: ReplaceTag("@@DispText.shipping_convenience_store.shopAddress@@") %></dt>
								<dd id="ddCvsShopAddress">
									<span style="font-weight:normal;">
										<asp:Literal ID="lCvsShopAddress" runat="server" Text="<%# this.FixedPurchaseShippingContainer.ShippingAddr4 %>"></asp:Literal>
									</span>
									<asp:HiddenField ID="hfCvsShopAddress" runat="server" Value="<%# this.FixedPurchaseShippingContainer.ShippingAddr4 %>" />
								</dd>
								<dt><%: ReplaceTag("@@DispText.shipping_convenience_store.shopTel@@") %></dt>
								<dd id="ddCvsShopTel">
									<span style="font-weight:normal;">
										<asp:Literal ID="lCvsShopTel" runat="server" Text="<%# this.FixedPurchaseShippingContainer.ShippingTel1 %>"></asp:Literal>
									</span>
									<asp:HiddenField ID="hfCvsShopTel" runat="server" Value="<%# this.FixedPurchaseShippingContainer.ShippingTel1 %>" />
								</dd>
					</div>
					<%-- △コンビニ△ --%>
					<div visible="<%# this.UseShippingAddress %>" id="divShippingInputFormInner" runat="server">
						<dt><%: ReplaceTag("@@User.name.name@@", this.ShippingAddrCountryIsoCode) %> <span class="icnKome">※</span></dt>
						<dd>
							<div class="nameFlex">
								<div class="nameFlex_list">
									<p>姓</p>
									<asp:TextBox ID="tbShippingName1" runat="server" MaxLength="10" ></asp:TextBox>
								</div>
								<div class="nameFlex_list">
									<p>名</p><asp:TextBox ID="tbShippingName2" runat="server" MaxLength="10" ></asp:TextBox>
								</div>
							</div>
							<asp:CustomValidator
								ID="cvShippingName1"
								runat="Server"
								ControlToValidate="tbShippingName1"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<asp:CustomValidator
								ID="cvShippingName2"
								runat="Server"
								ControlToValidate="tbShippingName2"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
					<% if (this.IsShippingAddrJp) { %>
						<dt>カナ <span class="icnKome">※</span></dt>
						<dd>
							<div class="nameFlex">
								<div class="nameFlex_list">
									<p>セイ</p>
									<asp:TextBox ID="tbShippingNameKana1" runat="server" MaxLength="20"></asp:TextBox>
								</div>
								<div class="nameFlex_list">
									<p>メイ</p><asp:TextBox ID="tbShippingNameKana2" runat="server" MaxLength="20"></asp:TextBox>
								</div>
							</div>
							<asp:CustomValidator
								ID="cvShippingNameKana1"
								runat="Server"
								ControlToValidate="tbShippingNameKana1"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<asp:CustomValidator
								ID="cvShippingNameKana2"
								runat="Server"
								ControlToValidate="tbShippingNameKana2"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
					<% } %>
					<% if (Constants.GLOBAL_OPTION_ENABLE) { %>
						<dt>
							<%: ReplaceTag("@@User.country.name@@", this.ShippingAddrCountryIsoCode) %>
							<span class="icnKome">※</span>
						</dt>
						<dd>
							<asp:DropDownList runat="server" id="ddlShippingCountry" AutoPostBack="true" OnSelectedIndexChanged="ddlShippingCountry_SelectedIndexChanged"></asp:DropDownList>
								<asp:CustomValidator
									ID="cvShippingCountry"
									runat="Server"
									ControlToValidate="ddlShippingCountry"
									ValidationGroup="FixedPurchaseModifyInput"
									ValidateEmptyText="true"
									SetFocusOnError="true"
									ClientValidationFunction="ClientValidate"
									EnableClientScript="false"
									CssClass="error_inline" />
						</dd>
					<% } %>
					<% if (this.IsShippingAddrJp) { %>
						<dt>
							<%: ReplaceTag("@@User.zip.name@@", this.ShippingAddrCountryIsoCode) %>
							<span class="icnKome">※</span>
						</dt>
						<dd>
							<div class="zipFlex">
								<div class="zipFlex_list">
									<asp:TextBox ID="tbShippingZip1" runat="server" MaxLength="3"></asp:TextBox>
								</div>
								<div class="zipFlex_line">
									<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
								</div>
								<div class="zipFlex_list">
									<asp:TextBox ID="tbShippingZip2" runat="server" MaxLength="4" OnTextChanged="lbSearchShippingAddr_Click"></asp:TextBox>
								</div>
							</div>
							<asp:LinkButton style="display: none;" ID="lbSearchShippingAddr" runat="server" OnClick="lbSearchShippingAddr_Click" class="btn btn-mini" OnClientClick="return false;">
								住所検索
							</asp:LinkButton><br/>
							<%--検索結果レイヤー--%>
							<uc:Layer ID="ucLayer" runat="server" />
							<asp:CustomValidator
								ID="cvShippingZip1"
								runat="Server"
								ControlToValidate="tbShippingZip1"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<asp:CustomValidator
								ID="cvShippingZip2"
								runat="Server"
								ControlToValidate="tbShippingZip2"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<span style="color :Red" id="addrSearchErrorMessage"><%: this.ZipInputErrorMessage %></span>
						</dd>
						<dt>
							住所
							<span class="icnKome">※</span>
						</dt>
						<dd>
							<div class="selectPrefecture">
								<asp:DropDownList ID="ddlShippingAddr1" runat="server"></asp:DropDownList>
							</div>
							<asp:CustomValidator
								ID="cvShippingAddr1"
								runat="Server"
								ControlToValidate="ddlShippingAddr1"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
					<% } %>
						<dd>
							<p class="adTtl">
								<%: ReplaceTag("@@User.addr2.name@@", this.ShippingAddrCountryIsoCode) %> 
								<span class="icnKome">※</span>
							</p>
							<% if (this.IsShippingAddrTw) { %>
								<asp:DropDownList runat="server" ID="ddlShippingAddr2" DataSource="<%# this.UserTwCityList %>" AutoPostBack="true" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="ddlShippingAddr2_SelectedIndexChanged"></asp:DropDownList>
							<% } else { %>
								<asp:TextBox ID="tbShippingAddr2" runat="server" MaxLength="40"></asp:TextBox>
								<asp:CustomValidator
									ID="cvShippingAddr2"
									runat="Server"
									ControlToValidate="tbShippingAddr2"
									ValidationGroup="FixedPurchaseModifyInput"
									ValidateEmptyText="true"
									SetFocusOnError="true"
									ClientValidationFunction="ClientValidate"
									CssClass="error_inline" />
							<% } %>
						</dd>
						<dd>
							<p class="adTtl">
								<%: ReplaceTag("@@User.addr3.name@@", this.ShippingAddrCountryIsoCode) %>
								<% if (IsAddress3Necessary(this.ShippingAddrCountryIsoCode)){ %><span class="icnKome">※</span><% } %>
							</p>
							<% if (this.IsShippingAddrTw) { %>
								<asp:DropDownList runat="server" ID="ddlShippingAddr3" DataTextField="Key" DataValueField="Value" ></asp:DropDownList>
							<% } else { %>
								<asp:TextBox ID="tbShippingAddr3" runat="server" MaxLength="40"></asp:TextBox>
								<asp:CustomValidator
									ID="cvShippingAddr3"
									runat="Server"
									ControlToValidate="tbShippingAddr3"
									ValidationGroup="FixedPurchaseModifyInput"
									ValidateEmptyText="true"
									SetFocusOnError="true"
									ClientValidationFunction="ClientValidate"/>
							<% } %>
						</dd>
						<dd <%: (Constants.DISPLAY_ADDR4_ENABLED || (this.IsShippingAddrJp == false)) ? "" : "style=\"display:none;\"" %>>
							<p class="adTtl">
								<%: ReplaceTag("@@User.addr4.name@@", this.ShippingAddrCountryIsoCode) %> 
								<%if (this.IsShippingAddrJp == false) {%><span class="icnKome">※</span><%} %>
							</p>
							<asp:TextBox ID="tbShippingAddr4" runat="server" MaxLength="40"></asp:TextBox>
							<asp:CustomValidator
								ID="cvShippingAddr4"
								runat="Server"
								ControlToValidate="tbShippingAddr4"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
					<% if (this.IsShippingAddrJp == false) { %>
						<dd>
							<p class="adTtl">
								<%: ReplaceTag("@@User.addr5.name@@", this.ShippingAddrCountryIsoCode) %>
								<% if (this.IsShippingAddrUs) { %><span class="icnKome">※</span><% } %>
							</p>
							<% if (this.IsShippingAddrUs) { %>
							<asp:DropDownList runat="server" id="ddlShippingAddr5"></asp:DropDownList>
								<asp:CustomValidator
									ID="cvShippingAddr5Ddl"
									runat="Server"
									ControlToValidate="ddlShippingAddr5"
									ValidationGroup="FixedPurchaseModifyInputGlobal"
									ValidateEmptyText="true"
									SetFocusOnError="true"
									ClientValidationFunction="ClientValidate"
									CssClass="error_inline" />
							<% } else { %>
							<asp:TextBox runat="server" ID="tbShippingAddr5"></asp:TextBox>
							<asp:CustomValidator
								ID="cvShippingAddr5"
								runat="Server"
								ControlToValidate="tbShippingAddr5"
								ValidationGroup="FixedPurchaseModifyInputGlobal"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<% } %>
						</dd>
					<%-- 郵便番号（海外向け） --%>
						<dt>
							<%: ReplaceTag("@@User.zip.name@@", this.ShippingAddrCountryIsoCode) %> 
							<% if (this.IsShippingAddrZipNecessary) { %><span class="icnKome">※</span><% } %>
						</dt>
						<dd>
							<asp:TextBox ID="tbShippingZipGlobal" runat="server" MaxLength="30"></asp:TextBox>
							<asp:CustomValidator
								ID="cvShippingZipGlobal"
								runat="Server"
								ControlToValidate="tbShippingZipGlobal"
								ValidationGroup="FixedPurchaseModifyInputGlobal"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
					<% } %>
					<% if (Constants.DISPLAY_CORPORATION_ENABLED) { %>
						<dt><%: ReplaceTag("@@User.company_name.name@@") %> </dt>
						<dd>
							<asp:TextBox ID="tbShippingCompanyName" runat="server" MaxLength="40"></asp:TextBox>
							<asp:CustomValidator
								ID="cvShippingCompanyName"
								runat="Server"
								ControlToValidate="tbShippingCompanyName"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
						<dt><%: ReplaceTag("@@User.company_post_name.name@@") %> </dt>
						<dd>
							<asp:TextBox ID="tbShippingCompanyPostName" runat="server" MaxLength="40"></asp:TextBox>
							<asp:CustomValidator
								ID="cvShippingCompanyPostName"
								runat="Server"
								ControlToValidate="tbShippingCompanyPostName"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
					<% } %>
						<% if (this.IsShippingAddrJp) { %>
						<dt>
							<%: ReplaceTag("@@User.tel1.name@@", this.ShippingAddrCountryIsoCode) %>
							<span class="icnKome">※</span>
						</dt>
						<dd>
							<div class="telFlex">
								<div class="telFlex_list">
									<asp:TextBox ID="tbShippingTel1" runat="server" MaxLength="6"></asp:TextBox>
								</div>
								<div class="telFlex_line">
									<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
								</div>
								<div class="telFlex_list">
									<asp:TextBox ID="tbShippingTel2" runat="server" MaxLength="4"></asp:TextBox>
								</div>
								<div class="telFlex_line">
									<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
								</div>
								<div class="telFlex_list">
									<asp:TextBox ID="tbShippingTel3" runat="server" MaxLength="4"></asp:TextBox>
								</div>
							</div>
							<asp:CustomValidator
								ID="cvShippingTel1"
								runat="Server"
								ControlToValidate="tbShippingTel1"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<asp:CustomValidator
								ID="cvShippingTel2"
								runat="Server"
								ControlToValidate="tbShippingTel2"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<asp:CustomValidator
								ID="cvShippingTel3"
								runat="Server"
								ControlToValidate="tbShippingTel3"
								ValidationGroup="FixedPurchaseModifyInput"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
						<% } else { %>
						<dt>
							<%: ReplaceTag("@@User.tel1.name@@", this.ShippingAddrCountryIsoCode) %>
							<span class="icnKome">※</span>
						</dt>
						<dd>
							<asp:TextBox runat="server" ID="tbShippingTel1Global" MaxLength="30"></asp:TextBox>
							<asp:CustomValidator
								ID="cvShippingTel1Global"
								runat="Server"
								ControlToValidate="tbShippingTel1Global"
								ValidationGroup="FixedPurchaseModifyInputGlobal"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
						<% } %>
						<dt>配送方法</dt>
						<dd>
							<div class="selectPrefecture">
								<asp:DropDownList ID="ddlShippingMethod" OnSelectedIndexChanged="ddlShippingMethod_OnSelectedIndexChanged" runat="server" AutoPostBack="true"></asp:DropDownList>
							</div>
						</dd>
						<% if (this.SelectedDeliveryCompany.IsValidShippingTimeSetFlg
							&& (this.WddlShippingMethod.SelectedValue
								== Constants.FLG_SHOPSHIPPINGSHIPPINGCOMPANY_SHIPPING_KBN_EXPRESS)
							&& this.UseShippingAddress) { %>
						<dt>配送希望時間帯</dt>
						<dd>
							<div class="selectPrefecture">
								<asp:DropDownList ID="ddlShippingTime" runat="server"></asp:DropDownList>
							</div>
						</dd>
					<%} %>
						</div>
						<div runat="server" visible="false" id="tbOwnerAddress">
								<dt>住所</dt>
								<dd>
									<% if (IsCountryJp(this.LoginUser.AddrCountryIsoCode)) { %>
										〒<%: this.LoginUser.Zip %><br />
									<% } %>
									<%: this.LoginUser.Addr1 %>
									<%: this.LoginUser.Addr2 %>
									<%: this.LoginUser.Addr3 %>
									<%: this.LoginUser.Addr4 %><br />
									<%: this.LoginUser.Addr5 %>
									<% if (IsCountryJp(this.LoginUser.AddrCountryIsoCode) == false) { %>
										<%: this.LoginUser.Zip %><br />
									<% } %>
								</dd>
							<% if (Constants.DISPLAY_CORPORATION_ENABLED) { %>
								<%-- 企業名・部署名 --%>
								<dt><%: ReplaceTag("@@User.company_name.name@@") %>・
							<%: ReplaceTag("@@User.company_post_name.name@@") %></dt>
								<dd>
									<%: this.LoginUser.CompanyName %><br />
									<%: this.LoginUser.CompanyPostName %>
								</dd>
							<% } %>
								<%-- 氏名 --%>
								<dt><%: ReplaceTag("@@User.name.name@@", this.LoginUser.AddrCountryIsoCode) %></dt>
								<dd>
									<%: this.LoginUser.Name1%><%: this.LoginUser.Name2%>&nbsp;様
							<% if (IsCountryJp(this.LoginUser.AddrCountryIsoCode)) { %>
							（<%: this.LoginUser.NameKana1%><%: this.LoginUser.NameKana2%>&nbsp;さま）
							<% } %>
								</dd>
								<%-- 電話番号 --%>
								<dt><%: ReplaceTag("@@User.tel1.name@@", this.LoginUser.AddrCountryIsoCode) %></dt>
								<dd>
									<%: this.LoginUser.Tel1 %>
								</dd>
						</div>
						<asp:Repeater Visible="false" runat="server" ItemType="UserShippingModel" DataSource="<%# this.UserShippingAddr %>" ID="rOrderShippingList">
							<ItemTemplate>
								<div id="Tbody1" class='<%# string.Format("user_addres{0}", Item.ShippingNo) %>' runat="server" visible="<%# (Item.ShippingReceivingStoreFlg == Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_ON) %>">
									<asp:HiddenField runat="server" ID="hfShippingNo" Value="<%# Item.ShippingNo %>" />
										<dt>店舗ID</dt>
										<dd class="convenience-store-item" id="ddCvsShopId">
											<span style="font-weight:normal;">
												<asp:Label ID="lbCvsShopId" runat="server" Text="<%# Item.ShippingReceivingStoreId %>"></asp:Label>
											</span>
											<asp:HiddenField ID="hfCvsShopId" runat="server" Value="<%# Item.ShippingReceivingStoreId %>" />
										</dd>
										<dt>店舗名</dt>
										<dd class="convenience-store-item" id="ddCvsShopName">
											<span style="font-weight:normal;">
												<asp:Literal ID="lCvsShopName" runat="server" Text="<%# Item.ShippingName %>"></asp:Literal>
											</span>
											<asp:HiddenField ID="hfCvsShopName" runat="server" Value="<%# Item.ShippingName %>" />
										</dd>
										<dt>店舗住所</dt>
										<dd class="convenience-store-item" id="ddCvsShopAddress">
											<span style="font-weight:normal;">
												<asp:Literal ID="lCvsShopAddress" runat="server" Text="<%# Item.ShippingAddr4 %>"></asp:Literal>
											</span>
											<asp:HiddenField ID="hfCvsShopAddress" runat="server" Value="<%# Item.ShippingAddr4 %>" />
										</dd>
										<dt>店舗電話番号</dt>
										<dd class="convenience-store-item" id="ddCvsShopTel">
											<span style="font-weight:normal;">
												<asp:Literal ID="lCvsShopTel" runat="server" Text="<%# Item.ShippingTel1 %>"></asp:Literal>
											</span>
											<asp:HiddenField ID="hfCvsShopTel" runat="server" Value="<%# Item.ShippingTel1 %>" />
										</dd>
								</div>
								<div id="Tbody2" runat="server" visible="<%# (Item.ShippingReceivingStoreFlg != Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_ON) %>">
										<dt>
											<%: ReplaceTag("@@User.addr.name@@") %>
										</dt>
										<dd>
											<%# IsCountryJp(Item.ShippingCountryIsoCode)
												? "〒" + WebSanitizer.HtmlEncode(Item.ShippingZip) + "<br />"
												: "" %>
											<%#: Item.ShippingAddr1 %>
											<%#: Item.ShippingAddr2 %>
											<%#: Item.ShippingAddr3 %>
											<%#: Item.ShippingAddr4 %>
											<%#: Item.ShippingAddr5 %>
											<%# (IsCountryJp(Item.ShippingCountryIsoCode) == false)
													? WebSanitizer.HtmlEncode(Item.ShippingZip) + "<br />"
													: "" %>
											<%#: Item.ShippingCountryName %>
										</dd>
									<% if (Constants.DISPLAY_CORPORATION_ENABLED) { %>
										<%-- 企業名・部署名 --%>
										<dt><%: ReplaceTag("@@User.company_name.name@@")%>・
												<%: ReplaceTag("@@User.company_post_name.name@@")%></dt>
										<dd>
											<%#: Item.ShippingCompanyName %><br />
											<%#: Item.ShippingCompanyPostName %>
										</dd>
									<%} %>
										<%-- 氏名 --%>
										<dt><%: ReplaceTag("@@User.name.name@@") %></dt>
										<dd>
											<%#: Item.ShippingName %>&nbsp;様<br />
											<%#: IsCountryJp(Item.ShippingCountryIsoCode)
												? "(" + Item.ShippingNameKana + " さま)" 
												: "" %>
										</dd>
										<%-- 電話番号 --%>
										<dt><%: ReplaceTag("@@User.tel1.name@@") %></dt>
										<dd>
											<%#: Item.ShippingTel1 %>
										</dd>
									</div>
							</ItemTemplate>
						</asp:Repeater>
						<dd>
							<div class="updateLink">
								<asp:Button Text="お届け先情報更新" runat="server" OnClientClick="return CheckStoreAvailable();" OnClick="btnUpdateUserShippingInfo_Click" />
								<asp:Button Text="キャンセル" runat="server" OnClick="lbDisplayUserShippingInfoForm_Click" />
							</div>
							<div class="noticeArea">
								<p>※注文済みのお届け先情報は変更されませんのでご注意ください。</p>
							</div>
							<small id="sErrorMessageShipping" runat="server" class="error" style="padding: 2px;"></small>
						</dd>
				</dl>
				<% } %>
			<%-- 利用発票情報 --%>
			<% if (this.IsShowTwFixedPurchaseInvoiceInfo) { %>
				<h3>利用発票情報</h3>
				<% if (this.IsTwFixedPurchaseInvoiceModify == false) { %>
				<table cellspacing="0">
					<tr>
						<th>発票種類</th>
						<td>
							<div style="float: right">
								<asp:LinkButton Text="発票情報変更" runat="server" Visible="<%# this.CanCancelFixedPurchase && this.BeforeCancelDeadline %>" OnClick="lbDisplayTwInvoiceInfoForm_Click" class="btn" /></div>
							<%: ValueText.GetValueText(Constants.TABLE_TWFIXEDPURCHASEINVOICE, Constants.FIELD_TWFIXEDPURCHASEINVOICE_TW_UNIFORM_INVOICE, this.TwFixedPurchaseInvoiceModel.TwUniformInvoice) %>
							<% if (this.TwFixedPurchaseInvoiceModel.TwUniformInvoice == Constants.FLG_TW_UNIFORM_INVOICE_COMPANY) { %>
								<p style="padding-bottom: 5px; padding-top: 15px;">統一編号</p>
								<%: this.TwFixedPurchaseInvoiceModel.TwUniformInvoiceOption1 %>
								<p style="padding-bottom: 5px; padding-top: 5px;">会社名</p>
								<%: this.TwFixedPurchaseInvoiceModel.TwUniformInvoiceOption2 %>
							<% } %>
							<% if (this.TwFixedPurchaseInvoiceModel.TwUniformInvoice == Constants.FLG_TW_UNIFORM_INVOICE_DONATE) { %>
								<p style="padding-bottom: 5px; padding-top: 15px;">寄付先コード</p>
								<%: this.TwFixedPurchaseInvoiceModel.TwUniformInvoiceOption1 %>
							<% } %>
						</td>
					</tr>
					<% if (this.TwFixedPurchaseInvoiceModel.TwUniformInvoice == Constants.FLG_TW_UNIFORM_INVOICE_PERSONAL) { %>
					<tr>
						<th>共通性載具</th>
						<td>
							<%: ValueText.GetValueText(Constants.TABLE_TWFIXEDPURCHASEINVOICE, Constants.FIELD_TWFIXEDPURCHASEINVOICE_TW_CARRY_TYPE, this.TwFixedPurchaseInvoiceModel.TwCarryType) %>
							<br />
							<%: this.TwFixedPurchaseInvoiceModel.TwCarryTypeOption %>
						</td>
					</tr>
					<% } %>
				</table>
				<% } else { %>
				<table cellspacing="0">
					<tr>
						<th>発票種類</th>
						<td>
							<asp:DropDownList ID="ddlTaiwanUniformInvoice" DataTextField="Text" DataValueField="Value" runat="server"
								OnSelectedIndexChanged="ddlTaiwanUniformInvoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
							&nbsp;
							<span id="spTaiwanUniformInvoiceOptionKbnList" runat="server" visible="false">
								<asp:DropDownList ID="ddlTaiwanUniformInvoiceOptionKbnList" DataTextField="Text" DataValueField="Value" runat="server"
									OnSelectedIndexChanged="ddlTaiwanUniformInvoiceOptionKbnList_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
							</span>
							<div id="dvInvoiceForm" runat="server" style="padding-top:5px;" visible="false">
								<div id="dvInvoiceDisp" runat="server" visible="false">
									<br />
									<div id="dvInvoiceDispForCompanyType" runat="server">
										<p style="padding-bottom: 5px; padding-top: 5px;">統一編号</p>
										<asp:Literal ID="lCompanyCode" runat="server"></asp:Literal>
										<p style="padding-bottom: 5px; padding-top: 5px;">会社名</p>
										<asp:Literal ID="lCompanyName" runat="server"></asp:Literal>
									</div>
									<div id="dvInvoiceDispForDonateType" runat="server" visible="false">
										<p style="padding-bottom: 5px; padding-top: 5px;">寄付先コード</p>
										<asp:Literal ID="lDonationCode" runat="server"></asp:Literal>
									</div>
									<br />
								</div>
								<div id="dvInvoiceInput" runat="server" visible="false">
								<br />
								<div id="dvInvoiceInputForCompanyType" runat="server" visible="false">
									<p style="padding-bottom: 5px; padding-top: 5px;">統一編号</p>
									<asp:TextBox runat="server" ID="tbUniformInvoiceOption1_8" placeholder="例:12345678" MaxLength="8"></asp:TextBox>
									<asp:CustomValidator
										ID="cvUniformInvoiceOption1_8"
										runat="Server"
										ControlToValidate="tbUniformInvoiceOption1_8"
										ValidationGroup="OrderShippingGlobal"
										ValidateEmptyText="true"
										SetFocusOnError="true"
										ClientValidationFunction="ClientValidate"
										CssClass="error_inline" />
									<p style="padding-bottom: 5px; padding-top: 5px;">会社名</p>
									<asp:TextBox runat="server" ID="tbUniformInvoiceOption2" placeholder="例:○○有限股份公司" MaxLength="20"></asp:TextBox>
									<asp:CustomValidator
										ID="cvUniformInvoiceOption2"
										runat="Server"
										ControlToValidate="tbUniformInvoiceOption2"
										ValidationGroup="OrderShippingGlobal"
										ValidateEmptyText="true"
										SetFocusOnError="true"
										ClientValidationFunction="ClientValidate"
										CssClass="error_inline" />
								</div>
								<div id="dvInvoiceInputForDonateType" runat="server" visible="false">
									<p style="padding-bottom: 5px; padding-top: 5px;">寄付先コード</p>
									<asp:TextBox runat="server" ID="tbUniformInvoiceOption1_3" MaxLength="7"></asp:TextBox>
									<asp:CustomValidator
										ID="cvUniformInvoiceOption1_3"
										runat="Server"
										ControlToValidate="tbUniformInvoiceOption1_3"
										ValidationGroup="OrderShippingGlobal"
										ValidateEmptyText="true"
										SetFocusOnError="true"
										ClientValidationFunction="ClientValidate"
										CssClass="error_inline" />
								</div>
								<asp:CheckBox ID="cbSaveToUserInvoice" Visible="false" runat="server" Text="電子発票管理情報を保存する" AutoPostBack="true" OnCheckedChanged="cbSaveToUserInvoice_CheckedChanged" />
								<div id="dvUniformInvoiceTypeRegistInput" runat="server" visible="false">
									電子発票情報名<span class="fred">※</span>
									<asp:TextBox ID="tbUniformInvoiceTypeName" MaxLength="30" runat="server"></asp:TextBox>
									<asp:CustomValidator
										ID="cvUniformInvoiceTypeName" runat="server"
										ControlToValidate="tbUniformInvoiceTypeName"
										ValidationGroup="OrderShippingGlobal"
										ValidateEmptyText="true"
										ClientValidationFunction="ClientValidate"
										SetFocusOnError="true"
										CssClass="error_inline" />
								</div>
								<br />
								</div>
							</div>
						</td>
					</tr>
					<tr id="trInvoiceDispForPersonalType" runat="server" visible="false">
						<th>共通性載具</th>
						<td>
							<asp:DropDownList ID="ddlTaiwanCarryType" DataTextField="Text" DataValueField="Value" runat="server"
								OnSelectedIndexChanged="ddlTaiwanCarryType_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
							&nbsp;
							<span id="spTaiwanCarryKbnList" runat="server" visible="false">
								<asp:DropDownList ID="ddlTaiwanCarryKbnList" DataTextField="Text" DataValueField="Value" runat="server"
									OnSelectedIndexChanged="ddlTaiwanCarryKbnList_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
							</span>
							<div id="dvInvoiceNoEquipmentMessage" runat="server" style="color:red; padding-top:15px;" visible="false">
								<asp:Literal ID="lInvoiceNoEquipmentMessage" runat="server" Text="※電子発票は商品と一緒に送ります。"></asp:Literal>
							</div>
							<div id="dvInvoiceDispForPersonalType" runat="server" visible="false">
								<br />
								<asp:Literal ID="lInvoiceCode" runat="server"></asp:Literal>
								<br />
							</div>
							<div id="dvInvoiceInputForPersonalType" runat="server" visible="false">
								<br />
								<div id="dvCarryTypeOption_8" runat="server" visible="false">
									<asp:TextBox runat="server" Width="220" ID="tbCarryTypeOption_8" placeholder="例:/AB201+9(限8個字)" MaxLength="8"></asp:TextBox>
									<asp:CustomValidator
										ID="cvCarryTypeOption_8"
										runat="Server"
										ControlToValidate="tbCarryTypeOption_8"
										ValidationGroup="OrderShippingGlobal"
										ValidateEmptyText="true"
										SetFocusOnError="true"
										ClientValidationFunction="ClientValidate"
										CssClass="error_inline" />
								</div>
								<div id="dvCarryTypeOption_16" runat="server" visible="false">
									<asp:TextBox runat="server" Width="220" ID="tbCarryTypeOption_16" placeholder="例:TP03000001234567(限16個字)" MaxLength="16"></asp:TextBox>
									<asp:CustomValidator
										ID="cvCarryTypeOption_16"
										runat="Server"
										ControlToValidate="tbCarryTypeOption_16"
										ValidationGroup="OrderShippingGlobal"
										ValidateEmptyText="true"
										SetFocusOnError="true"
										ClientValidationFunction="ClientValidate"
										CssClass="error_inline" />
								</div>
								<asp:CheckBox ID="cbCarryTypeOptionRegist" runat="server" Visible="false" Text="電子発票管理情報を保存する" AutoPostBack="true" OnCheckedChanged="cbCarryTypeOptionRegist_CheckedChanged" />
								<div id="dvCarryTypeOptionName" runat="server" visible="false">
									電子発票情報名<span class="fred">※</span>
									<asp:TextBox ID="tbCarryTypeOptionName" runat="server" MaxLength="30"></asp:TextBox>
									<asp:CustomValidator
										ID="cvCarryTypeOptionName" runat="server"
										ControlToValidate="tbCarryTypeOptionName"
										ValidationGroup="OrderShippingGlobal"
										ValidateEmptyText="true"
										ClientValidationFunction="ClientValidate"
										SetFocusOnError="true"
										CssClass="error_inline" />
								</div>
								<br />
							</div>
						</td>
					</tr>
					<tr align="center">
						<th></th>
						<td>
							<asp:Button ID="btnUpdateInvoiceInfo" Text="  情報更新  " runat="server" OnClientClick="return confirm('発票情報を変更してもよろしいですか？')" OnClick="btnUpdateInvoiceInfo_Click" class="btn" />
							<asp:Button Text="  キャンセル  " runat="server" OnClick="lbDisplayTwInvoiceInfoForm_Click" class="btn" />
						</td>
					</tr>
				</table>
				<% } %>
			<% } %>
			<%-- 購入商品一覧 --%>
			<div class="dvFixedPurchaseItem">
				<dl class="ohDetail_box borderT0">
					<asp:Repeater ID="rItem" runat="server">
						<ItemTemplate>
							<dt>
								商品名
							</dt>
							<dd>
								<%# WebSanitizer.HtmlEncode(((FixedPurchaseItemInput)Container.DataItem).CreateProductJointName()) %>
								<br />
								[<span class="productId"><%# WebSanitizer.HtmlEncode((((FixedPurchaseItemInput)Container.DataItem).ProductId == ((FixedPurchaseItemInput)Container.DataItem).VariationId) ? ((FixedPurchaseItemInput)Container.DataItem).ProductId : ((FixedPurchaseItemInput)Container.DataItem).VariationId)%></span>]
								<span visible='<%# ((FixedPurchaseItemInput)Container.DataItem).ProductOptionTexts != "" %>' runat="server">
									<br />
									<%# WebSanitizer.HtmlEncode(((FixedPurchaseItemInput)Container.DataItem).ProductOptionTexts).Replace("　", "<br />")%>
								</span>
							</dd>
							<dt>
								単価（<%#: this.ProductPriceTextPrefix %>）
							</dt>
							<dd>
								<%#: CurrencyManager.ToPrice(((FixedPurchaseItemInput)Container.DataItem).GetValidPrice()) %>
							</dd>
							<dt>
								注文数
							</dt>
							<dd>
								<%# WebSanitizer.HtmlEncode(StringUtility.ToNumeric(((FixedPurchaseItemInput)Container.DataItem).ItemQuantity))%>
							</dd>
							<dt>
								小計（<%#: this.ProductPriceTextPrefix %>）
							</dt>
							<dd>
								<%#: CurrencyManager.ToPrice(((FixedPurchaseItemInput)Container.DataItem).GetItemPrice()) %>
							</dd>
						</ItemTemplate>
					</asp:Repeater>
				</dl>
			</div>
		</div>
	</div>
		
	<div class="cartNextbtn one">
		<ul>
			<li class="prevBtn">
				<a href="<%# WebSanitizer.UrlAttrHtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_FIXED_PURCHASE_LIST) %>">前のページに戻る</a>
			</li>
		</ul>
	</div>
	</div>
	<uc:mypageMenu runat="server" />
</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					定期購入情報詳細
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>

</ContentTemplate>
</asp:UpdatePanel>
<%-- UpdatePanel終了 --%>
<%--▼▼ Amazon Pay(CV2)スクリプト ▼▼--%>
<script src="https://static-fe.payments-amazon.com/checkout.js"></script>
<%--▲▲ Amazon Pay(CV2)スクリプト ▲▲--%>
<script type="text/javascript">
	bindEvent();

	<%-- UpdataPanelの更新時のみ処理を行う --%>
	function bodyPageLoad() {
		if (Sys.WebForms == null) return;
		var isAsyncPostback = Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack();
		if (isAsyncPostback) {
			bindEvent();
		}
	}

	<%-- イベントをバインドする --%>
	function bindEvent() {
		bindExecAutoKana();
		bindZipCodeSearch();
		<%--▼▼ Amazon Pay(CV2)スクリプト ▼▼--%>
		showAmazonButton('#AmazonPayCv2Button');
		showAmazonButton('#AmazonPayCv2Button2');
		<%--▲▲ Amazon Pay(CV2)スクリプト ▲▲--%>
		<% if(Constants.GLOBAL_OPTION_ENABLE) { %>
		bindTwAddressSearch();
		<% } %>
	}

	<%-- 氏名（姓・名）の自動振り仮名変換のイベントをバインドする --%>
	function bindExecAutoKana() {
		execAutoKanaWithKanaType(
			$("#<%= tbShippingName1.ClientID %>"),
			$("#<%= tbShippingNameKana1.ClientID %>"),
			$("#<%= tbShippingName2.ClientID %>"),
			$("#<%= tbShippingNameKana2.ClientID %>"));
	}

	<%-- 郵便番号検索のイベントをバインドする --%>
	function bindZipCodeSearch() {
		$("#<%= tbShippingZip2.ClientID %>").keyup(function (e) {
			if (isValidKeyCodeForKeyEvent(e.keyCode) == false) return;
			checkZipCodeLengthAndExecPostback(
				$("#<%= tbShippingZip1.ClientID %>"),
				$("#<%= tbShippingZip2.ClientID %>"),
				"<%= tbShippingZip2.UniqueID %>",
				'<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_ZIPCODE_SEARCHER_GET_ADDR_JSON %>',
				'#addrSearchErrorMessage'
			);
		});
		$("#<%= lbSearchShippingAddr.ClientID %>").on('click', function () {
			checkZipCodeLengthAndExecPostback(
				$("#<%= tbShippingZip1.ClientID %>"),
				$("#<%= tbShippingZip2.ClientID %>"),
				"<%= lbSearchShippingAddr.UniqueID %>",
				'<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_ZIPCODE_SEARCHER_GET_ADDR_JSON %>',
				'#addrSearchErrorMessage'
			);
		});
	}

	<%--▼▼ Amazon Pay(CV2)スクリプト ▼▼--%>
	function showAmazonButton(divId) {
	showAmazonPayCv2Button(
	divId,
	'<%= Constants.PAYMENT_AMAZON_SELLERID %>',
	<%= Constants.PAYMENT_AMAZON_ISSANDBOX.ToString().ToLower() %>,
	'<%= this.AmazonRequest.Payload %>',
	'<%= this.AmazonRequest.Signature %>',
	'<%= Constants.PAYMENT_AMAZON_PUBLIC_KEY_ID %>');
	}
	<%--▲▲ Amazon Pay(CV2)スクリプト ▲▲--%>
	
	$(document).on('click', '.search-result-layer-close', function () {
		closePopupAndLayer();
	});

	$(document).on('click', '.search-result-layer-addr', function () {
		bindSelectedAddr($('li.search-result-layer-addr').index(this));
		});

	<%-- 複数住所検索結果からの選択値を入力フォームにバインドする --%>
	function bindSelectedAddr(selectedIndex) {
		var selectedAddr = $('.search-result-layer-addrs li').eq(selectedIndex);
		$("#<%= ddlShippingAddr1.ClientID %>").val(selectedAddr.find('.addr').text());
		$("#<%= tbShippingAddr2.ClientID %>").val(selectedAddr.find('.city').text() + selectedAddr.find('.town').text());
		$("#<%= tbShippingAddr3.ClientID %>").focus();

		closePopupAndLayer();
	}
	<%-- 定期購入再開の確認メッセージを生成 --%>
	function getResumeShippingMesasge() {
		var selectedValue =  $("#<%= ddlResumeFixedPurchaseDate.ClientID %>").val();
		if (selectedValue == "<%= ReplaceTag("@@DispText.shipping_date_list.none@@") %>") {
			return "次回配送日・次々回配送日は自動で計算します。"
		} else {
			return "次々回配送日は自動で計算します。"
		}
	}

	<% if(Constants.GLOBAL_OPTION_ENABLE) { %>
	<%-- 台湾郵便番号取得関数 --%>
	function bindTwAddressSearch() {
		$('#<%= this.WddlShippingAddr3.ClientID %>').change(function (e) {
			$('#<%= this.WtbShippingZipGlobal.ClientID %>').val(
				$('#<%= this.WddlShippingAddr3.ClientID %>').val().split('|')[0]);
		});
	}
	<% } %>

	<%--領収書情報が削除されるの確認メッセージ作成 --%>
	function getDeleteReceiptInfoMessage() {
		var message = "";

		// 領収書情報が削除される場合、アラート追加
		<% if (Constants.RECEIPT_OPTION_ENABLED && (this.FixedPurchaseContainer.ReceiptFlg == Constants.FLG_ORDER_RECEIPT_FLG_ON)) { %>
		if ("<%= string.Join(",", Constants.NOT_OUTPUT_RECEIPT_PAYMENT_KBN) %>".indexOf(document.getElementById("<%= hfPaymentIdSelected.ClientID %>").value) !== -1)
		{
			message = '\n指定したお支払い方法は、領収書の発行ができません。\n'
				+ '保存されている「領収書情報」が削除されます。\n';
		}
		<% } %>
		return message;
	}

	// Alert Payment Change
	function AlertPaymentChange() {
		var result = false;
		var spanErrorMessageForAtone = document.getElementsByClassName('spanErrorMessageForAtone');
		if ((spanErrorMessageForAtone.length > 0)
			&& (spanErrorMessageForAtone[0].style.display == "block")) {
			return result;
		}

		var spanErrorMessageForAftee = document.getElementsByClassName('spanErrorMessageForAftee');
		if ((spanErrorMessageForAftee.length > 0)
			&& (spanErrorMessageForAftee[0].style.display == "block")) {
			return result;
		}
		if (confirm('お支払い方法を変更します。' + getDeleteReceiptInfoMessage() + '\nよろしいですか？')) {
			<% if (Constants.PAYMENT_PAIDY_OPTION_ENABLED) { %>
			result = false;

			PaidyPayProcess();
			<% } else { %>
			result = true;
			<% } %>
		}

		return result;
	}

	<%-- Check Store Available --%>
	function CheckStoreAvailable() {
	<% if(Constants.RECEIVINGSTORE_TWPELICAN_CVSOPTION_ENABLED) { %>
		var isShopValid = false;
		var isShippingConvenience = ($("#<%= WhfCvsShopFlg.ClientID %>").val() == '<%= Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_ON %>');
		if (isShippingConvenience) {
			var hfCvsShopId = $("#<%= WhfShippingReceivingStoreId.ClientID %>");
			var hfSelectedShopId = $("#<%= WhfSelectedShopId.ClientID %>");
			var shopId = hfCvsShopId.val();
			if (shopId == undefined || shopId == null || shopId == '') {
				shopId = hfSelectedShopId.val();
			}
			var dvErrorShippingConvenience = $("#<%= WdvErrorShippingConvenience.ClientID %>");
			$.ajax({
				type: "POST",
				url: "<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_FIXED_PURCHASE_DETAIL %>/CheckStoreIdValid",
				data: JSON.stringify({ storeId: shopId }),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				cache: false,
				async: false,
				success: function (data) {
					if (data.d) {
						isShopValid = true;
						dvErrorShippingConvenience.css("display", "none");
					}
					else {
						dvErrorShippingConvenience.css("display", "");
					}
				}
			});

			if (isShopValid == false) return isShopValid;
		}
	<% } %>

		return confirm('お届け先情報を変更してもよろしいですか？');
	}

	<% if (Constants.RECEIVINGSTORE_TWPELICAN_CVSOPTION_ENABLED) { %>
	<%-- Open convenience store map popup --%>
	function openConvenienceStoreMapPopup() {
		$("#<%= WdvErrorShippingConvenience.ClientID %>").css("display", "none");
		var url = '<%= OrderCommon.CreateConvenienceStoreMapUrl() %>';
		window.open(url, "", "width=1000,height=800");
	}

	<%-- Set convenience store data --%>
	function setConvenienceStoreData(cvsspot, name, addr, tel) {
		var elements = document.getElementsByClassName("shipping_convenience")[0];
		if (elements == undefined || elements == null) {
			var seletedShippingNo = $('.UserShippingAddress').val();
			var className = '.user_addres' + seletedShippingNo;
			elements = $(className)[0];
			var hfSelectedShopId = $("#<%= WhfSelectedShopId.ClientID %>");
			hfSelectedShopId.val(cvsspot);
		}

		// For display
		elements.querySelector('[id$="ddCvsShopId"] > span').innerHTML = cvsspot;
		elements.querySelector('[id$="ddCvsShopName"] > span').innerHTML = name;
		elements.querySelector('[id$="ddCvsShopAddress"] > span').innerHTML = addr;
		elements.querySelector('[id$="ddCvsShopTel"] > span').innerHTML = tel;

		// For get value
		elements.querySelector('[id$="hfCvsShopId"]').value = cvsspot;
		elements.querySelector('[id$="hfCvsShopName"]').value = name;
		elements.querySelector('[id$="hfCvsShopAddress"]').value = addr;
		elements.querySelector('[id$="hfCvsShopTel"]').value = tel;
	}
	<% } %>
//-->
</script>
<%--▼▼ クレジットカードToken用スクリプト ▼▼--%>
<script type="text/javascript">
	var getTokenAndSetToFormJs = "<%= CreateGetCreditTokenAndSetToFormJsScript().Replace("\"", "\\\"") %>";
	var maskFormsForTokenJs = "<%= CreateMaskFormsForCreditTokenJsScript().Replace("\"", "\\\"") %>";
</script>
<uc:CreditToken runat="server" ID="CreditToken" />
<%--▲▲ クレジットカードToken用スクリプト ▲▲--%>

<%--▼▼ Paidy用スクリプト ▼▼--%>
<script type="text/javascript">
	var buyer = <%= PaidyUtility.CreatedBuyerDataObjectForPaidyPayment(this.LoginUserId) %>;
	var hfPaidyTokenIdControlId = "<%= this.WhfPaidyTokenId.ClientID %>";
	var hfPaidyPaySelectedControlId = "<%= this.WhfPaidyPaySelected.ClientID %>";
	var updatePaymentUniqueID = "<%= this.WlbUpdatePayment.UniqueID %>";
	var isHistoryPage = true;
</script>
<uc:PaidyCheckoutScript ID="ucPaidyCheckoutScript" runat="server" />
<%--▲▲ Paidy用スクリプト ▲▲--%>
</asp:Content>