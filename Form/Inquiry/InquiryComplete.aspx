﻿<%--
=========================================================================================================
  Module      : 問合せ完了画面(InquiryComplete.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_Inquiry_InquiryComplete, App_Web_inquirycomplete.aspx.97d9c6ad" title="お問い合わせ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />

<article id="cartComp">
	<div class="inner">
		<div class="cartComp_ttl">
			<h2>問い合わせ完了</h2>
		</div>
	
		<div class="cartComp_txt">
			<p class="cartComp_txt--ttl lh">
				お問い合わせいただき<br class="sp_contents">ありがとうございます。
			</p>
			<p class="cartComp_txt--cts lh">
				内容を確認のうえ、3 営業日以内に順次ご連絡をさせていただきます。<br>
				迅速な対応を心がけておりますが、混雑等によりご回答に日数をいただく場合もございますので、予めご了承ください。
			</p>
			<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>" class="cartComp_txt--btn">トップページへ</a>
		</div>
	</div>
</article>
</asp:Content>