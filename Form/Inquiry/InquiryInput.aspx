﻿<%--
=========================================================================================================
  Module      : 問合せ入力画面(InquiryInput.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_Inquiry_InquiryInput, App_Web_inquiryinput.aspx.97d9c6ad" title="お問い合わせ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />
<article id="cart" class="cartPeyment">

<!--▽ パンくず ▽-->
<div class="breadArea pc_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				お問い合わせ
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

	<!-- main -->
	<section class="cartTtl second">
		<h2>
			お問い合わせ
		</h2>
	</section>
	<!-- // main -->
	
	<div class="cartLead">
		<p>
			以下の項目をご入力いただき、「確認する」ボタンをクリックしてください。<br>
			<span class="icnKome">※</span> は必須入力です。
		</p>
	</div>

	<div>
		
		<%-- 問合せ項目入力フォーム --%>

		<%-- 問合せ項目開始 --%>
		<dl class="cartInputRegist">
			<%-- 問合せ件名 --%>
				<dt>お問い合わせ件名 <span class="icnKome">※</span></dt>
				<dd>
					<asp:DropDownList ID="ddlInquiryTitle" runat="server">
						<asp:ListItem Text="選択してください" Value=""></asp:ListItem>
						<asp:ListItem Text="商品について" Value="商品について"></asp:ListItem>
						<asp:ListItem Text="注文・お届けについて" Value="注文・お届けについて"></asp:ListItem>
						<asp:ListItem Text="サイトの利用方法について" Value="サイトの利用方法について"></asp:ListItem>
						<asp:ListItem Text="その他のお問合せ" Value="その他のお問合せ"></asp:ListItem>
					</asp:DropDownList>
					<asp:CustomValidator runat="Server"
						ControlToValidate="ddlInquiryTitle"
						ValidationGroup="Inquiry"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
					<%= WebSanitizer.HtmlEncode(this.ProductInquiry)%>
					<asp:DropDownList ID="ddlProductVariation" runat="server" Width="400"></asp:DropDownList>
					<asp:HiddenField ID="hfProductTitlePrefix" runat="server" Value="商品名 : " />
				</dd>
			<%-- 問合せ内容 --%>
				<dt>お問い合わせ内容 <span class="icnKome">※</span></dt>
				<dd>
					<asp:TextBox ID="tbInquiryText" runat="server" TextMode="MultiLine" Rows="10" CssClass="inquirytext" Text=""></asp:TextBox>
					<asp:CustomValidator runat="Server"
						ControlToValidate="tbInquiryText"
						ValidationGroup="Inquiry"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
				</dd>
			<%-- 氏名 --%>
				<dt><%: ReplaceTag("@@User.name.name@@") %> <span class="icnKome">※</span></dt>
				<dd>
					<div class="nameFlex">
						<div class="nameFlex_list">
							<% SetMaxLength(WtbUserName1, "@@User.name1.length_max@@"); %>
							<p>姓</p>
							<asp:TextBox id="tbUserName1" Runat="server" CssClass="nameFirst"></asp:TextBox>
						</div>
						<div class="nameFlex_list">
							<% SetMaxLength(WtbUserName2, "@@User.name2.length_max@@"); %>
							<p>名</p>
							<asp:TextBox id="tbUserName2" Runat="server" CssClass="nameLast"></asp:TextBox>
						</div>
					</div>
					<asp:CustomValidator runat="Server"
						ControlToValidate="tbUserName1"
						ValidationGroup="Inquiry"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
					<asp:CustomValidator runat="Server"
						ControlToValidate="tbUserName2"
						ValidationGroup="Inquiry"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
				</dd>
			<%-- 氏名（かな） --%>
			<% if (this.IsJapanese){ %>
				<dt>カナ
					<% if (this.IsJapanese){ %>
					<span class="icnKome">※</span>
					<% } %>
				</dt>
				<dd>
					<div class="nameFlex">
						<div class="nameFlex_list">
							<% SetMaxLength(WtbUserNameKana1, "@@User.name_kana1.length_max@@"); %>
							<p>セイ</p>
							<asp:TextBox id="tbUserNameKana1" Runat="server" CssClass="nameFirst"></asp:TextBox>
						</div>
						<div class="nameFlex_list">
							<% SetMaxLength(WtbUserNameKana2, "@@User.name_kana2.length_max@@"); %>
							<p>メイ</p>
							<asp:TextBox id="tbUserNameKana2" Runat="server" CssClass="nameLast"></asp:TextBox>
						</div>
					</div>
					<asp:CustomValidator runat="Server"
						ControlToValidate="tbUserNameKana1"
						ValidationGroup="Inquiry"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
					<asp:CustomValidator runat="Server"
						ControlToValidate="tbUserNameKana2"
						ValidationGroup="Inquiry"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
				</dd>
			<% } %>
			<%-- メールアドレス --%>
				<dt>
					<%: ReplaceTag("@@User.mail_addr.name@@") %>
					<span class="icnKome">※</span>
				</dt>
				<dd>
					<asp:TextBox id="tbUserMailAddr" Runat="server" MaxLength="256" CssClass="mailAddr" Type="email"></asp:TextBox>
					<asp:CustomValidator runat="Server"
						ControlToValidate="tbUserMailAddr"
						ValidationGroup="Inquiry"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
				</dd>
			<%-- メールアドレス(確認) --%>
				<dt>
					<%: ReplaceTag("@@User.mail_addr.name@@") %>(確認用)
					<span class="icnKome">※</span>
				</dt>
				<dd>
					<asp:TextBox id="tbUserMailAddrConf" Runat="server" MaxLength="256" CssClass="mailAddr" Type="email"></asp:TextBox>
					<asp:CustomValidator runat="Server"
						ControlToValidate="tbUserMailAddrConf"
						ValidationGroup="Inquiry"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
				</dd>
			<%-- 電話番号 --%>
				<dt>
					<%: ReplaceTag("@@User.tel1.name@@") %>
					<span class="icnKome">※</span>
				</dt>
				<dd>
					<div class="telFlex">
						<div class="telFlex_list">
							<asp:TextBox id="tbUserTel1" Runat="server" MaxLength="6" CssClass="tel1" Type="tel"></asp:TextBox>
						</div>
						<div class="telFlex_line">
							<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
						</div>
						<div class="telFlex_list">
							<asp:TextBox id="tbUserTel2" Runat="server" MaxLength="4" CssClass="tel2" Type="tel"></asp:TextBox>
						</div>
						<div class="telFlex_line">
							<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
						</div>
						<div class="telFlex_list">
							<asp:TextBox id="tbUserTel3" Runat="server" MaxLength="4" CssClass="tel3" Type="tel"></asp:TextBox>
						</div>
					</div>
					<asp:CustomValidator runat="Server"
						ControlToValidate="tbUserTel1"
						ValidationGroup="Inquiry"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
					<asp:CustomValidator runat="Server"
						ControlToValidate="tbUserTel2"
						ValidationGroup="Inquiry"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
					<asp:CustomValidator runat="Server"
						ControlToValidate="tbUserTel3"
						ValidationGroup="Inquiry"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
				</dd>
		</dl>
		<%-- 問合せ項目ここまで --%>

		<div class="registBtn">
			<p>送信前に<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "page/privacy/") %>" target="_blank">プライバシーポリシー</a>をご確認ください。</p>
			<ul>
				<li>
					<asp:LinkButton ID="lbConfirm" runat="server" ValidationGroup="Inquiry" OnClientClick="return exec_submit();" OnClick="lbConfirm_Click">
					確認する</asp:LinkButton>
				</li>
				<li style="display: none;">
					<a href="<%= WebSanitizer.HtmlEncode(this.ProductPageURL) %>" onclick="return exec_submit();">
						前のページに戻る
					</a>
				</li>
			</ul>
		</div>

		<div class="inquiryLink">
			<p>
				山本未奈子への取材 ／講演のご依頼は<a href="mailto:info@mncny.jp">こちら</a><br>
				商品貸出のご依頼は<a href="mailto:info@simplisse.jp">こちら</a>
			</p>
		</div>

		<div class="inquiryNote">
			<p>※お問い合わせいただく前に、以下もご確認ください。</p>
			<ul>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/") %>" target="_blank">
						ショッピングガイド
					</a>
				</li>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/faq/") %>" target="_blank">
						製品・サービスについてよくいただくご質問
					</a>
				</li>
			</ul>
		</div>
	</div>
</article>

<!--▽ パンくず ▽-->
<div class="breadArea sp_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				お問い合わせ
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

<script type="text/javascript">
<!--
	bindExecAutoKana();

	<%-- 氏名（姓・名）の自動振り仮名変換のイベントをバインドする --%>
	function bindExecAutoKana() {
		execAutoKanaWithKanaType(
			$("#<%= tbUserName1.ClientID %>"),
			$("#<%= tbUserNameKana1.ClientID %>"),
			$("#<%= tbUserName2.ClientID %>"),
			$("#<%= tbUserNameKana2.ClientID %>"));
	}
//-->
</script>

</asp:Content>
