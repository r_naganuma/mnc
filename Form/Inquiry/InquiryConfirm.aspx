﻿<%--
=========================================================================================================
  Module      : 問合せ確認画面(InquiryConfirm.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2010 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="Captcha" Src="~/Form/Common/Captcha.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_Inquiry_InquiryConfirm, App_Web_inquiryconfirm.aspx.97d9c6ad" title="お問い合わせ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />
	<article id="cart" class="cartPeyment">
	
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					お問い合わせ
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
	
	<!-- main -->
	<section class="cartTtl second">
		<h2>
			お問い合わせ情報の確認
		</h2>
	</section>
	<!-- // main -->

	<div class="cartLead">
		<p>
			以下の項目でよろしければ「送信する」ボタンをクリックしてください。
		</p>
	</div>

	<div>

		<%-- 問合せ項目入力フォーム --%>

		<%-- 問合せ項目開始 --%>
		<dl class="cartInputRegist confirm">
			<%-- 問合せ件名 --%>
				<dt>お問い合わせ件名</dt>
				<dd><%= WebSanitizer.HtmlEncode(this.InquiryInput["inquiry_title"])%></dd>
				<dt>お問い合わせ内容</dt>
				<dd><%= WebSanitizer.HtmlEncodeChangeToBr(this.InquiryInput["inquiry_text"])%></dd>
				<dt>
					<%: ReplaceTag("@@User.name.name@@") %>
				</dt>
				<dd><%= WebSanitizer.HtmlEncode(this.InquiryInput[Constants.FIELD_USER_NAME1])%>
				<%= WebSanitizer.HtmlEncode(this.InquiryInput[Constants.FIELD_USER_NAME2])%></dd>
			<% if (this.IsJapanese){ %>
				<dt>
					氏名(カナ)
				</dt>
				<dd><%= WebSanitizer.HtmlEncode(this.InquiryInput[Constants.FIELD_USER_NAME_KANA1])%>
				<%= WebSanitizer.HtmlEncode(this.InquiryInput[Constants.FIELD_USER_NAME_KANA2])%></dd>
			<% } %>
				<dt><%: ReplaceTag("@@User.mail_addr.name@@") %></dt>
				<dd><%= WebSanitizer.HtmlEncode(this.InquiryInput[Constants.FIELD_USER_MAIL_ADDR])%></dd>
				<dt>
					<%: ReplaceTag("@@User.tel1.name@@") %>
				</dt>
				<dd><%= WebSanitizer.HtmlEncode(this.InquiryInput[Constants.FIELD_USER_TEL1_1])%>-<%= WebSanitizer.HtmlEncode(this.InquiryInput[Constants.FIELD_USER_TEL1_2])%>-<%= WebSanitizer.HtmlEncode(this.InquiryInput[Constants.FIELD_USER_TEL1_3])%></dd>
		</table>

		<%-- キャプチャ認証 --%>
		<uc:Captcha ID="ucCaptcha" runat="server" EnabledControlClientID="<%# lbSend.ClientID %>" />

		<%-- 問合せ項目ここまで --%>
		<div class="registBtn">
			<ul>
				<li>
					<asp:LinkButton ID="lbSend" runat="server" OnClientClick="return exec_submit();" OnClick="lbSend_Click">
						送信する
					</asp:LinkButton>
				</li>
				<li>
					<asp:LinkButton ID="lbBack" runat="server" OnClientClick="return exec_submit()" OnClick="lbBack_Click">
						前のページに戻る
					</asp:LinkButton>
				</li>
			</ul>
		</div>
	</div>
</article>

<!--▽ パンくず ▽-->
<div class="breadArea sp_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				お問い合わせ
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->
</asp:Content>