﻿<%--
=========================================================================================================
  Module      : 商品一覧画面(ProductList.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%@ Register TagPrefix="uc" TagName="BodyProductRecommendAdvanced" Src="~/Form/Common/Product/BodyProductRecommendAdvanced.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductSearchBox" Src="~/Form/Common/Product/BodyProductSearchBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductAdvancedSearchBox" Src="~/Form/Common/Product/BodyProductAdvancedSearchBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductCategoryTree" Src="~/Form/Common/Product/BodyProductCategoryTree.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductRanking" Src="~/Form/Common/Product/BodyProductRanking.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductHistory" Src="~/Form/Common/Product/BodyProductHistory.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyMiniCart" Src="~/Form/Common/BodyMiniCart.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductRecommendByRecommendEngine" Src="~/Form/Common/Product/BodyProductRecommendByRecommendEngine.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyCategoryRecommendByRecommendEngine" Src="~/Form/Common/Product/BodyCategoryRecommendByRecommendEngine.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductColorSearchBox" Src="~/Form/Common/Product/ProductColorSearchBox.ascx" %>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Register TagPrefix="uc" TagName="BodyProductArrivalMailRegisterTr" Src="~/Form/Common/Product/BodyProductArrivalMailRegisterTr.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductCategoryHtml" Src="~/Form/Common/Product/BodyProductCategoryHtml.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductSortBox" Src="~/Form/Common/Product/BodyProductSortBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductCategoryLinks" Src="~/Form/Common/Product/BodyProductCategoryLinks.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductVariationImages" Src="~/Form/Common/Product/BodyProductVariationImages.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductGroupContentsHtml" Src="~/Form/Common/Product/BodyProductGroupContentsHtml.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductChildCategoryList" Src="~/Form/Common/Product/BodyProductChildCategoryList.ascx" %>
<%@ Register TagPrefix="uc" TagName="Criteo" Src="~/Form/Common/Criteo.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/DefaultPage.master" autoeventwireup="true" inherits="Form_Product_ProductList, App_Web_productlist.aspx.1e99e05" title="商品一覧ページ" %>
<%@ Import Namespace="ProductListDispSetting" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。In
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="ｗ２ユーザー" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/product/style.css?91831" rel="stylesheet" type="text/css" media="all" />
<link rel="canonical" href="<%# CreateProductListCanonicalUrl() %>" />
<% if (Constants.MOBILEOPTION_ENABLED){%>
	<link rel="Alternate" media="handheld" href="<%= WebSanitizer.HtmlEncode(GetMobileUrl()) %>" />
<% } %>
<%= this.BrandAdditionalDsignTag %>
<% if (Constants.SEOTAG_IN_PRODUCTLIST_ENABLED){ %>
	<meta name="Keywords" content="<%: this.SeoKeywords %>" />
	<meta name="description" content="<%: this.SeoDescription %>" />
<% } %>
<%-- △編集可能領域△ --%>
<%# this.PaginationTag %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
	$(function () {

		// 商品一覧:詳細検索
		function getUrlVars() {
			var vars = [], hash;
			var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
			for (var i = 0; i < hashes.length; i++) {
				hash = hashes[i].split('=');
				vars.push(hash[0]);
				vars[hash[0]] = hash[1];
			}
			return vars;
		}

		//ソート
		$(".btn-sort-search").click(function () {

			var urlVars = getUrlVars();

			// 店舗ID
			var $shop = (urlVars["<%= Constants.REQUEST_KEY_SHOP_ID %>"] == undefined)
				? "<%= Constants.CONST_DEFAULT_SHOP_ID %>"
				: urlVars["<%= Constants.REQUEST_KEY_SHOP_ID %>"];
			// カテゴリ及びカテゴリ名
			if ($(".sort-category select").val() != "") {
				var $cat = $(".sort-category select").val();
				var $catName = $(".sort-category select option:selected").text();
			} else {
				var $cat = "";
				var $catName = "";
			}
			// ブランド
			var $bid = urlVars["<%= Constants.REQUEST_KEY_BRAND_ID %>"];
			var $brand = "<%= this.BrandName %>";
			if ($("input[name='iBrand']").length != 0) {
				if (("<%= Constants.PRODUCT_BRAND_ENABLED %>" == "True") && ($("input[name='iBrand']:checked").val() != undefined)) {
					$bid  = $("input[name='iBrand']:checked").val();
				} else {
					$bid = undefined;
				}
			}

			// Product Group ID
			var $productGroupId = "&<%= Constants.REQUEST_KEY_PRODUCT_GROUP_ID %>=";
			$productGroupId = $productGroupId + ((urlVars["<%= Constants.REQUEST_KEY_PRODUCT_GROUP_ID %>"] == undefined) ? "" : urlVars["<%= Constants.REQUEST_KEY_PRODUCT_GROUP_ID %>"]);
			// キャンペーンアイコン
			var $cicon = "&<%= Constants.REQUEST_KEY_CAMPAINGN_ICOM %>="
				+ ((urlVars["<%= Constants.REQUEST_KEY_CAMPAINGN_ICOM %>"] == undefined)
					? ""
					: urlVars["<%= Constants.REQUEST_KEY_CAMPAINGN_ICOM %>"]);
			// 特別価格商品の表示
			var $dosp = "&<%= Constants.REQUEST_KEY_DISP_ONLY_SP_PRICE %>="
				+ ((urlVars["<%= Constants.REQUEST_KEY_DISP_ONLY_SP_PRICE %>"] == undefined)
					? ""
					: urlVars["<%= Constants.REQUEST_KEY_DISP_ONLY_SP_PRICE %>"]);
			// 表示件数
			if ($("input[name='dpcnt']:checked").val() != "") {
				var $dpcnt = "&<%= Constants.REQUEST_KEY_DISP_PRODUCT_COUNT %>="
					+ $("input[name='dpcnt']:checked").val();
			} else {
				var $dpcnt = "&<%= Constants.REQUEST_KEY_DISP_PRODUCT_COUNT %>="
					+ ((urlVars["<%= Constants.REQUEST_KEY_DISP_IMG_KBN %>"] == "<%= Constants.KBN_REQUEST_DISP_IMG_KBN_ON %>")
						? "<%= ProductListDispSettingUtility.CountDispContentsImgOn %>"
						: "<%= ProductListDispSettingUtility.CountDispContentsWindowShopping %>");
			}
			// 画像表示区分
			var $img = "&<%= Constants.REQUEST_KEY_DISP_IMG_KBN %>="
				+ ((urlVars["<%= Constants.REQUEST_KEY_DISP_IMG_KBN %>"] == undefined)
					? ""
					: urlVars["<%= Constants.REQUEST_KEY_DISP_IMG_KBN %>"]);
			// 価格帯
			if ($("input[name='price']:checked").val() != "") {
				var price = $("input[name='price']:checked").val();
				priceValue = price.split(",");
				var $min = "&<%= Constants.REQUEST_KEY_MIN_PRICE %>=" + priceValue[0];
				var $max = "&<%= Constants.REQUEST_KEY_MAX_PRICE %>=" + priceValue[1];
			} else {
				var $min = "&<%= Constants.REQUEST_KEY_MIN_PRICE %>=";
				var $max = "&<%= Constants.REQUEST_KEY_MAX_PRICE %>=";
			}
			// 表示順
			if ($("input[name='sort']:checked").val() != "") {
				var $sort = "&<%= Constants.REQUEST_KEY_SORT_KBN %>=" + $("input[name='sort']:checked").val();
			} else {
				var $sort = "&<%= Constants.REQUEST_KEY_SORT_KBN %>=<%= ProductListDispSettingUtility.SortDefault %>";
			}
			// キーワード
			if ($(".sort-word input").val() != "") {
				var $swrd = "&<%= Constants.REQUEST_KEY_SEARCH_WORD %>=" + $(".sort-word input").val();
			} else {
				var $swrd = "&<%= Constants.REQUEST_KEY_SEARCH_WORD %>=";
			}
			// 在庫
			if ($("input[name='udns']:checked").val() != "") {
				var $udns = "&<%= Constants.REQUEST_KEY_UNDISPLAY_NOSTOCK_PRODUCT %>=" + $("input[name='udns']:checked").val();
			} else {
				var $udns = "&<%= Constants.REQUEST_KEY_UNDISPLAY_NOSTOCK_PRODUCT %>=";
			}
			// 定期購入フィルタ
			if ($("input[name=<%= Constants.FORM_NAME_FIXED_PURCHASE_RADIO_BUTTON %>]:checked").val() != "") {
				var $fpfl = "&<%= Constants.REQUEST_KEY_FIXED_PURCHASE_FILTER %>=" + $("input[name=<%= Constants.FORM_NAME_FIXED_PURCHASE_RADIO_BUTTON %>]:checked").val();
			} else {
				var $fpfl = "&<%= Constants.REQUEST_KEY_FIXED_PURCHASE_FILTER %>=";
			}

			// 指定したURLにジャンプ(1ページ目へ)
			if (("<%= Constants.FRIENDLY_URL_ENABLED %>" == "True") && ($catName != "")) {
				if (("<%= Constants.PRODUCT_BRAND_ENABLED %>" == "True") && ($brand != "")) {
					var rootUrl = "<%= Constants.PATH_ROOT %>" + $brand + "-" + $catName + "/brandcategory/" + $bid + "/" + $shop + "/" + $cat + "/?";
				} else {
					var rootUrl = "<%= Constants.PATH_ROOT %>" + $catName + "/category/" + $shop + "/" + $cat + "/?" + (($bid != undefined) ? "<%= Constants.REQUEST_KEY_BRAND_ID %>=" + $bid : "");
				}
			} else {
				var rootUrl = "<%= Constants.PATH_ROOT %><%= Constants.PAGE_FRONT_PRODUCT_LIST %>?<%= Constants.REQUEST_KEY_SHOP_ID %>=" + $shop
					+ "&<%= Constants.REQUEST_KEY_CATEGORY_ID %>=" + $cat + (($bid != undefined) ? "&<%= Constants.REQUEST_KEY_BRAND_ID %>=" + $bid : "");
			}
			location.href = rootUrl + $productGroupId + $cicon + $dosp + $dpcnt + $img + $max + $min + $sort + $swrd + $udns + $fpfl + "&<%= Constants.REQUEST_KEY_PAGE_NO %>=1";
		});
	});

	function enterSearch() {
		//EnterキーならSubmit
		if (window.event.keyCode == 13) document.formname.submit();
	}

	function bodyPageLoad() {
		// 検索結果数
		$("span.total_counts").prependTo(".keywordsArea_num span");

		//エンマーク削除
		function word_assassin(target,word){
			if(target.length){
				target.each(function(){
				var txt = $(this).html();
				$(this).html(
					txt.replace(word,'')//unicode escape sequence
				);
				});
			}
		}
		word_assassin($('span.price'),'¥');

		// アイコンテキスト化
		$(".icon .icn01").replaceWith('<span class="icn01">ネコポス</span>');
		$(".icon .icn02").replaceWith('<span class="icn02">SALE</span>');
		$(".icon .icn03").replaceWith('<span class="icn03">再入荷</span>');
		$(".icon .icn04").replaceWith('<span class="icn04">NEW</span>');
		$(".icon .icn05").replaceWith('<span class="icn05">キャンペーン</span>');
		$(".icon .icn06").replaceWith('<span class="icn06">セット価格</span>');
		$(".icon .icn07").replaceWith('<span class="icn07">数量限定</span>');
		$(".icon .icn08").replaceWith('<span class="icn08">お一人様1点限り</span>');
		$(".icon .icn09").replaceWith('<span class="icn09">レフィル対応商品</span>');
		$(".icon .icn10").replaceWith('<span class="icn10">季節のおすすめ</span>');

		// バリエーション2つ目以降非表示
		$("#productList .listWrap_list").each(function(i, elem) {
			$(this).find(".addCartNormal").eq(1).css("display","none");
			$(this).find(".addCartSubsc").eq(1).css("display","none");
			$(this).find(".subscTxt").eq(1).css("display","none");
		});
		$("#productList .listWrap_list").each(function(i, elem) {
			$(this).find(".addCartNormal").eq(2).css("display","none");
			$(this).find(".addCartSubsc").eq(2).css("display","none");
			$(this).find(".subscTxt").eq(2).css("display","none");
		});
		$("#productList .listWrap_list").each(function(i, elem) {
			$(this).find(".addCartNormal").eq(3).css("display","none");
			$(this).find(".addCartSubsc").eq(3).css("display","none");
			$(this).find(".subscTxt").eq(3).css("display","none");
		});
		$("#productList .listWrap_list").each(function(i, elem) {
			$(this).find(".addCartNormal").eq(4).css("display","none");
			$(this).find(".addCartSubsc").eq(4).css("display","none");
			$(this).find(".subscTxt").eq(4).css("display","none");
		});
		$("#productList .listWrap_list").each(function(i, elem) {
			$(this).find(".addCartNormal").eq(5).css("display","none");
			$(this).find(".addCartSubsc").eq(5).css("display","none");
			$(this).find(".subscTxt").eq(5).css("display","none");
		});
	}

</script>


<article id="productList">

<!--▽ 上部カテゴリリンク ▽-->
<div class="breadArea pc_contents">
<uc:BodyProductCategoryLinks runat="server"></uc:BodyProductCategoryLinks>
</div>
<!--△ 上部カテゴリリンク △-->

<!--▽ カテゴリHTML領域 ▽-->
<uc:BodyProductCategoryHtml runat="server" />
<!--△ カテゴリHTML領域 △-->

<!--▽ 商品グループページHTML領域 ▽-->
<uc:BodyProductGroupContentsHtml runat="server" />
<!--△ 商品グループページHTML領域 △-->

<%--▽ <uc:BodyProductAdvancedSearchBox runat="server" /> △--%>

<!--▽ ソートコントロール
<uc:BodyProductSortBox CategoryName="<%# this.CategoryName %>" runat="server"></uc:BodyProductSortBox>
ソートコントロール △-->

<div class="listProduct">

<%-- カート投入ボタン押下時にどの画面へ遷移するか？ --%>
<%-- CART：カート一覧画面 CSCART:クロスセルカート画面 その他：画面遷移しない --%>
<asp:HiddenField ID="hfIsRedirectAfterAddProduct" Value="CART" runat="server" />

<%-- お気に入り追加ボタン押下時にどの画面へ遷移するか？ --%>
<%-- true:ポップアップ表示、false:お気に入り一覧ページへ遷移 --%>
<% IsDisplayPopupAddFavorite = true; %>
<div>
<p id="addFavoriteTip" class="toolTip" style="display: none; position: fixed;">
	<span style="margin: 10px;" id="txt-tooltip"></span>
	<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_FAVORITE_LIST) %>" class="btn btn-mini btn-inverse">お気に入り一覧</a>
</p>
</div>

<!-- 商品一覧ループ(通常表示) -->
<%-- UPDATE PANEL開始 --%>
<asp:UpdatePanel runat="server">
<ContentTemplate>
<%-- ▽商品一覧ループ(通常表示)▽ --%>
<asp:Repeater DataSource="<%# this.IsDispImageKbnOn ? this.ProductMasterList : null %>" runat="server" Visible="<%# this.IsDispImageKbnOn %>" OnItemCommand="ProductMasterList_ItemCommand">
<HeaderTemplate>
<section class="listWrap">
</HeaderTemplate>
<ItemTemplate>
<div class="listWrap_list">
<ul>
<li class="thumb">
<!-- 商品画像表示 -->
<div>
<% if(Constants.LAYER_DISPLAY_VARIATION_IMAGES_ENABLED){ %>
<uc:BodyProductVariationImages ImageSize="M" ProductMaster="<%# Container.DataItem %>" VariationList="<%# this.ProductVariationList %>" VariationNo="<%# Container.ItemIndex.ToString() %>" runat="server" />
<% } else { %>
<a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(Container.DataItem, true)) %>'>
<w2c:ProductImage ImageSize="M" ProductMaster="<%# Container.DataItem %>" IsVariation="false" runat="server" /></a>
<% } %>
<%-- ▽在庫切れ可否▽ --%>
<p visible='<%# ProductListUtility.IsProductSoldOut(Container.DataItem) %>' runat="server" class="soldout">SOLDOUT</p>
<%-- ▽在庫切れ可否▽ --%>
</div>
</li>
<li class="name">
<!-- 概要表示 -->
<p class="outline"><%# GetProductDataHtml(Container.DataItem, "outline") %></p>
<!-- アイコン表示 -->
<div class="icon">
<w2c:ProductIcon IconNo="1" ProductMaster="<%# Container.DataItem %>" cssClass="icn01" runat="server" />
<w2c:ProductIcon IconNo="2" ProductMaster="<%# Container.DataItem %>" cssClass="icn02" runat="server" />
<w2c:ProductIcon IconNo="3" ProductMaster="<%# Container.DataItem %>" cssClass="icn03" runat="server" />
<w2c:ProductIcon IconNo="4" ProductMaster="<%# Container.DataItem %>" cssClass="icn04" runat="server" />
<w2c:ProductIcon IconNo="5" ProductMaster="<%# Container.DataItem %>" cssClass="icn05" runat="server" />
<w2c:ProductIcon IconNo="6" ProductMaster="<%# Container.DataItem %>" cssClass="icn06" runat="server" />
<w2c:ProductIcon IconNo="7" ProductMaster="<%# Container.DataItem %>" cssClass="icn07" runat="server" />
<w2c:ProductIcon IconNo="8" ProductMaster="<%# Container.DataItem %>" cssClass="icn08" runat="server" />
<w2c:ProductIcon IconNo="9" ProductMaster="<%# Container.DataItem %>" cssClass="icn09" runat="server" />
<w2c:ProductIcon IconNo="10" ProductMaster="<%# Container.DataItem %>" cssClass="icn10" runat="server" />
</div>
<!-- 商品名表示 -->
<h3 class="nameLink"><a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(Container.DataItem, true)) %>'>
<%# WebSanitizer.HtmlEncode(GetProductData(Container.DataItem, "name")) %></a></h3>
<!-- TODO:表示テスト -->
</li>
<li class="nameSp">
	<!-- アイコン表示 -->
	<div class="icon">
	<w2c:ProductIcon IconNo="1" ProductMaster="<%# Container.DataItem %>" cssClass="icn01" runat="server" />
	<w2c:ProductIcon IconNo="2" ProductMaster="<%# Container.DataItem %>" cssClass="icn02" runat="server" />
	<w2c:ProductIcon IconNo="3" ProductMaster="<%# Container.DataItem %>" cssClass="icn03" runat="server" />
	<w2c:ProductIcon IconNo="4" ProductMaster="<%# Container.DataItem %>" cssClass="icn04" runat="server" />
	<w2c:ProductIcon IconNo="5" ProductMaster="<%# Container.DataItem %>" cssClass="icn05" runat="server" />
	<w2c:ProductIcon IconNo="6" ProductMaster="<%# Container.DataItem %>" cssClass="icn06" runat="server" />
	<w2c:ProductIcon IconNo="7" ProductMaster="<%# Container.DataItem %>" cssClass="icn07" runat="server" />
	<w2c:ProductIcon IconNo="8" ProductMaster="<%# Container.DataItem %>" cssClass="icn08" runat="server" />
	<w2c:ProductIcon IconNo="9" ProductMaster="<%# Container.DataItem %>" cssClass="icn09" runat="server" />
	<w2c:ProductIcon IconNo="10" ProductMaster="<%# Container.DataItem %>" cssClass="icn10" runat="server" />
	</div>
	<!-- 商品名表示 -->
	<h3 class="nameLink"><a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(Container.DataItem, true)) %>'>
	<%# WebSanitizer.HtmlEncode(GetProductData(Container.DataItem, "name")) %></a></h3>
</li>
<li style="display: none;">
<asp:Repeater ID="rSetPromotion" DataSource="<%# GetSetPromotionByProduct((DataRowView)Container.DataItem) %>" runat="server">
<ItemTemplate>
<span visible='<%# ((SetPromotionModel)Container.DataItem).Url != "" %>' runat="server">
	<a href="<%# WebSanitizer.HtmlEncode(Constants.PATH_ROOT + ((SetPromotionModel)Container.DataItem).Url) %>"><%# WebSanitizer.HtmlEncode(((SetPromotionModel)Container.DataItem).SetpromotionDispName) %></a><br />
</span>
<span visible='<%# (string)Eval("Url") == "" %>' runat="server">
	<%# WebSanitizer.HtmlEncode(((SetPromotionModel)Container.DataItem).SetpromotionDispName) %><br />
</span>
</ItemTemplate>
</asp:Repeater>
</li>

<li class="price">
<div style="display: none;">
<%-- ▽商品会員ランク価格有効▽ --%>
<span visible='<%# GetProductMemberRankPriceValid(Container.DataItem) %>' runat="server">
販売価格:<span class="productPrice"><strike><%#: CurrencyManager.ToPrice(ProductPage.GetProductPriceNumeric(Container.DataItem)) %>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）</strike></span><br />
<span>会員ランク価格:<%#: CurrencyManager.ToPrice(ProductPage.GetProductMemberRankPrice(Container.DataItem)) %></span>(<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>)
</span>
<%-- △商品会員ランク価格有効△ --%>
<%-- ▽商品セール価格有効▽ --%>
<span visible='<%# GetProductTimeSalesValid(Container.DataItem) %>' runat="server">
販売価格:<span class="productPrice"><strike><%#: CurrencyManager.ToPrice(ProductPage.GetProductPriceNumeric(Container.DataItem)) %>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）</strike></span><br />
<span>タイムセールス価格:<%#: CurrencyManager.ToPrice(ProductPage.GetProductTimeSalePriceNumeric(Container.DataItem)) %></span>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）
</span>
<%-- △商品セール価格有効△ --%>
<%-- ▽商品特別価格有効▽ --%>
<span visible='<%# GetProductSpecialPriceValid(Container.DataItem) %>' runat="server">
販売価格:<span class="productPrice"><strike><%#: CurrencyManager.ToPrice(ProductPage.GetProductPriceNumeric(Container.DataItem)) %>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）</strike></span><br />
<span>特別価格:<%#: CurrencyManager.ToPrice(ProductPage.GetProductSpecialPriceNumeric(Container.DataItem)) %></span>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）
</span>
<%-- △商品特別価格有効△ --%>
<%-- ▽商品通常価格有効▽ --%>
<span visible='<%# GetProductNormalPriceValid(Container.DataItem) %>' runat="server">
販売価格:<span class="productPrice"><%#: CurrencyManager.ToPrice(ProductPage.GetProductPriceNumeric(Container.DataItem)) %></span>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）
</span>
<%-- △商品通常価格有効△ --%>
<%-- ▽商品加算ポイント▽ --%>
<p visible='<%# (this.IsLoggedIn && (GetProductAddPointString(Container.DataItem) != "")) %>' runat="server">
	<span class="addPoint">ポイント<%# WebSanitizer.HtmlEncode(GetProductAddPointString(Container.DataItem)) %></span><span id="Span1" visible='<%# ((string)GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_POINT_KBN1)) != Constants.FLG_PRODUCT_POINT_KBN1_NUM %>' runat="server">(<%# WebSanitizer.HtmlEncode(GetProductAddPointCalculateAfterString(Container.DataItem, false, false))%>)
	</span>
</p>
<%-- △商品加算ポイント△ --%>
<%-- ▽商品定期購入価格▽ --%>
<% if (Constants.FIXEDPURCHASE_OPTION_ENABLED) {%>
<span visible='<%# (GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_FIXED_PURCHASE_FLG).ToString() != Constants.FLG_PRODUCT_FIXED_PURCHASE_FLG_INVALID) && ((CheckFixedPurchaseLimitedUserLevel(this.ShopId, (string)GetProductData(Container.DataItem, "product_id")) == false)) %>' runat="server">
	<span visible='<%# IsProductFixedPurchaseFirsttimePriceValid(Container.DataItem) %>' runat="server">
		<br />
		定期初回価格:<%#: CurrencyManager.ToPrice(ProductPage.GetProductFixedPurchaseFirsttimePrice(Container.DataItem)) %>（<%#: GetTaxIncludeString(Container.DataItem) %>）
	</span>
	<br />
	定期通常価格:<%#: CurrencyManager.ToPrice(ProductPage.GetProductFixedPurchasePrice(Container.DataItem)) %>（<%#: GetTaxIncludeString(Container.DataItem) %>）
</span>
<% } %>
<%-- △商品定期購入価格△ --%>
<%-- ▽定期商品加算ポイント▽ --%>
<p visible='<%# (this.IsLoggedIn && (GetProductAddPointString(Container.DataItem) != "") && (GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_FIXED_PURCHASE_FLG).ToString() != Constants.FLG_PRODUCT_FIXED_PURCHASE_FLG_INVALID) && ((CheckFixedPurchaseLimitedUserLevel(this.ShopId, (string)GetProductData(Container.DataItem, "product_id")) == false))) %>' runat="server">
	<span class="addPoint">ポイント<%# WebSanitizer.HtmlEncode(GetProductAddPointString(Container.DataItem, false, false, true)) %></span>
	<span visible='<%# ((string)GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_POINT_KBN2)) != Constants.FLG_PRODUCT_POINT_KBN1_NUM %>' runat="server">(<%# WebSanitizer.HtmlEncode(GetProductAddPointCalculateAfterString(Container.DataItem, false, false, true))%>)
	</span>
</p>
<%-- △定期商品加算ポイント△ --%>

<%-- ▽商品タグ項目：メーカー▽ --%>
<span visible='<%# StringUtility.ToEmpty(GetKeyValue(Container.DataItem, "tag_manufacturer")) != "" %>' runat="server">
メーカー:<%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, "tag_manufacturer")) %>
</span>
<%-- △商品タグ項目：メーカー△ --%>

<%-- ▽お気に入りの登録人数表示▽ --%>
<p visible="true" runat="server" class="favoriteRegistration">
お気に入りの登録人数：<%# this.GetFavoriteCount((string)GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTVARIATION_PRODUCT_ID)) %>人
</p>
<%-- △お気に入りの登録人数表示△ --%>

<%-- ▽お気に入り追加▽ --%>
<asp:LinkButton ID="lbAddFavorite" runat="server"
CommandName="FavoriteAdd" CommandArgument="<%# GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTVARIATION_PRODUCT_ID) %>" class="btn btn-mini">
	お気に入りへ
</asp:LinkButton>

<%-- △お気に入り追加△ --%>
</div>
<%-- ▽バリエーションリストループ▽ --%>
<%if (Constants.PRODUCTLIST_VARIATION_DISPLAY_ENABLED) { %>
<div id="divProductListMultiVariation">
<asp:Repeater ID="rAddCartVariationList" DataSource="<%# GetProductListVariation((DataRowView)Container.DataItem) %>" onitemcommand="AddCartVariationList_ItemCommand" runat="server">
<HeaderTemplate>
</HeaderTemplate>
<ItemTemplate>

<%-- 価格 --%>
<%-- ▽商品加算ポイント▽ --%>
<p visible='<%# (this.IsLoggedIn && (GetProductAddPointString(Container.DataItem, true, true) != "")) %>' runat="server">
<span class="addPoint">
ポイント：<%# WebSanitizer.HtmlEncode(GetProductAddPointString(Container.DataItem, true, true)) %></span><span visible='<%# ((string)GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_POINT_KBN1)) != Constants.FLG_PRODUCT_POINT_KBN1_NUM %>' runat="server">(<%# WebSanitizer.HtmlEncode(GetProductAddPointCalculateAfterString(Container.DataItem, true, true))%>)</span>
</p>
<%-- △商品加算ポイント△ --%>
<%-- ▽商品加算ポイント▽ --%>
<p visible='<%# (this.IsLoggedIn && (GetProductAddPointString(Container.DataItem, true, true, true) != "") && (((bool)GetKeyValue(Container.DataItem, "CanFixedPurchase")) && ((CheckFixedPurchaseLimitedUserLevel(this.ShopId, (string)GetProductData(Container.DataItem, "product_id")) == false)))) %>' runat="server">
<span class="addPoint">
ポイント：<%# WebSanitizer.HtmlEncode(GetProductAddPointString(Container.DataItem, true, true, true)) %></span><span visible='<%# ((string)GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_POINT_KBN2)) != Constants.FLG_PRODUCT_POINT_KBN2_NUM %>' runat="server">(<%# WebSanitizer.HtmlEncode(GetProductAddPointCalculateAfterString(Container.DataItem, true, true, true))%>)</span>
</p>

<%-- △商品加算ポイント△ --%>
<%-- カート投入ボタン --%>
<%-- 定期購入ボタン表示 --%>
<div class="addCartSubsc" visible='<%# (((bool)GetKeyValue(Container.DataItem, "CanFixedPurchase")) && (this.IsUserFixedPurchaseAble)) %>' runat="server">
<%-- ▽商品定期購入価格 --%>
<div class="price">
	<% if (Constants.FIXEDPURCHASE_OPTION_ENABLED) {%>
	<p visible='<%# (GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_FIXED_PURCHASE_FLG).ToString() != Constants.FLG_PRODUCT_FIXED_PURCHASE_FLG_INVALID) && ((CheckFixedPurchaseLimitedUserLevel(this.ShopId, (string)GetProductData(Container.DataItem, "product_id")) == false)) %>' runat="server">
		<p visible='<%# IsProductFixedPurchaseFirsttimePriceValid(Container.DataItem, true) %>' runat="server">
			<span class="txt">初回50%OFF</span><span class="tax"><%#: GetKeyValue(Container.DataItem, "TaxIncluded") %></span><span class="price"><%#: CurrencyManager.ToPrice(GetProductFixedPurchaseFirsttimePrice(Container.DataItem, true)) %></span><span class="yen">円</span>
		</p>
	</p>
	<% } %>
</div>
<%-- △商品定期購入価格△ --%>
<asp:LinkButton ID="lbCartAddFixedPurchaseVariationList" runat="server" Visible='<%# (((bool)GetKeyValue(Container.DataItem, "CanFixedPurchase")) && (this.IsUserFixedPurchaseAble)) %>' OnClientClick="return add_cart_check_for_fixedpurchase_variationlist();" CommandName="CartAddFixedPurchase" class="subscBtn">
定期購入する
</asp:LinkButton>
<span runat="server" Visible='<%# ((bool)GetKeyValue(Container.DataItem, "CanFixedPurchase")) && ((string)GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_FIXED_PURCHASE_FLG) == Constants.FLG_PRODUCT_FIXED_PURCHASE_FLG_ONLY) && (this.IsUserFixedPurchaseAble == false) %>' style="color: red;">定期購入のご利用はできません</span>
</div>
<div class="subscTxt" visible='<%# (((bool)GetKeyValue(Container.DataItem, "CanFixedPurchase")) && (this.IsUserFixedPurchaseAble)) %>' runat="server">
	<% if (Constants.FIXEDPURCHASE_OPTION_ENABLED) {%>
	<p visible='<%# (GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_FIXED_PURCHASE_FLG).ToString() != Constants.FLG_PRODUCT_FIXED_PURCHASE_FLG_INVALID) && ((CheckFixedPurchaseLimitedUserLevel(this.ShopId, (string)GetProductData(Container.DataItem, "product_id")) == false)) %>' runat="server">
		<p>
			<span class="txt">2回目以降15%OFF</span><span class="price"><%#: GetKeyValue(Container.DataItem, "TaxIncluded") %> <%#: CurrencyManager.ToPrice(GetProductFixedPurchasePrice(Container.DataItem, true)) %></span><span class="yen">円</span><span class="note">（6回以上のお受け取りが条件となる商品です）</span>
		</p>
	</p>
	<% } %>
</div>
<%-- ギフト購入ボタン表示 --%>
<div>
<asp:LinkButton ID="lbCartAddForGiftVariationList" runat="server" Visible='<%# GetKeyValue(Container.DataItem, "CanGiftOrder") %>' CommandName="CartAddGift">
カートに入れる(ギフト購入)
</asp:LinkButton>
</div>
<%-- カートに入れるボタン表示 --%>

<div class="addCartNormal">
	<%-- ▽商品通常価格有効▽ --%>
	<div class="price" visible='<%# GetProductNormalPriceValid(Container.DataItem, true) %>' runat="server">
		<p>
			<span class="tax"><%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, "TaxIncluded")) %></span><span class="price"><%#: CurrencyManager.ToPrice(GetProductPriceNumeric(Container.DataItem, true)) %></span><span class="yen">円</span>
		</p>
	</div>
	<%-- △商品通常価格有効△ --%>
	<%-- ▽商品セール価格有効▽ --%>
	<div class="price" visible='<%# GetProductTimeSalesValid(Container.DataItem) %>' runat="server">
		<p class="strike">
			<span class="tax"><%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, "TaxIncluded")) %></span><span class="price"><%#: CurrencyManager.ToPrice(GetProductPriceNumeric(Container.DataItem, true)) %></span><span class="yen">円</span>
		</p>
		<p class="salePrice">
			<span class="tax"><%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, "TaxIncluded")) %></span><span class="price"><%#: CurrencyManager.ToPrice(ProductPage.GetProductTimeSalePriceNumeric(Container.DataItem)) %></span><span class="yen">円（<%#: GetProductTimeSalesValid(Container.DataItem)
				? (Math.Floor((1 - Convert.ToDecimal(GetProductTimeSalePriceNumeric(Container.DataItem).Replace(",","")) 
					/ Convert.ToDecimal(GetProductPriceNumeric(Container.DataItem).Replace(",",""))) * 100)) 
				: 0 %>%OFF）</span>
		</p>
	</div>
	<%-- △商品セール価格有効△ --%>
	<%-- ▽商品特別価格有効▽ --%>
	<div class="price" visible='<%# GetProductSpecialPriceValid(Container.DataItem, true) %>' runat="server">
		<p class="strike">
			<span class="tax"><%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, "TaxIncluded")) %></span><span class="price"><%#: CurrencyManager.ToPrice(GetProductPriceNumeric(Container.DataItem, true)) %></span><span class="yen">円</span>
		</p>
		<p class="salePrice">
			<span class="tax"><%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, "TaxIncluded")) %></span><span class="price"><%#: CurrencyManager.ToPrice(GetProductSpecialPriceNumeric(Container.DataItem, true)) %></span><span class="yen">円（<%#: GetProductSpecialPriceValid(Container.DataItem)
				? (Math.Floor((1 - Convert.ToDecimal(GetProductSpecialPriceNumeric(Container.DataItem).Replace(",","")) 
					/ Convert.ToDecimal(GetProductPriceNumeric(Container.DataItem).Replace(",",""))) * 100)) 
				: 0 %>%OFF）</span>
		</p>
	</div>
	<%-- △商品特別価格有効△ --%>
	<%-- ▽商品会員ランク価格有効▽ --%>
	<div class="price" visible='<%# GetProductMemberRankPriceValid(Container.DataItem, true) %>' runat="server">
		<p class="strike rank">
			<span class="tax"><%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, "TaxIncluded")) %></span><span class="price"><%#: CurrencyManager.ToPrice(GetProductPriceNumeric(Container.DataItem, true)) %></span><span class="yen">円</span>
		</p>
		<p class="salePrice rank">
			<span class="offRate">
				<%: this.MemberRankName %>会員価格
			</span>
			<span class="tax"><%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, "TaxIncluded")) %></span><span class="price"><%#: CurrencyManager.ToPrice(GetProductMemberRankPrice(Container.DataItem, true)) %></span><span class="yen">円（
				<%#: GetProductMemberRankPriceValid(Container.DataItem, true)
					? (Math.Floor((1 - Convert.ToDecimal(GetProductMemberRankPrice(Container.DataItem, true).Replace(",","")) 
						/ Convert.ToDecimal(GetProductPriceNumeric(Container.DataItem, true).Replace(",",""))) * 100)) 
					: 0 %>%OFF）</span>
		</p>
	</div>
	<%-- △商品会員ランク価格有効△ --%>
	<asp:LinkButton ID="lbCartAddVariationList" runat="server" Visible='<%# GetKeyValue(Container.DataItem, "CanCart") %>' CommandName="CartAdd" class="normalBtn">
	通常購入する
	</asp:LinkButton>
	<%-- 再入荷通知メール申し込みボタン表示 --%>
	<asp:LinkButton CommandName="SmartArrivalMail" CommandArgument="Arrival" Runat="server" class="normalBtn" visible='<%# ((string)GetKeyValue(Container.DataItem, "ArrivalMailKbn") == Constants.FLG_USERPRODUCTARRIVALMAIL_ARRIVAL_MAIL_KBN_ARRIVAL) %>'>
	入荷お知らせメール申込
	</asp:LinkButton>
	<%-- 販売開始通知メール申し込みボタン表示 --%>
	<asp:LinkButton CommandName="SmartArrivalMail" CommandArgument="Release" Runat="server" class="normalBtn" visible='<%# ((string)GetKeyValue(Container.DataItem, "ArrivalMailKbn") == Constants.FLG_USERPRODUCTARRIVALMAIL_ARRIVAL_MAIL_KBN_RELEASE) %>'>
	販売開始通知メール申込
	</asp:LinkButton>
	<%-- 再販売通知メール申し込みボタン表示 --%>
	<asp:LinkButton CommandName="SmartArrivalMail" CommandArgument="Resale" Runat="server" class="normalBtn" visible='<%# ((string)GetKeyValue(Container.DataItem, "ArrivalMailKbn") == Constants.FLG_USERPRODUCTARRIVALMAIL_ARRIVAL_MAIL_KBN_RESALE) %>'>
	再販売通知メール申込
	</asp:LinkButton>
</div>
					
<%-- 入荷通知メールボタン --%>
<p class="arrivalMailButton">
<%-- エラー表示 --%>
<p class="error"><%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, "ErrorMessage")) %></p>
</p>	

<p>
<asp:Repeater ID="rSetPromotionByVariation" DataSource="<%# GetSetPromotionByVariation((Dictionary<string, object>)Container.DataItem) %>" runat="server">
<ItemTemplate>
<%# WebSanitizer.HtmlEncode(((SetPromotionModel)Container.DataItem).SetpromotionDispName) %><br />
</ItemTemplate>
</asp:Repeater>
</p>

<%-- 隠しフィールド --%>
<asp:HiddenField ID="hfProductId" Value="<%# GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTVARIATION_PRODUCT_ID) %>" runat="server" />
<asp:HiddenField ID="hfVariationId" Value="<%# GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTVARIATION_VARIATION_ID) %>" runat="server" />
<asp:HiddenField ID="hfArrivalMailKbn" Value='<%# GetKeyValue(Container.DataItem, "ArrivalMailKbn") %>' runat="server" />

<%-- 再入荷通知メール登録フォーム表示 --%>
<uc:BodyProductArrivalMailRegisterTr runat="server" ID="ucBpamrArrival" ArrivalMailKbn="<%#: Constants.FLG_USERPRODUCTARRIVALMAIL_ARRIVAL_MAIL_KBN_ARRIVAL %>" ProductId="<%#: GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_PRODUCT_ID) %>" VariationId="<%#: GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTVARIATION_VARIATION_ID) %>" Visible="false" />
<%-- 販売開始通知メール登録フォーム表示 --%>
<uc:BodyProductArrivalMailRegisterTr runat="server" ID="ucBpamrRelease" ArrivalMailKbn="<%#: Constants.FLG_USERPRODUCTARRIVALMAIL_ARRIVAL_MAIL_KBN_RELEASE %>" ProductId="<%#: GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_PRODUCT_ID) %>" VariationId="<%#: GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTVARIATION_VARIATION_ID) %>" Visible="false" />
<%-- 再販売知メール登録フォーム表示 --%>
<uc:BodyProductArrivalMailRegisterTr runat="server" ID="ucBpamrResale" ArrivalMailKbn="<%#: Constants.FLG_USERPRODUCTARRIVALMAIL_ARRIVAL_MAIL_KBN_RESALE %>" ProductId="<%#: GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_PRODUCT_ID) %>" VariationId="<%#: GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTVARIATION_VARIATION_ID) %>" Visible="false" />

</ItemTemplate>
<FooterTemplate>
</FooterTemplate>
</asp:Repeater>
</div>
<% } %>
<%-- △バリエーションリストループ△ --%>
</li>
</ul>
</div>
</ItemTemplate>
<FooterTemplate>
</section>
</FooterTemplate>
</asp:Repeater>
</ContentTemplate>
</asp:UpdatePanel>
<%-- UPDATE PANELここまで --%>
<%-- △商品一覧ループ(通常表示)△ --%>

</div>


<%-- ▽商品一覧ループ(ウインドウショッピング)▽ --%>
<asp:Repeater DataSource="<%# (this.IsDispImageKbnWindowsShopping) ? this.ProductMasterList : null %>" runat="server" Visible="<%# this.IsDispImageKbnWindowsShopping %>">
<HeaderTemplate>
<section class="listWrap">
</HeaderTemplate>
<ItemTemplate>

<div class="listWrap_list">

<ul>
<li class="icon">
</li>
<li class="thumb">
<% if(Constants.LAYER_DISPLAY_VARIATION_IMAGES_ENABLED){ %>
<uc:BodyProductVariationImages ImageSize="M" ProductMaster="<%# Container.DataItem %>" VariationList="<%# this.ProductVariationList %>" VariationNo="<%# Container.ItemIndex.ToString() %>" runat="server" />
<% } else { %>
<a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(Container.DataItem, true)) %>'>
<w2c:ProductImage ImageSize="M" ProductMaster="<%# Container.DataItem %>" IsVariation="false" runat="server" /></a>
<% } %><span visible='<%# ProductListUtility.IsProductSoldOut(Container.DataItem) %>' runat="server" class="soldout">SOLDOUT</span>
</li>
<li class="name">
	<p class="outline">
		<%# WebSanitizer.HtmlEncode(GetProductData(Container.DataItem, "outline")) %>
	</p>
	<w2c:ProductIcon IconNo="1" ProductMaster="<%# Container.DataItem %>" runat="server" />
	<w2c:ProductIcon IconNo="2" ProductMaster="<%# Container.DataItem %>" runat="server" />
	<w2c:ProductIcon IconNo="3" ProductMaster="<%# Container.DataItem %>" runat="server" />
	<w2c:ProductIcon IconNo="4" ProductMaster="<%# Container.DataItem %>" runat="server" />
	<w2c:ProductIcon IconNo="5" ProductMaster="<%# Container.DataItem %>" runat="server" />
	<w2c:ProductIcon IconNo="6" ProductMaster="<%# Container.DataItem %>" runat="server" />
	<w2c:ProductIcon IconNo="7" ProductMaster="<%# Container.DataItem %>" runat="server" />
	<w2c:ProductIcon IconNo="8" ProductMaster="<%# Container.DataItem %>" runat="server" />
	<w2c:ProductIcon IconNo="9" ProductMaster="<%# Container.DataItem %>" runat="server" />
	<w2c:ProductIcon IconNo="10" ProductMaster="<%# Container.DataItem %>" runat="server" />
	<a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(Container.DataItem, true)) %>' class="nameLink">
	<%# WebSanitizer.HtmlEncode(GetProductData(Container.DataItem, "name")) %></a>
</li>
<li class="price">

<%-- ▽商品会員ランク価格有効▽ --%>
<p visible='<%# GetProductMemberRankPriceValid(Container.DataItem) %>' runat="server">
<span style="text-decoration: line-through"><%#: CurrencyManager.ToPrice(ProductPage.GetProductPriceNumeric(Container.DataItem)) %>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）</span><br />
<span style="color: #f00;"><%#: CurrencyManager.ToPrice(ProductPage.GetProductMemberRankPrice(Container.DataItem)) %>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）</span>
</p>

<%-- ▽商品セール価格有効▽ --%>
<p visible='<%# GetProductTimeSalesValid(Container.DataItem) %>' runat="server">
	<span style="text-decoration: line-through"><%#: CurrencyManager.ToPrice(ProductPage.GetProductPriceNumeric(Container.DataItem)) %>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）</span><br />
	<span style="color: #f00;"><%#: CurrencyManager.ToPrice(ProductPage.GetProductTimeSalePriceNumeric(Container.DataItem)) %>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）</span>
</p>

<%-- ▽商品特別価格有効▽ --%>
<p visible='<%# GetProductSpecialPriceValid(Container.DataItem) %>' runat="server">
<span style="text-decoration: line-through"><%#: CurrencyManager.ToPrice(ProductPage.GetProductPriceNumeric(Container.DataItem)) %>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）</span><br />
<span style="color: #f00;"><%#: CurrencyManager.ToPrice(ProductPage.GetProductSpecialPriceNumeric(Container.DataItem)) %>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）</span>
</p>

<%-- ▽商品通常価格有効▽ --%>
<p visible='<%# GetProductNormalPriceValid(Container.DataItem) %>' runat="server">
<%#: CurrencyManager.ToPrice(ProductPage.GetProductPriceNumeric(Container.DataItem)) %>（<%# WebSanitizer.HtmlEncode(GetTaxIncludeString(Container.DataItem)) %>）
</p>
<%-- ▽定期購入価格有効▽ --%>
<% if (Constants.FIXEDPURCHASE_OPTION_ENABLED) {%>
<p visible='<%# (GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCT_FIXED_PURCHASE_FLG).ToString() != Constants.FLG_PRODUCT_FIXED_PURCHASE_FLG_INVALID) && (CheckFixedPurchaseLimitedUserLevel(this.ShopId, (string)GetProductData(Container.DataItem, "product_id")) == false) %>' runat="server">
	<p visible='<%# IsProductFixedPurchaseFirsttimePriceValid(Container.DataItem) %>' runat="server">
		定期初回:<%#: CurrencyManager.ToPrice(ProductPage.GetProductFixedPurchaseFirsttimePrice(Container.DataItem)) %>（<%#: GetTaxIncludeString(Container.DataItem) %>）
	</p>
	<p>
		定期通常:<%#: CurrencyManager.ToPrice(ProductPage.GetProductFixedPurchasePrice(Container.DataItem)) %>（<%#: GetTaxIncludeString(Container.DataItem) %>）
	</p>
</p>
<% } %>
</li>
<%-- ▽お気に入りの登録人数表示▽ --%>
<li visible='<%# this.GetFavoriteCount((string)GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTVARIATION_PRODUCT_ID)) != 0 %>' class="favorite" runat="server">
お気に入りの登録人数：<%# this.GetFavoriteCount((string)GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTVARIATION_PRODUCT_ID)) %>人
</li>
<%-- △お気に入りの登録人数表示△ --%>
</ul>
</div>
</ItemTemplate>
<FooterTemplate>
</section>
</FooterTemplate>
</asp:Repeater>
<%-- △商品一覧ループ(ウインドウショッピング)△ --%>
		
<!--▽ ページャ △-->
<div class="pagerArea" style="display: none;">
<%# this.PagerHtml %>
</div>
<!--▽ ページャ △-->

<div visible="<%# (this.ProductMasterList.Count == 0) %>" runat="server" class="noProduct">
<!--▽ 商品が1つもなかった場合のエラー文言 ▽-->
<%# WebSanitizer.HtmlEncode(this.AlertMessage) %>
<!--△ 商品が1つもなかった場合のエラー文言 △-->
</div><!-- (this.ProductMasterList.Count != 0) -->

<%-- ▽最近チェックした商品▽ --%>
<uc:BodyProductHistory runat="server" />
<%-- △最近チェックした商品△ --%>

<!--▽ 上部カテゴリリンク ▽-->
<div class="breadArea sp_contents">
<uc:BodyProductCategoryLinks runat="server"></uc:BodyProductCategoryLinks>
</div>
<!--△ 上部カテゴリリンク △-->

</article>

<script runat="server">
public new void Page_Load(Object sender, EventArgs e)
{
base.Page_Load(sender, e);

var recommendEngineUserControls = WebControlUtility.GetRecommendEngineUserControls(this.Form.FindControl("ContentPlaceHolder1"));
var lProductRecommendByRecommendEngineUserControls = recommendEngineUserControls.Item1;
var lCategoryRecommendByRecommendEngineUserControls = recommendEngineUserControls.Item2;

<%-- ▽編集可能領域：プロパティ設定▽ --%>
// 外部レコメンド連携パーツ設定
// 1つ目の商品レコメンド
if (lProductRecommendByRecommendEngineUserControls.Count > 0)
{
	// レコメンドコードを設定します
	lProductRecommendByRecommendEngineUserControls[0].RecommendCode = "pc211";
	// レコメンドタイトルを設定します
	lProductRecommendByRecommendEngineUserControls[0].RecommendTitle = "おすすめ商品一覧";
	// 商品最大表示件数を設定します
	lProductRecommendByRecommendEngineUserControls[0].MaxDispCount = 5;
	// レコメンド対象にするカテゴリIDを設定します（複数選択時はカンマ区切りで指定）
	lProductRecommendByRecommendEngineUserControls[0].DispCategoryId = "";
	// レコメンド非対象にするカテゴリIDを設定します（複数選択時はカンマ区切りで指定）
	lProductRecommendByRecommendEngineUserControls[0].NotDispCategoryId = "";
	// レコメンド非対象にするアイテムIDを設定します（複数選択時はカンマ区切りで指定）
	lProductRecommendByRecommendEngineUserControls[0].NotDispRecommendProductId = "";
}
<%-- △編集可能領域△ --%>
}
</script>

<script type="text/javascript">
<!--
	// バリエーション選択チェック(定期)
	function add_cart_check_for_fixedpurchase_variationlist() {
		return confirm('6回以上のお受け取りが条件となる「定期購入」で購入します。\nよろしいですか？\n※定期購入は会員登録が必須となります。');
	}

	// 入荷通知登録画面をポップアップウィンドウで開く
	function show_arrival_mail_popup(pid, vid, amkbn) {
		show_popup_window('<%= this.SecurePageProtocolAndHost %><%= Constants.PATH_ROOT %><%= Constants.PAGE_FRONT_USER_PRODUCT_ARRIVAL_MAIL_REGIST %>?<%= Constants.REQUEST_KEY_PRODUCT_ID %>=' + pid + '&<%= Constants.REQUEST_KEY_VARIATION_ID %>=' + vid + '&<%= Constants.REQUEST_KEY_USERPRODUCTARRIVALMAIL_ARRIVAL_MAIL_KBN %>=' + amkbn, 520, 310, true, true, 'Information');
	}

	// マウスイベントの初期化
	addOnload(function () { init(); });
//-->
</script>

<%-- CRITEOタグ（引数：商品一覧情報） --%>
<uc:Criteo ID="criteo" runat="server" Datas="<%# this.ProductMasterList %>" />

</asp:Content>
