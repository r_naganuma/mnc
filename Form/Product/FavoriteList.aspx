﻿<%--
=========================================================================================================
  Module      : お気に入り一覧画面(FavoriteList.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="BodyProductVariationImages" Src="~/Form/Common/Product/BodyProductVariationImages.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductRecommendByRecommendEngine" Src="~/Form/Common/Product/BodyProductRecommendByRecommendEngine.ascx" %>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_Product_FavoriteList, App_Web_favoritelist.aspx.1e99e05" title="お気に入りページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" language="javascript">
	function bodyPageLoad() {
		// 検索結果数
		var total = $("span.total_counts").html();
		$(".leadTxt .all").html(total);
		var totalList = $("span.pagination_page").html();
		$(".leadTxt .list").html(totalList);
	}
</script>
<%-- △編集可能領域△ --%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					お気に入りリスト
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<h2>お気に入りリスト</h2>
	<div class="mypageBox">
		<div class="mypageBox_cts">
			<p class="leadTxt"><span class="all">0</span>件の商品がリストに入っています（<span class="list">0件表示</span>中）</p>
			<!-- ///// お気に入りリスト一覧 ///// -->
				<asp:Repeater id="rFavoriteList" runat="server">
					<HeaderTemplate>
						<div class="favoBox">
					</HeaderTemplate>
					<ItemTemplate>
						<div class="favoBox_list">
							<dl>
								<dt>
									<% if(Constants.LAYER_DISPLAY_VARIATION_IMAGES_ENABLED){ %>
									<uc:BodyProductVariationImages ImageSize="L" ProductMaster="<%# Container.DataItem %>" VariationList="<%# this.ProductVariationList %>" VariationNo="<%# Container.ItemIndex.ToString() %>" runat="server" />
									<% } else { %>
									<a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(Container.DataItem)) %>'>
									<w2c:ProductImage ImageSize="L" ProductMaster="<%# Container.DataItem %>" IsVariation="false" runat="server" /></a>
									<% } %>
								</dt>
								<dd>
									<a href="<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(Container.DataItem)) %>" class="itemName">
										<%# WebSanitizer.HtmlEncode(Eval(Constants.FIELD_PRODUCT_NAME)) %>
									</a>
								</dd>
							</dl>
							<div class="btnArea">
								<a href="<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailUrl(Container.DataItem)) %>" class="btnArea_detail">
									詳細・ご購入はこちら
								</a>
								<!-- ///// お気に入りから削除 ///// -->
								<asp:LinkButton id="lbDelete" Text="削除" class="btnArea_delete" CommandArgument="<%# Eval(Constants.FIELD_FAVORITE_PRODUCT_ID) %>" OnClientClick="return confirm('本当に削除してもよろしいですか？')" OnClick="lbDelete_Click" runat="server"></asp:LinkButton>
							</div>
						</div>
					</ItemTemplate>
					<FooterTemplate>
						</div>
					</FooterTemplate>
				</asp:Repeater>
			<%-- エラーメッセージ --%>
			<% if (StringUtility.ToEmpty(this.ErrorMessage) != ""){ %>
				<p><%= this.ErrorMessage %></p>
			<% } %>
	
			<!-- ///// ページャ ///// -->
			<div class="pagerArea" style="display: none;"><%= this.PagerHtml %></div>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					お気に入りリスト
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
</asp:Content>