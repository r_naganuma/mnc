﻿<%--
=========================================================================================================
  Module      : タグ出力(AffiliateTag.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2019 All Rights Reserved.
=========================================================================================================
--%>
<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_AffiliateTag, App_Web_affiliatetag.ascx.2af06a88" %>
<asp:Repeater ID="rAffiliateTag" runat="server">
	<ItemTemplate>
		<%# Container.DataItem %>
	</ItemTemplate>
</asp:Repeater>