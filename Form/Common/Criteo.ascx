﻿<%--
=========================================================================================================
  Module      : Criteoタグ出力コントローラ(Criteo.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2015 All Rights Reserved.
=========================================================================================================
--%>
<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_Criteo, App_Web_criteo.ascx.2af06a88" %>

<% if ((Constants.CRITEO_OPTION_ENABLED) && (Constants.SETTING_PRODUCTION_ENVIRONMENT)) { %>
<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
<script type="text/javascript">
window.criteo_q = window.criteo_q || [];
window.criteo_q.push(
	{ event: "setAccount", account: <%= this.Account %> },
	{ event: "setSiteType", type: "<%= this.SiteType %>" },
	{ event: "setHashedEmail", email: "<%= this.HashedEmail %>" },
	<%= this.MainTag %>
);
</script>
<%} %>
