﻿<%--
=========================================================================================================
  Module      : 商品レビュー出力コントローラ(BodyProductReview.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_Product_BodyProductReview, App_Web_bodyproductreview.ascx.1b7f36c0" %>
<%--
下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LastChanged="最終更新者" %>
--%>
<%-- UPDATE PANEL開始 --%>
<asp:UpdatePanel ID="upUpdatePanel" runat="server">
<ContentTemplate>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<div class="reviewArea">
	<h3 class="ttl">商品レビュー<span class="block">※いただいたレビューは個人の感想であり、効果を保証するものではありません。</span><span class="block">（レビュー<span class="num"><%=rProductReview.Items.Count%></span>件）</span></h3>

	<%--▽ 商品レビューコメント一覧 ▽--%>
	<div id="dvProductReviewComments" runat="server">
		<div class="pager"><%= this.PagerHtml %></div>
		<asp:Repeater id="rProductReview" runat="server">
			<HeaderTemplate>
			<div class="reviewWrap">
			</HeaderTemplate>
			<ItemTemplate>
				<ul class="reviewList">
					<li>
						<p class="reviewTtl"><%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_REVIEW_TITLE)) %></p>
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating<%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_REVIEW_RATING)) %>.gif" alt="<%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_REVIEW_RATING)) %>">
						<p class="reviewDate" style="display: none;">
							<%#: DateTimeUtility.ToStringFromRegion(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_DATE_OPENED), DateTimeUtility.FormatType.ShortDateHourMinuteSecond2Letter) %>
						</p>
						<p class="reviewName">
							<%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_NICK_NAME)) %>
						</p>
						<p class="reviewTxt">
							<%# WebSanitizer.HtmlEncodeChangeToBr(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_REVIEW_COMMENT)) %>
						</p>
					</li>
				</ul>
			</ItemTemplate>
			<FooterTemplate>
			</div>
			</FooterTemplate>
		</asp:Repeater>
		<div class="">
			<asp:LinkButton id="lbReviewDispPager" runat="server" OnClick="lbReviewDispPager_Click" class="allReview">
				すべての商品レビューを表示(全<%= this.TotalComment %>件)
			</asp:LinkButton>
		</div>
		<div id="pagination" class="below clearFix"><%= this.PagerHtml %></div>
	</div>
	<%--△ 商品レビューコメント一覧 △--%>

		<!--▽ 商品レビュー「コメントを書く」ボタン ▽-->
　　　　　　　　<div id="dvProductReviewButtonControls">
		<% if (this.IsLoggedIn == false) { %>
				<a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_LOGIN + "?" + Constants.REQUEST_KEY_NEXT_URL + "=" + HttpUtility.UrlEncode(this.RawUrl)) %>" class="reviewBtn">レビューを書く</a>
				<p style="margin-top: 10px;">レビューを書くためにはログインが必要です。</p>
		<% } else { %>
　　			<asp:linkButton id="lbDispCommentForm" OnClick="lbDispCommentForm_Click" runat="server" class="reviewBtn">レビューを書く</asp:linkButton>
		<% } %>
		</div>
		<!--△	商品レビュー「コメントを書く」ボタン △-->

	<!--▽ 商品レビュー記入フォーム ▽-->
	<div id="dvProductReviewInput" runat="server">
		<dl class="reviewInput">
			<dt>
				<%: ReplaceTag("@@User.nickname.name@@") %>
				<span class="icnKome">※</span>
			</dt>
			<dd>
				<asp:TextBox id="tbNickName" MaxLength="20" runat="server"></asp:TextBox>
				<p class="attention" style="margin-top: 5px;">
				<asp:CustomValidator runat="Server"
					ControlToValidate="tbNickName"
					ValidationGroup="ProductReview"
					ValidateEmptyText="true"
					SetFocusOnError="true"
					ClientValidationFunction="ClientValidate"
					CssClass="error_inline" />
				</p>
			</dd>
			<dt>
				評価
				<span class="icnKome">※</span>
			</dt>
			<dd>
				<asp:DropDownList ID="ddlReviewRating" runat="server"></asp:DropDownList><span class="note">星5つが最高、星1つが最低</span>
				<p class="attention" style="margin-top: 5px;">
				<asp:CustomValidator runat="Server"
					ControlToValidate="ddlReviewRating"
					ValidationGroup="ProductReview"
					ValidateEmptyText="true"
					SetFocusOnError="true"
					ClientValidationFunction="ClientValidate"
					CssClass="error_inline" />
				</p>
			</dd>
			<dt>タイトル<span class="icnKome">※</span></dt>
			<dd>
				<asp:TextBox id="tbReviewTitle" MaxLength="20" Columns="40" runat="server"></asp:TextBox>
				<p class="attention" style="margin-top: 5px;">
				<asp:CustomValidator runat="Server"
					ControlToValidate="tbReviewTitle"
					ValidationGroup="ProductReview"
					ValidateEmptyText="true"
					SetFocusOnError="true"
					ClientValidationFunction="ClientValidate"
					CssClass="error_inline" />
				</p>
			</dd>
			<dt>コメント<span class="icnKome">※</span></dt>
			<dd>
				<asp:TextBox id="tbReviewComment" TextMode="MultiLine" runat="server" Columns="30" Rows="5"></asp:TextBox>
				<p class="attention" style="margin-top: 5px;">
				<asp:CustomValidator runat="Server"
					ControlToValidate="tbReviewComment"
					ValidationGroup="ProductReview"
					ValidateEmptyText="true"
					SetFocusOnError="true"
					ClientValidationFunction="ClientValidate"
					CssClass="error_inline" />
				</p>
			</dd>
		</dl>
		<ul class="previewBtn">
			<li class="nextBtn previewConfirm">
				<asp:LinkButton ID="lbReviewConfirm" OnClick="lbReviewConfirm_Click" runat="server" ValidationGroup="ProductReview">
					プレビューを確認する
				</asp:LinkButton>
			</li>
			<li class="prevBtn">
				<asp:LinkButton ID="lbBackToDetail" OnClick="lbBackToDetail_Click" runat="server" >
					戻る
				</asp:LinkButton>
			</li>
		</ul>
	</div>
	<!--△ 商品レビュー記入フォーム △-->

	<!--▽ 商品レビュープレビュー ▽-->
	<div id="dvProductReviewConfirm" runat="server">
		<p class="spMessage">以下のように表示されます</p>
		<div class="reviewWrap">
			<ul class="reviewList">
				<li>
					<p class="reviewTtl"><%= this.ReviewTitle %></p>
					<span id="sIconImageRating1" visible="false" runat="server">
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating1.gif" alt="レート1" />
					</span>
					<span id="sIconImageRating2" visible="false" runat="server">
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating2.gif" alt="レート2" />
					</span>
					<span id="sIconImageRating3" visible="false" runat="server">
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating3.gif" alt="レート3" />
					</span>
					<span id="sIconImageRating4" visible="false" runat="server">
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating4.gif" alt="レート4" />
					</span>
					<span id="sIconImageRating5" visible="false" runat="server">
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating5.gif" alt="レート5" />
					</span>
					<p class="reviewName"><%= this.NickName %></p>
					<p class="reviewTxt"><%= this.ReviewComment %></p>
				</li>
			</ul>
		</div>
		<ul class="previewBtn">
			<li class="nextBtn previewConfirm">
				<asp:LinkButton ID="lbReviewRegist" OnClick="lbProductReviewRegist_Click" runat="server" >投稿する</asp:LinkButton>
			</li>
			<li class="prevBtn">
				<asp:LinkButton ID="lbBackToInput" OnClick="lbBackToInput_Click" runat="server" >戻る</asp:LinkButton>
			</li>
		</ul>
	</div>
	<!--△ 商品レビュープレビュー △-->
	
	<!--▽ 商品レビュー完了 ▽-->
	<div id="dvProductReviewComplete" runat="server">
		<p class="msg" style="line-height: 1.5em;">
			ご登録ありがとうございました。<br />
			投稿は管理者承認後反映されます。
		</p>
		<ul class="previewBtn">
			<li class="nextBtn">
				<asp:LinkButton ID="lbBack" OnClick="lbBack_Click" runat="server">レビュー一覧に戻る</asp:LinkButton>
			</li>
		</ul>
	</div>
	<!--△ 商品レビュー投稿完了 △-->

</div>
<%-- △編集可能領域△ --%>
</ContentTemplate>
</asp:UpdatePanel>
<%-- UPDATE PANELここまで --%>