﻿<%--
=========================================================================================================
  Module      : 商品レビュー出力コントローラ(BodyProductReview.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_Product_BodyProductReview, App_Web_bodyproductreview.ascx.acb385f3" %>

<%-- UPDATE PANEL開始 --%>
<asp:UpdatePanel ID="upUpdatePanel" runat="server">
<ContentTemplate>
<div class="reviewArea">
	<div id="dvProductReviewImage">
		<h3 class="ttl">商品レビュー<span>※いただいたレビューは個人の感想であり、効果を保証するものではありません。</span></h3>
	</div>

	<!--▽ 商品レビューコメント一覧 ▽-->
	<div id="dvProductReviewComments" runat="server">
		<asp:Repeater id="rProductReview" runat="server" >
			<HeaderTemplate>
			<div class="reviewWrap">
			</HeaderTemplate>
			<ItemTemplate>
				<ul class="reviewList">
					<li>
						<p class="reviewTtl"><%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_REVIEW_TITLE)) %></p>
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating<%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_REVIEW_RATING)) %>.gif" alt="<%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_REVIEW_RATING)) %>">
						<p class="reviewDate" style="display: none;">投稿日：<%#: DateTimeUtility.ToStringFromRegion(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_DATE_OPENED), DateTimeUtility.FormatType.ShortDateHourMinuteSecond2Letter) %></p>
						<p class="reviewName"><%# WebSanitizer.HtmlEncode(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_NICK_NAME)) %></p>
						<p class="reviewTxt"><%# WebSanitizer.HtmlEncodeChangeToBr(GetKeyValue(Container.DataItem, Constants.FIELD_PRODUCTREVIEW_REVIEW_COMMENT)) %></p>
					</li>
				</ul>
			</ItemTemplate>
			<FooterTemplate>
			</div>
			</FooterTemplate>
		</asp:Repeater>
		<asp:LinkButton id="lbReviewDispPager" runat="server" OnClick="lbReviewDispPager_Click" class="allReview">すべての商品レビューを表示(全<%= this.TotalComment %>件)</asp:LinkButton>
		<div id="pagination" class="below clearFix"><%= this.PagerHtml %></div>
	</div>
	<!--△ 商品レビューコメント一覧 △-->

	<!--▽ 商品レビュー「コメントを書く」ボタン ▽-->
	<div id="dvProductReviewButtonControls">
		<asp:linkButton id="lbDispCommentForm" OnClick="lbDispCommentForm_Click" runat="server" class="reviewBtn">
		レビューを書く</asp:linkButton>
	</div>
	<!--△	商品レビュー「コメントを書く」ボタン △-->

	<!--▽ 商品レビュー記入フォーム ▽-->
	<div id="dvProductReviewInput" runat="server">
		<dl class="reviewInput">
			<dt>
				<%: ReplaceTag("@@User.nickname.name@@") %>
				<span class="icnKome">※</span>
			</dt>
			<dd>
				<asp:TextBox id="tbNickName" MaxLength="20" runat="server"></asp:TextBox>
				<asp:CustomValidator runat="Server"
					ControlToValidate="tbNickName"
					ValidationGroup="ProductReview"
					ValidateEmptyText="true"
					SetFocusOnError="true"
					ClientValidationFunction="ClientValidate"
					CssClass="error_inline" />
			</dd>
			<dt>
				評価
				<span class="icnKome">※</span>
			</dt>
			<dd>
				<asp:DropDownList ID="ddlReviewRating" runat="server"></asp:DropDownList><span class="note">星5つが最高、星1つが最低</span>
				<asp:CustomValidator runat="Server"
					ControlToValidate="ddlReviewRating"
					ValidationGroup="ProductReview"
					ValidateEmptyText="true"
					SetFocusOnError="true"
					ClientValidationFunction="ClientValidate"
					CssClass="error_inline" />	
			</dd>
			<dt>タイトル<span class="icnKome">※</span></dt>
			<dd>
				<asp:TextBox id="tbReviewTitle" MaxLength="20" Columns="40" runat="server"></asp:TextBox>
				<asp:CustomValidator runat="Server"
					ControlToValidate="tbReviewTitle"
					ValidationGroup="ProductReview"
					ValidateEmptyText="true"
					SetFocusOnError="true"
					ClientValidationFunction="ClientValidate"
					CssClass="error_inline" />	
			</dd>
			<dt>コメント<span class="icnKome">※</span></dt>
			<dd>
				<asp:TextBox id="tbReviewComment" TextMode="MultiLine" runat="server" Columns="30" Rows="5"></asp:TextBox>
				<asp:CustomValidator runat="Server"
					ControlToValidate="tbReviewComment"
					ValidationGroup="ProductReview"
					ValidateEmptyText="true"
					SetFocusOnError="true"
					ClientValidationFunction="ClientValidate"
					CssClass="error_inline" />	
			</dd>
		</dl>
		<ul class="previewBtn">
			<li class="nextBtn">
				<asp:LinkButton ID="lbReviewConfirm" OnClick="lbReviewConfirm_Click" runat="server" ValidationGroup="ProductReview">プレビューを確認する</asp:LinkButton>
			</li>
			<li class="prevBtn">
				<asp:LinkButton ID="lbBackToDetail" OnClick="lbBackToDetail_Click" runat="server">戻る</asp:LinkButton>
			</li>
		</ul>
	</div>
	<!--△ 商品レビュー記入フォーム △-->

	<!--▽ 商品レビュープレビュー ▽-->
	<div id="dvProductReviewConfirm" runat="server">
		<p class="spMessage">このように表示されます</p>
		<div class="reviewWrap">
			<ul class="reviewList">
				<li>
					<p class="reviewTtl"><%= this.ReviewTitle %></p>
					<span id="sIconImageRating1" visible="false" runat="server">
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating1.gif" alt="レート1" />
					</span>
					<span id="sIconImageRating2" visible="false" runat="server">
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating2.gif" alt="レート2" />
					</span>
					<span id="sIconImageRating3" visible="false" runat="server">
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating3.gif" alt="レート3" />
					</span>
					<span id="sIconImageRating4" visible="false" runat="server">
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating4.gif" alt="レート4" />
					</span>
					<span id="sIconImageRating5" visible="false" runat="server">
						<img class="reviewStar" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/product/icon_review_rating5.gif" alt="レート5" />
					</span>
					<p class="reviewName"><%= this.NickName %></p>
					<p class="reviewTxt"><%= this.ReviewComment %></p>
				</li>
			</ul>
		</div>
		<ul class="previewBtn">
			<li class="nextBtn">
				<asp:LinkButton ID="lbReviewRegist" OnClick="lbProductReviewRegist_Click" runat="server">投稿する</asp:LinkButton>
			</li>
			<li class="prevBtn">
				<asp:LinkButton ID="lbBackToInput" OnClick="lbBackToInput_Click" runat="server">戻る</asp:LinkButton>
			</li>
		</ul>
	</div>
	<!--△ 商品レビュープレビュー △-->
	
	<!--▽ 商品レビュー完了 ▽-->
	<div id="dvProductReviewComplete" runat="server">
		ご登録ありがとうございました。<br />
		投稿は管理者承認後反映されます。<br />
		<br />
		<asp:LinkButton ID="lbBack" OnClick="lbBack_Click" runat="server">レビュー一覧に戻る</asp:LinkButton>
	</div>
	<!--△ 商品レビュー投稿完了 △-->
</div>
</ContentTemplate>
</asp:UpdatePanel>
<%-- UPDATE PANELここまで --%>

