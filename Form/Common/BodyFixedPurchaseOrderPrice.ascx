﻿<%--
=========================================================================================================
  Module      : 定期注文購入金額出力コントローラ(BodyFixedPurchaseOrderPrice.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2018 All Rights Reserved.
=========================================================================================================
--%>
<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_BodyFixedPurchaseOrderPrice, App_Web_bodyfixedpurchaseorderprice.ascx.2af06a88" %>
<script runat="server">
public new void Page_Init(Object sender, EventArgs e)
{
	<%-- ▽編集可能領域：プロパティ設定▽ --%>
	// 定期購入回数最大表示数(何回分の注文金額情報を表示させるか)
	this.DisplayMaxCount = 3;
	<%-- △編集可能領域△ --%>
}
</script>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<div runat="server" ID="divSubCartList">
<p class="paymentBox_ttl">次回以降の注文金額</p>
<div class="message">
	<p>次回以降の定期注文金額は、それぞれ以下のとおりになります。</p>
</div>
<div>
<asp:Repeater ID="rFixedPurchaseOrderPriceList" runat="server">
<ItemTemplate>
	<div class="cartTotal paymentColumnInner fixedArea">
		<dl class="sub">
			<dt><p><%# Container.ItemIndex + 2 %>回目の注文金額</p></dt>
		</dl>
		<div>
			<dl>
				<dt><p>小計（<%#: this.ProductPriceTextPrefix %>）</p></dt>
				<dd><p><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceSubtotal) %>円</p></dd>
			</dl>
			<%if (this.ProductIncludedTaxFlg == false) { %>
				<dl>
					<dt><p>消費税額</p></dt>
					<dd><p><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceSubtotalTax) %>円</p></dd>
				</dl>
			<%} %>
			<dl class="discountTxt">
				<dt><p>各種割引額</p></dt>
				<dd class='<%#: (((CartObject)Container.DataItem).DiscountPriceForFixedPurchase) > 0 ? "minus" : "" %>'><p><%#: (((CartObject)Container.DataItem).DiscountPriceForFixedPurchase) > 0 ? "-" : "" %><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).DiscountPriceForFixedPurchase) %>円</p></dd>
			</dl>
			<dl>
				<dt><p>配送料・決済手数料</p></dt>
				<dd><p><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceShipping + ((CartObject)Container.DataItem).Payment.PriceExchange) %>円</p></dd>
			</dl>
			<dl class="total">
				<dt><p>合計</p></dt>
				<dd><p><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceTotal)%>円</p></dd>
			</dl>
		</div>
	</div>
</ItemTemplate>
</asp:Repeater>
<div style="display: none;" class="block">
<h3 class="price"><%: this.DisplayMaxCount + 1 %>回購入した場合の総額</h3>
<div class="fixedPurchasePriceList">
	<dl class="result">
		<dt>合計（税込）</dt>
		<dd><%: CurrencyManager.ToPrice(this.SummaryOfFixedPurchasePriceTotal) %></dd>
	</dl>
</div>
</div>
<!-- <div class="message indent">
	<p>※各回の注文には、配送料金<%: CurrencyManager.ToPrice(this.FixedPurchasePriceShipping) %>、決済手数料<%: CurrencyManager.ToPrice(this.FixedPurchasePriceExchange) %>がかかります。</p>
	<p>※各種割引額には、セットプロモーション割引額、会員ランク割引額、定期購入割引額が含まれています。</p>
	<p>※各種金額は注文時点に適用されている金額のため、実際には異なることがあります。</p>
	<p>※この定期注文は出荷回数が<%# this.CancelableCount %>回以上から定期購入キャンセル、一時休止、スキップが可能になります。</p>
</div> -->
</div>
</div>
<%-- △編集可能領域：コンテンツ△ --%>
