﻿<%--
=========================================================================================================
  Module      : Ec Pay Script(EcPayScript.ascx)
  ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2020 All Rights Reserved.
=========================================================================================================
--%>
<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_Order_EcPay_EcPayScript, App_Web_ecpayscript.ascx.9fa3dab8" %>
<script type="text/javascript">
	function NextPageSelectReceivingStore(params) {
		var url = '<%= Constants.RECEIVINGSTORE_TWECPAY_APIURL + "Express/map" %>';
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("action", url);
		form.setAttribute("target", "");

		for (var item in params) {
			if (params.hasOwnProperty(item)) {
				var input = document.createElement('input');
				input.type = 'hidden';
				input.name = item;
				input.value = params[item];
				form.appendChild(input);
			}
		}
		document.body.appendChild(form);
		form.submit();
	}
</script>