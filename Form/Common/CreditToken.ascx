﻿<%--
=========================================================================================================
  Module      : クレジットTokenユーザーコントロール(CreditToken.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2016 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" AutoEventWireup="true" %>

<%if (OrderCommon.CreditTokenUse) {%>
<script type="text/javascript" charset="Shift_JIS" src="<%= Constants.PATH_ROOT %>Js/w2.credit.token.aspx?<%: Constants.QUERY_STRING_FOR_UPDATE_EXTERNAL_FILE_URLENCODED %>"></script>
<%} %>

<%-- ▼▼GMOトークン取得用のスクリプト▼▼ --%>
<%if (Constants.PAYMENT_CARD_KBN == w2.App.Common.Constants.PaymentCard.Gmo) { %>
<script type="text/javascript" charset="UTF-8" src="<%=Constants.PAYMENT_CREDIT_GMO_GETTOKEN_JS %>"></script>
<% } %>
<%-- ▲▲GMOトークン取得用のスクリプト▲▲ --%>
<%-- ▼▼SBPSトークン取得用のスクリプト▼▼ --%>
<%if (Constants.PAYMENT_CARD_KBN == w2.App.Common.Constants.PaymentCard.SBPS) { %>
<script type="text/javascript" charset="UTF-8" src="<%=Constants.PAYMENT_SETTING_SBPS_CREDIT_GETTOKEN_JS_URL %>"></script>
<% } %>
<%-- ▲▲SBPSトークン取得用のスクリプト▲▲ --%>
<%-- ▼▼ZEUSトークン取得用のスクリプト▼▼ --%>
<%if (Constants.PAYMENT_CARD_KBN == w2.App.Common.Constants.PaymentCard.Zeus) { %>
<script type="text/javascript" charset="UTF-8" src="<%= Constants.PATH_ROOT %>Js/CredtTokenZeus.js?<%: Constants.QUERY_STRING_FOR_UPDATE_EXTERNAL_FILE_URLENCODED %>"></script>
<% } %>
<%-- ▲▲ZEUSトークン取得用のスクリプト▲▲ --%>
<%-- ▼▼ヤマトKWCトークン取得用のスクリプト▼▼ --%>
<%if (Constants.PAYMENT_CARD_KBN == w2.App.Common.Constants.PaymentCard.YamatoKwc) { %>
<script type="text/javascript" charset="UTF-8" src="<%=Constants.PAYMENT_SETTING_YAMATO_KWC_CREDIT_GETTOKEN_JS_URL %>"></script>
<% } %>
<%-- ▲▲ヤマトKWCトークン取得用のスクリプト▲▲ --%>
