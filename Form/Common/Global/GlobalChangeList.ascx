﻿<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_Global_GlobalChangeList, App_Web_globalchangelist.ascx.d580736f" %>
<% if (Constants.GLOBAL_OPTION_ENABLE)
   { %>
<ul>
	<asp:Repeater ID="rRegionMenuList" runat="server" ItemType="RegionMenuViewModel">
		<ItemTemplate>
			<li>
				<a href="<%#: Item.Url %>">
					<img src="<%#: Item.Image %>"><p><%#: Item.SelectName %></p>
				</a>
			</li>
		</ItemTemplate>
	</asp:Repeater>
</ul>
<% } %>
