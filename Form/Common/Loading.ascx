﻿<%--
=========================================================================================================
  Module      : ローディングジェスチャー コントローラ(Loading.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2020 All Rights Reserved.
=========================================================================================================
--%>

<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_Loading, App_Web_loading.ascx.2af06a88" %>
<style>
	/* ローディング時バックグラウンド */
	.loadingBackground {
		position: fixed;
		width: 100%;
		height: 100%;
		top: 0;
		left: 0;
		background-color: #333;
		/* 背景に色を設定したい場合はopacityに0以上の値を設定してください。 */
		opacity: 0;
		cursor: wait;
		z-index: 100000;
	}

	/* ローディングアイコン */
	.loadingIcon{
		position: fixed;
		top: 45%;
		left: 45%;
		z-index: 100001;
		display: inline-block;
		width: 50px;
		height: 50px;
		background: #fff;
		text-align: center;
		line-height: 50px;
		border-radius: 10%;
		box-shadow: 0 0 8px gray;
	}

	/* ローディング画像 */
	.loadingImage{
		margin: 10px auto;
	}
</style>

<div id="loadingBackground" class="loadingBackground" style="display: none;"></div>
<div id="loadingIcon" class="loadingIcon" style="display: none;">
	<img id="loadingImage" class="loadingImage" src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/check_running.gif" alt="ローディング中"/>
</div>

<% if (this.UpdatePanelReload) { %>
<script type="text/javascript">
	Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(function (sender, args) {
		LoadingShow();
	});

	Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function (sender, args) {
		LoadingHide();
	});
</script>
<% } %>