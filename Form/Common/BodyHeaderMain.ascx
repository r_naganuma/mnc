﻿<%--
=========================================================================================================
  Module      : 共通ヘッダ出力コントローラ(BodyHeaderMain.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="BodyMiniCart" Src="~/Form/Common/BodyMiniCart.ascx" %>
<%@ Register TagPrefix="uc" TagName="GlobalChangeMenu" Src="~/Form/Common/Global/GlobalChangeMenu.ascx" %>
<%@ control language="c#" autoeventwireup="true" inherits="Form_Common_BodyHeaderMain, App_Web_bodyheadermain.ascx.2af06a88" %>
<%--

下記は保持用のダミー情報です。削除しないでください。
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<%
	// 検索テキストボックスEnterで検索させる（UpdatePanelで括っておかないと非同期処理時に検索が効かなくなる）
	this.WtbSearchWord.Attributes["onkeypress"] = "if (event.keyCode==13){__doPostBack('" + WlbSearch.UniqueID + "',''); return false;}";
%>
</ContentTemplate>
</asp:UpdatePanel>

<%-- ▽編集可能領域：共通ヘッダ領域▽ --%>
<script>
$(function(){    
    $("#searchBtnAreaSp").click(function () {
        $("#ctl00_BodyHeaderMain_tbSearchWord").attr("value", $("#ctl00_BodyHeaderMain_tbSearchWordSp").val());
        __doPostBack('ctl00$BodyHeaderMain$lbSearch', '');
    });
    $("#ctl00_BodyHeaderMain_tbSearchWordSp").keypress(function(e){
        if(e.which == 13){
            $("#searchBtnAreaSp").click();
        }
    });
});
</script>
<asp:UpdatePanel ID="upUpdatePanel2" runat="server">
<ContentTemplate>
<% this.Reload(); %>

<header>
    <div class="headerBnr">
        <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/renewal/") %>">
            <picture>
                <source media="(max-width: 769px)" srcset="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/head-bnr-sp.jpg">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/head-bnr.jpg" />
            </picture>
        </a>
    </div>
    <div class="headerCts">
        <div class="headerCts_inner">
            <div class="headerCts_main">
                <h1>
                    <a href="<%= WebSanitizer.HtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">
                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/logo.svg" alt="SIMPLISSE" />
                    </a>
                </h1>
                <ul>
                    <li class="menuList">
                        <a class="linkTxt" href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=bty&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                            BEAUTY
                        </a>
                        <div class="megaMenu">
                            <ul>
                                <li>
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/welcome/") %>">はじめての方へ</a>
                                </li>
                                <li>
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/beauty/") %>">BEAUTYについて</a>
                                </li>
                                <li>
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/regularly/") %>">定期便のご案内</a>
                                </li>
                            </ul>
                            <div class="megaMenu_item">
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btyskn&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btyskn.png" alt="スキンケア" />
                                        <p>SKIN CARE<span>スキンケア</span></p>
                                    </a>
                                </div>
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btyhai&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btyhai.png" alt="ヘアケア" />
                                        <p>HAIR CARE<span>ヘアケア</span></p>
                                    </a>
                                </div>
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btybod&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btybod.png" alt="ボディケア" />
                                        <p>BODY CARE<span>ボディケア</span></p>
                                    </a>
                                </div>
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btymen&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btymen.png" alt="メンズケア" />
                                        <p>MEN’S CARE<span>メンズケア</span></p>
                                    </a>
                                </div>
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btyoth&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btyoth.png" alt="その他" />
                                        <p>OTHER<span>その他</span></p>
                                    </a>
                                </div>
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btytry&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btytry.png" alt="トライアルキット" />
                                        <p>TRIAL KIT<span>トライアルキット</span></p>
                                    </a>
                                </div>
                            </div>
                            <a class="megaMenu_btn" href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=bty&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">BEAUTY すべての商品へ</a>
                        </div>
                    </li>
                    <li class="menuList">
                        <a class="linkTxt" href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=wel&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                            WELLNESS
                        </a>
                        <div class="megaMenu">
                            <ul>
                                <li>
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/welcome/") %>">はじめての方へ</a>
                                </li>
                                <li>
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/wellness/") %>">WELLNESSについて</a>
                                </li>
                                <li>
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/regularly/") %>">定期便のご案内</a>
                                </li>
                            </ul>
                            <div class="megaMenu_item">
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=welsup&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/welsup.png" alt="サプリメント" />
                                        <p>SUPPLEMENT<span>サプリメント</span></p>
                                    </a>
                                </div>
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=welfoo&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/welfoo.png" alt="フード&ドリンク" />
                                        <p>FOODS & DRINK<span>フード&ドリンク</span></p>
                                    </a>
                                </div>
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=welbrd&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/welbrd.png" alt="ボーダーズ" />
                                        <p>BORDERS<span>ボーダーズ</span></p>
                                    </a>
                                </div>
                            </div>
                            <a class="megaMenu_btn" href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=wel&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">WELLNESS すべての商品へ</a>
                        </div>
                    </li>
                    <li class="menuList">
                        <a class="linkTxt" href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=sen&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                            SENSUAL
                        </a>
                        <div class="megaMenu">
                            <ul>
                                <li>
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/welcome/") %>">はじめての方へ</a>
                                </li>
                                <li>
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/sensual/") %>">SENSUALについて</a>
                                </li>
                                <li>
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/regularly/") %>">定期便のご案内</a>
                                </li>
                            </ul>
                            <div class="megaMenu_item">
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001141&cat=sen") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/40000001141.png" alt="ネンマクケア" />
                                        <p>NEN-MAKU CARE<span>ネンマクケア</span></p>
                                    </a>
                                </div>
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001281&cat=sen") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/40000001281.png" alt="フェミニン デュオセラム" />
                                        <p class="lh">FEMININE DUO <br>SERUM<span>フェミニン <br>デュオセラム</span></p>
                                    </a>
                                </div>
                                <div class="megaMenu_item--list">
                                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001293&cat=sen") %>">
                                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/40000001293.png" alt="フェミニン デオウォッシュ" />
                                        <p class="lh">FEMININE DEO <br>WASH<span>フェミニン <br>デオウォッシュ</span></p>
                                    </a>
                                </div>
                            </div>
                            <a class="megaMenu_btn" href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=sen&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">SENSUAL すべての商品へ</a>
                        </div>
                    </li>
                    <li class="menuList">
                        <a class="linkTxt" href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=&dpcnt=100&img=1&sort=07&swrd=topsale&udns=0&fpfl=0&pno=1") %>">
                            SALE
                        </a>
                    </li>
                    <li class="menuList">
                        <a class="linkTxt" href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/otherBrands/") %>">
                            OTHER BRANDS
                        </a>
                    </li>
                    <li class="about menuList">
                        <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandConcept/") %>">
                            ABOUT
                        </a>
                    </li>
                    <li class="menuList">
                        <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "content/journals/?category=ALL") %>">
                            JOURNAL
                        </a>
                    </li>
                </ul>
            </div>
            <div class="headerCts_sub">
                <ul>
                    <li>
                        <%if (this.IsLoggedIn) { %>
                            <a href="<%: this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_MYPAGE %>">ようこそ <%= WebSanitizer.HtmlEncode(this.LoginUserName) %> 様
                                <% if (Constants.W2MP_POINT_OPTION_ENABLED) { %>
                                    <br />
                                    ポイント <%= WebSanitizer.HtmlEncode(GetNumeric(this.LoginUserPointUsable)) %>pt
                                <% } %>
                            </a>
                            <a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_LOGOFF) %>" onclick="return confirm('ログアウトします。\nよろしいですか？');return false;">ログアウト</a>
                        <% } %>
                    </li>
                    <li>
                        <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/welcome/") %>">
                            はじめての方へ
                        </a>
                    </li>
                    <li>
                        <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/faq/") %>">
                            よくあるご質問
                        </a>
                    </li>
                </ul>
                <div class="headerCts_sub--wrap">
                    <div class="spSearch">
                        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_search_black.svg" />
                    </div>
                    <div class="searchArea">
                        <div class="searchArea_txt">
                            <%
                                tbSearchWord.Attributes["placeholder"] = "商品名、キーワードで検索";
                            %>
                            <asp:TextBox ID="tbSearchWord" runat="server"></asp:TextBox>
                        </div>
                        <div class="searchArea_btn">
                            <asp:LinkButton ID="lbSearch" runat="server" OnClick="lbSearch_Click">
                                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_search.png" />
                            </asp:LinkButton>
                        </div>
                    </div>
                    <div class="heartArea">
                        <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_FAVORITE_LIST) %>">
                            <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_heart.svg" />
                        </a>
                    </div>
                    <div class="myArea">
                        <%if (this.IsLoggedIn) { %>
                            <a href="<%: this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_MYPAGE %>">
                                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_my.svg?31124" />
                                <p>マイページ</p>
                            </a>
                        <% }else{ %>
                            <a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_LOGIN + "?" + Constants.REQUEST_KEY_NEXT_URL + "=" + HttpUtility.UrlEncode(this.NextUrl)) %>">
                                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_my.svg?31124" />
                                <p>登録 / ログイン</p>
                            </a>
                        <% } %>
                    </div>
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_CART_LIST) %>" class="bagArea">
                        <p>
                            BAG
                        </p>
                        <div class="bagArea_num">
                            <p><%= WebSanitizer.HtmlEncode(GetNumeric(this.ProductCount))%></p>
                        </div>
                    </a>
                    <div class="spLine">
                        <span class="line line01"></span>
                        <span class="line line02"></span>
                        <span class="line line03"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="headerInner">
        <div class="headerInner_close">
            <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/close.svg" />
        </div>
        <div class="headerInner_inner">
            <div class="headerInner_login">
                <%if (this.IsLoggedIn) { %>
                    <a href="<%: this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_MYPAGE %>"><%= WebSanitizer.HtmlEncode(this.LoginUserName) %> 様のマイページ
                        <% if (Constants.W2MP_POINT_OPTION_ENABLED) { %>
                            <br />
                            ポイント <%= WebSanitizer.HtmlEncode(GetNumeric(this.LoginUserPointUsable)) %>pt
                        <% } %>
                    </a>
                    <a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_LOGOFF) %>" onclick="return confirm('ログアウトします。\nよろしいですか？');return false;">ログアウト</a>
                <% }else{ %>
                    <a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_LOGIN + "?" + Constants.REQUEST_KEY_NEXT_URL + "=" + HttpUtility.UrlEncode(this.NextUrl)) %>">メンバー登録 / ログイン</a>
                <% } %>
            </div>
            <ul class="headerInner_about">
                <li class="headerInner_about--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandConcept/") %>">
                        ABOUT
                    </a>
                </li>
            </ul>
            <p class="headerInner_pdc">PRODUCTS</p>
            <ul class="headerInner_cat">
                <li class="headerInner_cat--list">
                    <p>BEAUTY</p>
                    <ul>
                        <li>
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=bty&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                ビューティー商品一覧
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btyskn&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                スキンケア
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btyhai&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                ヘアケア
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btybod&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                ボディケア・フェミニンケア
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btymen&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                メンズケア
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btytry&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                トライアル
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btyoth&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                その他
                            </a>
                        </li>
                        <li>
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/welcome/") %>">
                                はじめての方へ
                            </a>
                        </li>
                        <li>
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/beauty/") %>">
                                BEAUTYについて
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="headerInner_cat--list">
                    <p>WELLNESS</p>
                    <ul>
                        <li>
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=wel&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                ウェルネス商品一覧
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=welsup&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                サプリメント
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=welfoo&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                フーズ&ドリンク
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=weloth&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                その他
                            </a>
                        </li>
                        <li>
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/wellness/") %>">
                                WELLNESSについて
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="headerInner_cat--list">
                    <p>SENSUAL</p>
                    <ul>
                        <li>
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=sen&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                センシュアル商品一覧
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001141&cat=sen") %>">
                                ネンマク ケア
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001281&cat=sen") %>">
                                フェミニン デュオセラム
                            </a>
                        </li>
                        <li class="indent">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001293&cat=sen") %>">
                                フェミニン デオウォッシュ
                            </a>
                        </li>
                        <li>
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/sensual/") %>">
                                SENSUALについて
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="headerInner_cat--list">
                    <p>OTHER BRANDS</p>
                    <ul>
                        <li>
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=othbea&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                Bé-A / ベア
                            </a>
                        </li>
                        <li>
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=othdam&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                DAMAI / ダマイ
                            </a>
                        </li>
                        <li>
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=othsol&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                                solace / ソラーチェ
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="headerInner_blue">
                <li class="headerInner_blue--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=&dpcnt=100&img=1&sort=07&swrd=topsale&udns=0&fpfl=0&pno=1") %>">
                        SALE
                    </a>
                </li>
                <li class="headerInner_blue--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "content/journals/?category=ALL") %>">
                        JOURNAL
                    </a>
                </li>
            </ul>
            <ul class="headerInner_link">
                <li class="headerInner_link--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/benefits/") %>">
                        会員特典のご案内
                    </a>
                </li>
                <li class="headerInner_link--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/regularly/") %>">
                        定期便のご案内
                    </a>
                </li>
                <li class="headerInner_link--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/welcome/") %>">
                        はじめての方へ
                    </a>
                </li>
                <li class="headerInner_link--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/") %>">
                        ショッピングガイド
                    </a>
                </li>
                <li class="headerInner_link--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/faq/") %>">
                        よくあるご質問
                    </a>
                </li>
                <li class="headerInner_link--list">
                    <a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_INQUIRY_INPUT) %>">
                        お問い合わせ
                    </a>
                </li>
                <li class="headerInner_link--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/shoplist/") %>">
                        取り扱い店舗
                    </a>
                </li>
                <!-- <li class="headerInner_link--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/company/") %>">
                        企業情報
                    </a>
                </li>
                <li class="headerInner_link--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/law/") %>">
                        特定商取引法に基づく表記
                    </a>
                </li>
                <li class="headerInner_link--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/terms/") %>">
                        ご利用規約
                    </a>
                </li>
                <li class="headerInner_link--list">
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/policy/") %>">
                        プライバシーポリシー
                    </a>
                </li> -->
            </ul>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/regularly/") %>" class="headerInner_bnr">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/bnr_spMenu.jpg" />
            </a>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_MAILMAGAZINE_REGIST_INPUT) %>" class="headerInner_mag">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/envelope.svg" />
                <p>メールマガジン登録はこちらから</p>
            </a>
            <div class="headerInner_sns">
                <div class="snsBox_list ig">
                    <a href="https://www.instagram.com/simplisse_official/?hl=ja" target="_blank"><img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/instagram.svg" /></a>
                </div>
                <div class="snsBox_list tw">
                    <a href="https://twitter.com/mncny" target="_blank"><img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/twitter.svg" /></a>
                </div>
                <div class="snsBox_list fb">
                    <a href="https://www.facebook.com/simplisse.jp/" target="_blank"><img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/facebook.svg" /></a>
                </div>
            </div>
        </div>
    </div>

    <div class="searchSp">
        <p>商品名、キーワードで検索</p>
        <div class="searchSp_input">
            <input name="ctl00$BodyHeaderMain$tbSearchWord2" type="text" maxlength="168" id="ctl00_BodyHeaderMain_tbSearchWordSp">
            <div class="searchSp_btn">
                <a id="searchBtnAreaSp">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_search.png" />
                </a>
            </div>
        </div>
    </div>
</header>

</ContentTemplate>
</asp:UpdatePanel>
<%-- △編集可能領域△ --%>

<%--
下記はファイル情報保持用のダミーです。削除しないでください。
<%@ FileInfo LastChanged="ｗ２ユーザー" %>
--%>