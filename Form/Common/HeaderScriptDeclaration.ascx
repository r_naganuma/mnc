﻿<%@ Control Language="C#" ClassName="HeaderScriptDeclaration" Inherits="BaseUserControl" %>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-M9WBSZG');</script>
	<!-- End Google Tag Manager -->
	
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-21820326-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-21820326-1');
</script>

<%-- w2標準js --%>
<script type="text/javascript" charset="Shift_JIS" src="<%= Constants.PATH_ROOT %>Js/w2.js?<%: Constants.QUERY_STRING_FOR_UPDATE_EXTERNAL_FILE_URLENCODED %>"></script>
<script type="text/javascript" charset="Shift_JIS" src="<%= Constants.PATH_ROOT %>Js/w2.textboxevents.js"></script>
<%-- jQuery --%>
<script type="text/javascript" charset="Shift_JIS" src="<%= Constants.PATH_ROOT %>Js/jquery-1.11.1.min.js"></script>
<%-- etc --%>
<script type="text/javascript" charset="Shift_JIS" src="<%= Constants.PATH_ROOT %>Js/AC_ActiveX.js"></script>
<script type="text/javascript" charset="Shift_JIS" src="<%= Constants.PATH_ROOT %>Js/AC_RunActiveContent.js"></script>
<%-- slick --%>
<script type="text/javascript" src="<%= Constants.PATH_ROOT %>Js/slick.min.js"></script>
<%-- 追加 --%>
<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/6485914/6168352/css/fonts.css" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<script type="text/javascript" src="//typesquare.com/3/tsst/script/ja/typesquare.js?5cbdc2165410404182bf22b0e90393a3" charset="utf-8"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/yakuhanjp@3.3.1/dist/css/yakuhanjp.min.css">
<script type="text/javascript" src="<%= Constants.PATH_ROOT %>Js/script.js"></script>
<script type="text/javascript" src="<%= Constants.PATH_ROOT %>Js/jquery.inview.min.js"></script>


<meta name="msapplication-square70x70logo" content="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/site-tile-70x70.png">
<meta name="msapplication-square150x150logo" content="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/site-tile-150x150.png">
<meta name="msapplication-wide310x150logo" content="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/site-tile-310x150.png">
<meta name="msapplication-square310x310logo" content="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/site-tile-310x310.png">
<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/favicon.ico">
<link rel="icon" type="image/vnd.microsoft.icon" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/favicon.ico">
<link rel="apple-touch-icon" sizes="57x57" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" sizes="36x36" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/android-chrome-36x36.png">
<link rel="icon" type="image/png" sizes="48x48" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/android-chrome-48x48.png">
<link rel="icon" type="image/png" sizes="72x72" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/android-chrome-72x72.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/android-chrome-96x96.png">
<link rel="icon" type="image/png" sizes="128x128" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/android-chrome-128x128.png">
<link rel="icon" type="image/png" sizes="144x144" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/android-chrome-144x144.png">
<link rel="icon" type="image/png" sizes="152x152" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/android-chrome-152x152.png">
<link rel="icon" type="image/png" sizes="192x192" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/android-chrome-192x192.png">
<link rel="icon" type="image/png" sizes="256x256" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/android-chrome-256x256.png">
<link rel="icon" type="image/png" sizes="384x384" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/android-chrome-384x384.png">
<link rel="icon" type="image/png" sizes="512x512" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/android-chrome-512x512.png">
<link rel="icon" type="image/png" sizes="36x36" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-36x36.png">
<link rel="icon" type="image/png" sizes="48x48" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-48x48.png">
<link rel="icon" type="image/png" sizes="72x72" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-72x72.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-96x96.png">
<link rel="icon" type="image/png" sizes="128x128" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-128x128.png">
<link rel="icon" type="image/png" sizes="144x144" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-144x144.png">
<link rel="icon" type="image/png" sizes="152x152" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-152x152.png">
<link rel="icon" type="image/png" sizes="160x160" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-160x160.png">
<link rel="icon" type="image/png" sizes="192x192" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-192x192.png">
<link rel="icon" type="image/png" sizes="196x196" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-196x196.png">
<link rel="icon" type="image/png" sizes="256x256" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-256x256.png">
<link rel="icon" type="image/png" sizes="384x384" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-384x384.png">
<link rel="icon" type="image/png" sizes="512x512" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-512x512.png">
<link rel="icon" type="image/png" sizes="16x16" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-16x16.png">
<link rel="icon" type="image/png" sizes="24x24" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-24x24.png">
<link rel="icon" type="image/png" sizes="32x32" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/icon-32x32.png">
<link rel="manifest" href="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/favicons/manifest.json">

<%-- クライアント検証用モジュール --%>
<script type="text/javascript" charset="UTF-8" src="<%= Constants.PATH_ROOT %><%= Constants.PAGE_FRONT_VALIDATE_SCRIPT %>"></script>

<script type="text/javascript" charset="UTF-8" src="<%= Constants.PATH_ROOT %>Js/jquery.bxslider.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="<%= Constants.PATH_ROOT %>Js/jquery.biggerlink.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="<%= Constants.PATH_ROOT %>Js/jquery.heightLine.js"></script>
<script type="text/javascript" charset="UTF-8" src="<%= Constants.PATH_ROOT %>Js/jquery.mousewheel.js"></script>
<script type="text/javascript" charset="UTF-8" src="<%= Constants.PATH_ROOT %>Js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="<%= Constants.PATH_ROOT %>Js/jquery.ah-placeholder.js"></script>

<script type="text/javascript" charset="UTF-8" src="<%= Constants.PATH_ROOT %>Js/function.js?<%: Constants.QUERY_STRING_FOR_UPDATE_EXTERNAL_FILE_URLENCODED %>"></script>

<script type="text/javascript" src="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Js/AutoKana/jquery.autoKana.js") %>"></script>
<script type="text/javascript">
	<%-- 氏名（姓・名）の自動振り仮名変換を実行する --%>
	function execAutoKanaWithKanaType(firstName, firstNameKana, lastName, lastNameKana) {
		execAutoKana(
			firstName,
			firstNameKana,
			lastName,
			lastNameKana,
			'<%: ReplaceTag("@@User.name_kana.type@@") %>');
	}
</script>
<%-- 検索結果レイヤー用スタイル --%>
<link id="lsearchResultLayerCss" rel="stylesheet" href="<%= Constants.PATH_ROOT %>Css/searchResultLayer.css" type="text/css" media="screen" />

<%if (SmartPhoneUtility.CheckSmartPhoneSite(Request.Path) == false) { %>
<%-- SmartPhoneでない場合 --%>
<%} else {%>
<%-- SmartPhoneの場合 --%>
	<%-- iUi --%>
	<link rel="apple-touch-icon" href="<%: SmartPhoneUtility.GetSmartPhoneContentsUrl("Contents/iui/iui-logo-touch-icon.png") %>" />
	<meta name="apple-touch-fullscreen" content="YES" />
	<!--script type="application/x-javascript" src="<%: SmartPhoneUtility.GetSmartPhoneContentsUrl("Contents/iui/iui.js") %>"></script-->
	<link id="liuiCss" rel="stylesheet" href="<%: SmartPhoneUtility.GetSmartPhoneContentsUrl("Contents/iui/iui.css") %>" type="text/css" media="screen" />
	<link id="lspCss" rel="stylesheet" href="<%: SmartPhoneUtility.GetSmartPhoneContentsUrl("Css/sp.css") %>" type="text/css" media="screen" />
<%} %>

<script type="text/javascript">
<!--
	// ページロード処理
	function pageLoad(sender, args) {
		// ページロード共通処理
		pageLoad_common();

		// function.js実行
		if (typeof initializeFunctionJs == "function") initializeFunctionJs();

		// グローバル切り替え実行
		if (typeof switchGlobalFunction == 'function') switchGlobalFunction();

		// PayPal
		if (typeof InitializePaypal == "function") InitializePaypal(sender, args);

		// body側のpageLoad実行
		if (typeof bodyPageLoad == "function") bodyPageLoad();
	}
//-->
</script>