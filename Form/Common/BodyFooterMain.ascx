﻿<%--
=========================================================================================================
  Module      : 共通フッタ出力コントローラ(BodyFooterMain.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="AccessLogTrackerScript" Src="~/Form/Common/AccessLogTrackerScript.ascx" %>
<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_BodyFooterMain, App_Web_bodyfootermain.ascx.2af06a88" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LastChanged="最終更新者" %>

--%>
<%-- ▽編集可能領域：フッタ領域▽ --%>
<footer>
	<div class="footGuide">
		<div class="footGuide_list">
			<div class="footGuide_list--pic pic02">
				<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/box.svg" />
			</div>
			<div class="footGuide_list--txt">
				<p class="guideTtl">発送・送料</p>
				<p class="guideTxt">
					ご注文した商品は、佐川急便にて配送、<br>
					送料全国一律880円。10,000円(税込)以上で送料無料。
				</p>
			</div>
		</div>
		<div class="footGuide_list">
			<div class="footGuide_list--pic">
				<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/help-button.svg" />
			</div>
			<div class="footGuide_list--txt">
				<p class="guideTtl">ショッピングガイド</p>
				<p class="guideTxt">
					お困りごとは<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/") %>">こちら</a>をご確認の上、<br>
					ショッピングをお楽しみください。
				</p>
			</div>
		</div>
		<div class="footGuide_list">
			<div class="footGuide_list--pic">
				<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/recycling-water.svg" />
			</div>
			<div class="footGuide_list--txt">
				<p class="guideTtl">定期便</p>
				<p class="guideTxt">
					お好みの商品を一度のご注文で、定期的にお届けするサービスです。お得な特別価格＆特典をご用意。
				</p>
			</div>
		</div>
	</div>

	<div class="footMain">
		<div class="footMain_inner">
			<div class="footMain_inner--menu">
				<ul>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=bty&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">BEAUTY</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=wel&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">WELLNESS</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=sen&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">SENSUAL</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/otherBrands/") %>">OTHER BRANDS</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/regularly/") %>">定期便のご案内</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/shoplist/") %>">取り扱い店舗</a>
					</li>
				</ul>
				<ul>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandConcept/") %>">ABOUT</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/") %>">ショッピングガイド</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/benefits/") %>">会員特典のご案内</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/wrapping/") %>">ラッピングについて</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/faq/") %>">よくある質問</a>
					</li>
				</ul>
				<ul>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/companyprofile/") %>">企業情報</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/termsofuse/") %>">特定商取引法に基づく表記</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/agreement/") %>">ご利用規約</a>
					</li>
					<li>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/privacy/") %>">プライバシーポリシー</a>
					</li>
				</ul>
				<ul class="sns">
					<li class="ig">
						<a href="https://www.instagram.com/simplisse_official/?hl=ja" target="_blank">INSTAGRAM</a>
					</li>
					<li class="tw">
						<a href="https://twitter.com/mncny" target="_blank">TWITTER</a>
					</li>
					<li class="fb">
						<a href="https://www.facebook.com/simplisse.jp/" target="_blank">FACEBOOK</a>
					</li>
				</ul>
			</div>
			<div class="footMain_inner--contact">
				<div class="contactBox">
					<p class="contactBox_ttl">お電話でのご注文・お問い合わせはこちらから</p>
					<div class="contactBox_num">
						<div class="contactBox_num--pic">
							<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/smartphone.svg" />
						</div>
						<div class="contactBox_num--txt">
							<p class="num">0120-370-063<span>(通話無料)</span></p>
							<p class="time">10:00-12:00 / 13:30-16:00 土日・祝日は休業</p>
						</div>
					</div>
				</div>
				<div class="magBox mrgB0">
					<a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_INQUIRY_INPUT) %>" class="magBox_btn">
						<p>お問い合わせ</p>
					</a>
				</div>
				<div class="magBox">
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_MAILMAGAZINE_REGIST_INPUT) %>" class="magBox_btn">
						<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/envelope.svg" />
						<p>メールマガジン登録はこちらから</p>
					</a>
					<p class="magBox_txt">
						新商品の入荷やお得なセール情報など盛りだくさん。<br>
						会員限定情報などお得な情報をお届け。
					</p>
				</div>
				<div class="snsBox">
					<div class="snsBox_list ig">
						<a href="https://www.instagram.com/simplisse_official/?hl=ja" target="_blank"><img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/instagram.svg" /></a>
					</div>
					<div class="snsBox_list tw">
						<a href="https://twitter.com/mncny" target="_blank"><img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/twitter.svg" /></a>
					</div>
					<div class="snsBox_list fb">
						<a href="https://www.facebook.com/simplisse.jp/" target="_blank"><img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/facebook.svg" /></a>
					</div>
				</div>
				<div class="payBox">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/card.png" />
				</div>
			</div>
			<ul class="footMain_inner--link">
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/companyprofile/") %>">企業情報</a>
				</li>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/termsofuse/") %>">特定商取引法に基づく表記</a>
				</li>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/agreement/") %>">ご利用規約</a>
				</li>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/privacy/") %>">プライバシーポリシー</a>
				</li>
			</ul>
			<p class="footMain_inner--copy">Copyright©MNC New York Inc. All Rights Reserved.</p>
		</div>
	</div>

<%if (this.IsSmartPhone 
		&& SmartPhoneUtility.GetSmartPhoneUrl(
								Request.AppRelativeCurrentExecutionFilePath,
								Request.UserAgent,
								HttpContext.Current) != null)
{%>
<%} %>
</footer>
<%-- △編集可能領域△ --%>

<%-- w2アクセスログトラッカー出力 --%>
<uc:AccessLogTrackerScript id="AccessLogTrackerScript1" runat="server" />