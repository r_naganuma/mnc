﻿<%--
=========================================================================================================
  Module      : 注文履歴詳細画面(OrderHistoryDetail.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_Order_OrderHistoryDetail, App_Web_orderhistorydetail.aspx.a8f71c32" title="購入履歴詳細ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<%@ Import Namespace="System.Runtime.CompilerServices" %>
<%@ Import Namespace="w2.App.Common.Order.Payment.Paidy" %>
<%@ Import Namespace="w2.Domain.UserShipping" %>
<%-- ▼削除禁止：クレジットカードTokenコントロール▼ --%>
<%@ Register TagPrefix="uc" TagName="CreditToken" Src="~/Form/Common/CreditToken.ascx" %>
<%-- ▲削除禁止：クレジットカードTokenコントロール▲ --%>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ Register TagPrefix="uc" TagName="Layer" Src="~/Form/Common/Layer/SearchResultLayer.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaymentDescriptionCvsDef" Src="~/Form/Common/Order/PaymentDescriptionCvsDef.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaymentDescriptionPayPal" Src="~/Form/Common/Order/PaymentDescriptionPayPal.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaypalScriptsForm" Src="~/Form/Common/PayPalScriptsForm.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaymentDescriptionTriLinkAfterPay" Src="~/Form/Common/Order/PaymentDescriptionTriLinkAfterPay.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaidyCheckoutScript" Src="~/Form/Common/Order/PaidyCheckoutScript.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaidyCheckoutControl" Src="~/Form/Common/Order/PaidyCheckoutControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="AtonePaymentScript" Src="~/Form/Common/AtonePaymentScript.ascx" %>
<%@ Register TagPrefix="uc" TagName="AfteePaymentScript" Src="~/Form/Common/AfteePaymentScript.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaymentDescriptionAtone" Src="~/Form/Common/Order/PaymentDescriptionAtone.ascx" %>
<%@ Register TagPrefix="uc" TagName="LinePaymentScript" Src="~/Form/Common/LinePaymentScript.ascx" %>
<%@ Register TagPrefix="uc" TagName="PaymentDescriptionNPAfterPay" Src="~/Form/Common/Order/PaymentDescriptionNPAfterPay.ascx" %>
<%@ Register TagPrefix="uc" TagName="EcPayScript" Src="~/Form/Common/ECPay/EcPayScript.ascx" %>
<%@ Import Namespace="w2.Domain.User.Helper" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<% if(Constants.PAYMENT_ATONEOPTION_ENABLED) { %>
<asp:HiddenField runat="server" ID="hfAtoneToken"/>
<asp:HiddenField ID="hfAtoneTransactionId" runat="server" />
<% } %>
<% if (Constants.PAYMENT_AFTEEOPTION_ENABLED) { %>
<asp:HiddenField runat="server" ID="hfAfteeToken"/>
<asp:HiddenField ID="hfAfteeTransactionId" runat="server" />
<% } %>
<% if (Constants.RECEIVINGSTORE_TWECPAY_CVSOPTION_ENABLED) { %>
<uc:EcPayScript runat="server" ID="ucECPayScript" />
<% } %>
<%-- UpdatePanel開始 --%>
<asp:UpdatePanel ID="upUpdatePanel" UpdateMode="Conditional" runat="server">
<ContentTemplate>
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />

<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					購入履歴詳細
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<h2>購入履歴詳細</h2>

	<div class="mypageBox">
		<div class="mypageBox_cts">
			
			<%-- 注文情報 --%>
			<div class="ohDetail">
				<h3>ご注文情報</h3>
				<dl class="ohDetail_box">
						<dt>ご注文番号</dt>
						<dd>
							<%#: this.OrderModel.OrderId %>
						</dd>
					<% if (this.IsFixedPurchase){ %>
						<dt>定期購入ID</dt>
						<dd>
							<%#: this.OrderModel.FixedPurchaseId %>
							<div style="display: none;" class="changeBtn">
								<asp:LinkButton Text="次回以降の注文変更" runat="server" OnClick="lbDisplayFixedPurchaseDetail_Click" />
							</div>
						</dd>
					<%}%>
						<dt>購入日</dt>
						<dd>
							<%#: DateTimeUtility.ToStringFromRegion(this.OrderModel.OrderDate, DateTimeUtility.FormatType.ShortDate2Letter) %></dd>
						<dt>ご注文状況</dt>
						<dd>
							<%#: ValueText.GetValueText(Constants.TABLE_ORDER, Constants.FIELD_ORDER_ORDER_STATUS, this.OrderModel.OrderStatus) %><%#: this.OrderModel.ShippedChangedKbn == Constants.FLG_ORDER_SHIPPED_CHANGED_KBN_CHANAGED ? "（変更有り）" : "" %></dd>
						<dt>ご入金状況</dt>
						<dd>
							<%#: ValueText.GetValueText(Constants.TABLE_ORDER, Constants.FIELD_ORDER_ORDER_PAYMENT_STATUS, this.OrderModel.OrderPaymentStatus) %></dd>
						<dt>お支払い方法</dt>
						<dd>
							<%#: this.PaymentModel.PaymentName %>
							<% if ((this.OrderModel.OrderPaymentKbn == Constants.FLG_PAYMENT_PAYMENT_ID_CREDIT) && (string.IsNullOrEmpty(this.OrderModel.CardInstruments) == false)) { %>
								(<%: this.OrderModel.CardInstruments %>)
							<% } %>
							<% if (this.OrderModel.OrderPaymentKbn == Constants.FLG_PAYMENT_PAYMENT_ID_AMAZON_PAYMENT) { %>
								<div style="margin: 10px 0;">
									<small>※現在のAmazon Payでの配送先情報、お支払い方法を表示しています。</small>
								</div>
								<iframe id="AmazonDetailWidget" src="<%: PageUrlCreatorUtility.CreateAmazonPayWidgetUrl(true, orderId: this.OrderModel.OrderId) %>" style="width:100%;border:none;"></iframe>
							<% } %>
							<div style="display: none;" class="changeBtn">
								<asp:LinkButton ID="lbDisplayInputOrderPaymentKbn" Text="お支払い方法変更" runat="server" OnClick="lbDisplayInputOrderPaymentKbn_Click"
									Enabled="<%# this.IsDisplayInputOrderPaymentKbn %>" AutoPostBack="true" />
							</div>
							<div>
								<%#: this.ExplanationOrderPaymentKbn %>
							</div>
							<%-- ▼PayPalログインここから▼ --%>
							<div style="display: none">
								<%if (Constants.PAYPAL_LOGINPAYMENT_ENABLED) {%>
									<%
										ucPaypalScriptsForm.LogoDesign = "Payment";
										ucPaypalScriptsForm.AuthCompleteActionControl = lbPayPalAuthComplete;
									%>
									<uc:PaypalScriptsForm ID="ucPaypalScriptsForm" runat="server" />
									<br /><asp:LinkButton ID="lbPayPalAuthComplete" runat="server" OnClick="lbPayPalAuthComplete_Click"></asp:LinkButton>
								<%} %>
							</div>
							<%-- ▲PayPalログインここまで▲ --%>
						</dd>

						<asp:HiddenField ID="hfPaidyTokenId" runat="server" />
						<asp:HiddenField ID="hfPaidyPaySelected" runat="server" />
						<div id="dvOrderPaymentPattern" class="creditWrap" Visible="False" runat="server">
								<div>
									<div>
										<div>
											<span style="color:red" runat="server" visible="<%# (string.IsNullOrEmpty(StringUtility.ToEmpty(this.DispLimitedPaymentMessages[0])) == false) %>">
												<%# StringUtility.ToEmpty(this.DispLimitedPaymentMessages[0]) %><br/>
											</span>
										<dl class="cartInput">
											<asp:Repeater ID="rPayment" DataSource="<%# this.ValidPayments[0] %>" ItemType="w2.Domain.Payment.PaymentModel" runat="server" >
											<ItemTemplate>
												<asp:HiddenField ID="hfPaymentId" Value='<%# Item.PaymentId %>' runat="server" />
												<asp:HiddenField ID="hfPaymentName" Value='<%# Item.PaymentName %>' runat="server" />
												<asp:HiddenField ID="hfPaymentPrice" Value="<%# OrderCommon.GetPaymentPrice(Item.ShopId, Item.PaymentId, this.OrderModel.OrderPriceSubtotal, OrderCommon.GetPriceCartTotalWithoutPaymentPrice(this.OrderModel), this.OrderModel.IsContainPaymentChargeFreeSet) %>" runat="server" />
												<dt><w2c:RadioButtonGroup ID="rbgPayment" GroupName='Payment' Checked="<%# this.OrderModel.OrderPaymentKbn == Item.PaymentId %>" Text="<%#: Item.PaymentName %>" OnCheckedChanged="rbgPayment_OnCheckedChanged" AutoPostBack="true" CssClass="radioBtn" runat="server" /></dt>

												<%-- クレジット --%>
												<dd id="ddCredit" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_CREDIT) %>" runat="server">
													<div class="creditPic">
														<img src="../../Contents/ImagesPkg/cart/credit.png" alt="クレジットカード">
													</div>
													<asp:DropDownList style="display: none;" ID="ddlUserCreditCard" runat="server" AutoPostBack="true" DataTextField="text" DataValueField="value" OnSelectedIndexChanged="ddlUserCreditCard_OnSelectedIndexChanged"></asp:DropDownList>
												
													<%-- ▽新規カード▽ --%>
													<% if (IsNewCreditCard()){ %>
													
													<% if (this.IsCreditCardLinkPayment() == false) { %>
													<%--▼▼ カード情報取得用 ▼▼--%>
													<input type="hidden" id="hidCinfo" name="hidCinfo" value="<%# CreateGetCardInfoJsScriptForCreditToken(Container) %>" />
													<%--▲▲ カード情報取得用 ▲▲--%>

													<%--▼▼ クレジット Token保持用 ▼▼--%>
													<asp:HiddenField ID="hfCreditToken" Value="" runat="server" />
													<%--▲▲ クレジット Token保持用 ▲▲--%>

													<%--▼▼ カード情報入力（トークン未取得・利用なし） ▼▼--%>
													<dl id="divCreditCardNoToken" runat="server" class="cartInput">
														<%if (OrderCommon.CreditCompanySelectable) {%>
														<strong>カード会社</strong>
														<p><asp:DropDownList id="ddlCreditCardCompany" runat="server" DataTextField="Text" DataValueField="Value" CssClass="input_widthG input_border"></asp:DropDownList></p>
														<%} %>
														<p class="adTtl">カード番号&nbsp;<span class="icnKome">※</span></p>
														<dd class="creditDD">
															<w2c:ExtendedTextBox id="tbCreditCardNo1" Type="tel" runat="server" CssClass="tel" MaxLength="16" autocomplete="off"></w2c:ExtendedTextBox><br />
														<small class="icnKome">
															<asp:CustomValidator ID="cvCreditCardNo1" runat="Server"
																ControlToValidate="tbCreditCardNo1"
																ValidationGroup="OrderPayment"
																ValidateEmptyText="true"
																SetFocusOnError="true"
																ClientValidationFunction="ClientValidate"
																CssClass="error_inline" />
														</small>
														<p class="inputNotice">
														カードの表記のとおりご入力ください。<br />
														例：1234567890123456（ハイフンなし）
														</p></dd>
														<p class="adTtl">有効期限&nbsp;<span class="icnKome">※</span></p>
														<dd class="creditDD">
															<div class="creditLimitedFlex">
																<div class="creditLimitedFlex_list">
																	<asp:DropDownList id="ddlCreditExpireMonth" runat="server" CssClass="expMonth"></asp:DropDownList>
																</div>
																<div class="creditLimitedFlex_txt">
																	<p>月 /</p>
																</div>
																<div class="creditLimitedFlex_list">
																	<asp:DropDownList id="ddlCreditExpireYear" runat="server" CssClass="expYear"></asp:DropDownList>
																</div>
																<div class="creditLimitedFlex_txt">
																	<p>年</p>
																</div>
															</div>
														</dd>
														<p class="adTtl">カード名義人&nbsp;<span class="icnKome">※</span></p>
														<dd class="creditDD">
															<asp:TextBox id="tbCreditAuthorName" runat="server" CssClass="nameFull" MaxLength="50" autocomplete="off"></asp:TextBox><br />
																<small class="fred">
																	<asp:CustomValidator ID="cvCreditAuthorName" runat="Server"
																		ControlToValidate="tbCreditAuthorName"
																		ValidationGroup="OrderPayment"
																		ValidateEmptyText="true"
																		SetFocusOnError="true"
																		ClientValidationFunction="ClientValidate"
																		CssClass="error_inline" />
																</small>
															<p class="inputNotice">例：「TAROU YAMADA」</p>
														</dd>
														<div id="trSecurityCode" visible="<%# OrderCommon.CreditSecurityCodeEnable %>" runat="server">
														<p class="adTtl">セキュリティコード&nbsp;<span class="icnKome">※</span></p>
														<dd class="inputCode creditDD">
															<div class="inputCodeBox">
																<asp:TextBox id="tbCreditSecurityCode" runat="server" CssClass="securityCode" MaxLength="4" autocomplete="off"></asp:TextBox>
															</div>
															<small class="fred">
															<asp:CustomValidator ID="cvCreditSecurityCode" runat="Server"
																ControlToValidate="tbCreditSecurityCode"
																ValidationGroup="OrderPayment"
																ValidateEmptyText="true"
																SetFocusOnError="true"
																ClientValidationFunction="ClientValidate"
																CssClass="error_inline" />
															</small>
														</dd>
														</div>
													</dl>
													<%--▲▲ カード情報入力（トークン未取得・利用なし） ▲▲--%>

													<%--▼▼ カード情報入力（トークン取得済） ▼▼--%>
													<dl id="divCreditCardForTokenAcquired" runat="server" class="cartInput">
														<%if (OrderCommon.CreditCompanySelectable) {%>
														<p class="adTtl">カード会社</p>
														<dd class="creditDD"><asp:Literal ID="lCreditCardCompanyNameForTokenAcquired" runat="server"></asp:Literal></dd>
														<%} %>
														<p class="adTtl">カード番号</p>
														<dd class="creditDD">XXXXXXXXXXXX<asp:Literal ID="lLastFourDigitForTokenAcquired" runat="server"></asp:Literal>
														<asp:LinkButton id="lbEditCreditCardNoForToken" OnClick="lbEditCreditCardNoForToken_Click" runat="server">再入力</asp:LinkButton></dd>
														<p class="adTtl">有効期限</p>
														<dd class="creditDD"><asp:Literal ID="lExpirationMonthForTokenAcquired" runat="server"></asp:Literal>
															/
															<asp:Literal ID="lExpirationYearForTokenAcquired" runat="server"></asp:Literal>
														(月/年)</dd>
														<p class="adTtl">カード名義人</p>
														<dd class="creditDD"><asp:Literal ID="lCreditAuthorNameForTokenAcquired" runat="server"></asp:Literal></dd>
													</dl>
													<%--▲▲ カード情報入力（トークン取得済） ▲▲ --%>

													<dl id="Div3" visible="<%# OrderCommon.CreditInstallmentsSelectable %>" runat="server" class="cartInput">
														<p class="adTtl">支払い回数</p>
														<dd class="creditDD">
															<div class="inputCodeBox">
																<asp:DropDownList id="dllCreditInstallments" runat="server" DataTextField="Text" DataValueField="Value" CssClass="input_border" autocomplete="off"></asp:DropDownList>
															</div>
															<span style="display: none;" class="fgray">※AMEX/DINERSは一括のみとなります。</span>
														</dd>
													</dl>
													<% } else { %>
														<div>遷移する外部サイトでカード番号を入力してください。</div>
													<% } %>
													<asp:CheckBox style="display: none;" ID="cbRegistCreditCard" runat="server" Checked="false" OnCheckedChanged="cbRegistCreditCard_OnCheckedChanged" Text="登録する" autocomplete="off" AutoPostBack="true" />
													<div id="divUserCreditCardName" visible="false" runat="server">
														<p>クレジットカードを保存する場合は、以下をご入力ください。</p>
														<strong>クレジットカード登録名&nbsp;<span class="fred">※</span></strong>
														<p>
															<asp:TextBox ID="tbUserCreditCardName" Text="" MaxLength="100" CssClass="input_widthD input_border" runat="server" autocomplete="off"></asp:TextBox><br />
															<small class="fred">
															<asp:CustomValidator ID="cvUserCreditCardName" runat="Server"
																ControlToValidate="tbUserCreditCardName"
																ValidationGroup="OrderPayment"
																ValidateEmptyText="true"
																SetFocusOnError="true"
																ClientValidationFunction="ClientValidate"
																CssClass="error_inline" />
															</small>
														</p>
													</div>
													<span id="spanErrorMessageForCreditCard" style="color: red; display: none" runat="server"></span>
													<%-- △新規カード△ --%>

													<%-- ▽登録済みカード▽ --%>
													<% }else{ %>
													<div id="divCreditCardDisp" runat="server">
														<%if (OrderCommon.CreditCompanySelectable) {%>
														<strong>カード会社</strong>
														<p><%: this.CreditCardCompanyName %><br /></p>
														<%} %>
														<strong>カード番号</strong>
														<p>XXXXXXXXXXXX<%: this.LastFourDigit %><br /></p>
														<strong>有効期限</strong>
														<p><%: this.ExpirationMonth %>/<%: this.ExpirationYear %> (月/年)</p>
														<strong>カード名義人</strong>
														<p><%: this.CreditAuthorName %></p>
														<asp:HiddenField ID="hfCreditCardId" runat="server" />

														<div visible="<%# OrderCommon.CreditInstallmentsSelectable %>" runat="server">
															<strong>支払い回数</strong>
															<p>
																<asp:DropDownList id="dllCreditInstallments2" runat="server" CssClass="input_border"></asp:DropDownList>
																<br/>
																<span class="fgray">※AMEX/DINERSは一括のみとなります。</span>
															</p>
														</div>
													</div>
													<% } %>
												<%-- △登録済みカード△ --%>
												</dd>

												<%-- コンビニ(後払い) --%>
												<dd id="ddCvsDef" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_CVS_DEF) %>" runat="server">
													<uc:PaymentDescriptionCvsDef runat="server" id="ucPaymentDescriptionCvsDef" />
												</dd>

												<%-- 後付款(TriLink後払い) --%>
												<dd id="ddTriLinkAfterPayPayment" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_TRILINK_AFTERPAY) %>" runat="server">
													<uc:PaymentDescriptionTriLinkAfterPay runat="server" id="ucPaymentDescriptionTryLinkAfterPay" />
												</dd>

												<%-- Amazon Pay --%>
												<dd id="ddAmazonPay" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_AMAZON_PAYMENT) %>" runat="server">
													<div style="display: none;">
														<small>※配送先情報、または、お支払い方法の変更を希望される方は「アドレス帳」→「お支払い方法」の順で選択してください。</small>
													</div>
													<iframe id="AmazonInputWidget" src="<%: PageUrlCreatorUtility.CreateAmazonPayWidgetUrl(false, orderId: this.OrderModel.OrderId) %>" style="width:100%;border:none;"></iframe>
													<asp:HiddenField ID="hfAmazonOrderRefID" ClientIDMode="Static" runat="server" />
												</dd>

												<%-- 代金引換 --%>
												<dd id="ddCollect" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_COLLECT) %>" runat="server">
												</dd>

												<%-- PayPal --%>
												<dd id="ddPayPal" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_PAYPAL) %>" runat="server">
													<div style="display: <%= dvOrderPaymentPattern.Visible ? "block" : "none"%>">
														<%if (Constants.PAYPAL_LOGINPAYMENT_ENABLED) {%>
															<div id="paypal-button" style="margin: 5px;"></div>
															<%if (SessionManager.PayPalCooperationInfo != null) {%>
																<%: (SessionManager.PayPalCooperationInfo != null) ? SessionManager.PayPalCooperationInfo.AccountEMail : "" %> 連携済<br/>
															<%} else { %>
																<div class="error">
																	<%: WebMessages.GetMessages(WebMessages.ERRMSG_FRONT_PAYPAL_NEEDS_LOGIN_ERROR) %>
																</div>
															<%} %>
														<%} %>
													</div>
													<uc:PaymentDescriptionPayPal runat="server" id="PaymentDescriptionPayPal" />
												</dd>

												<%-- Paidy --%>
												<dd id="ddPaidy" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_PAIDY) %>" runat="server">
													<uc:PaidyCheckoutControl ID="ucPaidyCheckoutControl" runat="server" />
												</dd>

												<!-- NP後払い -->
												<dd id="ddNpAfterPay" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_NP_AFTERPAY) %>" runat="server">
													<uc:PaymentDescriptionNPAfterPay runat="server" id="PaymentDescriptionNPAfterPay" />
												</dd>

												<%-- 決済なし --%>
												<dd id="ddNoPayment" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_NOPAYMENT) %>" runat="server">
												</dd>

												<%-- atone翌月払い --%>
												<dd id="ddPaymentAtone" class="Atone_0" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_ATONE) %>" runat="server">
													<span id="spanErrorMessageForAtone" class="spanErrorMessageForAtone" style="color: red; display: none" runat="server"></span>
													<uc:PaymentDescriptionAtone runat="server" id="PaymentDescriptionAtone" />
												</dd>

												<%-- aftee決済設定 --%>
												<dd id="ddPaymentAftee" class="Aftee_0" visible="<%# (Item.PaymentId == Constants.FLG_PAYMENT_PAYMENT_ID_AFTEE) %>" runat="server">
													<span id="spanErrorMessageForAftee" class="spanErrorMessageForAftee" style="color: red; display: none" runat="server"></span>
												</dd>
											</ItemTemplate>
											</asp:Repeater>
											</dl>
										</div><!--list-->
									</div>
								</div>
								<div>
									<% if (this.IsFixedPurchase && (this.FixedPurchaseModel.IsCancelFixedPurchaseStatus == false)) { %>
										<asp:CheckBox ID="cbIsUpdateFixedPurchaseByOrderPayment" Text="今後の定期注文にも反映させる" cssClass="radioBtn" Checked="false" runat="server"/><br />
									<% } %>
									<div class="updateLink">
										<asp:LinkButton ID="lbUpdatePayment" Text="情報更新" runat="server" ValidationGroup="OrderPayment" OnClientClick="doPostbackEvenIfCardAuthFailed=false;return CheckPaymentChange(this);" OnClick="btnUpdatePaymentPatternInfo_Click" ></asp:LinkButton>
										<asp:LinkButton Text="キャンセル" runat="server" OnClick="btnClosePaymentPatternInfo_Click"></asp:LinkButton>
									</div>
									<div id="divOrderPaymentUpdateExecFroms" style="display: none"> 
										更新中です...
									</div>
									<small id="sErrorMessagePayment" class="error" runat="server"></small>
								</div>
							</div>
					<% if ((this.OrderModel.OrderPaymentKbn == Constants.FLG_PAYMENT_PAYMENT_ID_CREDIT) && (this.UserCreditCardInfo != null)) { %>
						<dt>利用クレジットカード情報</dt>
						<dd>
							<% if (this.UserCreditCardInfo.DispFlg == Constants.FLG_USERCREDITCARD_DISP_FLG_ON) { %>
							クレジットカード登録名: <%:this.UserCreditCardInfo.CardDispName %><%: this.UserCreditCardInfo.DispFlag ? "" : " (削除済)" %><br />
							<% } %>
							<%if (OrderCommon.CreditCompanySelectable && (this.UserCreditCardInfo.CompanyName != string.Empty)) {%>
							カード会社: <%: this.UserCreditCardInfo.CompanyName %><br />
							<%} %>
							カード番号: XXXXXXXXXXXX<%: this.UserCreditCardInfo.LastFourDigit %><br />
							有効期限: <%: this.UserCreditCardInfo.ExpirationMonth + "/" + this.UserCreditCardInfo.ExpirationYear + " (月/年)" %><br />
							カード名義人: <%: this.UserCreditCardInfo.AuthorName %>
						</dd>
					<%} %>
						<dt>注文メモ</dt>
						<dd>
							<%#: StringUtility.ToEmpty(this.OrderModel.Memo) != "" ? this.OrderModel.Memo : "指定なし" %></dd>
					
					<%-- ポイントオプションが有効な場合 --%>
					<%if (Constants.W2MP_POINT_OPTION_ENABLED) {%>
						<dt>購入時付与ポイント</dt>
						<dd>
							<%#: GetNumeric(this.OrderModel.OrderPointAdd) %><%: Constants.CONST_UNIT_POINT_PT %></dd>
						<dt>ご利用ポイント</dt>
						<dd>
							<%#: GetNumeric(this.OrderModel.OrderPointUse) %><%: Constants.CONST_UNIT_POINT_PT %>
							<div style="display: none;" class="changeBtn">
								<asp:LinkButton ID="lbDisplayInputOrderPointUse" Text="利用ポイント変更" OnClick="lbDisplayInputOrderPointUse_Click"  runat="server" Enabled="<%# this.IsModifyUsePoint %>" />
							</div>
							<br />
							<br />
							<small id="slErrorMessageChangePointUse" runat="server" style="text-align:right; float:right; "></small>
							<div style="text-align:right; padding-top:15px;">
								<%: this.ExplanationPointUse %>
							</div>
							<% if (this.IsOrderPointAddDisplayStatus) { %>
							<br />
							利用可能ポイントは<%: StringUtility.ToNumeric(this.LoginUserPointUsable + this.OrderModel.OrderPointUse) %><%: Constants.CONST_UNIT_POINT_PT %>です。<br />
							※1<%: Constants.CONST_UNIT_POINT_PT %> = <%: CurrencyManager.ToPrice(1m) %><br />
							<asp:TextBox ID="tbOrderPointUse" runat="server" style="width: 70px;"></asp:TextBox> <%: Constants.CONST_UNIT_POINT_PT %>
							<br />
							<small id="slErrorMessagePointUse" runat="server" class="fred"></small>
							<% } %>
						</dd>
						<% if (this.IsOrderPointAddDisplayStatus) { %>
						<dt></dt>
						<dd>
							<div id="divOrderPointUpdateButtons" style="display: block"> 
								<asp:LinkButton Text="情報更新" runat="server" OnClientClick="return AlertDataChange('OrderPointUse', null);" OnClick="lbUpdateOrderPointUse_Click" class="btn" />
								<asp:LinkButton Text="キャンセル" runat="server" OnClick="lbHideOrderPointUse_Click" class="btn" />
							</div>
							<div id="divOrderPointUpdateExecFroms" style="display: none"> 
								更新中です...
							</div>
						</dd>
						<% } %>
					<% } %>
						<dt class="orderTotal">
							総合計（税込）</dt>
						<dd class="orderTotal">
							<%#: CurrencyManager.ToPrice(this.OrderModel.OrderPriceTotal) %></dd>
					<%if (Constants.GLOBAL_OPTION_ENABLE) { %>
						<dt class="orderTotal">
							決済金額（税込）</dt>
						<dd class="orderTotal">
							<%#: this.SendingAmount %></dd>
					<% } %>
				</dl>

			<%--▽領収書情報▽--%>
			<% if (Constants.RECEIPT_OPTION_ENABLED) { %>
			<div class="ohShipping">
				<h3>領収書情報</h3>
				<% if (this.IsReceiptInfoModify == false) { %>
				<dl class="ohDetail_box borderB0">
					<dt>領収書希望</dt>
					<dd>
						<%: ValueText.GetValueText(Constants.TABLE_ORDER, Constants.FIELD_ORDER_RECEIPT_FLG, this.OrderModel.ReceiptFlg) %>
						<%: (this.OrderModel.ReceiptOutputFlg == Constants.FLG_ORDER_RECEIPT_OUTPUT_FLG_ON) ? "(出力済み)" : "" %>
						<div class="changeBtn"><asp:LinkButton id="lbReceiptFlg" runat="server" OnClick="lbDisplayReceiptInfoForm_Click" Text="領収書情報変更" Visible="<%# this.CanModifyReceiptInfo %>" /></div>
					</dd>
				</dl>
				<dl class="ohDetail_box borderT0 borderB0" runat="server" visible="<%# this.OrderModel.ReceiptFlg == Constants.FLG_ORDER_RECEIPT_FLG_ON %>">
					<dt>宛名</dt>
					<dd><%: this.OrderModel.ReceiptAddress %></dd>
				</dl>
				<dl class="ohDetail_box borderT0" runat="server" visible="<%# this.OrderModel.ReceiptFlg == Constants.FLG_ORDER_RECEIPT_FLG_ON %>">
					<dt>但し書き</dt>
					<dd><%: this.OrderModel.ReceiptProviso %></dd>
				</dl>

				<div class="receiptDL">
					<a href="<%#: Constants.PROTOCOL_HTTPS + Constants.SITE_DOMAIN +Constants.PATH_ROOT + Constants.PAGE_FRONT_RECEIPTDOWNLOAD + "?okey=" + HttpUtility.UrlEncode(UserPassowordCryptor.PasswordEncrypt(this.OrderModel.OrderId)) %>" class="receiptDL_btn">領収書をダウンロード</a>
					<div class="receiptDL_note">
						<p>
							※Web領収書の発行（表示）は、1回のご注文につき1回のみです。
						</p>
						<p>
							※ご注文いただいた商品が出荷されてから発行が可能となります。
						</p>
					</div>
				</div>
				<% } else { %>
				<dl class="cartInput">
					<tr>
						<dt>領収書希望</dt>
						<dd>
							<asp:DropDownList ID="ddlReceiptFlg" runat="server" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="ddlReceiptFlg_OnSelectedIndexChanged" AutoPostBack="true" DataSource="<%# this.ReceiptFlgListItems %>" />
						</dd>
					</tr>
					<tr id="trReceiptAddressInput" runat="server">
						<th>
							<dt>宛名<span class="icnKome">※</span></dt>
						</th>
						<td>
							<dd>
								<asp:TextBox ID="tbReceiptAddress" runat="server" />
								<p><asp:CustomValidator runat="Server"
									ControlToValidate="tbReceiptAddress"
									ValidationGroup="ReceiptRegisterModify"
									ClientValidationFunction="ClientValidate"
									ValidateEmptyText="true"
									SetFocusOnError="true" />
								</p>
							</dd>
						</td>
					</tr>
					<tr id="trReceiptProvisoInput" runat="server">
						<th>
							<dt>但し書き<span class="icnKome">※</span></dt>
						</th>
						<td>
							<dd>
								<asp:TextBox ID="tbReceiptProviso" runat="server" />
								<p><asp:CustomValidator runat="Server"
									ControlToValidate="tbReceiptProviso"
									ValidationGroup="ReceiptRegisterModify"
									ClientValidationFunction="ClientValidate"
									ValidateEmptyText="true"
									SetFocusOnError="true" />
								</p>
							</dd>
						</td>
					</tr>
					<div style="display: block" class="updateLink">
						<asp:LinkButton Text="領収書情報更新" runat="server" OnClientClick="return confirm('領収書情報を変更してもよろしいですか？')" OnClick="lbUpdateReceiptInfo_Click" />
						<asp:LinkButton Text="キャンセル" runat="server"  OnClick="lbDisplayReceiptInfoForm_Click" />
					</div>
				</dl>
				<small class="error"><%: this.ReceiptInfoModifyErrorMessage %></small>
				<% } %>
			</div>
			<% } %>
			<%--△領収書情報△--%>

			<a name="ShippingArea" runat="server"></a>
			<%
				this.CartShippingItemIndexTmp = -1;
			%>
			<asp:Repeater ID="rOrderShipping" DataSource="<%# this.OrderShippingItems %>" Runat="server">
				<ItemTemplate>
					<% if (this.OrderModel.DigitalContentsFlg != Constants.FLG_ORDER_DIGITAL_CONTENTS_FLG_ON) {  %>
					<div class="ohShipping">
						<%-- お届け先情報 --%>
						<h3>お届け先情報</h3>
						<asp:Label ID="lOrderHistoryErrorMessage" CssClass="fred" runat="server" Visible="false"></asp:Label>
							<tr visible='<%# StringUtility.ToEmpty(this.OrderModel.GiftFlg) == Constants.FLG_ORDER_GIFT_FLG_ON %>' runat="server">
								<th colspan="2">送り主</th>
							</tr>
							<tr visible='<%# StringUtility.ToEmpty(this.OrderModel.GiftFlg) == Constants.FLG_ORDER_GIFT_FLG_ON %>' runat="server">
								<th>
									<%: ReplaceTag("@@User.addr.name@@") %>
								</th>
								<td>
									<%# IsCountryJp((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_COUNTRY_ISO_CODE))
										? "〒" + WebSanitizer.HtmlEncode(GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_ZIP)) + "<br />"
										: "" %>
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_ADDR1) %>
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_ADDR2) %>
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_ADDR3) %>
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_ADDR4) %>
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_ADDR5) %>
									<%#: (IsCountryJp((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_COUNTRY_ISO_CODE)) == false)
										? GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_ZIP)
										: "" %><br />
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_COUNTRY_NAME) %>
								</td>
							</tr>
							<% if (Constants.DISPLAY_CORPORATION_ENABLED){ %>
							<tr visible='<%# StringUtility.ToEmpty(this.OrderModel.GiftFlg) == Constants.FLG_ORDER_GIFT_FLG_ON %>' runat="server">
								<%-- 企業名・部署名 --%>
								<th><%: ReplaceTag("@@User.company_name.name@@")%>・
									<%: ReplaceTag("@@User.company_post_name.name@@")%></th>
								<td>
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_COMPANY_NAME) %><br />
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_COMPANY_POST_NAME) %>
								</td>
							</tr>
							<%} %>
							<tr visible='<%# StringUtility.ToEmpty(this.OrderModel.GiftFlg) == Constants.FLG_ORDER_GIFT_FLG_ON %>' runat="server">
								<%-- 氏名 --%>
								<th><%: ReplaceTag("@@User.name.name@@") %></th>
								<td>
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_NAME) %>&nbsp;様<br />
									<%#: IsCountryJp((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_COUNTRY_ISO_CODE))
										? "(" + GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_NAME_KANA) + " さま)"
										: "" %>
								</td>
							</tr>
							<tr visible='<%# StringUtility.ToEmpty(this.OrderModel.GiftFlg) == Constants.FLG_ORDER_GIFT_FLG_ON %>' runat="server">
								<%-- 電話番号 --%>
								<th><%: ReplaceTag("@@User.tel1.name@@") %></th>
								<td>
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SENDER_TEL1) %>
								</td>
							</tr>
							<tr visible='<%# StringUtility.ToEmpty(this.OrderModel.GiftFlg) == Constants.FLG_ORDER_GIFT_FLG_ON %>' runat="server">
								<th colspan="2">お届け先</th>
							</tr>
							<dl class="ohDetail_box borderB0" id="dShippingInfo" runat="server" visible="true">
								<% if (this.UseShippingAddress) { %>
								<% if (this.OrderModel.OrderPaymentKbn != Constants.FLG_PAYMENT_PAYMENT_ID_AMAZON_PAYMENT) { %>
									<dt>
										<%: ReplaceTag("@@User.addr.name@@") %>
									</dt>
									<dd>
										<%# IsCountryJp((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_COUNTRY_ISO_CODE))
											? "〒" + WebSanitizer.HtmlEncode(GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_ZIP)) + "<br />"
											: "" %>
										<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_ADDR1) %>
										<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_ADDR2) %>
										<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_ADDR3) %>
										<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_ADDR4) %>
										<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_ADDR5) %>
										<%# (IsCountryJp((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_COUNTRY_ISO_CODE)) == false)
											? WebSanitizer.HtmlEncode(GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_ZIP)) + "<br />"
											: "" %>
										<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_COUNTRY_NAME) %>
										<div style="display: none;" class="changeBtn">
											<asp:LinkButton ID="LinkButton1" Text="お届け先変更" runat="server" CommandArgument="<%# Container.ItemIndex %>" OnClick="lbDisplayUserShippingInfoForm_Click" Enabled="<%# this.IsModifyShipping %>" />
										</div>
										<div>
											<%#: this.ExplanationShipping %>
										</div>
									</dd>
								<% if (Constants.DISPLAY_CORPORATION_ENABLED){ %>
									<%-- 企業名・部署名 --%>
									<dt><%: ReplaceTag("@@User.company_name.name@@")%>・
										<%: ReplaceTag("@@User.company_post_name.name@@")%></dt>
									<dd>
										<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_COMPANY_NAME) %><br />
										<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_COMPANY_POST_NAME) %>
									</dd>
								<%} %>
									<%-- 氏名 --%>
									<dt><%: ReplaceTag("@@User.name.name@@") %></dt>
									<dd>
										<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_NAME) %>&nbsp;様<br />
										<%#: IsCountryJp((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_COUNTRY_ISO_CODE))
											? "(" + GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_NAME_KANA) + " さま)" 
											: ""%>
									</dd>
								<%} %>
									<%-- 電話番号 --%>
									<dt><%: ReplaceTag("@@User.tel1.name@@") %></dt>
									<dd>
										<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_TEL1) %>
									</dd>
								<% } else { %>
									<dt>店舗ID</dt>
									<dd>
										<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_ID) %>
										<div style="text-align:right; float:right; ">
											<asp:LinkButton ID="lbDisplayUserShippingInfoForm" Text="お届け先変更" runat="server" CommandArgument="<%# Container.ItemIndex %>" OnClick="lbDisplayUserShippingInfoForm_Click" Enabled="<%# this.IsModifyShipping %>" class="btn" />
										</div>
									</dd>
									<dt>店舗名称</dt>
									<dd><%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_NAME) %></dd>
									<dt>店舗住所</dt>
									<dd><%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_ADDR4) %></dd>
									<dt>店舗電話番号</dt>
									<dd><%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_TEL1) %></dd>
								<% } %>
							</dl>
							<dl id="dShippngInput" runat="server" visible="false" class="cartInput pdg">
								<dd>
									<div style="display: none;" class="changeBtn mrg">
										<asp:LinkButton Text="お届け先変更" runat="server" CommandArgument="<%# Container.ItemIndex %>" OnClick="lbDisplayUserShippingInfoForm_Click" Enabled="<%# this.IsModifyShipping %>" />
									</div>
									<asp:DropDownList
										DataTextField="Text"
										DataValueField="Value"
										SelectedValue='<%# IsDisplayButtonConvenienceStore((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_FLG))
											? CartShipping.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE
											: null %>'
										ID="ddlShippingType"
										AutoPostBack="true"
										OnSelectedIndexChanged="ddlShippingType_SelectedIndexChanged"
										DataSource='<%# GetPossibleShippingType((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_FLG)) %>'
										runat="server"
										CssClass="UserShippingAddress">
									</asp:DropDownList>
									<% if (Constants.RECEIVINGSTORE_TWECPAY_CVSOPTION_ENABLED) { %>
									<asp:DropDownList
										DataTextField="Text"
										DataValueField="Value"
										SelectedValue='<%# GetShippingReceivingStoreTypeValue(
											IsDisplayButtonConvenienceStore((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_FLG)),
											(string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_TYPE)) %>'
										ID="ddlShippingReceivingStoreType"
										Visible='<%# IsDisplayButtonConvenienceStore((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_FLG)) %>'
										AutoPostBack="true"
										OnSelectedIndexChanged="ddlShippingReceivingStoreType_SelectedIndexChanged"
										DataSource='<%# ShippingReceivingStoreType() %>'
										runat="server"
										CssClass="UserShippingAddress" />
									<% } %>
									<% if (Constants.RECEIVINGSTORE_TWPELICAN_CVSOPTION_ENABLED) { %>
									<span id="spConvenienceStoreSelect" runat="server" visible='<%# (IsDisplayButtonConvenienceStore((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_FLG)) && (Constants.RECEIVINGSTORE_TWECPAY_CVSOPTION_ENABLED == false)) %>'>
										<a href='<%# string.Format(@"javascript:openConvenienceStoreMapPopup({0});", Container.ItemIndex) %>' class="btn btn-success convenience-store-button" >Family/OK/Hi-Life</a>
									</span>
									<span id="spConvenienceStoreEcPaySelect" runat="server" visible='<%# (IsDisplayButtonConvenienceStore((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_FLG)) && Constants.RECEIVINGSTORE_TWECPAY_CVSOPTION_ENABLED) %>'>
										<asp:LinkButton
											ID="lbOpenEcPay"
											runat="server"
											class="btn btn-success convenience-store-button"
											OnClick="lbOpenEcPay_Click"
											CommandArgument="<%# Container.ItemIndex %>"
											Text="  電子マップ  " />
									</span>
									<div id="dvErrorShippingConvenience" runat="server" style="display:none;">
										<span class="error_inline"><%#: WebMessages.GetMessages(WebMessages.ERRMSG_FRONT_GROCERY_STORE) %></span>
									</div>
									<div id="dvErrorPaymentAndShippingConvenience" runat="server" visible="false">
										<span class="error_inline"><%#: WebMessages.GetMessages(WebMessages.ERRMSG_FRONT_PAYMENT_METHOD_CHANGED_TO_CONVENIENCE_STORE) %></span>
									</div>
									<% } %>
								</dd>
								<div id="divShippingInputFormInner" runat="server">
								<%
									this.CartShippingItemIndexTmp++;
									var shippingAddrCountryIsoCode = GetShippingAddrCountryIsoCode(this.CartShippingItemIndexTmp);
									var isShippingAddrCountryJp = IsCountryJp(shippingAddrCountryIsoCode);
									var isShippingAddrCountryUs = IsCountryUs(shippingAddrCountryIsoCode);
									var isShippingAddrCountryTw = IsCountryTw(shippingAddrCountryIsoCode);
									var isShippingAddrZipNecessary = IsAddrZipcodeNecessary(shippingAddrCountryIsoCode);
								%>
								<dt><%: ReplaceTag("@@User.name.name@@") %> <span class="icnKome">※</span></dt>
									<dd>
										<div class="nameFlex">
											<div class="nameFlex_list">
												<p>姓</p>
												<asp:TextBox ID="tbShippingName1" runat="server" MaxLength="10" ></asp:TextBox>
											</div>
											<div class="nameFlex_list">
												<p>名</p><asp:TextBox ID="tbShippingName2" runat="server" MaxLength="10" ></asp:TextBox>
											</div>
										</div>
										<asp:CustomValidator ID="CustomValidator1" runat="Server"
											ControlToValidate="tbShippingName1"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
										<asp:CustomValidator ID="CustomValidator2" runat="Server"
											ControlToValidate="tbShippingName2"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
									</dd>
								<% if (isShippingAddrCountryJp) { %>
									<dt>カナ <span class="icnKome">※</span></dt>
									<dd>
										<div class="nameFlex">
											<div class="nameFlex_list">
												<p>セイ</p>
												<asp:TextBox ID="tbShippingNameKana1" runat="server" MaxLength="20"></asp:TextBox>
											</div>
											<div class="nameFlex_list">
												<p>メイ</p><asp:TextBox ID="tbShippingNameKana2" runat="server" MaxLength="20"></asp:TextBox>
											</div>
										</div>
										<asp:CustomValidator ID="CustomValidator3" runat="Server"
											ControlToValidate="tbShippingNameKana1"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
										<asp:CustomValidator ID="CustomValidator4" runat="Server"
											ControlToValidate="tbShippingNameKana2"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
									</dd>
								<% } %>
								<% if (Constants.GLOBAL_OPTION_ENABLE) { %>
									<dt>
										<%: ReplaceTag("@@User.country.name@@", shippingAddrCountryIsoCode) %>
										<span class="icnKome">※</span>
									</dt>
									<dd>
										<asp:DropDownList ID="ddlShippingCountry" runat="server" DataTextField="<%#: Container.ItemIndex %>" AutoPostBack="true" OnSelectedIndexChanged="ddlShippingCountry_SelectedIndexChanged"></asp:DropDownList>
										<asp:CustomValidator
											ID="cvShippingCountry"
											runat="Server"
											ControlToValidate="ddlShippingCountry"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											EnableClientScript="false"
											CssClass="error_inline" />
									</dd>
								<% } %>
									<% if (isShippingAddrCountryJp) { %>
									<dt>
										<%: ReplaceTag("@@User.zip.name@@") %>
										<span class="icnKome">※</span>
									</dt>
									<dd>
										<div class="zipFlex">
											<div class="zipFlex_list">
												<asp:TextBox ID="tbShippingZip1" runat="server" MaxLength="3"></asp:TextBox>
											</div>
											<div class="zipFlex_line">
												<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
											</div>
											<div class="zipFlex_list">
												<asp:TextBox ID="tbShippingZip2" runat="server" MaxLength="4" ValidationGroup="<%# Container.ItemIndex %>" OnTextChanged="lbSearchAddr_TextBox_Click"></asp:TextBox>
											</div>
										</div>
										<asp:LinkButton style="display: none;" ID="lbSearchShippingAddr" runat="server" CommandArgument="<%# Container.ItemIndex %>" OnClientClick="return false;" OnClick="lbSearchAddr_LinkButton_Click" class="btn btn-mini">
										郵便番号から住所を入力
										</asp:LinkButton><br />
										<%--検索結果レイヤー--%>
										<uc:Layer ID="ucLayerForOwner" runat="server" />
										<asp:CustomValidator
											ID="cvShippingZip1"
											runat="Server"
											ControlToValidate="tbShippingZip1"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
										<asp:CustomValidator
											ID="cvShippingZip2"
											runat="Server"
											ControlToValidate="tbShippingZip2"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
										<small id="sShippingZipError" runat="server" class="fred"></small>
									</dd>
									<%-- 都道府県 --%>
									<dt>
										住所
										<span class="icnKome">※</span>
									</dt>
									<dd>
										<div class="selectPrefecture">
											<asp:DropDownList ID="ddlShippingAddr1" runat="server" DataTextField="<%#: Container.ItemIndex %>" OnSelectedIndexChanged="ddlShippingAddr1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
										</div>
										<asp:CustomValidator
											ID="cvShippingAddr1"
											runat="Server"
											ControlToValidate="ddlShippingAddr1"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
									</dd>
								<% } %>
									<%-- 市区町村 --%>
									<dd>
										<p class="adTtl">
											<%: ReplaceTag("@@User.addr2.name@@", shippingAddrCountryIsoCode) %>
											<span class="icnKome">※</span>
										</p>
										<% if (isShippingAddrCountryTw) { %>
											<asp:DropDownList runat="server" ID="ddlShippingAddr2" DataSource="<%# this.UserTwCityList %>" AutoPostBack="true" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="ddlShippingAddr2_SelectedIndexChanged"></asp:DropDownList>
										<% } else { %>
											<asp:TextBox ID="tbShippingAddr2" runat="server" MaxLength="40"></asp:TextBox>
											<asp:CustomValidator
												ID="cvShippingAddr2"
												runat="Server"
												ControlToValidate="tbShippingAddr2"
												ValidationGroup="OrderShipping"
												ValidateEmptyText="true"
												SetFocusOnError="true"
												ClientValidationFunction="ClientValidate"
												CssClass="error_inline" />
										<% } %>
									</dd>
									<%-- 番地 --%>
									<dd>
										<p class="adTtl">
											<%: ReplaceTag("@@User.addr3.name@@", shippingAddrCountryIsoCode) %>
											<% if (IsAddress3Necessary(shippingAddrCountryIsoCode)){ %><span class="icnKome">※</span><% } %>
										</p>
										<% if (isShippingAddrCountryTw) { %>
											<asp:DropDownList runat="server" ID="ddlShippingAddr3" DataTextField="Key" DataValueField="Value" Width="95" ></asp:DropDownList>
										<% } else { %>
											<asp:TextBox ID="tbShippingAddr3" runat="server" MaxLength="40"></asp:TextBox>
											<asp:CustomValidator
												ID="cvShippingAddr3"
												runat="Server"
												ControlToValidate="tbShippingAddr3"
												ValidationGroup="OrderShipping"
												ValidateEmptyText="true"
												SetFocusOnError="true"
												ClientValidationFunction="ClientValidate"
												CssClass="error_inline" />
										<% } %>
									</dd>
									<%-- ビル・マンション名 --%>
									<dd <%: (Constants.DISPLAY_ADDR4_ENABLED || (isShippingAddrCountryJp == false)) ? "" : "style=\"display:none;\""  %>>
										<p class="adTtl">
											<%: ReplaceTag("@@User.addr4.name@@", shippingAddrCountryIsoCode) %>
											<% if (isShippingAddrCountryJp == false) {%><span class="icnKome">※</span><% } %>
										</p>
										<asp:TextBox ID="tbShippingAddr4" runat="server" MaxLength="40"></asp:TextBox>
										<asp:CustomValidator
											ID="cvShippingAddr4"
											runat="Server"
											ControlToValidate="tbShippingAddr4"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
									</dd>
								<% if (isShippingAddrCountryJp == false) { %>
									<%-- 州 --%>
									<dd>
										<p class="adTtl">
											<%: ReplaceTag("@@User.addr5.name@@", shippingAddrCountryIsoCode) %>
											<% if (isShippingAddrCountryUs) { %> <span class="icnKome">※</span><% } %>
										</p>
										<% if (isShippingAddrCountryUs) { %>
										<asp:DropDownList runat="server" ID="ddlShippingAddr5"></asp:DropDownList>
											<asp:CustomValidator
												ID="cvShippingAddr5Ddl"
												runat="Server"
												ControlToValidate="ddlShippingAddr5"
												ValidationGroup="OrderShippingGlobal"
												ValidateEmptyText="true"
												SetFocusOnError="true"
												ClientValidationFunction="ClientValidate"
												CssClass="error_inline" />
										<% } else { %>
										<asp:TextBox runat="server" ID="tbShippingAddr5"></asp:TextBox>
										<asp:CustomValidator
											ID="cvShippingAddr5"
											runat="Server"
											ControlToValidate="tbShippingAddr5"
											ValidationGroup="OrderShippingGlobal"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
										<% } %>
									</dd>
									<%-- 郵便番号（海外向け） --%>
									<dt>
										<%: ReplaceTag("@@User.zip.name@@", shippingAddrCountryIsoCode) %>
										<% if (isShippingAddrZipNecessary) { %><span class="icnKome">※</span><% } %>
									</dt>
									<dd>
										<asp:TextBox runat="server" ID="tbShippingZipGlobal" MaxLength="30"></asp:TextBox>
										<asp:CustomValidator
											ID="cvShippingZipGlobal"
											runat="Server"
											ControlToValidate="tbShippingZipGlobal"
											ValidationGroup="OrderHistoryDetailGlobal"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
									</dd>
								<% } %>
								<% if (Constants.DISPLAY_CORPORATION_ENABLED) { %>
									<dt><%: ReplaceTag("@@User.company_name.name@@") %> </dt>
									<dd>
										<asp:TextBox ID="tbShippingCompanyName" runat="server" MaxLength="40"></asp:TextBox>
										<asp:CustomValidator
											ID="cvShippingCompanyName"
											runat="Server"
											ControlToValidate="tbShippingCompanyName"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
									</dd>
									<dt><%: ReplaceTag("@@User.company_post_name.name@@") %> </dt>
									<dd>
										<asp:TextBox ID="tbShippingCompanyPostName" runat="server" MaxLength="40"></asp:TextBox>
										<asp:CustomValidator
											ID="cvShippingCompanyPostName"
											runat="Server"
											ControlToValidate="tbShippingCompanyPostName"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
									</dd>
								<% } %>
									<% if (isShippingAddrCountryJp) { %>
									<dt>
										<%: ReplaceTag("@@User.tel1.name@@") %>
										<span class="icnKome">※</span></dt>
									<dd class="mrg">
										<div class="telFlex">
											<div class="telFlex_list">
												<asp:TextBox ID="tbShippingTel1_1" runat="server" MaxLength="6"></asp:TextBox>
											</div>
											<div class="telFlex_line">
												<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
											</div>
											<div class="telFlex_list">
												<asp:TextBox ID="tbShippingTel1_2" runat="server" MaxLength="4"></asp:TextBox>
											</div>
											<div class="telFlex_line">
												<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
											</div>
											<div class="telFlex_list">
												<asp:TextBox ID="tbShippingTel1_3" runat="server" MaxLength="4"></asp:TextBox>
											</div>
										</div>
										<asp:CustomValidator
											ID="cvShippingTel1_1"
											runat="Server"
											ControlToValidate="tbShippingTel1_1"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
										<asp:CustomValidator
											ID="cvShippingTel1_2"
											runat="Server"
											ControlToValidate="tbShippingTel1_2"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
										<asp:CustomValidator
											ID="cvShippingTel1_3"
											runat="Server"
											ControlToValidate="tbShippingTel1_3"
											ValidationGroup="OrderShipping"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
									</dd>
									<% } else { %>
									<dt>
										<%: ReplaceTag("@@User.tel1.name@@", shippingAddrCountryIsoCode) %>
										<span class="icnKome">※</span></dt>
									<dd>
										<asp:TextBox runat="server" ID="tbShippingTel1Global" MaxLength="30"></asp:TextBox>
										<asp:CustomValidator
											ID="cvShippingTel1Global"
											runat="Server"
											ControlToValidate="tbShippingTel1Global"
											ValidationGroup="OrderShippingGlobal"
											ValidateEmptyText="true"
											SetFocusOnError="true"
											ClientValidationFunction="ClientValidate"
											CssClass="error_inline" />
									</dd>
									<% } %>
								</div>
								<%-- ▽コンビニ▽ --%>
								<asp:HiddenField ID="hfCvsShopFlg" runat="server" Value='<%# GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_FLG) %>' />
								<asp:HiddenField ID="hfSelectedShopId" runat="server" Value='<%# GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_ID) %>' />
								<tbody id="divConvenienceStore" class="<%# Container.ItemIndex %>" runat="server">
										<dt>店舗ID</dt>
										<dd class="convenience-store-item" id="ddCvsShopId">
											<span style="font-weight:normal;">
												<asp:Literal ID="lCvsShopId" runat="server" Text='<%# GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_ID) %>'/>
											</span>
											<asp:HiddenField ID="hfCvsShopId" runat="server" Value='<%# GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_ID) %>' />
										</dd>
										<dt>店舗名称</dt>
										<dd class="convenience-store-item" id="ddCvsShopName">
											<span style="font-weight:normal;">
												<asp:Literal ID="lCvsShopName" runat="server" Text='<%# GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_NAME) %>'/>
											</span>
											<asp:HiddenField ID="hfCvsShopName" runat="server" Value='<%# GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_NAME) %>' />
										</dd>
										<dt>店舗住所</dt>
										<dd class="convenience-store-item" id="ddCvsShopAddress">
											<span style="font-weight:normal;">
												<asp:Literal ID="lCvsShopAddress" runat="server" Text='<%# GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_ADDR4) %>'></asp:Literal>
											</span>
											<asp:HiddenField ID="hfCvsShopAddress" runat="server" Value='<%# GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_ADDR4) %>' />
										</dd>
										<dt>店舗電話番号</dt>
										<dd class="convenience-store-item" id="ddCvsShopTel">
											<span style="font-weight:normal;">
												<asp:Literal ID="lCvsShopTel" runat="server" Text='<%# GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_TEL1) %>'></asp:Literal>
											</span>
											<asp:HiddenField ID="hfCvsShopTel" runat="server" Value='<%# GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_TEL1) %>' />
										</dd>
								</tbody>
								<tbody id="tbOwnerAddress" runat="server" visible="false">
										<dt>
											<%: ReplaceTag("@@User.addr.name@@") %>
										</dt>
										<dd>
											<%# IsCountryJp(this.LoginUser.AddrCountryIsoCode)
														? "〒" + WebSanitizer.HtmlEncode(this.LoginUser.Zip) + "<br />"
														: "" %>
											<%#: this.LoginUser.Addr1 %>
											<%#: this.LoginUser.Addr2 %>
											<%#: this.LoginUser.Addr3 %>
											<%#: this.LoginUser.Addr4 %>
											<%#: this.LoginUser.Addr5 %>
											<%# (IsCountryJp(this.LoginUser.AddrCountryIsoCode) == false)
															? WebSanitizer.HtmlEncode(this.LoginUser.Zip) + "<br />"
															: "" %>
											<%#: this.LoginUser.AddrCountryName %>
										</dd>
									<% if (Constants.DISPLAY_CORPORATION_ENABLED)
									{ %>
										<%-- 企業名・部署名 --%>
										<dt>
											<%: ReplaceTag("@@User.company_name.name@@")%>・
											<%: ReplaceTag("@@User.company_post_name.name@@")%>
										</dt>
										<dd>
											<%#: this.LoginUser.CompanyName %><br />
											<%#: this.LoginUser.CompanyPostName %>
										</dd>
									<%} %>
										<%-- 氏名 --%>
										<dt><%: ReplaceTag("@@User.name.name@@") %></dt>
										<dd>
											<%#: this.LoginUser.Name %>&nbsp;様<br />
											<%#: IsCountryJp(this.LoginUser.AddrCountryIsoCode)
														? "(" + this.LoginUser.NameKana + " さま)" 
														: "" %>
										</dd>
										<%-- 電話番号 --%>
										<dt><%: ReplaceTag("@@User.tel1.name@@") %></dt>
										<dd>
											<%#: this.LoginUser.Tel1 %>
										</dd>
								</tbody>
								<asp:Repeater Visible="false" runat="server" ItemType="UserShippingModel" DataSource="<%# this.UserShippingAddr %>" ID="rOrderShippingList">
									<ItemTemplate>
										<tbody runat="server" class='<%# string.Format("user_addres{0}", Item.ShippingNo) %>' visible="<%# (Item.ShippingReceivingStoreFlg == Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_ON) %>">
											<asp:HiddenField runat="server" ID="hfShippingNo" Value="<%# Item.ShippingNo %>" />
												<dt>店舗ID</dt>
												<dd class="convenience-store-item" id="ddCvsShopId">
													<span style="font-weight:normal;">
														<asp:Literal ID="lCvsShopId" runat="server" Text='<%# Item.ShippingReceivingStoreId %>' />
													</span>
													<asp:HiddenField ID="hfCvsShopId" runat="server" Value="<%# Item.ShippingReceivingStoreId %>" />
												</dd>
												<dt>店舗名称</dt>
												<dd class="convenience-store-item" id="ddCvsShopName">
													<span style="font-weight:normal;">
														<asp:Literal ID="lCvsShopName" runat="server" Text='<%# Item.ShippingName %>' />
													</span>
													<asp:HiddenField ID="hfCvsShopName" runat="server" Value="<%# Item.ShippingName %>" />
												</dd>
												<dt>店舗住所</dt>
												<dd class="convenience-store-item" id="ddCvsShopAddress">
													<span style="font-weight:normal;">
														<asp:Literal ID="lCvsShopAddress" runat="server" Text='<%# Item.ShippingAddr4 %>'></asp:Literal>
													</span>
													<asp:HiddenField ID="hfCvsShopAddress" runat="server" Value="<%# Item.ShippingAddr4 %>" />
												</dd>
												<dt>店舗電話番号</dt>
												<dd class="convenience-store-item" id="ddCvsShopTel">
													<span style="font-weight:normal;">
														<asp:Literal ID="lCvsShopTel" runat="server" Text='<%# Item.ShippingTel1 %>'></asp:Literal>
													</span>
													<asp:HiddenField ID="hfCvsShopTel" runat="server" Value="<%# Item.ShippingTel1 %>" />
												</dd>
										</tbody>
										<tbody runat="server" visible="<%# (Item.ShippingReceivingStoreFlg != Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_ON) %>">
												<dt>
													<%: ReplaceTag("@@User.addr.name@@") %>
												</dt>
												<dd>
													<%# IsCountryJp(Item.ShippingCountryIsoCode)
														? "〒" + WebSanitizer.HtmlEncode(Item.ShippingZip) + "<br />"
														: "" %>
													<%#: Item.ShippingAddr1 %>
													<%#: Item.ShippingAddr2 %>
													<%#: Item.ShippingAddr3 %>
													<%#: Item.ShippingAddr4 %>
													<%#: Item.ShippingAddr5 %>
													<%# (IsCountryJp(Item.ShippingCountryIsoCode) == false)
															? WebSanitizer.HtmlEncode(Item.ShippingZip) + "<br />"
															: "" %>
													<%#: Item.ShippingCountryName %>
												</dd>
											<% if (Constants.DISPLAY_CORPORATION_ENABLED) { %>
												<%-- 企業名・部署名 --%>
												<dt><%: ReplaceTag("@@User.company_name.name@@")%>・
														<%: ReplaceTag("@@User.company_post_name.name@@")%></dt>
												<dd>
													<%#: Item.ShippingCompanyName %><br />
													<%#: Item.ShippingCompanyPostName %>
												</dd>
											<%} %>
												<%-- 氏名 --%>
												<dt><%: ReplaceTag("@@User.name.name@@") %></dt>
												<dd>
													<%#: Item.ShippingName %>&nbsp;様<br />
													<%#: IsCountryJp(Item.ShippingCountryIsoCode)
														? "(" + Item.ShippingNameKana + " さま)" 
														: "" %>
												</dd>
												<%-- 電話番号 --%>
												<dt><%: ReplaceTag("@@User.tel1.name@@") %></dt>
												<dd>
													<%#: Item.ShippingTel1 %>
												</dd>
										</tbody>
									</ItemTemplate>
								</asp:Repeater>
								<dd>
									<% if (this.IsFixedPurchase && (this.FixedPurchaseModel.IsCancelFixedPurchaseStatus == false) && (this.IsUpdateShippingFixedPurchase)) { %>
										<asp:CheckBox ID="cbIsUpdateFixedPurchaseByOrderShippingInfo" Text="今後の定期注文にも反映させる" Checked="false" cssClass="radioBtn" runat="server"/>
									<% } %>
									<div id="divOrderShippingUpdateButtons" style="display: block" class="updateLink">
										<asp:LinkButton Text="情報更新" runat="server" ValidationGroup="OrderShipping" CommandArgument="<%# Container.ItemIndex %>" OnClientClick="return AlertDataChange('Shipping', this);" OnClick="lbUpdateUserShippingInfo_Click" ></asp:LinkButton>
										<asp:LinkButton Text="キャンセル" runat="server" CommandArgument="<%# Container.ItemIndex %>"  OnClick="lbHideUserShippingInfoForm_Click" ></asp:LinkButton>
										<input type="hidden" id="parentShippingRepeater" name="parentShippingRepeater" value="<%#: Container.UniqueID %>" />
									</div>
									<div id="divOrderShippingUpdateExecFroms" style="display: none"> 
										更新中です...
									</div>
									<small id="sErrorMessageShipping" runat="server" class="error"></small>
								</dd>
							</dl>
							<dl class="ohDetail_box borderT0 borderB0">
							<% if ((this.OrderModel.Shippings[0].ShippingReceivingStoreFlg == Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_OFF)
								|| (Constants.RECEIVINGSTORE_TWECPAY_CVSOPTION_ENABLED == false)) { %>
								<dt>配送方法</dt>
								<dd>
									<%#: ValueText.GetValueText(Constants.TABLE_ORDERSHIPPING, Constants.FIELD_ORDERSHIPPING_SHIPPING_METHOD, GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_METHOD)) %>
								</dd>
							<% } %>
								<dt>配送サービス</dt>
								<dd>
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_DELIVERYCOMPANY_DELIVERY_COMPANY_NAME) %>
								</dd>
							</dl>
							<div visible='<%# (string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_METHOD) != Constants.FLG_SHOPSHIPPINGSHIPPINGCOMPANY_SHIPPING_KBN_MAIL %>' runat="server">
								<% if ((this.OrderModel.Shippings[0].ShippingReceivingStoreFlg == Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_OFF)
									|| (Constants.RECEIVINGSTORE_TWECPAY_CVSOPTION_ENABLED == false)) { %>
								<dl class="ohDetail_box borderT0 borderB0" runat="server" visible='<%# (string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_SHOPSHIPPING_SHIPPING_DATE_SET_FLG) == Constants.FLG_SHOPSHIPPING_SHIPPING_DATE_SET_FLG_VALID %>'>
									<dt>配送希望日</dt>
									<dd>
										<div class="shippingSelect">
											<asp:DropDownList ID="ddlShippingDateList" Visible="false" runat="server" DataSource="<%# GetListShippingDate() %>" DataTextField="text" DataValueField="value"></asp:DropDownList>
										</div>
										<div id="dvShippingDateText"  runat="server" ><%#: DateTimeUtility.ToStringFromRegion(GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_DATE), DateTimeUtility.FormatType.LongDateWeekOfDay2Letter, ReplaceTag("@@DispText.shipping_date_list.none@@")) %></div>
										<div style="display: none;" class="changeBtn">
											<asp:LinkButton CommandArgument="<%# Container.ItemIndex %>" Text="配送希望日変更" runat="server" OnClick="lbDisplayInputShippingDate_Click" Enabled="<%# this.IsModifyShippingDates[Container.ItemIndex] %>" />
										</div>
										<div><%#: this.ExplanationShippingDates[Container.ItemIndex] %></div>
										<asp:Label ID="lShippingDateErrorMessage" CssClass="fred" runat="server" Visible="false"></asp:Label>
										<div id="trShippingDateInput" visible='false'  runat="server">
											<div id="divShippingDateUpdateButtons" style="display: block" class="updateLink"> 
												<asp:LinkButton Text="情報更新" runat="server" ValidationGroup="OrderShipping" CommandArgument="<%# Container.ItemIndex %>" OnClientClick="return AlertUpdateShippingDate(this);" OnClick="lbUpdateShippingDate_Click" ></asp:LinkButton>
												<asp:LinkButton Text="キャンセル" runat="server" CommandArgument="<%# Container.ItemIndex %>" OnClick="lbHideShippingDate_Click" />
												<input type="hidden" id="parentShippingDateRepeater" name="parentShippingDateRepeater" value="<%#: Container.UniqueID %>" />
											</div>
											<div id="divShippingDateUpdateExecFroms" style="display: none"> 
												更新中です...
											</div>
											<small id="sErrorMessageShippingDate" runat="server" class="error"></small>
										</div>
									</dd>
								</dl>
								<% } %>
								<% if (this.OrderModel.Shippings[0].ShippingReceivingStoreFlg == Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_OFF) { %>
								<dl class="ohDetail_box borderT0 borderB0" runat="server" visible="<%# this.DisplayScheduledShippingDate %>">
									<dt>出荷予定日</dt>
									<dd>
										<%#: DateTimeUtility.ToStringFromRegion(GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SCHEDULED_SHIPPING_DATE), DateTimeUtility.FormatType.LongDateWeekOfDay2Letter, ReplaceTag("@@DispText.shipping_date_list.none@@")) %>
									</dd>
								</dl>
								<% } %>
								<% if (this.UseShippingAddress) { %>
								<dl class="ohDetail_box borderT0 borderB0" runat="server" visible='<%# (string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_DELIVERYCOMPANY_SHIPPING_TIME_SET_FLG) == Constants.FLG_DELIVERYCOMPANY_SHIPPING_TIME_SET_FLG_VALID %>'>
									<dt>配送希望時間帯</dt>
									<dd>
										<div class="shippingSelect">
											<asp:DropDownList ID="ddlShippingTimeList" Visible="false" runat="server" DataSource='<%# GetShippingTimeList((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_DELIVERY_COMPANY_ID)) %>' DataTextField="text" DataValueField="value" ></asp:DropDownList>
										</div>
										<div id="dvShippingTimeText"  runat="server">
											<%#: ((w2.Common.Util.Validator.IsNullEmpty(GetKeyValue(((Hashtable)Container.DataItem)["row"], "shipping_time_message")) == false)
												&& (StringUtility.ToEmpty(GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_RECEIVING_STORE_FLG)) == Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_OFF))
													? GetKeyValue(((Hashtable)Container.DataItem)["row"], "shipping_time_message")
													: ReplaceTag("@@DispText.shipping_time_list.none@@") %>
										</div>
										<div style="display: none;" class="changeBtn">
											<asp:LinkButton CommandArgument="<%# Container.ItemIndex %>" Text="配送時間帯変更" runat="server" OnClick="lbDisplayInputShippingTime_Click" Enabled="<%# this.IsModifyShippingTimes[Container.ItemIndex] %>" />
										</div>
										<div><%#: this.ExplanationShippingTimes[Container.ItemIndex] %></div>
										<div id="trShippingTimeInput" visible='false'  runat="server">
											<div id="divShippingTimeUpdateButtons" style="display: block" class="updateLink"> 
												<asp:LinkButton Text="情報更新" runat="server" ValidationGroup="OrderShipping" CommandArgument="<%# Container.ItemIndex %>" OnClientClick="return AlertUpdateShippingTime(this);" OnClick="lbUpdateShippingTime_Click" ></asp:LinkButton>
												<asp:LinkButton Text="キャンセル" runat="server" CommandArgument="<%# Container.ItemIndex %>" OnClick="lbHideShippingTime_Click" />
												<input type="hidden" id="parentShippingTimeRepeater" name="parentShippingTimeRepeater" value="<%#: Container.UniqueID %>" />
											</div>
											<div id="divShippingTimeUpdateExecFroms" style="display: none"> 
												更新中です...
											</div>
											<small id="sErrorMessageShippingTime" runat="server" class="error"></small>
										</div>
									</dd>
								</dl>
								<% } %>
							</div>
							<tr visible='<%# (string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_CHECK_NO) != "" %>' runat="server">
								<th>配送伝票番号</th>
								<td>
									<%#: GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_CHECK_NO) %>
									<% if(Constants.RECEIVINGSTORE_TWPELICAN_CVSOPTION_ENABLED && IsShowCheckDeliveryStatus()) { %>
										<a href="javascript:openSiteMapPopup('<%# StringUtility.ToEmpty(GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_SHIPPING_CHECK_NO)) %>');" class="btn">配送状況確認</a>
									<% } %>
								</td>
							</tr>
							<tbody visible='<%# Constants.GIFTORDER_OPTION_ENABLED %>' runat="server">
							<tbody visible='<%# StringUtility.ToEmpty(this.OrderModel.GiftFlg) == Constants.FLG_ORDER_GIFT_FLG_ON %>' runat="server">
							<tr visible='<%# StringUtility.ToEmpty(this.ShopShippingModel.WrappingPaperFlg) == Constants.FLG_SHOPSHIPPING_WRAPPING_PAPER_FLG_VALID %>' runat="server">
								<th>のし種類</th>
								<td>
									<%#: (string.IsNullOrEmpty((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_WRAPPING_PAPER_TYPE)) == false) ? WebSanitizer.HtmlEncodeChangeToBr(GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_WRAPPING_PAPER_TYPE)) : "なし" %>
								</td>
							</tr>
							<tr visible='<%# StringUtility.ToEmpty(this.ShopShippingModel.WrappingPaperFlg) == Constants.FLG_SHOPSHIPPING_WRAPPING_PAPER_FLG_VALID %>' runat="server">
								<th>のし差出人</th>
								<td>
									<%#: (string.IsNullOrEmpty((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_WRAPPING_PAPER_NAME)) == false) ? WebSanitizer.HtmlEncodeChangeToBr(GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_WRAPPING_PAPER_NAME)) : "なし" %>
								</td>
							</tr>
							<tr visible='<%# StringUtility.ToEmpty(this.ShopShippingModel.WrappingPaperFlg) == Constants.FLG_SHOPSHIPPING_WRAPPING_BAG_FLG_VALID %>' runat="server">
								<th>包装種類</th>
								<td>
									<%#: (string.IsNullOrEmpty((string)GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_WRAPPING_BAG_TYPE)) == false) ? WebSanitizer.HtmlEncodeChangeToBr(GetKeyValue(((Hashtable)Container.DataItem)["row"], Constants.FIELD_ORDERSHIPPING_WRAPPING_BAG_TYPE)) : "なし" %>
								</td>
							</tr>
							</tbody>
							</tbody>
							<% if (this.IsShowTaiwanOrderInvoiceInfo) { %>
								<dl class="ohDetail_box borderT0">
									<dt>発票番号</dt>
									<dd><%: this.TwOrderInvoiceModel.TwInvoiceNo %></dd>
									<dt>発票種類</dt>
									<dd>
										<%: ValueText.GetValueText(Constants.TABLE_TWORDERINVOICE, Constants.FIELD_TWORDERINVOICE_TW_UNIFORM_INVOICE, this.TwOrderInvoiceModel.TwUniformInvoice) %>
										<% if (this.TwOrderInvoiceModel.TwUniformInvoice == Constants.FLG_TW_UNIFORM_INVOICE_COMPANY) { %>
											<p style="padding-bottom: 5px; padding-top: 15px;">統一編号</p>
											<%: this.TwOrderInvoiceModel.TwUniformInvoiceOption1 %>
											<p style="padding-bottom: 5px; padding-top: 5px;">会社名</p>
											<%: this.TwOrderInvoiceModel.TwUniformInvoiceOption2 %>
										<% } %>
										<% if (this.TwOrderInvoiceModel.TwUniformInvoice == Constants.FLG_TW_UNIFORM_INVOICE_DONATE) { %>
											<p style="padding-bottom: 5px; padding-top: 15px;">寄付先コード</p>
											<%: this.TwOrderInvoiceModel.TwUniformInvoiceOption1 %>
										<% } %>
									</dd>
								<% if (this.TwOrderInvoiceModel.TwUniformInvoice == Constants.FLG_TW_UNIFORM_INVOICE_PERSONAL) { %>
										<dt>共通性載具</dt>
										<dd>
											<%: ValueText.GetValueText(Constants.TABLE_TWORDERINVOICE, Constants.FIELD_TWORDERINVOICE_TW_CARRY_TYPE, this.TwOrderInvoiceModel.TwCarryType) %>
											<br />
											<%: this.TwOrderInvoiceModel.TwCarryTypeOption %>
										</dd>
								<% } %>
								</dl>
							<% } %>
					</div>
					<%} %>
					
					<%-- 購入商品一覧 --%>
					<div class="dvOrderHistoryProduct">
						<asp:Repeater ID="rOrderShippingItem" DataSource='<%# ((Hashtable)Container.DataItem)["childs"] %>' Runat="server">
							<ItemTemplate>
								<%-- 通常商品 --%>
								<dl class="ohDetail_box borderT0" visible='<%# (((string)Eval(Constants.FIELD_ORDERITEM_PRODUCT_SET_ID)).Length == 0) %>' runat="server">
									<dt>
										商品名
									</dt>
									<dd>
										<%-- 一致する商品IDが現在も存在する場合、商品詳細ページへのリンクを表示する --%>
										<a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailVariationUrl(Container.DataItem)) %>' target="_blank" runat="server" Visible="<%# IsProductValid((DataRowView)Container.DataItem) %>">
											<%#: Eval(Constants.FIELD_ORDERITEM_PRODUCT_NAME) %>
										</a>
										<%# (IsProductValid((DataRowView)Container.DataItem) == false) ? WebSanitizer.HtmlEncode(Eval(Constants.FIELD_ORDERITEM_PRODUCT_NAME)) : ""%>
										[<%#: Eval(Constants.FIELD_ORDERITEM_VARIATION_ID) %>]
										<span visible='<%# (string)Eval(Constants.FIELD_ORDERITEM_PRODUCT_OPTION_TEXTS) != "" %>' runat="server">
											<%#: Eval(Constants.FIELD_ORDERITEM_PRODUCT_OPTION_TEXTS).ToString().Replace("　", "\r\n") %>
										</span>
										<small>
											<asp:Repeater DataSource='<%# this.OrderItemSerialKeys[((string)Eval(Constants.FIELD_ORDER_ORDER_ID)) + (Eval(Constants.FIELD_ORDERITEM_ORDER_ITEM_NO).ToString())] %>' runat="server">
											<ItemTemplate>
												<br />
												&nbsp;シリアルキー:&nbsp;<%# Eval(Constants.FIELD_SERIALKEY_SERIAL_KEY)%>
											</ItemTemplate>
											</asp:Repeater>
										</small>
									</dd>
									<dt>
										単価（<%#: this.ProductPriceTextPrefix %>）
									</dt>
									<dd>
										<%#: CurrencyManager.ToPrice(Eval(Constants.FIELD_ORDERITEM_PRODUCT_PRICE)) %>
									</dd>
									<dt>
										注文数
									</dt>
									<dd>
										<%#: StringUtility.ToNumeric(Eval(Constants.FIELD_ORDERITEM_ITEM_QUANTITY)) %>
									</dd>
									<dt>
										消費税率
									</dt>
									<dd>
										<%#: TaxCalculationUtility.GetTaxRateForDIsplay(Eval(Constants.FIELD_ORDERITEM_PRODUCT_TAX_RATE)) %>%
									</dd>
									<dt>
										小計（<%#: this.ProductPriceTextPrefix %>）
									</dt>
									<dd visible='<%# (this.OrderModel.GiftFlg == Constants.FLG_ORDER_GIFT_FLG_ON) || (StringUtility.ToEmpty(Eval(Constants.FIELD_ORDERITEM_ORDER_SETPROMOTION_NO)) == "") %>' runat="server">
										<%#: CurrencyManager.ToPrice(Eval(Constants.FIELD_ORDERITEM_ITEM_PRICE)) %>
									</dd>
									<dd visible='<%# (this.OrderModel.GiftFlg == Constants.FLG_ORDER_GIFT_FLG_OFF) && (StringUtility.ToEmpty(Eval(Constants.FIELD_ORDERITEM_ORDER_SETPROMOTION_NO)) != "") && ((int)Eval(Constants.FIELD_ORDERITEM_ORDER_SETPROMOTION_ITEM_NO) == 1) %>' rowspan="<%# GetSetPromotionRowSpan(Container.DataItem, ((Repeater)Container.Parent).DataSource) %>" runat="server">
										<%#: Eval(Constants.FIELD_ORDERSETPROMOTION_SETPROMOTION_DISP_NAME) %><br />
										<span visible='<%# (StringUtility.ToEmpty(Eval(Constants.FIELD_ORDERITEM_ORDER_SETPROMOTION_NO)) != "") && ((string)Eval(Constants.FIELD_ORDERSETPROMOTION_PRODUCT_DISCOUNT_FLG) == Constants.FLG_SETPROMOTION_PRODUCT_DISCOUNT_FLG_ON) %>' runat="server">
											<strike><%#: CurrencyManager.ToPrice(Eval(Constants.FIELD_ORDERSETPROMOTION_UNDISCOUNTED_PRODUCT_SUBTOTAL)) %></strike><br />
										</span>
										<%#: CurrencyManager.ToPrice(((StringUtility.ToEmpty(Eval(Constants.FIELD_ORDERITEM_ORDER_SETPROMOTION_NO)) != "") ? (decimal)Eval(Constants.FIELD_ORDERSETPROMOTION_UNDISCOUNTED_PRODUCT_SUBTOTAL) : 0) - ((StringUtility.ToEmpty(Eval(Constants.FIELD_ORDERITEM_ORDER_SETPROMOTION_NO)) != "") ? (decimal)Eval(Constants.FIELD_ORDERSETPROMOTION_PRODUCT_DISCOUNT_AMOUNT) : 0)) %>(税込)
									</dd>
								</dl>

								<%-- セット商品 --%>
								<dl class="ohDetail_box borderT0" visible='<%# (((string)Eval(Constants.FIELD_ORDERITEM_PRODUCT_SET_ID)).Length != 0) %>' runat="server">
									<dt>
										商品名
									</dt>
									<dd>
										<%-- 一致する商品IDが現在も存在する場合、商品詳細ページへのリンクを表示する --%>
										<a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailVariationUrl(Container.DataItem)) %>' target="_blank" runat="server" Visible="<%# IsProductValid((DataRowView)Container.DataItem) %>">
											<%#: Eval(Constants.FIELD_ORDERITEM_PRODUCT_NAME) %>
										</a>
										<%# (IsProductValid((DataRowView)Container.DataItem) == false) ? WebSanitizer.HtmlEncode(Eval(Constants.FIELD_ORDERITEM_PRODUCT_NAME)) : ""%>
										<br />
										[<span class="productId"><%#: Eval(Constants.FIELD_ORDERITEM_VARIATION_ID) %></span>]
									</dd>
									<dt>
										単価（<%#: this.ProductPriceTextPrefix %>）
									</dt>
									<dd>
										<%#: CurrencyManager.ToPrice(Eval(Constants.FIELD_ORDERITEM_PRODUCT_PRICE)) %>
										x
										<%#: StringUtility.ToNumeric(Eval(Constants.FIELD_ORDERITEM_ITEM_QUANTITY_SINGLE)) %>
									</dd>
									<dt>
										注文数
									</dt>
									<dd rowspan='<%# GetProductSetRowspan(Container.DataItem, ((Repeater)Container.Parent).DataSource) %>' visible='<%# IsProductSetItemTop(Container.DataItem, ((Repeater)Container.Parent).DataSource) %>' runat="server">
										<%#: StringUtility.ToNumeric(Eval(Constants.FIELD_ORDERITEM_PRODUCT_SET_COUNT)) %>
									</dd>
									<dt>
										消費税率
									</dt>
									<dd>
										<%#: TaxCalculationUtility.GetTaxRateForDIsplay(Eval(Constants.FIELD_ORDERITEM_PRODUCT_TAX_RATE)) %>
									</dd>
									<dt>
										小計（<%#: this.ProductPriceTextPrefix %>）
									</dt>
									<dd rowspan='<%# GetProductSetRowspan(Container.DataItem, ((Repeater)Container.Parent).DataSource) %>' visible='<%# IsProductSetItemTop(Container.DataItem, ((Repeater)Container.Parent).DataSource) %>' runat="server">
										<%#: CurrencyManager.ToPrice(CreateSetPriceSubtotal(Container.DataItem, ((Repeater)Container.Parent).DataSource)) %>
									</dd>
								</dl>
							</ItemTemplate>
						</asp:Repeater>
					</div>
				</ItemTemplate>
			</asp:Repeater>
			<div class="cartTotal">
				<%-- 合計情報 --%>
				<div>
					<dl>
						<dt>商品合計</dt>
						<dd><%#: CurrencyManager.ToPrice(this.OrderModel.OrderPriceSubtotal) %></dd>
						<%if (this.ProductIncludedTaxFlg == false) { %>
							<dt>消費税</dt>
							<dd><%#: CurrencyManager.ToPrice(this.OrderModel.OrderPriceSubtotalTax) %></dd>
						<%} %>
						<asp:Repeater ID="rOrderSetPromotion" DataSource="<%# this.OrderSetPromotions %>" runat="server">
						<ItemTemplate>
							<span visible="<%# (string)((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_PRODUCT_DISCOUNT_FLG] == Constants.FLG_SETPROMOTION_PRODUCT_DISCOUNT_FLG_ON %>" runat="server">
							<dt>
							<%#: ((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_SETPROMOTION_DISP_NAME] %>
							</dt>
							<dd style="color: #ff0000;">
								<%#: (((decimal)((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_PRODUCT_DISCOUNT_AMOUNT] > 0) ? "-" : "") + CurrencyManager.ToPrice((decimal)((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_PRODUCT_DISCOUNT_AMOUNT]) %>
							</dd>
							</span>
							<asp:HiddenField ID="hfOrderSetPromotionPaymentChargeFreeFlg" Value="<%# (string)((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_PAYMENT_CHARGE_FREE_FLG] %>" runat="server" />
						</ItemTemplate>
						</asp:Repeater>

						<%-- 会員ランク情報リスト(有効な場合) --%>
						<%if (Constants.MEMBER_RANK_OPTION_ENABLED) { %>
							<dt style="display: none;" class="discountTxt">会員ランク割引額</dt>
							<dd style="display: none;" class="discountTxt"><span><%#: ((this.OrderModel.MemberRankDiscountPrice > 0) ? "-" : "") + CurrencyManager.ToPrice(this.OrderModel.MemberRankDiscountPrice) %></span></dd>
						<%} %>

						<%-- 定期会員割引額(有効な場合) --%>
						<%if (Constants.MEMBER_RANK_OPTION_ENABLED && Constants.FIXEDPURCHASE_OPTION_ENABLED) { %>
							<dt style="display: none;" class="discountTxt">定期会員割引額</dt>
							<dd style="display: none;" class="discountTxt"><span><%#: ((this.OrderModel.FixedPurchaseMemberDiscountAmount > 0) ? "-" : "") + CurrencyManager.ToPrice(this.OrderModel.FixedPurchaseMemberDiscountAmount) %></span></dd>
						<%} %>

						<%-- クーポン情報リスト(有効な場合) --%>
						<%if (Constants.W2MP_COUPON_OPTION_ENABLED) { %>
							<dt class="discountTxt">クーポン割引額</dt>
							<dd class="discountTxt"><span><%#: ((this.OrderModel.OrderCouponUse > 0) ? "-" : "") + CurrencyManager.ToPrice(this.OrderModel.OrderCouponUse) %></span></dd>
						<%} %>
						<%-- ポイント情報リスト(有効な場合) --%>
						<%if (Constants.W2MP_POINT_OPTION_ENABLED) { %>
							<dt class="discountTxt">ポイント利用額</dt>
							<dd class="discountTxt"><span><%#: ((this.OrderModel.OrderPointUseYen > 0) ? "-" : "") + CurrencyManager.ToPrice(this.OrderModel.OrderPointUseYen) %></span></dd>
						<%} %>
						<%-- 定期購入割引(有効な場合) --%>
						<%if (this.IsFixedPurchase) { %>
							<dt style="display: none;" class="discountTxt" runat="server">定期購入割引額</dt>
							<dd style="display: none;" class="discountTxt" runat="server">
								<span><%#: ((this.OrderModel.FixedPurchaseDiscountPrice > 0) ? "-" : "") + CurrencyManager.ToPrice(this.OrderModel.FixedPurchaseDiscountPrice) %></span>
							</dd>
						<%} %>
						<div visible='<%# (this.OrderModel.OrderPriceRegulation != 0) %>' runat="server">
							<dt class="discountTxt">調整金額</dt>
							<dd class="discountTxt"><span><%#: ((this.OrderModel.OrderPriceRegulation < 0) ? "-" : "") %><%#: CurrencyManager.ToPrice(Math.Abs(this.OrderModel.OrderPriceRegulation)) %></span></dd>
						</div>
						<dt>配送料金</dt>
						<dd runat="server" style='<%# (this.OrderModel.ShippingPriceSeparateEstimatesFlg == Constants.FLG_SHOPSHIPPING_SHIPPING_PRICE_SEPARATE_ESTIMATES_FLG_VALID) ? "display:none;" : "" %>'>
							<%#: CurrencyManager.ToPrice(this.OrderModel.OrderPriceShipping) %></dd>
						<dd runat="server" style='<%# (this.OrderModel.ShippingPriceSeparateEstimatesFlg == Constants.FLG_SHOPSHIPPING_SHIPPING_PRICE_SEPARATE_ESTIMATES_FLG_INVALID) ? "display:none;" : "" %>'>
							<%#: this.ShopShippingModel.ShippingPriceSeparateEstimatesMessage %></dd>
						<asp:Repeater DataSource="<%# this.OrderSetPromotions %>" runat="server">
						<ItemTemplate>
							<span visible="<%# (string)((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_SHIPPING_CHARGE_FREE_FLG] == Constants.FLG_SETPROMOTION_SHIPPING_CHARGE_FREE_FLG_ON %>" runat="server">
							<dt>
							<%#: ((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_SETPROMOTION_DISP_NAME] %>(送料割引)
							</dt>
							<dd style="color: #ff0000;">
								<%#: (((decimal)((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_SHIPPING_CHARGE_DISCOUNT_AMOUNT] > 0) ? "-" : "") + CurrencyManager.ToPrice((decimal)((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_SHIPPING_CHARGE_DISCOUNT_AMOUNT]) %>
							</dd>
							</span>
						</ItemTemplate>
						</asp:Repeater>
						<dt>決済手数料</dt>
						<dd><%#: CurrencyManager.ToPrice(this.OrderModel.OrderPriceExchange) %></dd>
						<asp:Repeater DataSource="<%# this.OrderSetPromotions %>" runat="server">
						<ItemTemplate>
							<span visible="<%# (string)((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_PAYMENT_CHARGE_FREE_FLG] == Constants.FLG_SETPROMOTION_PAYMENT_CHARGE_FREE_FLG_ON %>" runat="server">
							<dt>
							<%#: ((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_SETPROMOTION_DISP_NAME] %>(決済手数料割引)
							</dt>
							<dd style="color: #ff0000;">
								<%#: (((decimal)((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_PAYMENT_CHARGE_DISCOUNT_AMOUNT] > 0) ? "-" : "") + CurrencyManager.ToPrice((decimal)((Hashtable)Container.DataItem)[Constants.FIELD_ORDERSETPROMOTION_PAYMENT_CHARGE_DISCOUNT_AMOUNT]) %>
							</dd>
							</span>
						</ItemTemplate>
						</asp:Repeater>
					</dl>
					<dl class="all">
						<dt>総合計</dt>
						<dd><%#: CurrencyManager.ToPrice(this.OrderModel.OrderPriceTotal) %></dd>
					</dl>
				</div>
			</div>
			</div>
			
			<div class="cartNextbtn one">
				<ul>
					<li class="prevBtn">
						<a href="javascript:history.back()">前のページに戻る</a>
					</li>
				</ul>
			</div>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					購入履歴詳細
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
<asp:HiddenField ID="hfTotalPrice" runat="server" />
<asp:HiddenField ID="hfPaymentNameSelected" runat="server" />
<asp:HiddenField ID="hfPaymentIdSelected" runat="server" />
<asp:HiddenField ID="hfPaymentTotalPriceNew" runat="server" />
<asp:HiddenField ID="hfShippingTotalPriceNew" runat="server" />
<asp:HiddenField ID="hfConfirmSenderId" runat="server"/>
<asp:HiddenField ID="hfIsCheckFixedPurchaseFirstTime" runat="server"/>
<asp:HiddenField ID="hfFixedPurchasePaymentId" Value="<%# this.FixedPurchaseModel.OrderPaymentKbn %>" runat="server"/>
<% if (Constants.PAYMENT_LINEPAY_OPTION_ENABLED) { %>
<asp:HiddenField ID="hfLinePayTransactionId" runat="server" />
<% } %>
</ContentTemplate>
</asp:UpdatePanel>
<%-- UpdatePanel終了 --%>

<script type="text/javascript">
	<%-- UpdataPanelの更新時のみ処理を行う --%>
	function bodyPageLoad() {
		if (Sys.WebForms == null) return;
		var isAsyncPostback = Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack();
		if (isAsyncPostback) {
			bindEvent();

			if (document.getElementById("<%= hfConfirmSenderId.ClientID %>").value != "") {
				document.getElementById('divOrderShippingUpdateButtons').style.display = "none";
				document.getElementById('divOrderShippingUpdateExecFroms').style.display = "block";
				if (confirm('<%: WebMessages.GetMessages(WebMessages.ERRMSG_FRONT_NOT_FIXED_PRODUCT_ORDER_LIMIT) %>' + "\nよろしいですか？")) {
					__doPostBack(document.getElementById("<%= hfConfirmSenderId.ClientID %>").value, "");
				} else {
					document.getElementById("<%= hfConfirmSenderId.ClientID %>").value = "";
					document.getElementById("<%= hfIsCheckFixedPurchaseFirstTime.ClientID %>").value = "";
					document.getElementById('divOrderShippingUpdateButtons').style.display = "block";
					document.getElementById('divOrderShippingUpdateExecFroms').style.display = "none";
				}
			}
		}
	}

	<%-- イベントをバインドする --%>
	function bindEvent() {
		bindExecAutoKana();
		bindZipCodeSearch();
		<% if(Constants.GLOBAL_OPTION_ENABLE) { %>
		bindTwAddressSearch();
		<% } %>
	}

	<%-- 氏名（姓・名）の自動振り仮名変換のイベントをバインドする --%>
	function bindExecAutoKana() {
		<% foreach (RepeaterItem ri in rOrderShipping.Items)  { %>
		execAutoKanaWithKanaType(
			$('#<%= ((TextBox)ri.FindControl("tbShippingName1")).ClientID %>'),
			$('#<%= ((TextBox)ri.FindControl("tbShippingNameKana1")).ClientID %>'),
			$('#<%= ((TextBox)ri.FindControl("tbShippingName2")).ClientID %>'),
			$('#<%= ((TextBox)ri.FindControl("tbShippingNameKana2")).ClientID %>'));
		<%} %>
	}

	function ValidateAndConfirm(validationGroup, message) {
		if (typeof (Page_ClientValidate) != 'function' || window.Page_ClientValidate(validationGroup)) {
			return confirm(message);
		}
		else {
			return false;
		}
	}

	function Validate(validationGroup) {
		if (typeof (Page_ClientValidate) != 'function' || window.Page_ClientValidate(validationGroup)) {
			return true;
		}
		else {
			return false;
		}
	}

	//お支払い方法変更時の確認フォーム
	function AlertPaymentChange(priceTotalOld, priceTotalNew, showMessage) {
		var messagePayment;
		
		messagePayment = 'お支払方法を「' + document.getElementById("<%= hfPaymentNameSelected.ClientID %>").value + '」に変更します。';
		if (showMessage) {
			messagePayment += '\nまた、お支払い方法変更に伴い、請求金額が変更されます。\n\n'
				+ '    変更前の請求金額：' + priceTotalOld + '\n'
				+ '    変更後の請求金額：' + priceTotalNew + '\n\n';
		}

		// 領収書情報が削除される場合、アラート追加
		<% if (Constants.RECEIPT_OPTION_ENABLED && (this.OrderModel.ReceiptFlg == Constants.FLG_ORDER_RECEIPT_FLG_ON)) { %>
		if ("<%= string.Join(",", Constants.NOT_OUTPUT_RECEIPT_PAYMENT_KBN) %>".indexOf(document.getElementById("<%= hfPaymentIdSelected.ClientID %>").value) !== -1)
		{
			messagePayment += '\n指定したお支払い方法は、領収書の発行ができません。\n'
				+ '保存されている「領収書情報」が削除されます。\n\n';
		}
		<% } %>

		messagePayment += 'よろしいですか？\n\n';

		<% if (this.IsFixedPurchase && (this.FixedPurchaseModel.IsCancelFixedPurchaseStatus == false)) { %>
		if (document.getElementById("<%: cbIsUpdateFixedPurchaseByOrderPayment.ClientID %>").checked)
		{
			messagePayment += "この設定は今後の定期注文につきましても反映されます"
		}
		<% } %>
		var exec = ValidateAndConfirm("OrderPayment", messagePayment);
		if (exec) {
			document.getElementById('divOrderPaymentUpdateButtons').style.display = "none";
			document.getElementById('divOrderPaymentUpdateExecFroms').style.display = "block";
			document.getElementById('<%= sErrorMessagePayment.ClientID %>').style.display = "none";

			onErrorScript = "document.getElementById('divOrderPaymentUpdateButtons').style.display = 'block';"
				+ "document.getElementById('divOrderPaymentUpdateExecFroms').style.display = 'none';"
				+ "document.getElementById('<%= sErrorMessagePayment.ClientID %>').style.display = 'block';";
		}
		return exec;
	}

	//利用ポイント変更時の確認フォーム
	function AlertUpdateOrderPointUse(priceTotalOld, priceTotalNew, showMessage) {
		var messagePoint;

		messagePoint = 'ご利用ポイントを下記に変更します。\n\n    ご利用ポイント： ' + Separate(parseFloat(document.getElementById("<%= tbOrderPointUse.ClientID %>").value)) + '<%: Constants.CONST_UNIT_POINT_PT %>\n\n';
		if (showMessage) {
			messagePoint += 'また、ご利用ポイントの変更に伴い、請求金額が変更されます。\n\n'
				+ '    変更前の請求金額：' + priceTotalOld + '\n'
				+ '    変更後の請求金額：' + priceTotalNew + '\n\n';
		}
		messagePoint += 'よろしいですか？';

		var exec = ValidateAndConfirm("", messagePoint);
		if (exec) {
			document.getElementById('divOrderPointUpdateButtons').style.display = "none";
			document.getElementById('divOrderPointUpdateExecFroms').style.display = "block";
			document.getElementById('<%= slErrorMessagePointUse.ClientID %>').style.display = "none";
		}
		return exec;
	}

	//数値にカンマを付与
	function Separate(num) {
		return String(num).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
	}

	//お届け先変更時の確認フォーム
	function AlertUpdateShippingInfo(priceTotalOld, priceTotalNew, showMessage, e) {
		var parentRepeaterId = $(e.parentNode).children('#parentShippingRepeater').val().replace(/\$/g, '_');
		var tbShippingName1 = $("#" + parentRepeaterId + "_" + "tbShippingName1");
		var tbShippingName2 = $("#" + parentRepeaterId + "_" + "tbShippingName2");
		var tbShippingZip1 = $("#" + parentRepeaterId + "_" + "tbShippingZip1");
		var tbShippingZip2 = $("#" + parentRepeaterId + "_" + "tbShippingZip2");
		var tbShippingZipGlobal = $("#" + parentRepeaterId + "_" + "tbShippingZipGlobal");
		var ddlShippingCountry = $("#" + parentRepeaterId + "_" + "ddlShippingCountry");
		var ddlShippingAddr1 = $("#" + parentRepeaterId + "_" + "ddlShippingAddr1");
		var tbShippingAddr2 = $("#" + parentRepeaterId + "_" + "tbShippingAddr2");
		var tbShippingAddr3 = $("#" + parentRepeaterId + "_" + "tbShippingAddr3");
		var tbShippingAddr4 = $("#" + parentRepeaterId + "_" + "tbShippingAddr4");
		var tbShippingAddr5 = $("#" + parentRepeaterId + "_" + "tbShippingAddr5");
		var ddlShippingAddr5 = $("#" + parentRepeaterId + "_" + "ddlShippingAddr5");
		var tbShippingTel1_1 = $("#" + parentRepeaterId + "_" + "tbShippingTel1_1");
		var tbShippingTel1_2 = $("#" + parentRepeaterId + "_" + "tbShippingTel1_2");
		var tbShippingTel1_3 = $("#" + parentRepeaterId + "_" + "tbShippingTel1_3");
		var tbShippingTel1Global = $("#" + parentRepeaterId + "_" + "tbShippingTel1Global");
		var ddlShippingAddr2 = $("#" + parentRepeaterId + "_" + "ddlShippingAddr2");
		var ddlShippingAddr3 = $("#" + parentRepeaterId + "_" + "ddlShippingAddr3");
		var ddlShippingType = $("#" + parentRepeaterId + "_" + "ddlShippingType");
		<% if (Constants.RECEIVINGSTORE_TWPELICAN_CVSOPTION_ENABLED) { %>
		var isShopValid = false;
		var isShippingConvenience = ($("#" + parentRepeaterId + "_" + "hfCvsShopFlg").val() == '<%= Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_ON %>');
		if (isShippingConvenience) {
			var hfCvsShopId = $("#" + parentRepeaterId + "_" + "hfCvsShopId");
			var hfSelectedShopId = $("#" + parentRepeaterId + "_" + "hfSelectedShopId");
			var shopId = hfCvsShopId.val();
			if (shopId == undefined) {
				shopId = hfSelectedShopId.val();
			}
			var dvErrorShippingConvenience = $("#" + parentRepeaterId + "_" + "dvErrorShippingConvenience");
			$.ajax({
				type: "POST",
				url: "<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_ORDER_HISTORY_DETAIL %>/CheckStoreIdValid",
				data: JSON.stringify({ storeId: shopId }),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				cache: false,
				async: false,
				success: function (data) {
					if (data.d) {
						isShopValid = true;
						dvErrorShippingConvenience.css("display", "none");
					}
					else {
						dvErrorShippingConvenience.css("display", "");
					}
				}
			});

			return isShopValid;
		}
		<% } %>

		var shippingCountryName = "";
		var shippingAddr1 = "";
		var shippingAddr5 = "";
		var shippingZip = "";
		var shippingTel = "";
		var validateName = "OrderShipping";
		var shippingAddr2 = tbShippingAddr2.val();
		var shippingAddr3 = tbShippingAddr3.val();
		var shippingName = "";
		var shippingAddr = "";

		<% if (Constants.GLOBAL_OPTION_ENABLE) { %>
		shippingCountryName = ddlShippingCountry.children("option:selected").text();

			if (ddlShippingCountry.val() != "<%= Constants.COUNTRY_ISO_CODE_JP %>") {
				shippingAddr2 = (ddlShippingCountry.val() == "<%= Constants.COUNTRY_ISO_CODE_TW %>")
					? ddlShippingAddr2.val()
					: tbShippingAddr2.val();
				shippingAddr3 = (ddlShippingCountry.val() == "<%= Constants.COUNTRY_ISO_CODE_TW %>")
					? ddlShippingAddr3.find('option:selected').text()
					: tbShippingAddr3.val();
				shippingAddr5 = (ddlShippingCountry.val() == "<%= Constants.COUNTRY_ISO_CODE_US %>")
					? ddlShippingAddr5.val()
					: tbShippingAddr5.val();

				shippingZip = tbShippingZipGlobal.val();
				shippingTel = tbShippingTel1Global.val();
				shippingAddr1 = "";

				validateName = "OrderShippingGlobal";

			} else {
				shippingZip = tbShippingZip1.val() + '-' + tbShippingZip2.val();
				shippingTel = tbShippingTel1_1.val() + '-' + tbShippingTel1_2.val() + '-' + tbShippingTel1_3.val();
				shippingAddr1 = ddlShippingAddr1.val();
			}

		<% } else { %>
			shippingZip = tbShippingZip1.val() + '-' + tbShippingZip2.val();
			shippingTel = tbShippingTel1_1.val() + '-' + tbShippingTel1_2.val() + '-' + tbShippingTel1_3.val();
			shippingAddr1 = ddlShippingAddr1.val();
		<% } %>

		var messageShipping;
		if (ddlShippingType.val() === 'OWNER') {
			shippingName = '<%#: this.LoginUser.Name %>';
			shippingZip = '<%#: this.LoginUser.Zip %>';
			shippingAddr = '<%#: this.LoginUser.Addr1 %>' +
				' ' +
				'<%#: this.LoginUser.Addr2 %>' +
				' ' +
				'<%#: this.LoginUser.Addr3 %>' +
				'\n' +
				' ' +
				'<%#: this.LoginUser.Addr4%>' +
				' ' +
				'<%#: this.LoginUser.Addr5 %>' +
				' ' +
				'<%#: this.LoginUser.AddrCountryName %>';
			shippingTel = '<%#: this.LoginUser.Tel1 %>';
		} else {
			shippingName = tbShippingName1.val() + ' ' + tbShippingName2.val();
			shippingAddr = shippingAddr1 +
				' ' +
				shippingAddr2 +
				' ' +
				shippingAddr3 +
				'\n' +
				' ' +
				tbShippingAddr4.val() +
				' ' +
				shippingAddr5 +
				' ' +
				shippingCountryName;
		}
		messageShipping = 'お届け先を下記に変更します。\n\n'
			+ '    <%: ReplaceTag("@@User.name.name@@") %>： ' + shippingName + '\n'
			+ '    <%: ReplaceTag("@@User.zip.name@@") %>： ' + shippingZip + '\n'
			+ '    <%: ReplaceTag("@@User.addr.name@@") %>： ' + shippingAddr + '\n';

		<% if (Constants.DISPLAY_CORPORATION_ENABLED) { %>
		var tbShippingCompanyName = $("#" + parentRepeaterId + "_" + "tbShippingCompanyName");
		var tbShippingCompanyPostName = $("#" + parentRepeaterId + "_" + "tbShippingCompanyPostName");
		var shippingCompanyName = tbShippingCompanyName.val();
		var shippingCompanyPostName = tbShippingCompanyPostName.val();

		if (ddlShippingType.val() === 'OWNER') {
			shippingCompanyName = '<%#: this.LoginUser.CompanyName %>';
			shippingCompanyPostName = '<%#: this.LoginUser.CompanyPostName %>';
		}

		messageShipping += '    <%: ReplaceTag("@@User.company_name.name@@") %>： ' + shippingCompanyName + '\n'
			+ '    <%: ReplaceTag("@@User.company_post_name.name@@") %>： ' + shippingCompanyPostName + '\n';
		<% } %>

		messageShipping += '    <%: ReplaceTag("@@User.tel1.name@@") %>： ' + shippingTel + '\n\n';

		if (showMessage) {
			messageShipping += 'また、お届け先変更に伴い、請求金額が変更されます。\n\n'
				+ ' 変更前の請求金額：' + priceTotalOld + '\n'
				+ ' 変更後の請求金額：' + priceTotalNew + '\n\n';
		}

		messageShipping += 'よろしいですか？\n\n';

		<% if (this.IsFixedPurchase && (this.FixedPurchaseModel.IsCancelFixedPurchaseStatus == false)) { %>
		if (document.getElementById("<%#: rOrderShipping.Items[0].FindControl("cbIsUpdateFixedPurchaseByOrderShippingInfo").ClientID %>").checked) {
			messageShipping += "この設定は今後の定期注文につきましても反映されます"
		}
		<% } %>

		var exec = true;
		var execUs = true;
		if ((ddlShippingCountry.val() != "<%= Constants.COUNTRY_ISO_CODE_US %>")
			&& (ddlShippingCountry.val() != "<%= Constants.COUNTRY_ISO_CODE_TW %>"))
		{
			exec = ValidateAndConfirm(validateName, messageShipping);
		} else {
			exec = Validate(validateName);
			execUs = ValidateAndConfirm("OrderHistoryDetailGlobal", messageShipping);
		}

		if (exec && execUs) {
			document.getElementById('divOrderShippingUpdateButtons').style.display = "none";
			document.getElementById('divOrderShippingUpdateExecFroms').style.display = "block";
		}
		return (exec && execUs);
	}
	
	//配送希望日・時間帯変更時の確認フォーム
	function AlertUpdateShippingDate(e) {
		var parentRepeaterId = $(e.parentNode).children('#parentShippingDateRepeater').val().replace(/\$/g, '_');
		var ddlShippingDateList = $("#" + parentRepeaterId + "_" + "ddlShippingDateList option:selected");
		var messageShippingDate;
		messageShippingDate = '配送希望日を下記に変更します。\n\n'
			+ '    配送希望日： ' + ddlShippingDateList.text() + '\n\n';

		messageShippingDate += 'よろしいですか？';
		var exec = ValidateAndConfirm("OrderShipping", messageShippingDate);
		if (exec) {
			document.getElementById('divShippingDateUpdateButtons').style.display = "none";
			document.getElementById('divShippingDateUpdateExecFroms').style.display = "block";
		}
		return exec;
	}

	//配送希望日・時間帯変更時の確認フォーム
	function AlertUpdateShippingTime(e) {
		var parentRepeaterId = $(e.parentNode).children('#parentShippingTimeRepeater').val().replace(/\$/g, '_');
		var ddlShippingTimeList = $("#" + parentRepeaterId + "_" + "ddlShippingTimeList option:selected");
		var messageShippingTime;
		messageShippingTime = '配送希望時間帯を下記に変更します。\n\n'
			+ '    配送希望時間帯： ' + ddlShippingTimeList.text() + '\n\n';

		messageShippingTime += 'よろしいですか？';
		var exec = ValidateAndConfirm("OrderShipping", messageShippingTime);
		if (exec) {
			document.getElementById('divShippingTimeUpdateButtons').style.display = "none";
			document.getElementById('divShippingTimeUpdateExecFroms').style.display = "block";
		}
		return exec;
	}

	var bindTargetForAddr1 = "";
	var bindTargetForAddr2 = "";
	var bindTargetForAddr3 = "";
	var multiAddrsearchTriggerType = "";
	<%-- 郵便番号検索のイベントをバインドする --%>
	function bindZipCodeSearch() {
		<% foreach (RepeaterItem ri in rOrderShipping.Items) { %>
			$('#<%= ((TextBox)ri.FindControl("tbShippingZip2")).ClientID %>').keyup(function (e) {
				if (isValidKeyCodeForKeyEvent(e.keyCode) == false) return;
				checkZipCodeLengthAndExecPostback(
					$('#<%= ((TextBox)ri.FindControl("tbShippingZip1")).ClientID %>'),
					$('#<%= ((TextBox)ri.FindControl("tbShippingZip2")).ClientID %>'),
					"<%= ((TextBox)ri.FindControl("tbShippingZip2")).UniqueID %>",
					'<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_ZIPCODE_SEARCHER_GET_ADDR_JSON %>',
					"<%= '#' + (ri.FindControl("sShippingZipError")).ClientID %>"
				);
				bindTargetForAddr1 = "<%= ((DropDownList)ri.FindControl("ddlShippingAddr1")).ClientID %>";
				bindTargetForAddr2 = "<%= ((TextBox)ri.FindControl("tbShippingAddr2")).ClientID %>";
				bindTargetForAddr3 = "<%= ((TextBox)ri.FindControl("tbShippingAddr3")).ClientID %>";
				$("#search-result-layer").css("top", $(this).position().top + 20);
				$("#search-result-layer").css("left", $(this).position().left - 72);
				multiAddrsearchTriggerType = "shipping";
			});
			$('#<%= ((LinkButton)ri.FindControl("lbSearchShippingAddr")).ClientID %>').on('click', function () {
				checkZipCodeLengthAndExecPostback(
					$('#<%= ((TextBox)ri.FindControl("tbShippingZip1")).ClientID %>'),
					$('#<%= ((TextBox)ri.FindControl("tbShippingZip2")).ClientID %>'),
					"<%= ((LinkButton)ri.FindControl("lbSearchShippingAddr")).UniqueID %>",
					'<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_ZIPCODE_SEARCHER_GET_ADDR_JSON %>',
					"<%= '#' + (ri.FindControl("sShippingZipError")).ClientID %>"
				);
				bindTargetForAddr1 = "<%= ((DropDownList)ri.FindControl("ddlShippingAddr1")).ClientID %>";
				bindTargetForAddr2 = "<%= ((TextBox)ri.FindControl("tbShippingAddr2")).ClientID %>";
				bindTargetForAddr3 = "<%= ((TextBox)ri.FindControl("tbShippingAddr3")).ClientID %>";
				$("#search-result-layer").css("top", $(this).position().top + 23);
				$("#search-result-layer").css("left", $(this).position().left - 135);
				multiAddrsearchTriggerType = "shipping";
			});
		<%} %>
	}

	$(document).on('click', '.search-result-layer-close', function () {
		closePopupAndLayer();
	});

	$(document).on('click', '.search-result-layer-addr', function (e) {
		bindSelectedAddr($('li.search-result-layer-addr').index(this), multiAddrsearchTriggerType);
	});

	<%-- 複数住所検索結果からの選択値を入力フォームにバインドする --%>
	function bindSelectedAddr(selectedIndex, multiAddrsearchTriggerType) {
		var selectedAddr = $('.search-result-layer-addrs li').eq(selectedIndex);
		if (multiAddrsearchTriggerType == "shipping") {
			<% foreach (RepeaterItem ri in rOrderShipping.Items) { %>
			$('#' + bindTargetForAddr1).val(selectedAddr.find('.addr').text());
			$('#' + bindTargetForAddr2).val(selectedAddr.find('.city').text() + selectedAddr.find('.town').text());
			$('#' + bindTargetForAddr3).focus();
			<%} %>
		}
		closePopupAndLayer();
	}

	<%-- 変更後の表示通貨価格取得 --%>
	function AlertDataChange(kbnName, e) {
		var priceTotalOld = parseFloat(document.getElementById("<%= hfTotalPrice.ClientID %>").value);
		var priceTotalOldStr = '<%= CurrencyManager.ToPrice(hfTotalPrice.Value) %>';
		var priceTotalNew = '';
		var orderUsePointNew = 0;

		switch (kbnName) {
			case "Payment":
				priceTotalNew = parseFloat(document.getElementById("<%= hfPaymentTotalPriceNew.ClientID %>").value);
				break;

			case "OrderPointUse":
				orderUsePointNew = parseFloat(document.getElementById("<%= tbOrderPointUse.ClientID %>").value);
				priceTotalNew = priceTotalOld + <%#: this.OrderModel.OrderPointUse %> -orderUsePointNew;
				if (isNaN(orderUsePointNew) || (orderUsePointNew < 0)) return;
				break;

			case "Shipping":
				priceTotalNew = parseFloat(document.getElementById("<%= hfShippingTotalPriceNew.ClientID %>").value);
				break;

			default:
				return;
		}

		if (isNaN(priceTotalOld) || isNaN(priceTotalNew) || (priceTotalNew < 0)) return;
		

		var showMessage = (priceTotalOld != priceTotalNew);

		var exec = false;
		$.ajax({
			type: "POST",
			url: "<%= Constants.PATH_ROOT %>Form/OrderHistory/OrderHistoryDetail.aspx/GetPriceString",
			data: JSON.stringify({ price: priceTotalNew }),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			cache: false,
			async: false,
			success: function(data) {
				switch (kbnName) {
					case "Payment":
						exec = AlertPaymentChange(priceTotalOldStr, data.d, showMessage);
						<% if (Constants.PAYMENT_PAIDY_OPTION_ENABLED) { %>
						if (exec
							&& ($('#<%= hfPaymentIdSelected.ClientID %>').val() == '<%= Constants.FLG_PAYMENT_PAYMENT_ID_PAIDY %>')) {
							exec = false;

							// Paidy Pay Process
							PaidyPayProcess();
						}
						<% } %>
						break;

					case "OrderPointUse":
						exec = AlertUpdateOrderPointUse(priceTotalOldStr, data.d, showMessage);
						break;

					case "Shipping":
						exec = AlertUpdateShippingInfo(priceTotalOldStr, data.d, showMessage, e);
						break;

					default:
						return;
				}
			}
		});
		return exec;
	}

	<%-- Check Payment Change --%>
	function CheckPaymentChange(element) {
		completeButton = element;

		if ((typeof isAuthoriesAtone === "boolean") && isAuthoriesAtone) {
			document.getElementById('divOrderPaymentUpdateButtons').style.display = "none";
			document.getElementById('divOrderPaymentUpdateExecFroms').style.display = "block";

			return isAuthoriesAtone;
		}
		else
		{
			var spanErrorMessageForAtone = document.getElementsByClassName('spanErrorMessageForAtone');
			if ((spanErrorMessageForAtone.length > 0)
				&& (spanErrorMessageForAtone[0].style.display == "block"))
			{
				return false;
			}
		}

		if ((typeof isAuthoriesAftee === "boolean") && isAuthoriesAftee) {
			document.getElementById('divOrderPaymentUpdateButtons').style.display = "none";
			document.getElementById('divOrderPaymentUpdateExecFroms').style.display = "block";

			return isAuthoriesAftee;
		}
		else
		{
			var spanErrorMessageForAftee = document.getElementsByClassName('spanErrorMessageForAftee');
			if ((spanErrorMessageForAftee.length > 0)
				&& (spanErrorMessageForAftee[0].style.display == "block")) {
				return false;
			}
		}

		if ((typeof isAuthories === "boolean") && isAuthories) {
			document.getElementById('divOrderPaymentUpdateButtons').style.display = "none";
			document.getElementById('divOrderPaymentUpdateExecFroms').style.display = "block";

			return isAuthories;
		}

		var result = AlertDataChange('Payment', element);
		<% if(Constants.PAYMENT_ATONEOPTION_ENABLED) { %>
		if (result
			&& ($('#<%= hfPaymentIdSelected.ClientID %>').val() == '<%= Constants.FLG_PAYMENT_PAYMENT_ID_ATONE %>')
			&& ('<%= this.OrderModel.OrderPaymentKbn %>' != '<%= Constants.FLG_PAYMENT_PAYMENT_ID_ATONE %>')) {
			AtoneAuthoriesForMyPage('<%# this.OrderModel.OrderId %>', $('#<%= hfPaymentIdSelected.ClientID %>').val(), element);
			document.getElementById('divOrderPaymentUpdateButtons').style.display = "block";
			document.getElementById('divOrderPaymentUpdateExecFroms').style.display = "none";

			return false;
		}
		<% } %>

		<% if (Constants.PAYMENT_AFTEEOPTION_ENABLED) { %>
		if (result
			&& ($('#<%= hfPaymentIdSelected.ClientID %>').val() == '<%= Constants.FLG_PAYMENT_PAYMENT_ID_AFTEE %>')
			&& ('<%= this.OrderModel.OrderPaymentKbn %>' != '<%= Constants.FLG_PAYMENT_PAYMENT_ID_AFTEE %>')) {
			AfteeAuthoriesForMyPage('<%# this.OrderModel.OrderId %>', $('#<%= hfPaymentIdSelected.ClientID %>').val(), element);
			document.getElementById('divOrderPaymentUpdateButtons').style.display = "block";
			document.getElementById('divOrderPaymentUpdateExecFroms').style.display = "none";

			return false;
		}
		<% } %>

		<% if (Constants.PAYMENT_LINEPAY_OPTION_ENABLED) { %>
		if (result
			&& ($('#<%= hfPaymentIdSelected.ClientID %>').val() == '<%= Constants.FLG_PAYMENT_PAYMENT_ID_LINEPAY %>')
			&& ('<%= this.OrderModel.OrderPaymentKbn %>' != '<%= Constants.FLG_PAYMENT_PAYMENT_ID_LINEPAY %>')) {
			ExecRequestPaymentMyPage('<%= StringUtility.ToEmpty(Request[Constants.REQUEST_KEY_ORDER_ID]) %>', $('#<%= hfPaymentIdSelected.ClientID %>').val());
			document.getElementById('divOrderPaymentUpdateButtons').style.display = "block";
			document.getElementById('divOrderPaymentUpdateExecFroms').style.display = "none";

			return false;
		}
		<% } %>

		return result;
	}

	<% if(Constants.GLOBAL_OPTION_ENABLE) { %>
	<%-- 台湾郵便番号取得関数 --%>
	function bindTwAddressSearch() {
		<% foreach (RepeaterItem item in rOrderShipping.Items) { %>
			<% if (((DropDownList)item.FindControl("ddlShippingAddr3") != null) && ((TextBox)item.FindControl("tbShippingZipGlobal") != null)) { %>
			$('#<%= ((DropDownList)item.FindControl("ddlShippingAddr3")).ClientID %>').change(function (e) {
				$('#<%= ((TextBox)item.FindControl("tbShippingZipGlobal")).ClientID %>').val(
					$('#<%= ((DropDownList)item.FindControl("ddlShippingAddr3")).ClientID %>').val().split('|')[0]);
			});
			<% } %>
		<% } %>
	}
	<% } %>

	<% if(Constants.RECEIVINGSTORE_TWPELICAN_CVSOPTION_ENABLED) { %>
		<%-- Open convenience store map popup --%>
	function openConvenienceStoreMapPopup(cartIndex) {
		selectedCartIndex = cartIndex;
		currentRepeaterItemId = '<%= string.Join(",", this.WrOrderShipping.Items.Cast<RepeaterItem>().Select(item => item.ClientID)) %>';
			currentRepeaterItemId = currentRepeaterItemId.split(",")[selectedCartIndex];

			var url = '<%= OrderCommon.CreateConvenienceStoreMapUrl() %>';
			window.open(url, "", "width=1000,height=800");
		}

		<%-- Set convenience store data --%>
	function setConvenienceStoreData(cvsspot, name, addr, tel) {
		var elements = document.getElementsByClassName(selectedCartIndex)[0];
		if (elements == undefined || elements == null) {
			var seletedShippingNo = $('.UserShippingAddress').val();
			var className = '.user_addres' + seletedShippingNo;
			elements = $(className)[0];
			var hfSelectedShopId = $("#" + currentRepeaterItemId + "_" + "hfSelectedShopId");
			hfSelectedShopId.val(cvsspot);
		}

		// For display
		elements.querySelector('[id$="ddCvsShopId"] > span').innerHTML = cvsspot;
		elements.querySelector('[id$="ddCvsShopName"] > span').innerHTML = name;
		elements.querySelector('[id$="ddCvsShopAddress"] > span').innerHTML = addr;
		elements.querySelector('[id$="ddCvsShopTel"] > span').innerHTML = tel;

		// For get value
		elements.querySelector('[id$="hfCvsShopId"]').value = cvsspot;
		elements.querySelector('[id$="hfCvsShopName"]').value = name;
		elements.querySelector('[id$="hfCvsShopAddress"]').value = addr;
		elements.querySelector('[id$="hfCvsShopTel"]').value = tel;
	}

		<%-- Open site map popup --%>
	function openSiteMapPopup(shippingCheckNo) {
		var url = "http://query2.e-can.com.tw/self_link/id_link.asp?txtMainID=" + shippingCheckNo;
		window.open(url, "", "width=850,height=500");
	}
	<% } %>
</script>
<% if (Constants.PAYMENT_LINEPAY_OPTION_ENABLED) { %>
	<uc:LinePaymentScript ID="ucLinePaymentScript" runat="server" />
	<script type="text/javascript">
		var completeButton = null;
		function SetTransactionIdLinePayment(id) {
			$('#<%= hfLinePayTransactionId.ClientID %>').val(id);
		}
	</script>
<% } %>

<%--▼▼ クレジットカードToken用スクリプト ▼▼--%>
<script type="text/javascript">
	var getTokenAndSetToFormJs = "<%= CreateGetCreditTokenAndSetToFormJsScript().Replace("\"", "\\\"") %>";
	var maskFormsForTokenJs = "<%= CreateMaskFormsForCreditTokenJsScript().Replace("\"", "\\\"") %>";
	var isMyPage = true;
</script>
<uc:CreditToken runat="server" ID="CreditToken" />
<%--▲▲ クレジットカードToken用スクリプト ▲▲--%>

<%--▼▼ Paidy用スクリプト ▼▼--%>
<script type="text/javascript">
	var buyer = <%= PaidyUtility.CreatedBuyerDataObjectForPaidyPayment(this.LoginUserId) %>;
	var hfPaidyTokenIdControlId = "<%= this.WhfPaidyTokenId.ClientID %>";
	var hfPaidyPaySelectedControlId = "<%= this.WhfPaidyPaySelected.ClientID %>";
	var updatePaymentUniqueID = "<%= this.WlbUpdatePayment.UniqueID %>";
	var isHistoryPage = true;
</script>
<uc:PaidyCheckoutScript ID="ucPaidyCheckoutScript" runat="server" />
<%--▲▲ Paidy用スクリプト ▲▲--%>

<%--▼▼ Payment Atone And Aftee Script ▼▼--%>
<% if(Constants.PAYMENT_ATONEOPTION_ENABLED) { %>
	<script type="text/javascript">
		$('#<%= this.WhfAtoneToken.ClientID %>').val('<%= this.IsLoggedIn
			? this.LoginUser.UserExtend.UserExtendDataValue[Constants.FLG_USEREXTEND_USREX_ATONE_TOKEN_ID]
			: string.Empty %>');

		<%-- Set token --%>
		function SetAtoneTokenFromChildPage(token) {
			$('#<%= this.WhfAtoneToken.ClientID %>').val(token);
		}

		<%-- Get Current Token --%>
		function GetCurrentAtoneToken() {
			return $('#<%= this.WhfAtoneToken.ClientID %>').val();
		}

		<%-- Set Atone Transaction Id From My page --%>
		function SetAtoneTransactionIdFromMypage(id) {
			$('#<%= this.WhfAtoneTransactionId.ClientID %>').val(id);
		}
	</script>
	<% ucAtonePaymentScript.CurrentUrl = string.Format("{0}{1}",
			Constants.PATH_ROOT,
			string.Format("{0}{1}", this.IsSmartPhone
				? "SmartPhone/"
				: string.Empty, Constants.PAGE_FRONT_ORDER_HISTORY_DETAIL)); %>
	<uc:AtonePaymentScript ID="ucAtonePaymentScript" runat="server"/>
<% } %>

<% if(Constants.PAYMENT_AFTEEOPTION_ENABLED) { %>
	<script type="text/javascript">
		$('#<%= this.WhfAfteeToken.ClientID %>').val('<%= this.IsLoggedIn
			? this.LoginUser.UserExtend.UserExtendDataValue[Constants.FLG_USEREXTEND_USREX_AFTEE_TOKEN_ID]
			: string.Empty %>');

		<%-- Set token --%>
		function SetAfteeTokenFromChildPage(token) {
			$('#<%= this.WhfAfteeToken.ClientID %>').val(token);
		}

		<%-- Get Current Aftee token--%>
		function GetCurrentAfteeToken() {
			return $('#<%= this.WhfAfteeToken.ClientID %>').val();
		}

		<%-- Set Aftee Transaction Id From My page --%>
		function SetAfteeTransactionIdFromMypage(id) {
			$('#<%= this.WhfAfteeTransactionId.ClientID %>').val(id);
		}
	</script>
	<% ucAfteePaymentScript.CurrentUrl = string.Format("{0}{1}",
					Constants.PATH_ROOT,
					string.Format("{0}{1}", this.IsSmartPhone
						? "SmartPhone/"
						: string.Empty, Constants.PAGE_FRONT_ORDER_HISTORY_DETAIL)); %>
	<uc:AfteePaymentScript ID="ucAfteePaymentScript" runat="server"/>
<% } %>
<%--▲▲ Payment Atone And Aftee Script ▲▲--%>
</asp:Content>