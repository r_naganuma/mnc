﻿<%--
=========================================================================================================
  Module      : ログイン画面(Login.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_Login, App_Web_login.aspx.b129f0c2" title="ログインページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<%@ Register Src="~/Form/Common/PaypalScriptsForm.ascx" TagPrefix="uc" TagName="PaypalScriptsForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />
<article id="cart" class="cartListPage">

<!--▽ パンくず ▽-->
<div class="breadArea pc_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				ログイン
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

<!-- main -->
<section class="cartTtl second">
	<h2>
		ログイン
	</h2>
</section>
<!-- // main -->

<div id="dvMessages" runat="server" class="contentsInfo"><p><%= WebSanitizer.HtmlEncodeChangeToBr(this.ErrorMessage) %></p></div>
<div class="loginBox">
	<div class="loginBox_member">
		<h3>会員のお客様<span>ログインして、ご購入手続きへお進みください。</span></h3>
		<div class="loginBox_member--wrap">
			<dl class="cartInput">
				<%if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED) { %>
				<dd class="input">
					<p class="adTtl"><%: ReplaceTag("@@User.mail_addr.name@@") %></p>
					<asp:TextBox ID="tbLoginIdInMailAddr" Runat="server" CssClass="loginIdInMailAddr" PlaceHolder="xxx@simplisse.jp" MaxLength="256" Type="email"></asp:TextBox>
				</dd>
				<%} else { %>
				<dd class="input">
					<p class="adTtl"><%: ReplaceTag("@@User.login_id.name@@") %></p>
					<asp:TextBox ID="tbLoginId" Runat="server" CssClass="loginId" MaxLength="15"></asp:TextBox></dd>
				<%} %>
				<dd class="input">
					<p class="adTtl"><%: ReplaceTag("@@User.password.name@@") %></p>
					<asp:TextBox ID="tbPassword" Runat="server" TextMode="Password" autocomplete="off" CssClass="loginPass" MaxLength="15"></asp:TextBox></dd>
			</dl>
			<asp:CheckBox ID="cbAutoCompleteLoginIdFlg" runat="server" CssClass="radioBtn" Text="ログインIDを記憶する" />
			<asp:LinkButton ID="lbLogin" runat="server" onclick="lbLogin_Click" class="loginBtn">ログイン</asp:LinkButton>
			<a href="<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_PASSWORD_REMINDER_INPUT %>" class="loginLink">パスワードをお忘れのお客様</a>
		</div>
	</div>
	<div class="loginBox_other">
		<h3>新規会員登録はこちら<span>会員さまだけの特典をご用意しています。</span></h3>
		<p class="loginBox_other--txt">
			・会員登録で即日使える500円クーポン進呈中！<br>
			・いつでも送料無料 ＋ 会員ランクに応じて最大10％OFF<br>
			・会員限定セール・特別クーポン・誕生日プレゼント<br>
			・ご購入履歴、ご配送先やお気に入り商品の登録　など
		</p>
		<a href="<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_REGIST_INPUT + "?" + Constants.REQUEST_KEY_NEXT_URL + "=" + Server.UrlEncode(Request[Constants.REQUEST_KEY_NEXT_URL]) %>" class="loginBtn">新規登録する（無料）</a>
		<p style="display: none;"><a href="<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_EASY_REGIST_INPUT %>" class="btn-org btn-large btn-org-blk">かんたん会員登録をする</a></p>
		<% if (Constants.COMMON_SOCIAL_LOGIN_ENABLED) { %>
		<div style="display: none;">
			<h3>ソーシャルログイン</h3>
			<div style="padding: 0 1em;">
				<ul>
					<% if (Constants.SOCIAL_LOGIN_ENABLED) { %>
					<%-- Facebook --%>
					<li>
						<a style="width: 185px;background-color: #305097;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 13px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
							href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
									w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Facebook,
									Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
									Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
									true,
									Request.Url.Authority) %>">Facebookで新規登録/ログイン</a>
					</li>
					<%-- Twitter --%>
					<li>
						<a style="width: 185px;background-color: #1da1f2;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 13px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
							href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
									w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Twitter,
									Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
									Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
									true,
									Request.Url.Authority) %>">Twitterで新規登録/ログイン</a>
					</li>
					<%-- Yahoo --%>
					<li>
						<a style="width: 185px;background-color: #FF0020;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 13px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
							href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
									w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Yahoo,
									Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
									Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
									true,
									Request.Url.Authority) %>">Yahoo!で新規登録/ログイン</a>
					</li>
					<%-- LINE --%>
					<li>
						<a style="width: 185px;background-color: #00c300;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 13px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
							href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
									w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Line,
									Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
									Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
									true,
									Request.Url.Authority) %>">LINEで新規登録/ログイン</a>
						<p style="margin:3px 0 4px;">※LINE連携時に友だち追加します</p>
					</li>
					<% } %>
					<%-- AmazonPay --%>
					<% if (Constants.AMAZON_LOGIN_OPTION_ENABLED) { %>
					<li>
						<%--▼▼Amazonログインボタンウィジェット▼▼--%>
						<div id="AmazonLoginButton" style="margin-bottom:5px;"></div>
						<%--▲▲Amazonログインボタンウィジェット▲▲--%>
					</li>
					<% } %>
					<%-- PayPal --%>
					<%if (Constants.PAYPAL_LOGINPAYMENT_ENABLED) {%>
					<li style="margin :10px 0 10px 0">
						<%
							ucPaypalScriptsForm.LogoDesign = "Login";
							ucPaypalScriptsForm.AuthCompleteActionControl = lbPayPalAuthComplete;
							ucPaypalScriptsForm.GetShippingAddress = (this.IsLoggedIn == false);
						%>
						<uc:PaypalScriptsForm ID="ucPaypalScriptsForm" runat="server" />
						<div>
						<div id="paypal-button" style="width: 236px;"></div>
						<% if (SessionManager.PayPalCooperationInfo != null) {%>
							※<%: SessionManager.PayPalCooperationInfo.AccountEMail %> 連携済
						<% } else {%>
							※PayPalで新規登録/ログインします
						<%} %>
						</div>
						<asp:LinkButton ID="lbPayPalAuthComplete" runat="server" OnClick="lbPayPalAuthComplete_Click"></asp:LinkButton>
					</li>
					<% } %>
					<%-- 楽天Connect --%>
					<% if (Constants.RAKUTEN_LOGIN_ENABLED) { %>
					<li>
						<asp:LinkButton ID="lbRakutenIdConnectRequestAuthForUserRegister" runat="server" OnClick="lbRakutenIdConnectRequestAuth_Click">
							<img src="https://checkout.rakuten.co.jp/p/common/img/btn_idconnect_03.gif" style="width: 237px;"/></asp:LinkButton>
						<p style="margin: 4px 0;">
							楽天会員のお客様は、楽天IDに登録している情報を利用して、<br/>
							「新規会員登録/ログイン」が可能です。
						</p>
					</li>
					<% } %>
				</ul>
			</div>
		</div>
		<% } %>
	</div>
</div>

<div class="reminderArea">
	<p class="reminderArea_ttl">SIMPLISSE Online Store <br class="sp_contents">パスワード再設定のお願い</p>
	<p class="reminderArea_txt">
		2021年1月28日のオンラインストアリニューアル以降、ログインをされていないお客さまには、パスワードの再設定をお願いしております。<br>
		誠に恐れ入りますが、パスワード再設定のお手続きをお願いいたします。<br>
		<br>
		パスワードの再設定はこちら：<br class="sp_contents"><a href="<%= Constants.PATH_ROOT %>Form/User/PasswordReminderInput.aspx">https://www.simplisse.jp/Form/User/PasswordReminderInput.aspx</a><br>
		<br>
		ご登録のメールアドレス宛に、再設定用のリンクを記載したメールをお送りいたします。<br>
		必要事項と新しいパスワードをご入力の上、再設定を実施いただくとログインが可能となります。<br>
		お手数をおかけしますが、何卒お願いいたします。<br>
		<br>
		ご不明な点は、下記までお問い合わせください。<br>
		<br>
		メール：<a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_INQUIRY_INPUT) %>">お問い合わせフォーム</a><br>
		電話：0120-370-063（通話料無料）平日 <br class="sp_contents">10:00-12:00 / 13:30-16:00
	</p>
</div>
</article>

<!--▽ パンくず ▽-->
<div class="breadArea sp_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				ログイン
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

<%--▼▼Amazonウィジェット用スクリプト▼▼--%>
<% if (Constants.AMAZON_LOGIN_OPTION_ENABLED) { %>
<script type='text/javascript'>
	window.onAmazonLoginReady = function () {
		amazon.Login.setClientId('<%=Constants.PAYMENT_AMAZON_CLIENTID %>');
	};
	window.onAmazonPaymentsReady = function () {
		if ($('#AmazonLoginButton').length) showButton();
	};

	<%--Amazonボタン表示ウィジェット--%>
	function showButton() {
		var authRequest;
		OffAmazonPayments.Button("AmazonLoginButton", "<%=Constants.PAYMENT_AMAZON_SELLERID %>", {
			type: "LwA",
			color: "Gold",
			size: "large",
			authorization: function () {
				loginOptions = {
					scope: "payments:shipping_address payments:widget profile",
					popup: true
				};
				authRequest = amazon.Login.authorize(loginOptions, "<%=w2.App.Common.Amazon.Util.AmazonUtil.CreateCallbackUrl(Constants.PAGE_FRONT_AMAZON_LOGIN_CALLBACK, this.NextUrl)%>");
			},
			onError: function (error) {
				alert(error.getErrorMessage());
			}
		});
		$('#OffAmazonPaymentsWidgets0').css({ 'height': '44px', 'width': '237px' });
	}
</script>
<script async="async" type="text/javascript" charset="utf-8" src="<%=Constants.PAYMENT_AMAZON_WIDGETSSCRIPT %>"></script>
<% } %>
<%-- ▲▲Amazonウィジェット用スクリプト▲▲ --%>
</asp:Content>