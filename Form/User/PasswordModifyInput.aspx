﻿<%--
=========================================================================================================
  Module      : パスワード変更入力画面(PasswordModifyInput.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2010 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_PasswordModifyInput, App_Web_passwordmodifyinput.aspx.b2a7112d" title="パスワード変更入力ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />
<article id="cart" class="cartPeyment">

<!--▽ パンくず ▽-->
<div class="breadArea pc_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				パスワード変更
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

<!-- main -->
<section class="cartTtl second">
	<h2>
		パスワード変更
	</h2>
</section>
<!-- // main -->

<div class="cartLead">
	<p>
		下記のフォームに必要事項をご入力の上、「変更する」ボタンをクリックして下さい。<br>
		<span class="icnKome">※</span> は必須入力です。
	</p>
</div>

	<div>
		<%-- UPDATE PANEL開始 --%>
		<asp:UpdatePanel ID="upUpdatePanel" runat="server">
		<ContentTemplate>
			<dl class="cartInputRegist">
					<dt style="margin-top: 0;">
						<%: ReplaceTag("@@User.login_id.name@@") %>
					</dt>
					<dd>
						<%: this.LoginId %>
					</dd>
				<%-- かんたん会員の場合は、生年月日や電話番号による確認をスキップ --%>
				<% if (this.EasyRegisterFlg == Constants.FLG_USER_EASY_REGISTER_FLG_NORMAL) {%>
				<% if (Constants.PASSWORDRIMINDER_AUTHITEM == Constants.PasswordReminderAuthItem.Birth) {%>
					<dt>
						<%: ReplaceTag("@@User.birth.name@@") %>
						<span class="icnKome">※</span>
					</dt>
					<dd>
						<asp:TextBox ID="tbBirth" Runat="server" CssClass="loginId" MaxLength="8"></asp:TextBox><p class="noteTxt">※例）19700101</p>
						<asp:CustomValidator ID="cvBirth" runat="Server"
							ControlToValidate="tbBirth"
							ValidationGroup="PasswordModify"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</dd>
				<% } else if (Constants.PASSWORDRIMINDER_AUTHITEM == Constants.PasswordReminderAuthItem.Tel) {%>
					<dt>
						<%: ReplaceTag("@@User.tel1.name@@") %>
						<span class="icnKome">※</span>
					</dt>
					<dd>
						<div class="telFlex">
							<div class="telFlex_list">
								<asp:TextBox ID="tbTel1_1" Runat="server" CssClass="tel1" MaxLength="6" Type="tel"></asp:TextBox>
							</div>
							<div class="telFlex_line">
								<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
							</div>
							<div class="telFlex_list">
								<asp:TextBox ID="tbTel1_2" Runat="server" CssClass="tel2" MaxLength="4" Type="tel"></asp:TextBox>
							</div>
							<div class="telFlex_line">
								<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
							</div>
							<div class="telFlex_list">
								<asp:TextBox ID="tbTel1_3" Runat="server" CssClass="tel3" MaxLength="4" Type="tel"></asp:TextBox>
							</div>
						</div>
						<asp:CustomValidator ID="cvTel1_1" runat="Server"
							ControlToValidate="tbTel1_1"
							ValidationGroup="PasswordModify"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator ID="cvTel1_2" runat="Server"
							ControlToValidate="tbTel1_2"
							ValidationGroup="PasswordModify"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator ID="cvTel1_3" runat="Server"
							ControlToValidate="tbTel1_3"
							ValidationGroup="PasswordModify"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</dd>
				<% } %>
				<% } %>
					<dt>
						新しい<%: ReplaceTag("@@User.password.name@@") %>
						<span class="icnKome">※</span>
					</dt>
					<dd>
						<asp:TextBox ID="tbPassword" Runat="server" TextMode="Password" autocomplete="off" CssClass="loginId" MaxLength="15"></asp:TextBox><p class="noteTxt">※半角英数字混合7～15文字</p>
						<asp:CustomValidator ID="cvPassword" runat="Server"
							ControlToValidate="tbPassword"
							ValidationGroup="PasswordModify"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</dd>
					<dt>
						新しい<%: ReplaceTag("@@User.password.name@@") %>（確認用）
						<span class="icnKome">※</span>
					</dt>
					<dd>
						<asp:TextBox ID="tbPasswordConf" Runat="server" TextMode="Password" autocomplete="off" CssClass="loginId" MaxLength="15"></asp:TextBox><p class="noteTxt">※半角英数字混合7～15文字</p>
						<asp:CustomValidator ID="cvPasswordConf" runat="Server"
							ControlToValidate="tbPasswordConf"
							ValidationGroup="PasswordModify"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</dd>
			</dl>
			<span class="icnKome" style="display:block; margin: 10px 0 0;">注意：<%= this.PasswordReminder.ChangeTrialLimitCount %>回失敗するとパスワードの変更が出来なくなります。</span>
			<div class="registBtn">
				<ul>
					<li>
						<asp:LinkButton ID="lbModify" ValidationGroup="PasswordModify" OnClientClick="return exec_submit();" OnClick="lbModify_Click" runat="server">
							変更する
						</asp:LinkButton>
					</li>
					<li style="display: none;"></li>
				</ul>
			</div>
		</ContentTemplate>
		</asp:UpdatePanel>
		<%-- UPDATE PANELここまで --%>
	</div>
</article>

<!--▽ パンくず ▽-->
<div class="breadArea sp_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				パスワード変更
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->
</asp:Content>