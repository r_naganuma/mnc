﻿<%--
=========================================================================================================
  Module      : リマインダー入力画面(PasswordReminderInput.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_PasswordReminderInput, App_Web_passwordreminderinput.aspx.b2a7112d" title="リマインダー入力ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/user/PasswordReminderInput/style.css") %>" rel="stylesheet" type="text/css" media="all" />


<!--▽ パンくず ▽-->
<div class="breadArea pc_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				パスワードの再設定
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

<div id="dvUserContents">

		<h2 class="dvUserContents__title"><%: ReplaceTag("@@User.password.name@@") %>の再設定</h2>

	<div id="dvPasswordReminderInput" class="unit">
		<div class="explainText">
			<p>
			ご登録時のメールアドレスを入力してください。<br>
			パスワードの再設定についてのメールをお届けします。<br>
			</p>
		</div>
		<div class="dvReminder">
			<table cellspacing="0">
				<%if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED == false) { %>
				<tr>
					<th>
						ログインＩＤ<span class="necessary">*</span>
					</th>
					<td>
						<asp:TextBox ID="tbLoginId" Runat="server" CssClass="loginId" MaxLength="15" Type="email"></asp:TextBox>
						<asp:CustomValidator ID="CustomValidator1" runat="Server"
							ControlToValidate="tbLoginId"
							ValidationGroup="PasswordReminderInput"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<%} %>
				<div class="inputAreaContainer">
					<a class="mailAdress">
						<%: ReplaceTag("@@User.mail_addr.name@@") %>
					</a>
					<div>
						<asp:TextBox ID="tbMailAddr" Runat="server" CssClass="mailAddr" MaxLength="256" Type="email" PlaceHolder="xxx@simplisse.jp"></asp:TextBox>
						<asp:CustomValidator ID="CustomValidator2" runat="Server"
							ControlToValidate="tbMailAddr"
							ValidationGroup="PasswordReminderInput"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" 
							/>
						<%if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED) { %>
						<%} %>
						<% if(StringUtility.ToEmpty(this.ErrorMessage) != "") {%>
							<span class="error_inline">
								<%: this.ErrorMessage %></span>
						<%} %>
						</div>
				</div>
			</table>
		</div>
		<div class="dvUserBtnBox">
			<p>
<!-- 				<span><a href="javascript:history.back();" class="btn btn-large">キャンセル</a></span> -->
				<span>
					<asp:LinkButton ID="lbSend" ValidationGroup="PasswordReminderInput" runat="server" OnClientClick="return exec_submit()" OnClick="lbSend_Click" class="btn btn-large btn-inverse">送信する</asp:LinkButton>
				</span>
			</p>
		</div>
	</div>
</div>
</asp:Content>
