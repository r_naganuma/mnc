﻿<%--
=========================================================================================================
  Module      : 会員登録変更確認画面(UserModifyConfirm.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="BodyUserExtendModify" Src="~/Form/Common/User/BodyUserExtendModify.ascx" %>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_UserModifyConfirm, App_Web_usermodifyconfirm.aspx.b2a7112d" title="登録情報変更確認ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css?31131" rel="stylesheet" type="text/css" media="all" />
<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					入力内容の確認
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<h2>入力内容の確認</h2>


	<div class="mypageBox">
		<div class="mypageBox_cts">
			
			<%-- メッセージ --%>
			<p class="leadTxt">
				お客様の入力された内容は以下の通りでよろしいでしょうか？<br>
				よろしければ「更新する」ボタンを押して下さい。
			</p>
			
			<div class="dvUserInfo">
				<dl class="cartInput confirm">
					<dt>
						入力内容
					</dt>
						<dd>
							<%-- 氏名 --%>
							<%: ReplaceTag("@@User.name.name@@") %>：<%: this.UserInput.Name1 %>
							<%: this.UserInput.Name2 %>
							<% if (this.IsUserAddrJp) { %>
							（<%: this.UserInput.NameKana1 %>
							<%: this.UserInput.NameKana2 %> さま）
							<% } %>
						</dd>
					<%if (Constants.PRODUCTREVIEW_ENABLED) { %>
						<dd style="display: none;">
							<%-- ニックネーム --%>
							<%: ReplaceTag("@@User.nickname.name@@") %>
							<%: this.UserInput.NickName %></dd>
					<%} %>
						<dd>
							<%-- ＰＣメールアドレス --%>
							<%: ReplaceTag("@@User.mail_addr.name@@") %>：<%: this.UserInput.MailAddr %></dd>
					<% if (Constants.DISPLAYMOBILEDATAS_OPTION_ENABLED) { %>
						<dd>
							<%-- モバイルメールアドレス --%>
							<%: ReplaceTag("@@User.mail_addr2.name@@") %>
							<%: this.UserInput.MailAddr2 %>
							<%if ((this.IsPcSiteOrOfflineUser == false) && Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED) {%>
							<span class="notes">※ログイン時に利用します</span>
							<%} %></dd>
					<% } %>
						<dd>
							<%: ReplaceTag("@@User.addr.name@@") %>：<% if (this.IsUserAddrJp) { %>
							〒<%: this.UserInput.Zip %><br />
							<% } %>
							<%: this.UserInput.Addr1 %> <%: this.UserInput.Addr2 %><br />
							<%: this.UserInput.Addr3 %> <%: this.UserInput.Addr4 %><br />
							<%: this.UserInput.Addr5 %> 
							<% if (this.IsUserAddrJp == false) { %>
							<%: this.UserInput.Zip %><br />
							<% } %>
							<%: this.UserInput.AddrCountryName %>
						</dd>
					<% if (Constants.DISPLAY_CORPORATION_ENABLED){ %>
						<dd>
							<!-- 企業名 -->
							<%: ReplaceTag("@@User.company_name.name@@")%>
							<%: this.UserInput.CompanyName %></dd>
						<dd>
							<!-- 部署名 -->
							<%: ReplaceTag("@@User.company_post_name.name@@")%>
							<%: this.UserInput.CompanyPostName %></dd>
					<%}%>
						<dd>
							<%-- 電話番号 --%>
							<%: ReplaceTag("@@User.tel1.name@@", this.UserAddrCountryIsoCode) %>：<%: this.UserInput.Tel1 %></dd>
						<dd style="display: none;">
							<%: ReplaceTag("@@User.tel2.name@@", this.UserAddrCountryIsoCode) %><%: WebSanitizer.HtmlEncode(this.UserInput.Tel2) %></dd>
						<dd>
							<%-- 生年月日 --%>
							<%: ReplaceTag("@@User.birth.name@@") %>：<%if (this.UserInput.Birth != null) {%><%: this.UserInput.BirthYear %>年<%: this.UserInput.BirthMonth %>月<%: this.UserInput.BirthDay %>日
						<%} %></dd>
						<dd>
							<%-- 性別 --%>
							<%: ReplaceTag("@@User.sex.name@@") %>：<%: this.UserInput.SexValueText %></dd>
						<dd>
							<%: ReplaceTag("@@User.mail_flg.name@@") %>：<%: this.UserInput.MailFlgValueText %></dd>
					<%-- ユーザー拡張項目　HasInput:true(入力画面)/false(確認画面)　HasRegist:true(新規登録)/false(登録編集) --%>
					<uc:BodyUserExtendModify ID="ucBodyUserExtendModify" runat="server" HasInput="false" HasRegist="false" />

					<%if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED == false) { %>
						<dd>
						<%-- ログインID --%>
						<%: ReplaceTag("@@User.login_id.name@@") %>
							<%: this.UserInput.LoginId %></dd>
					<%} %>
						<dd>
						<%-- パスワード --%>
						<%: ReplaceTag("@@User.password.name@@") %>
							<%if (StringUtility.ToEmpty(this.UserInput.Password) != "") { %>
								<%: StringUtility.ChangeToAster(this.UserInput.Password) %>
							<%} else {%>
								（変更なし）
							<%} %>
						</dd>
				</dl>
			</div>
			
			<div class="cartNextLead">
				<p>
					※ご注意ください<br>
					定期便のお届け先が変わる場合、ご契約商品ごとに変更が必要です。<br>
					マイページの定期購入情報一覧から定期商品をご選択いただき、必ずお届け先のご変更をお願いします。
				</p>
			</div>

			<div class="cartNextbtn">
				<ul>
					<li class="prevBtn">
						<asp:LinkButton ID="lbSend" runat="server" OnClientClick="return exec_submit()" OnClick="lbBack_Click">
							前のページに戻る
						</asp:LinkButton>
					</li>
					<li class="nextBtn">
						<asp:LinkButton ID="lbModity" runat="server" OnClick="lbModity_Click">
							更新する
						</asp:LinkButton>
					</li>
				</ul>
			</div>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					入力内容の確認
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
</asp:Content>