﻿<%--
=========================================================================================================
  Module      : メールマガジン登録確認画面(MailMagazineRegistConfirm.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2010 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_MailMagazineRegistConfirm, App_Web_mailmagazineregistconfirm.aspx.b2a7112d" title="メルマガ登録 | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />
<article id="cart" class="cartPeyment">

<!--▽ パンくず ▽-->
<div class="breadArea pc_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				メールマガジン登録
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

<!-- main -->
<section class="cartTtl second">
	<h2>
		入力内容の確認
	</h2>
</section>
<!-- // main -->

<div class="cartLead">
	<p>
		以下の項目でよろしければ「送信する」ボタンをクリックしてください。
	</p>
</div>

	<div>
		<dl class="cartInputRegist confirm">
				<dt style="display: none;">
					<%: ReplaceTag("@@User.name.name@@") %>
				</dt>
				<dd style="display: none;"><%: this.UserInput.Name1 %> <%: this.UserInput.Name2 %></dd>
				<dt style="display: none;">
					氏名(カナ)
				</dt>
				<dd style="display: none;"><%: this.UserInput.NameKana1 %> <%: this.UserInput.NameKana2 %></dd>
				<dt>
					<%: ReplaceTag("@@User.mail_addr.name@@") %>
				</dt>
				<dd><%: this.UserInput.MailAddr %></dd>
		</dl>

		<div class="registBtn">
			<ul>
				<li>
					<asp:LinkButton ID="lbSend" runat="server" OnClientClick="return exec_submit()" OnClick="lbSend_Click">
						送信する
					</asp:LinkButton>
				</li>
				<li>
					<asp:LinkButton ID="lbBack" runat="server" OnClientClick="return exec_submit()" OnClick="lbBack_Click">
						戻る
					</asp:LinkButton>
				</li>
			</ul>
		</div>
	</div>
</article>

<!--▽ パンくず ▽-->
<div class="breadArea sp_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				メールマガジン登録
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->
</asp:Content>