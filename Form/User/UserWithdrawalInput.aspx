﻿<%--
=========================================================================================================
  Module      : 会員退会入力画面(UserWithdrawalInput.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_UserWithdrawalInput, App_Web_userwithdrawalinput.aspx.b2a7112d" title="退会入力ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<%@ Import Namespace="w2.Common.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />
<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					会員の退会
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
	<h2>会員の退会</h2>


	<div class="mypageBox">
		<div class="mypageBox_cts">
			<div class="withdrawInfo">
				<p>
					<%= WebSanitizer.HtmlEncode(ShopMessage.GetMessage("ShopName")) %> にご登録頂いているすべてのデータを削除します。<br>
					<br>
					・退会された場合は､ご購入履歴をご確認いただけません｡<br>
					・同一メールアドレスで再登録された場合も､以前のご注文履歴はご確認いただけません｡<br>
					・獲得された会員ランク特典、お持ちのクーポンはご利用いただけなくなります。<br>
					・退会後にデータの復元は出来ませんので、ご了承ください。<br>
					<br>
					「退会する」ボタンを押すことで退会手続きが完了いたします。
				</p>
			</div>
			<% if (IsWithdrawalLimit(this.LoginUserId)) { %>
			<div runat="server" Visible="True" class="withdrawNotice">
				<p>
					有効な定期購入情報がございますため、退会手続きが行えません。<br>
					すべての定期便をご解約されてから、会員の退会を行ってください。<br>
					定期便のご解約は、下記のお問い合わせフォームまたはお電話にてご依頼をお願いいたします。<br>
					<br>
					<%= WebSanitizer.HtmlEncode(ShopMessage.GetMessage("ShopName")) %><br>
					<a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_INQUIRY_INPUT) %>">お問い合わせフォーム</a><br>
					お電話：0120-370-063（通話料無料）10:00-12:00 / 13:30-16:00 土日・祝日は休業
				</p>
			</div>
			<% } else { %>
			<div class="cartNextbtn one one02">
				<ul>
					<li class="prevBtn">
						<asp:LinkButton ID="lbWithdrawal" runat="server" OnClick="lbWithdrawal_Click" OnClientClick="return confirm('本当によろしいですか？')">退会する</asp:LinkButton>
					</li>
				</ul>
			</div>
			<% } %>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					会員の退会
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
</asp:Content>