﻿<%--
=========================================================================================================
  Module      : アドレス帳確認画面(UserShippingConfirm.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_UserShippingConfirm, App_Web_usershippingconfirm.aspx.b2a7112d" title="配送先リスト確認ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />
<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					入力内容の確認
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<h2>入力内容の確認</h2>

	<div class="mypageBox">
		<div class="mypageBox_cts">
			<p class="leadTxt">
				登録する住所に間違いがなければ、「<%= (lbRegist.Visible == true) ? "登録" : "更新"%>する」ボタンを押してください。
			</p>
			<div class="dvUserShippingInfo">
				<dl class="cartInput confirm">
					<dt>
						入力内容
					</dt>
						<dd>配送先名：<%: this.UserShipping.Name %></dd>
						<%-- 氏名 --%>
						<dd>
							<%: ReplaceTag("@@User.name.name@@") %>：<%: this.UserShipping.ShippingName1 %><%: this.UserShipping.ShippingName2 %>&nbsp;様
							<% if (this.IsShippingAddrJp) { %>
							（<%: this.UserShipping.ShippingNameKana1 %><%: this.UserShipping.ShippingNameKana2 %>&nbsp;さま）
							<% } %>
						</dd>
						<%-- 住所 --%>
						<dd>
							<%: ReplaceTag("@@User.addr.name@@") %>：<% if (this.IsShippingAddrJp){ %>〒<%: this.UserShipping.ShippingZip %><br /><% } %>
							<%: this.UserShipping.ShippingAddr1 %>
							<%: this.UserShipping.ShippingAddr2 %><br />
							<%: this.UserShipping.ShippingAddr3 %>
							<%: this.UserShipping.ShippingAddr4 %>
							<%: this.UserShipping.ShippingAddr5 %><br />
							<% if (this.IsShippingAddrJp == false) { %>
							<%: this.UserShipping.ShippingZip %><br />
							<% } %>
							<%: this.UserShipping.ShippingCountryName %>
						</dd>
					<% if (Constants.DISPLAY_CORPORATION_ENABLED){ %>
						<dd>
							<%: ReplaceTag("@@User.company_name.name@@")%>・<%: ReplaceTag("@@User.company_post_name.name@@")%>：<%: this.UserShipping.ShippingCompanyName %><br />
							<%: this.UserShipping.ShippingCompanyPostName %>
						</dd>
					<%} %>
						<dd><%: ReplaceTag("@@User.tel1.name@@", this.ShippingAddrCountryIsoCode) %>：<%: this.UserShipping.ShippingTel1 %></dd>
				</dl>
			</div>
			<div class="cartNextbtn">
				<ul>
					<li class="prevBtn">
						<asp:LinkButton ID="lbBack" runat="server" OnClientClick="return exec_submit()" OnClick="lbBack_Click">前のページに戻る</asp:LinkButton>
					</li>
					<li class="nextBtn">
						<asp:LinkButton ID="lbRegist" runat="server" OnClientClick="return exec_submit()" OnClick="lbSend_Click">登録する</asp:LinkButton>
						<asp:LinkButton ID="lbModify" runat="server" OnClientClick="return exec_submit()" OnClick="lbSend_Click">更新する</asp:LinkButton>
					</li>
				</ul>
			</div>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					入力内容の確認
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
</asp:Content>