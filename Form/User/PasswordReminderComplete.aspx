﻿<%--
=========================================================================================================
  Module      : リマインダー完了画面(PasswordReminderComplete.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_PasswordReminderComplete, App_Web_passwordremindercomplete.aspx.b2a7112d" title="パスワード変更受付完了ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />

<article id="cartComp">

	<div class="inner">

		<div class="cartComp_ttl">
			<h2>受付完了</h2>
		</div>

		<div class="cartComp_txt">
			<p class="cartComp_txt--cts lh">
				<%: ReplaceTag("@@User.password.name@@") %>再設定用のリンクを記載したメールを<br>
				<%= this.MailAddress %><br/>
				にお送りいたしましたのでご確認下さい。<br /><br />
				今後とも、「<%= WebSanitizer.HtmlEncode(ShopMessage.GetMessage("ShopName")) %>」をどうぞ宜しくお願い申し上げます。<br />
				※メールアドレスが未登録の場合、メールは送信されません。
			</p>
			<% if (string.IsNullOrEmpty(this.NextUrl) == false) { %>
				<asp:LinkButton ID="lbReturn" runat="server" OnClick="lbReturn_Click" class="cartComp_txt--btn">
					戻る
				</asp:LinkButton>
				<% } else { %>
				<asp:LinkButton ID="lbTopPage" runat="server" OnClick="lbTopPage_Click" class="cartComp_txt--btn">
					トップページへ
				</asp:LinkButton>
			<% } %>
		</div>
	</div>
</article>
</asp:Content>