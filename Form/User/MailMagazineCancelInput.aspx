﻿<%--
=========================================================================================================
  Module      : メールマガジン解除入力画面(MailMagazineCancelInput.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_MailMagazineCancelInput, App_Web_mailmagazinecancelinput.aspx.b2a7112d" title="メルマガ解除 | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />
<article id="cart" class="cartPeyment">
	
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					メールマガジン解除
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
	
	<!-- main -->
	<section class="cartTtl second">
		<h2>
			メールマガジン解除
		</h2>
	</section>
	<!-- // main -->
	
	<div class="cartLead">
		<p>
			メールマガジンを解除される方は、下記のフォームに必要事項をご入力の上、「解除する」ボタンをクリックして下さい。<br>
			<span class="icnKome">※</span>は必須入力となります。
		</p>
	</div>

	<div>
		<dl class="cartInputRegist">
			<dt>
				<%: ReplaceTag("@@User.mail_addr.name@@") %> <span class="icnKome">※</span>
			</dt>
			<dd>
				<asp:TextBox ID="tbMailAddr" Runat="server" CssClass="mailAddr" MaxLength="256" Type="email"></asp:TextBox>
			</dd>
		</dl>

		<div class="registBtn">
			<ul>
				<li>
					<asp:LinkButton ID="lbCancel" runat="server" OnClick="lbCancelClick">
						解除する
					</asp:LinkButton>
				</li>
				<li>
					<a href="javascript:history.back();">
						キャンセル
					</a>
				</li>
			</ul>
		</div>
	</div>

</article>

<!--▽ パンくず ▽-->
<div class="breadArea sp_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				メールマガジン解除
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->
</asp:Content>