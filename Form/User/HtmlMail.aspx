﻿<%--
=========================================================================================================
  Module      : Htmlメール画面(HtmlMail.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2019 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/MailPage.master" autoeventwireup="true" inherits="HtmlMail, App_Web_htmlmail.aspx.b2a7112d" title="Htmlメールページ" %>
<%@ Import Namespace="w2.Common.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<asp:Literal ID="lMailBody" runat="server" />
</asp:Content>