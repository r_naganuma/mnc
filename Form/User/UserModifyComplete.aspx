﻿<%--
=========================================================================================================
  Module      : 会員登録変更完了画面(UserModifyComplete.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_UserModifyComplete, App_Web_usermodifycomplete.aspx.b2a7112d" title="登録情報変更完了ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />
<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					変更完了
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<h2>変更完了</h2>


	<div class="mypageBox">
		<div class="mypageBox_cts">
			<%-- メッセージ --%>
			<p class="leadTxt">
				<%= WebSanitizer.HtmlEncode(ShopMessage.GetMessage("ShopName")) %>会員情報の変更を受け付けました。<br>
				今後とも<%= WebSanitizer.HtmlEncode(ShopMessage.GetMessage("ShopName")) %>をどうぞ宜しくお願い申し上げます。
			</p>
			<div class="completeBtn">
				<a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_MYPAGE) %>">マイトップページ</asp:LinkButton></a>
			</div>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					変更完了
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
</asp:Content>