﻿<%--
=========================================================================================================
  Module      : かんたん会員登録入力画面(UserEasyRegistInput.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2017 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_UserEasyRegistInput, App_Web_usereasyregistinput.aspx.b2a7112d" title="かんたん会員登録入力ページ" %>
<%@ Import Namespace="w2.Common.Web" %>
<%@ Import Namespace="w2.App.Common.User.SocialLogin.Util" %>
<%@ Register Src="~/Form/Common/PaypalScriptsForm.ascx" TagPrefix="uc" TagName="PaypalScriptsForm" %>
<%@ Register TagPrefix="uc" TagName="UserRegistRegulationMessage" Src="~/Form/User/UserRegistRegulationMessage.ascx" %>
<%@ Register TagPrefix="uc" TagName="Layer" Src="~/Form/Common/Layer/SearchResultLayer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div id="dvUserContents">

	<h2>会員情報の入力</h2>

	<div id="dvUserRegistInput" class="unit">
		
		<%-- メッセージ --%>
		<div class="dvContentsInfo" >
			<p>
				<%: ShopMessage.GetMessage("ShopName") %>会員へのお申込みにあたっては、以下の項目にご入力が必要です。<br />
				会員規約を必ずお読みの上、「登録する」ボタンを押して下さい。</p>
		</div>
		<% if (Constants.SOCIAL_LOGIN_ENABLED || Constants.AMAZON_LOGIN_OPTION_ENABLED || Constants.PAYPAL_LOGINPAYMENT_ENABLED) { %>
		<%-- ソーシャルログイン連携 --%>
		<div class="dvSocialLoginCooperation">
			<h3>ソーシャルログイン連携</h3>

			<ul style="display:flex;margin-bottom:20px;flex-wrap:wrap">
			<% if (Constants.SOCIAL_LOGIN_ENABLED) { %>
				<%-- Facebook --%>
				<li style="padding: 0 0.5em;">
					<a style="width: 170px;background-color: #305097;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 12px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
						href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
								w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Facebook,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_USER_EASY_REGISTER_CALLBACK,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_USER_EASY_REGISTER_CALLBACK,
								true,
								Request.Url.Authority) %>">Facebookで新規登録/ログイン</a>
				</li>
				<%-- Twitter --%>
				<li style="padding: 0 0.5em;">
					<a style="width: 170px;background-color: #1da1f2;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 12px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
						href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
								w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Twitter,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_USER_EASY_REGISTER_CALLBACK,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_USER_EASY_REGISTER_CALLBACK,
								true,
								Request.Url.Authority) %>">Twitterで新規登録/ログイン</a>
				</li>
				<%-- Yahoo --%>
				<li style="padding:0 0.5em;">
					<a style="width: 170px;background-color:#ff0020;border:none;border-radius:5px;color:white;padding:1em 2em;text-align:center;text-decoration:none;display: inline-block;font-size:12px;font-family:'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
						href="<%= w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
								w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Yahoo,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_USER_EASY_REGISTER_CALLBACK,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_USER_EASY_REGISTER_CALLBACK,
								true,
								Request.Url.Authority) %>">Yahoo!で新規登録/ログイン</a>
				</li>
				<%-- LINE --%>
				<li style="padding: 0 0.5em;">
					<a style="width: 170px;background-color: #00c300;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 12px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
						href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
								w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Line,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_USER_EASY_REGISTER_CALLBACK,
								Constants.PAGE_FRONT_SOCIAL_LOGIN_USER_EASY_REGISTER_CALLBACK,
								true,
								Request.Url.Authority) %>">LINEで新規登録/ログイン</a>
					<p style="padding-top:3px;">※LINE連携時に友だち追加します</p>
				</li>
				<% } %>
				<%-- Amazon --%>
				<% if (Constants.AMAZON_LOGIN_OPTION_ENABLED) { %>
				<li style="padding: 0 0.5em;">
					<%-- ▼▼Amazonログイン連携ボタンウィジェット▼▼ --%>
					<div id="AmazonLoginButton"></div>
					<%-- ▲▲Amazonログイン連携ボタンウィジェット▲▲ --%>
				</li>
				<% } %>
				<%-- PayPal --%>
				<% if (Constants.PAYPAL_LOGINPAYMENT_ENABLED) { %>
				<li style="padding: 0 0.5em;">
					<%-- ▼PayPalここから▼ --%>
					<%
						ucPaypalScriptsForm.LogoDesign = "Login";
						ucPaypalScriptsForm.AuthCompleteActionControl = lbPayPalAuthComplete;
						ucPaypalScriptsForm.GetShippingAddress = (this.IsLoggedIn == false);
					%>
					<uc:PaypalScriptsForm ID="ucPaypalScriptsForm" runat="server" />
					<div id="paypal-button" style="width: 218px;"></div>
					<div id="paypal-button2" style="width: 218px;">
					<% if (SessionManager.PayPalCooperationInfo != null) { %>
						※<%: SessionManager.PayPalCooperationInfo.AccountEMail %> 連携済
					<% } else { %>
						<p style="padding-top:7px;">※PayPalで新規登録/ログインします</p>
					<% } %>
					</div>
					<asp:LinkButton ID="lbPayPalAuthComplete" runat="server" OnClick="lbPayPalAuthComplete_Click"></asp:LinkButton>
					<%-- ▲PayPalここまで▲ --%>
				</li>
				<% } %>
			</ul>
			<asp:HiddenField ID="hfSocialLoginJson" runat="server" />
		</div>
		<% } %>

		<%-- お客様情報入力フォーム --%>
		<div class="dvUserInfo">
			<h3>お客様情報</h3>
			<ins><span class="necessary">*</span>は必須入力となります。</ins>

		<%-- UPDATE PANEL開始 --%>
		<asp:UpdatePanel ID="upUpdatePanel" runat="server">
		<ContentTemplate>
			<table>
				<% if (this.IsVisible_UserName) { %>
				<tr>
					<%-- 氏名 --%>
					<th><%: ReplaceTag("@@User.name.name@@") %><span class="necessary">*</span><span id="efo_sign_name"/></th>
					<td>
						<table cellspacing="0">
							<tr>
								<td>
									<% SetMaxLength(this.WtbUserName1, "@@User.name1.length_max@@"); %>
									<span class="fname">姓</span><asp:TextBox ID="tbUserName1" runat="server" CssClass="nameFirst"></asp:TextBox></td>
								<td>
									<% SetMaxLength(this.WtbUserName2, "@@User.name2.length_max@@"); %>
									<span class="lname">名</span><asp:TextBox ID="tbUserName2" runat="server" CssClass="nameLast"></asp:TextBox><span class="notes">※全角入力</span></td>
							</tr>
							<tr>
								<td><span class="notes">例：山田</span></td>
								<td><span class="notes">太郎</span></td>
							</tr>
						</table>
						<asp:CustomValidator
							ID="cvUserName1"
							runat="server"
							ControlToValidate="tbUserName1"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserName2"
							runat="server"
							ControlToValidate="tbUserName2"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (this.IsVisible_UserNameKana) { %>
				<tr>
					<%-- 氏名（かな） --%>
					<th>
						<%: ReplaceTag("@@User.name_kana.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_kana"/>
					</th>
					<td>
						<table cellspacing="0">
							<tr>
								<td>
									<% SetMaxLength(this.WtbUserNameKana1, "@@User.name_kana1.length_max@@"); %>
									<span class="fname">姓</span><asp:TextBox ID="tbUserNameKana1" runat="server" CssClass="nameFirst"></asp:TextBox></td>
								<td>
									<% SetMaxLength(this.WtbUserNameKana2, "@@User.name_kana2.length_max@@"); %>
									<span class="lname">名</span><asp:TextBox ID="tbUserNameKana2" runat="server" CssClass="nameLast"></asp:TextBox><span class="notes">※全角ひらがな入力</span></td>
							</tr>
							<tr>
								<td><span class="notes">例：やまだ</span></td>
								<td><span class="notes">たろう</span></td>
							</tr>
						</table>
						<small>
						<asp:CustomValidator
							ID="cvUserNameKana1"
							runat="server"
							ControlToValidate="tbUserNameKana1"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserNameKana2"
							runat="server"
							ControlToValidate="tbUserNameKana2"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						</small>
					</td>
				</tr>
				<% } %>
				<% if (Constants.PRODUCTREVIEW_ENABLED) { %>
				<% if (this.IsVisible_UserNickName) { %>
				<tr>
					<%-- ニックネーム --%>
					<th><%: ReplaceTag("@@User.nickname.name@@") %></th>
					<td>
						<asp:TextBox ID="tbUserNickName" runat="server" MaxLength="20" CssClass="nickname"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserNickName"
							runat="server"
							ControlToValidate="tbUserNickName"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% } %>
					<%
						for (int iLoop = 1900; iLoop <= 1940; iLoop++)
						{
							this.WddlUserBirthYear.Items.Remove(new ListItem(iLoop.ToString())) ;
						}
					 %>
				<% if (this.IsVisible_UserBirth) { %>
				<tr>
					<%-- 生年月日 --%>
					<th>
						<%: ReplaceTag("@@User.birth.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_birth"/>
					</th>
					<td>
						<asp:DropDownList ID="ddlUserBirthYear" runat="server" CssClass="year"></asp:DropDownList>年
						<asp:DropDownList ID="ddlUserBirthMonth" runat="server" CssClass="month"></asp:DropDownList>月
						<asp:DropDownList ID="ddlUserBirthDay" runat="server" CssClass="date"></asp:DropDownList>日
						<asp:CustomValidator
							ID="cvUserBirthYear"
							runat="Server"
							ControlToValidate="ddlUserBirthYear"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							EnableClientScript="false"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserBirthMonth"
							runat="Server"
							ControlToValidate="ddlUserBirthMonth"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							EnableClientScript="false"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserBirthDay"
							runat="Server"
							ControlToValidate="ddlUserBirthDay"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							EnableClientScript="false"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (this.IsVisible_UserSex) { %>
				<tr>
					<%-- 性別 --%>
					<th>
						<%: ReplaceTag("@@User.sex.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_sex"/>
					</th>
					<td>
						<asp:RadioButtonList ID="rblUserSex" runat="server" RepeatDirection="Horizontal" CellSpacing="0" RepeatLayout="Flow" CssClass="radioBtn"></asp:RadioButtonList>
						<asp:CustomValidator
							ID="cvUserSex"
							runat="server"
							ControlToValidate="rblUserSex"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							EnableClientScript="false"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<tr>
					<%-- PCメールアドレス --%>
					<th>
						<%: ReplaceTag("@@User.mail_addr.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_mail_addr"/>
					</th>
					<td>
						<asp:TextBox ID="tbUserMailAddr" runat="server" MaxLength="256" CssClass="mailAddr"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserMailAddr"
							runat="server"
							ControlToValidate="tbUserMailAddr"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserMailAddrForCheck"
							runat="server"
							ControlToValidate="tbUserMailAddr"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							CssClass="error_inline" />
					</td>
				</tr>
				<tr>
					<%-- PCメールアドレス（確認用） --%>
					<th>
						<%: ReplaceTag("@@User.mail_addr.name@@") %>（確認用）
						<span class="necessary">*</span><span id="efo_sign_mail_addr_conf"/>
					</th>
					<td>
						<asp:TextBox ID="tbUserMailAddrConf" runat="server" MaxLength="256" CssClass="mailAddr"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserMailAddrConf"
							runat="server"
							ControlToValidate="tbUserMailAddrConf"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% if (this.IsVisible_UserMailAddr2) { %>
				<tr>
					<%-- モバイルメールアドレス --%>
					<th>
						<%: ReplaceTag("@@User.mail_addr2.name@@") %>
						<span class="necessary" style="display:<%= (Constants.DISPLAYMOBILEDATAS_OPTION_ENABLED) ? "" : "none" %>">*</span>
					</th>
					<td>
						<asp:TextBox ID="tbUserMailAddr2" runat="server" MaxLength="256" CssClass="mailAddr" Type="email"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserMailAddr2"
							runat="server"
							ValidationGroup="UserRegist"
							ControlToValidate="tbUserMailAddr2"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (this.IsVisible_UserMailAddr2Conf) { %>
				<tr>
					<%-- モバイルメールアドレス（確認用） --%>
					<th><%: ReplaceTag("@@User.mail_addr2.name@@") %>（確認用）<span class="necessary" style="display:<%= (Constants.DISPLAYMOBILEDATAS_OPTION_ENABLED) ? "" : "none" %>">*</span></th>
					<td>
						<asp:TextBox ID="tbUserMailAddr2Conf" runat="server" MaxLength="256" CssClass="mailAddr" Type="email"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserMailAddr2Conf"
							runat="server"
							ControlToValidate="tbUserMailAddr2Conf"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (Constants.GLOBAL_OPTION_ENABLE && this.IsVisible_UserCountry) { %>
				<tr>
					<%-- 国 --%>
					<th>
						<%: ReplaceTag("@@User.country.name@@") %>
						<span class="necessary">*</span>
					</th>
					<td>
						<asp:DropDownList ID="ddlUserCountry" runat="server" CssClass="district"></asp:DropDownList>
						<span id="countryAlertMessage" class="notes" runat="server" Visible='false'>※Amazonログイン連携では国はJapan以外選択できません。</span>
						<asp:CustomValidator
							ID="cvUserCountry"
							runat="server"
							ControlToValidate="ddlUserCountry"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (Constants.GLOBAL_OPTION_ENABLE == false) { %>
				<% if (this.IsVisible_AmazonAddressWidget) { %>
					<tr>
						<th>
							<%: ReplaceTag("@@User.addr.name@@") %>
						</th>
						<td>
							<%--▼▼Amazonアドレス帳ウィジェット▼▼--%>
							<div id="addressBookWidgetDiv" style="width:100%;height:300px;"></div>
							<div id="addressBookErrorMessage" style="color:red;padding:5px" ClientIDMode="Static" runat="server"></div>
							<%--▲▲Amazonアドレス帳ウィジェット▲▲--%>
							<%--▼▼AmazonリファレンスID格納▼▼--%>
							<asp:HiddenField runat="server" ID="hfAmazonOrderRefID" />
							<%--▲▲AmazonリファレンスID格納▲▲--%>
						</td>
					</tr>
				<% } else { %>
				<% if (this.IsVisible_UserZip) { %>
				<tr>
					<%-- 郵便番号 --%>
					<th>
						<%: ReplaceTag("@@User.zip.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_zip"/>
					</th>
					<td valign="middle">
						<asp:TextBox ID="tbUserZip1" runat="server" MaxLength="3" CssClass="zipFirst"></asp:TextBox>-
						<asp:TextBox ID="tbUserZip2" runat="server" MaxLength="4" CssClass="zipLast"></asp:TextBox>
						<asp:LinkButton ID="lbSearchAddr" runat="server" OnClick="lbSearchAddr_Click" class="btn btn-mini" OnClientClick="return false;">
							住所検索</asp:LinkButton><br/>
						<%--検索結果レイヤー--%>
						<uc:Layer ID="ucLayer" runat="server" />
						<asp:CustomValidator
							ID="cvUserZip1"
							runat="server"
							ControlToValidate="tbUserZip1"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserZip2"
							runat="server"
							ControlToValidate="tbUserZip2"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<span style="color :Red" id="addrSearchErrorMessage">
							<%: this.ZipInputErrorMessage %></span>
					</td>
				</tr>
				<% } %>
				<% if (this.IsVisible_UserAddr1) { %>
				<tr>
					<%-- 都道府県 --%>
					<th>
						<%: ReplaceTag("@@User.addr1.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_ship_addr1"/>
					</th>
					<td>
						<asp:DropDownList ID="ddlUserAddr1" runat="server" CssClass="district"></asp:DropDownList>
						<asp:CustomValidator
							ID="cvUserAddr1"
							runat="server"
							ControlToValidate="ddlUserAddr1"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (this.IsVisible_UserAddr2) { %>
				<tr>
					<%-- 市区町村 --%>
					<th>
						<%: ReplaceTag("@@User.addr2.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_ship_addr2"/>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserAddr2, "@@User.addr2.length_max@@"); %>
						<asp:TextBox ID="tbUserAddr2" runat="server" CssClass="addr"></asp:TextBox><span class="notes">※全角入力</span>
						<asp:CustomValidator
							ID="cvUserAddr2"
							runat="server"
							ControlToValidate="tbUserAddr2"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (this.IsVisible_UserAddr3) { %>
				<tr>
					<%-- 番地 --%>
					<th>
						<%: ReplaceTag("@@User.addr3.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_ship_addr3"/>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserAddr3, "@@User.addr3.length_max@@"); %>
						<asp:TextBox ID="tbUserAddr3" runat="server" CssClass="addr2"></asp:TextBox><span class="notes">※全角入力</span>
						<asp:CustomValidator
							ID="cvUserAddr3"
							runat="server"
							ControlToValidate="tbUserAddr3"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (Constants.DISPLAY_ADDR4_ENABLED) { %>
				<% if (this.IsVisible_UserAddr4) { %>
				<tr>
					<%-- ビル・マンション名 --%>
					<th>
						<%: ReplaceTag("@@User.addr4.name@@") %>
						<span class="necessary">*</span>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserAddr4, "@@User.addr4.length_max@@"); %>
						<asp:TextBox ID="tbUserAddr4" runat="server" CssClass="addr2"></asp:TextBox><span class="notes">※全角入力</span>
						<asp:CustomValidator
							ID="cvUserAddr4"
							runat="server"
							ControlToValidate="tbUserAddr4"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% } %>
				<% } %>
				<% } %>
				<% if (Constants.DISPLAY_CORPORATION_ENABLED){ %>
				<% if (this.IsVisible_UserCompanyName) { %>
				<tr>
					<%-- 企業名 --%>
					<th>
						<%: ReplaceTag("@@User.company_name.name@@")%>
						<span class="necessary"></span>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserCompanyName, "@@User.company_name.length_max@@"); %>
						<asp:TextBox ID="tbUserCompanyName" runat="server" CssClass="addr2"></asp:TextBox><span class="notes">※全角入力</span>
						<asp:CustomValidator
							ID="cvUserCompanyName"
							runat="server"
							ControlToValidate="tbUserCompanyName"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (this.IsVisible_UserCompanyPostName) { %>
				<tr>
					<%-- 部署名 --%>
					<th>
						<%: ReplaceTag("@@User.company_post_name.name@@")%>
						<span class="necessary"></span>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserCompanyPostName, "@@User.company_post_name.length_max@@"); %>
						<asp:TextBox ID="tbUserCompanyPostName" runat="server" CssClass="addr2"></asp:TextBox><span class="notes">※全角入力</span>
						<asp:CustomValidator
							ID="cvUserCompanyPostName"
							runat="server"
							ControlToValidate="tbUserCompanyPostName"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% } %>
				<% if (this.IsVisible_UserTel1) { %>
				<tr>
					<%-- 電話番号 --%>
					<th>
						<%: ReplaceTag("@@User.tel1.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_tel1"/>
					</th>
					<td>
						<asp:TextBox ID="tbUserTel1" runat="server" MaxLength="6" CssClass="tel1"></asp:TextBox> -
						<asp:TextBox ID="tbUserTel2" runat="server" MaxLength="4" CssClass="tel2"></asp:TextBox> -
						<asp:TextBox ID="tbUserTel3" runat="server" MaxLength="4" CssClass="tel3"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserTel1"
							runat="server"
							ControlToValidate="tbUserTel1"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserTel2"
							runat="server"
							ControlToValidate="tbUserTel2"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserTel3"
							runat="server"
							ControlToValidate="tbUserTel3"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (this.IsVisible_UserTel2) { %>
				<tr>
					<%-- 電話番号（予備） --%>
					<th>
						<%: ReplaceTag("@@User.tel2.name@@") %>
					</th>
					<td>
						<asp:TextBox ID="tbUserTel2_1" runat="server" MaxLength="6" CssClass="tel1"></asp:TextBox> -
						<asp:TextBox ID="tbUserTel2_2" runat="server" MaxLength="4" CssClass="tel2"></asp:TextBox> -
						<asp:TextBox ID="tbUserTel2_3" runat="server" MaxLength="4" CssClass="tel3"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserTel2_1"
							runat="server"
							ControlToValidate="tbUserTel2_1"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="false"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserTel2_2"
							runat="server"
							ControlToValidate="tbUserTel2_2"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="false"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserTel2_3"
							runat="server"
							ControlToValidate="tbUserTel2_3"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="false"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (this.IsVisible_UserMailFlg) { %>
				<tr>
					<th>
						<%: ReplaceTag("@@User.mail_flg.name@@") %>
					</th>
					<td class="checkBox">
						<asp:CheckBox ID="cbUserMailFlg" Text=" 希望する " CssClass="checkBox" runat="server" />
					</td>
				</tr>
				<% } %>
			</table>
		</ContentTemplate>
		</asp:UpdatePanel>
		<%-- UPDATE PANELここまで --%>
		</div>
		<%if ((Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED == false) || this.IsVisible_UserPassword) { %>
		<div class="dvLoginInfo">
			<h3>ログイン情報</h3>
			<table>
				<%if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED == false) { %>
				<tr>
					<th>
						<%-- ログインID --%>
						<%: ReplaceTag("@@User.login_id.name@@") %><span class="necessary">*</span><span id="efo_sign_login_id"/></th>
					<td>
						<% SetMaxLength(this.WtbUserLoginId, "@@User.login_id.length_max@@"); %>
						<% if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED) tbUserLoginId.Attributes["Type"] = "email"; %>
						<asp:textbox ID="tbUserLoginId" width="120" runat="server"></asp:textbox>
						<span class="notes">※6～15桁</span>
						<asp:customvalidator
							ID="cvUserLoginId"
							runat="server"
							ControlToValidate="tbUserLoginId"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<%-- ソーシャルログイン連携されている場合はパスワードスキップ --%>
				<%if (this.IsVisible_UserPassword){ %>
				<tr>
					<th>
						<%: ReplaceTag("@@User.password.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_password"/>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserPassword, "@@User.password.length_max@@"); %>
						<asp:Textbox ID="tbUserPassword" textmode="Password" autocomplete="off" cssclass="password" runat="server"></asp:Textbox>
						<span class="notes">※半角英数字混合7～15文字</span>
						<asp:CustomValidator
							ID="cvUserPassword"
							runat="server"
							ControlToValidate="tbUserPassword"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<tr>
					<th>
						<%: ReplaceTag("@@User.password.name@@") %>(確認用)
						<span class="necessary">*</span><span id="efo_sign_password_conf"/>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserPasswordConf, "@@User.password.length_max@@"); %>
						<asp:Textbox ID="tbUserPasswordConf" textmode="Password" autocomplete="off" cssclass="password" runat="server"></asp:Textbox>
						<span class="notes">※半角英数字混合7～15文字</span>
						<asp:CustomValidator
							ID="cvUserPasswordConf"
							runat="server"
							ControlToValidate="tbUserPasswordConf"
							ValidationGroup="UserEasyRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
			</table>
		</div>
		<% } %>
	</div>
	<div ID="dvUserRegistRegulation" class="unit" style="margin-top:20px;">
		<h3>会員規約について</h3>
		<div class="dvRegulation">
			<uc:UserRegistRegulationMessage runat="server" />
		</div>
		<div style="text-align:center;margin-top:1.5em">
			<asp:CheckBox ID="cbUserAcceptedRegulation" Text="会員規約に同意する" runat="server" />
			<asp:CustomValidator
				ID="cvUserAcceptedRegulation"
				runat="server"
				ValidationGroup="UserEasyRegist"
				ValidateEmptyText="true"
				SetFocusOnError="true"
				ClientValidationFunction="ClientValidationUserAcceptedRegulation"
				ErrorMessage="会員規約に同意いただけない場合は、ご登録することができません。"
				CssClass="error_inline" />
		</div>
	</div>
	<div class="dvUserBtnBox">
		<p>
			<span><a href="javascript:history.back();" class="btn btn-large">戻る</a></span>
			<span><asp:LinkButton ID="lbRegister" ValidationGroup="UserEasyRegist" OnClientClick="return exec_submit();" OnClick="lbRegister_Click" class="btn btn-large btn-inverse" runat="server">登録する</asp:LinkButton></span>
		</p>
	</div>
</div>

<script type="text/javascript">
<!--
	bindEvent();

	<%-- UpdataPanelの更新時のみ処理を行う --%>
	function bodyPageLoad() {
		if (Sys.WebForms == null) return;
		var isAsyncPostback = Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack();
		if (isAsyncPostback) {
			bindEvent();
		}
	}

	<%-- イベントをバインドする --%>
	function bindEvent() {
		bindExecAutoKana();
		bindZipCodeSearch();
	}

	<%-- 氏名（姓・名）の自動振り仮名変換のイベントをバインドする --%>
	function bindExecAutoKana() {
		execAutoKanaWithKanaType(
			$("#<%= tbUserName1.ClientID %>"),
			$("#<%= tbUserNameKana1.ClientID %>"),
			$("#<%= tbUserName2.ClientID %>"),
			$("#<%= tbUserNameKana2.ClientID %>"));
	}

	<%-- 郵便番号検索のイベントをバインドする --%>
	function bindZipCodeSearch() {
		$("#<%= tbUserZip2.ClientID %>").keyup(function (e) {
			if (isValidKeyCodeForKeyEvent(e.keyCode) == false) return;
			checkZipCodeLengthAndExecPostback(
				$("#<%= tbUserZip1.ClientID %>"),
				$("#<%= tbUserZip2.ClientID %>"),
				"<%= lbSearchAddr.UniqueID %>",
				'<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_ZIPCODE_SEARCHER_GET_ADDR_JSON %>',
				'#addrSearchErrorMessage'
			);
		});
		$("#<%= lbSearchAddr.ClientID %>").on('click', function () {
			checkZipCodeLengthAndExecPostback(
				$("#<%= tbUserZip1.ClientID %>"),
				$("#<%= tbUserZip2.ClientID %>"),
				"<%= lbSearchAddr.UniqueID %>",
				'<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_ZIPCODE_SEARCHER_GET_ADDR_JSON %>',
				'#addrSearchErrorMessage'
			);
		});
	}

	$(document).on('click', '.search-result-layer-close', function () {
		closePopupAndLayer();
	});

	$(document).on('click', '.search-result-layer-addr', function () {
		bindSelectedAddr($('li.search-result-layer-addr').index(this));
	});

	<%-- 複数住所検索結果からの選択値を入力フォームにバインドする --%>
	function bindSelectedAddr(selectedIndex) {
		var selectedAddr = $('.search-result-layer-addrs li').eq(selectedIndex);
		$("#<%= ddlUserAddr1.ClientID %>").val(selectedAddr.find('.addr').text());
		$("#<%= tbUserAddr2.ClientID %>").val(selectedAddr.find('.city').text() + selectedAddr.find('.town').text());
		$("#<%= tbUserAddr3.ClientID %>").focus();

		closePopupAndLayer();
	}

	<%-- ソーシャルログイン用 --%>
	<% if (Constants.SOCIAL_LOGIN_ENABLED) { %>
	$(function () {
		var data = $('#<%= WhfSocialLoginJson.ClientID %>').val();
		var json = data ? JSON.parse(data) : {};
		<%-- メールアドレス補完 --%>
		var mails = json['email'];
		if (mails && mails.length > 0) {
			var email = mails[0]['email'];
			if ($('#<%= WtbUserMailAddr.ClientID %>').val().length === 0) $('#<%= WtbUserMailAddr.ClientID %>').val(email);
			if ($('#<%= WtbUserMailAddrConf.ClientID %>').val().length === 0) $('#<%= WtbUserMailAddrConf.ClientID %>').val(email);
		}
	});
	<% } %>

	<%-- 会員規約に同意のクライアントチェック --%>
	function ClientValidationUserAcceptedRegulation(sender, e) {
		e.IsValid = $("#<%= this.WcbUserAcceptedRegulation.ClientID %>").is(':checked');
	}
	//-->
</script>
<%--▼▼Amazonウィジェット用スクリプト▼▼--%>
<% if (Constants.AMAZON_LOGIN_OPTION_ENABLED) { %>
<script type="text/javascript">
	window.onAmazonLoginReady = function () {
		amazon.Login.setClientId('<%=Constants.PAYMENT_AMAZON_CLIENTID %>');
	};
	window.onAmazonPaymentsReady = function () {
		if ($('#AmazonLoginButton').length) showButton();
		if ($('#addressBookWidgetDiv').length) showAddressBookWidget();
	};

	<%--Amazonアドレス帳表示ウィジェット--%>
	function showAddressBookWidget() {
		new OffAmazonPayments.Widgets.AddressBook({
			sellerId: '<%=Constants.PAYMENT_AMAZON_SELLERID %>',
			onOrderReferenceCreate: function (orderReference) {
				var orderReferenceId = orderReference.getAmazonOrderReferenceId();
				$('#<%= this.WhfAmazonOrderRefID.ClientID%>').val(orderReferenceId);
			},
			onAddressSelect: function (orderReference) {
				var $addressBookErrorMessage = $('#addressBookErrorMessage');
				$addressBookErrorMessage.empty();
				getAmazonAddress(function (response) {
					var data = JSON.parse(response.d);
					if (data.Error) $addressBookErrorMessage.html(data.Error);
					$("#<%= tbUserTel1.ClientID %>").val(data.Input.Phone1);
					$("#<%= tbUserTel2.ClientID %>").val(data.Input.Phone2);
					$("#<%= tbUserTel3.ClientID %>").val(data.Input.Phone3);
				});
			},
			design: { designMode: 'responsive' },
			onError: function (error) {
				alert(error.getErrorMessage());
				location.href = "<%=Constants.PATH_ROOT + Constants.PAGE_FRONT_LOGIN %>";
			}
		}).bind("addressBookWidgetDiv");
	}

	<%--Amazonボタン表示ウィジェット--%>
	function showButton() {
		var authRequest;
		OffAmazonPayments.Button("AmazonLoginButton", "<%=Constants.PAYMENT_AMAZON_SELLERID %>", {
			type: "LwA",
			color: "Gold",
			size: "large",
			authorization: function () {
				loginOptions = { scope: "payments:shipping_address payments:widget profile", popup: true };
				authRequest = amazon.Login.authorize(loginOptions, "<%=w2.App.Common.Amazon.Util.AmazonUtil.CreateCallbackUrl(Constants.PAGE_FRONT_AMAZON_USER_EASY_REGISTER_CALLBACK) %>");
				<% Session[Constants.SESSION_KEY_NEXT_PAGE_FOR_CHECK] = Constants.PAGE_FRONT_AMAZON_USER_EASY_REGISTER_CALLBACK; %>
			},
			onError: function (error) {
				alert(error.getErrorMessage());
				location.href = "<%=Constants.PATH_ROOT + Constants.PAGE_FRONT_LOGIN %>";
			}
		});
		$('#OffAmazonPaymentsWidgets0').css({ 'height': '44px', 'width': '218px' });
	}

	<%--Amazon住所取得関数--%>
	function getAmazonAddress(callback) {
		$.ajax({
			type: "POST",
			url: "<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_EASY_REGIST_INPUT%>/GetAmazonAddress",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify({
				amazonOrderReferenceId: $('#<%= this.WhfAmazonOrderRefID.ClientID %>').val()
			}),
			success: callback
		});
	}

</script>
<script async="async" type="text/javascript" charset="utf-8" src="<%=Constants.PAYMENT_AMAZON_WIDGETSSCRIPT %>"></script>
<% } %>
<%-- ▲▲Amazonウィジェット用スクリプト▲▲ --%>

</asp:Content>