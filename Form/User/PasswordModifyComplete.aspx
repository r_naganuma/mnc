﻿<%--
=========================================================================================================
  Module      : パスワード変更完了画面(PasswordModifyComplete.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2010 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_PasswordModifyComplete, App_Web_passwordmodifycomplete.aspx.b2a7112d" title="パスワード変更完了ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />

<article id="cartComp">

	<div class="inner">

		<div class="cartComp_ttl">
			<h2>変更完了</h2>
		</div>

		<div class="cartComp_txt">
			<p class="cartComp_txt--ttl">
				パスワード変更を完了いたしました。
			</p>
			<p class="cartComp_txt--cts lh">
				今後とも、「<%= WebSanitizer.HtmlEncode(ShopMessage.GetMessage("ShopName")) %>」をどうぞ宜しくお願い申し上げます。
			</p>
			<asp:LinkButton ID="lbTopPage" runat="server" OnClick="lbTopPage_Click" class="cartComp_txt--btn">
				トップページへ
			</asp:LinkButton>
		</div>
	</div>
</article>
</asp:Content>
