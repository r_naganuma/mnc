﻿<%--
=========================================================================================================
  Module      : 入荷通知メール情報一覧画面(UserProductArrivalMailList.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2010 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductRecommendByRecommendEngine" Src="~/Form/Common/Product/BodyProductRecommendByRecommendEngine.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_UserProductArrivalMailList, App_Web_userproductarrivalmaillist.aspx.b2a7112d" title="入荷お知らせメールリスト | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />
<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					入荷お知らせメール情報
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<h2>入荷お知らせメール情報</h2>

	<div class="mypageBox">
		<div class="mypageBox_cts">
			<%-- 登録完了メッセージ --%>
			<%if (this.blRequestComp){ %>
				<p class="leadTxt">リクエストを受け付けました。</p>
			<%} %>
			<p class="leadTxt">入荷お知らせメールをお申し込みいただいている商品の一覧です。</p>
			<%-- メール配信希望アラート --%>
			<% if (CheckUserMailFlg() == false){ %>
				<p class="leadTxt" style="display: none;">※入荷通知メールはお知らせメールの配信が無効でも配信されます。<br />
				</p>
			<% } %>
			<!-- ///// ページャ ///// -->
			<div style="display: none;" id="pagination" class="above clearFix"><%= this.PagerHtml %></div>
			<!-- ///// 入荷通知メール情報一覧 ///// -->
				<asp:Repeater id="rUserProductArrivalMailList" runat="server">
					<HeaderTemplate>
						<div class="avMailBox">
					</HeaderTemplate>
					<ItemTemplate>
						<div class="avMailBox_list">
							<div class="avMailBox_list--pic">
								<a href="<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailVariationUrl(Container.DataItem)) %>">
									<w2c:ProductImage ID="ProductImage1" ImageSize="M" ProductMaster="<%# Container.DataItem %>" IsVariation="true" runat="server" />
								</a>
							</div>
							<div class="avMailBox_list--name">
								<a href="<%# WebSanitizer.UrlAttrHtmlEncode(CreateProductDetailVariationUrl(Container.DataItem)) %>">
									<asp:Literal ID="lProductname" Text='<%# WebSanitizer.HtmlEncode(CreateProductJointName(Container.DataItem))%>' runat="server" ></asp:Literal>
								</a>
							</div>
							<div class="avMailBox_list--detail">
								<div class="detailInner">
									<div class="detailBox">
										<p class="detailBox_ttl">
											配信先
										</p>
										<p class="detailBox_txt">
											<%# (StringUtility.ToEmpty(Eval("guest_mail_addr")) == "") ? StringUtility.ToHankaku(UserProductArrivalMailCommon.CreateMailAddressNameFromTagReplacer((string)Eval(Constants.FIELD_USERPRODUCTARRIVALMAIL_PCMOBILE_KBN))) : "" %>
											<%# (StringUtility.ToEmpty(Eval("guest_mail_addr")) != "") ? WebSanitizer.HtmlEncode((string)Eval("guest_mail_addr")) : "" %>
											<%# (StringUtility.ToEmpty(Eval("guest_mail_addr")) != "") ? "(" + StringUtility.ToHankaku(UserProductArrivalMailCommon.CreateMailAddressKbnNameFromTagReplacer((string)Eval(Constants.FIELD_USERPRODUCTARRIVALMAIL_PCMOBILE_KBN))) + ")" : ""%>
										</p>
									</div>
									<div class="detailBox">
										<p class="detailBox_ttl">
											通知期限
										</p>
										<div class="detailBox_date">
											<asp:TextBox ID="tbDateExpiredYearYear" MaxLength="4" Text="<%# WebSanitizer.HtmlEncode(((DateTime)Eval(Constants.FIELD_USERPRODUCTARRIVALMAIL_DATE_EXPIRED)).Year) %>" class="input01" runat="server"></asp:TextBox>
											<p class="detailBox_txt">年</p>
											<asp:TextBox ID="tbDateExpiredYearMonth" MaxLength="2" Text="<%# WebSanitizer.HtmlEncode(((DateTime)Eval(Constants.FIELD_USERPRODUCTARRIVALMAIL_DATE_EXPIRED)).Month) %>" class="input02" runat="server"></asp:TextBox>
											<p class="detailBox_txt">月末日</p>
										</div>
										<!-- ///// 入荷通知メール情報削除 ///// -->
										<a href='<%# WebSanitizer.UrlAttrHtmlEncode(CreateDeleteUserProductArrivalMailUrl(Container.DataItem)) %>' class="detailBox_delete">
											削除
										</a>
										<asp:HiddenField ID="hfArrivalMailNo" runat="server" Value="<%# Eval(Constants.FIELD_USERPRODUCTARRIVALMAIL_MAIL_NO) %>" />		
									</div>
								</div>
							</div>
						</div>			
					</ItemTemplate>
					<FooterTemplate>
						</div>
					</FooterTemplate>
				</asp:Repeater>
			<%-- エラーメッセージ --%>
			<% if (StringUtility.ToEmpty(this.ErrorMessage) != ""){ %>
				<p class="leadTxt"><%= this.ErrorMessage %></p>
			<% } %>

			<!-- ///// ページャ ///// -->
			<div style="display: none;" id="pagination" class="below clearFix"><%= this.PagerHtml %></div>
			
			<div class="cartNextbtn">
				<ul>
					<li class="nextBtn">
						<asp:LinkButton ID="btnUpdate" OnClick="btnUpdate_Click" OnClientClick="javascript:return confirm('内容を更新します。よろしいですか？')" Text="内容を更新する" runat="server" >
							内容を更新する
						</asp:LinkButton>
					</li>
				</ul>
			</div>

			<!-- 買い物を続けるボタン -->
			<%if (this.blRequestComp){ %>
				<a href='<%= WebSanitizer.UrlAttrHtmlEncode(this.BeforeProductUrl) %>' class="btn btn-large">
					お買い物を続ける</a>
			<%} %>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					入荷お知らせメール情報
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
</asp:Content>