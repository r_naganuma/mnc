﻿<%--
=========================================================================================================
  Module      : マイページ画面(MyPage.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="BodyUserProductRecommend" Src="~/Form/Common/Product/BodyUserProductRecommend.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductRecommendByRecommendEngine" Src="~/Form/Common/Product/BodyProductRecommendByRecommendEngine.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_MyPage, App_Web_mypage.aspx.b2a7112d" title="マイページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />

	<article id="mypage">

		<!--▽ パンくず ▽-->
		<div class="breadArea pc_contents">
			<ul>
				<li>
					<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
				</li>
				<li>
					<a href="#">
						マイページ
					</a>
				</li>
			</ul>
		</div>
		<!--△ パンくず △-->

		<h2><%= WebSanitizer.HtmlEncode(this.LoginUserName) %>様の<br class="sp_contents">マイページ</h2>
		<div class="topMain">
			<% if (this.LastLoggedinDate != "") { %>
				<h4 style="margin-bottom: 30px; display: none;">
					前回ログイン日時：<%: DateTimeUtility.ToStringFromRegion(this.LastLoggedinDate, DateTimeUtility.FormatType.LongDateHourMinute1Letter) %>
				</h4>
			<% } %>
			<%-- ▽ポイントオプション利用時▽ --%>
			<% if (Constants.W2MP_POINT_OPTION_ENABLED) { %>
				<table style="font-size: medium" width="100%">
					<tr style="font-size: 125%">
						<td width="45%">利用可能ポイント</td>
						<td width="55%" align="left">
							<%: GetNumeric(this.LoginUserPointUsable) %>pt
						</td>
					</tr>
					<tr>
					<td>通常ポイント</td>
						<td align="left"><%: GetNumeric(this.LoginUserBasePoint) %>pt</td>
					</tr>
					<% if (this.LoginUserPointExpiry.HasValue) { %>
						<tr>
							<td >通常ポイント有効期限</td>
							<td align="left"><%: DateTimeUtility.ToStringFromRegion(this.LoginUserPointExpiry, DateTimeUtility.FormatType.LongDate1LetterNoneServerTime) %></td>
						</tr>
					<% } %>
					<tr>
						<td style="vertical-align: top;">
							期間限定ポイント
							<span style="font-size: 0.8em;"><a id="aDetails" style="cursor: pointer; display: <%: this.HasLimitedTermPoint ? "inline" : "none" %>;">内訳を表示</a></span>
						</td>
						<td align="left"><%: GetNumeric(this.LoginUserLimitedTermPointTotal) %>pt</td>
					</tr>
					<tr style="display: <%: (this.LoginUserLimitedTermPointUsableTotal > 0) ? "" : "none" %>">
						<td></td>
						<td>
							<%: string.Format("(有効期間中：{0}pt)", GetNumeric(this.LoginUserLimitedTermPointUsableTotal)) %>
						</td>
					</tr>

					<asp:Repeater
						ID="rUsableLimitedTermPoint"
						ItemType="w2.App.Common.Option.PointOption.LimitedTermPoint"
						runat="server">
						<ItemTemplate>
							<tr id="usableLimitedTermPoint<%#: Container.ItemIndex %>">
								<td></td>
								<td>&nbsp;
									<%#:
										string.Format(
											"{0}pt {1}から{2}まで有効",
											GetNumeric(Item.Point),
											DateTimeUtility.ToStringFromRegion(Item.EffectiveDate, DateTimeUtility.FormatType.LongDate1LetterNoneServerTime),
											DateTimeUtility.ToStringFromRegion(Item.ExpiryDate, DateTimeUtility.FormatType.LongDate1LetterNoneServerTime))
									%>
								</td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>

					<tr style="display: <%: (this.LoginUserLimitedTermPointUnusableTotal > 0) ? "" : "none" %>">
						<td></td>
						<td>
							<%: string.Format("(有効期間前：{0}pt)", GetNumeric(this.LoginUserLimitedTermPointUnusableTotal)) %>
						</td>
					</tr>

					<asp:Repeater
						ID="rUnusableLimitedTermPoint"
						ItemType="w2.App.Common.Option.PointOption.LimitedTermPoint"
						runat="server">
						<ItemTemplate>
							<tr id="unusableLimitedTermPoint<%#: Container.ItemIndex %>">
								<td></td>
								<td>&nbsp;
									<%#:
										string.Format(
											"{0}pt {1}から{2}まで有効",
											GetNumeric(Item.Point),
											DateTimeUtility.ToStringFromRegion(Item.EffectiveDate, DateTimeUtility.FormatType.LongDate1LetterNoneServerTime),
											DateTimeUtility.ToStringFromRegion(Item.ExpiryDate, DateTimeUtility.FormatType.LongDate1LetterNoneServerTime))
									%>
								</td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>

					<tr>
						<td>(仮ポイント)</td>
						<td align="left">(<%: GetNumeric(this.LoginUserPoint.PointTemp) %>pt)</td>
					</tr>
				</table>
				<br/>
			<% } %>
			<% if (HasLoginMemberRank())
			   { %>
				<h3>現在の会員ランク：<%: this.MemberRankName %></h3>
				<% if (MemberRankUtility.IsBenefitPointAdd(this.LoginMemberRankInfo))
				   { %>
					<div class="topMain_rank"><p>ポイント加算：<%: MemberRankUtility.GetBenefitPointAdd(this.LoginMemberRankInfo) %></p></div>
				<% } %>
				<% if (MemberRankUtility.IsBenefitShippingDiscount(this.LoginMemberRankInfo))
				   { %>
					<div class="topMain_rank"><p>配送料割引：<%: MemberRankUtility.GetBenefitShippingDiscount(this.LoginMemberRankInfo) %></p></div>
				<% } %>
				<% if (MemberRankUtility.IsBenefitFixedFuchaseDiscountRate(this.LoginMemberRankInfo))
				   { %>
					<div class="topMain_rank"><p>定期会員割引：<%: MemberRankUtility.GetBenefitFixedPurchaseDiscountRate(this.LoginMemberRankInfo, " {0} % 割引") %></p></div>
				<% } %>
				<% if (MemberRankUtility.IsBenefitOrderDiscount(this.LoginMemberRankInfo))
				   { %>
					<div class="topMain_rank"><p>会員割引：<%: MemberRankUtility.GetBenefitOrderDiscount(this.LoginMemberRankInfo, "{0} 以上お買い上げ時 {1}") %></p></div>
				<% } %>
			<% } %>
			<div style="display: none;" id="dvUpSell">
				<dl>
					<%-- ユーザおすすめ商品 --%>
					<uc:BodyProductRecommendByRecommendEngine runat="server" RecommendCode="pc611" RecommendTitle="おすすめ商品一覧" MaxDispCount="4" DispCategoryId="" NotDispCategoryId="" NotDispRecommendProductId="" />
				</dl>
			</div>
		</div>

		<div class="topIndex">
			<%if (Constants.FIXEDPURCHASE_OPTION_ENABLED) { %>
			<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_FIXED_PURCHASE_LIST) %>" class="topIndex_list">
				<div class="topIndex_list--pic">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/mypage/icon-01.svg" />
				</div>
				<div class="topIndex_list--txt">
					<p>
						定期購入情報
						<span>定期便のお届け日やサイクルなどの確認・変更はこちら</span>
					</p>
				</div>
			</a>
			<%} %>
			<%if (Constants.W2MP_COUPON_OPTION_ENABLED) { %>
			<a href="<%: Constants.PATH_ROOT + Constants.PAGE_FRONT_COUPON_BOX %>" class="topIndex_list">
				<div class="topIndex_list--pic">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/mypage/icon-02.svg" />
				</div>
				<div class="topIndex_list--txt">
					<p>
						クーポンBOX
						<span>現在、ご利用可能なクーポンをご確認いただけます</span>
					</p>
				</div>
			</a>
			<%} %>
			<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_ORDER_HISTORY_LIST) %>" class="topIndex_list">
				<div class="topIndex_list--pic">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/mypage/icon-03.svg" />
				</div>
				<div class="topIndex_list--txt">
					<p>
						購入履歴一覧（通常・定期）
						<span>過去のお買い物履歴をご確認いただけます</span>
					</p>
				</div>
			</a>
			<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_FAVORITE_LIST) %>" class="topIndex_list">
				<div class="topIndex_list--pic">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/mypage/icon-04.svg" />
				</div>
				<div class="topIndex_list--txt">
					<p>
						お気に入りリスト
						<span>お気に入りに登録した商品がご確認いただけます</span>
					</p>
				</div>
			</a>
			<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_PRODUCT_ARRIVAL_MAIL_LIST) %>" class="topIndex_list">
				<div class="topIndex_list--pic">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/mypage/icon-05.svg" />
				</div>
				<div class="topIndex_list--txt">
					<p>
						入荷お知らせメール情報
						<span>入荷お知らせメールをお申し込みいただいている商品です</span>
					</p>
				</div>
			</a>
			<a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_MODIFY_INPUT) %>" class="topIndex_list">
				<div class="topIndex_list--pic">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/mypage/icon-06.svg" />
				</div>
				<div class="topIndex_list--txt">
					<p>
						登録情報の変更
						<span>ご登録住所やメールアドレス・パスワードなどの変更はこちら</span>
					</p>
				</div>
			</a>
			<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_SHIPPING_LIST) %>" class="topIndex_list">
				<div class="topIndex_list--pic">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/mypage/icon-07.svg" />
				</div>
				<div class="topIndex_list--txt">
					<p>
						配送先リスト
						<span>よくご利用になるお届け先をご登録いただけます</span>
					</p>
				</div>
			</a>
			<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_WITHDRAWAL_INPUT) %>" class="topIndex_list">
				<div class="topIndex_list--pic">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/mypage/icon-08.svg" />
				</div>
				<div class="topIndex_list--txt">
					<p>
						会員の退会
						<span>退会のお手続きはこちら</span>
					</p>
				</div>
			</a>
		</div>

		<!--▽ パンくず ▽-->
		<div class="breadArea sp_contents">
			<ul>
				<li>
					<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
				</li>
				<li>
					<a href="#">
						マイページ
					</a>
				</li>
			</ul>
		</div>
		<!--△ パンくず △-->
	</article>

	<script type="text/javascript">
		var detailsDisplayFlag = false;
		$(function () {
			$(document).ready(function() {
				$('[id^=usableLimitedTermPoint]').hide();
				$('[id^=unusableLimitedTermPoint]').hide();
			});

			$('#aDetails').on('click', function () {
				detailsDisplayFlag = (detailsDisplayFlag === false);
				$('#aDetails').html(detailsDisplayFlag ? "隠す" : "内訳を表示");

				if (detailsDisplayFlag) {
					$('[id^=usableLimitedTermPoint]').show();
					$('[id^=unusableLimitedTermPoint]').show();
				} else {
					$('[id^=usableLimitedTermPoint]').hide();
					$('[id^=unusableLimitedTermPoint]').hide();
				}
			});
		});
	</script>
</asp:Content>