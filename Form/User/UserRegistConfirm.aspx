﻿<%--
=========================================================================================================
  Module      : 会員登録確認画面(UserRegistConfirm.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="BodyUserExtendRegist" Src="~/Form/Common/User/BodyUserExtendRegist.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_UserRegistConfirm, App_Web_userregistconfirm.aspx.b2a7112d" title="会員新規登録確認ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />
<article id="cart" class="cartPeyment">

<!--▽ パンくず ▽-->
<div class="breadArea pc_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				会員情報の入力
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

<!-- main -->
<section class="cartTtl second">
	<h2>
		会員情報の入力
	</h2>
</section>
<!-- // main -->

<div class="cartLead">
	<p>
		お客様の入力された内容は以下の通りでよろしいでしょうか？<br class="pc_contents">よろしければ「登録する」ボタンを押して下さい。
	</p>
</div>

<div>
	<div>
		<dl class="cartInputRegist confirm">
				<dt>
					<%-- 氏名 --%>
					<%: ReplaceTag("@@User.name.name@@") %></dt>
				<dd>
					<%: this.UserInput.Name1 %>
					<%: this.UserInput.Name2 %>
				</dd>
				<% if (this.IsUserAddrJp) { %>
				<dt>
					<%-- 氏名 --%>
					カナ</dt>
				<dd>
					<%: this.UserInput.NameKana1 %>
					<%: this.UserInput.NameKana2 %>
				</dd>
				<% } %>
			<%if (Constants.PRODUCTREVIEW_ENABLED) { %>
				<dt style="display: none;">
					<%-- ニックネーム --%>
					<%: ReplaceTag("@@User.nickname.name@@") %></dt>
				<dd style="display: none;">
					<%: this.UserInput.NickName %></dd>
			<%} %>
				<dt>
					<%-- ＰＣメールアドレス --%>
					<%: ReplaceTag("@@User.mail_addr.name@@") %></dt>
				<dd>
					<%: this.UserInput.MailAddr %></dd>
			<% if (Constants.DISPLAYMOBILEDATAS_OPTION_ENABLED) { %>
				<dt>
					<%-- モバイルメールアドレス --%>
					<%: ReplaceTag("@@User.mail_addr2.name@@") %></dt>
				<dd>
					<%: this.UserInput.MailAddr2 %></dd>
			<% } %>
				<dt>
					<%: ReplaceTag("@@User.addr.name@@") %>
				</dt>
				<dd>
					<% if (this.IsUserAddrJp) { %>〒<%: this.UserInput.Zip %><br /><% } %>
					<%: this.UserInput.Addr1 %> <%: this.UserInput.Addr2 %><br />
					<%: this.UserInput.Addr3 %> <%: this.UserInput.Addr4 %><br />
					<% if (this.IsUserAddrJp == false) { %><%: this.UserInput.Addr5 %> <%: this.UserInput.Zip %><br /><% } %>
					<%: this.UserInput.AddrCountryName %>
				</dd>
			<% if (Constants.DISPLAY_CORPORATION_ENABLED){ %>
				<dt>
					<!-- 企業名 -->
					<%: ReplaceTag("@@User.company_name.name@@")%>
				</dt>
				<dd>
					<%: this.UserInput.CompanyName %></dd>
				<dt>
					<!-- 部署名 -->
					<%: ReplaceTag("@@User.company_post_name.name@@")%>
				</dt>
				<dd>
					<%: this.UserInput.CompanyPostName %></dd>
			<%}%>
				<%-- 電話番号 --%>
				<dt><%: ReplaceTag("@@User.tel1.name@@", this.UserAddrCountryIsoCode) %></dt>
				<dd><%: this.UserInput.Tel1 %></dd>
				<dt style="display: none;"><%: ReplaceTag("@@User.tel2.name@@", this.UserAddrCountryIsoCode) %></dt>
				<dd style="display: none;"><%: this.UserInput.Tel2 %></dd>
				<dt>
					<%-- 生年月日 --%>
					<%: ReplaceTag("@@User.birth.name@@") %></dt>
				<dd>
				<%if (this.UserInput.Birth != null) {%>
					<%: this.UserInput.BirthYear %>年<%: this.UserInput.BirthMonth %>月<%: this.UserInput.BirthDay %>日
				<%} %></dd>
				<dt>
					<%-- 性別 --%>
					<%: ReplaceTag("@@User.sex.name@@") %></dt>
				<dd class="mrg">
					<%: this.UserInput.SexValueText %></dd>
				<dt style="display: none;">
					<%: ReplaceTag("@@User.mail_flg.name@@") %>
				</dt>
				<dd style="display: none;">
					<%: this.UserInput.MailFlgValueText %></dd>
			<%-- ユーザー拡張項目　HasInput:true(入力画面)/false(確認画面)　HasRegist:true(新規登録)/false(登録編集) --%>
			<uc:BodyUserExtendRegist ID="ucBodyUserExtendRegist" runat="server" HasInput="false" HasRegist="true" />
		</dl>
	</div>

	<%if ((Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED == false) || this.IsVisible_UserPassword) { %>
	<div>
		<dl class="cartInputRegist confirm">
			<%if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED == false) { %>
				<dt>
					<%-- ログインID --%>
					<%: ReplaceTag("@@User.login_id.name@@") %></dt>
				<dd>
					<%: this.UserInput.LoginId %></dd>
			<%} %>
			<%-- ソーシャルログイン連携されている場合はパスワードスキップ --%>
			<%if (this.IsVisible_UserPassword){ %>
				<dt>
					<%-- パスワード --%>
					<%: ReplaceTag("@@User.password.name@@") %></dt>
				<dd>
					<%: StringUtility.ChangeToAster(this.UserInput.Password) %></dd>
			<% } %>
		</dl>
	</div>
	<% } %>

	<div class="registBtn">
		<ul>
			<li><asp:LinkButton ID="lbSend" runat="server" OnClientClick="return exec_submit()" OnClick="lbSend_Click">登録する</asp:LinkButton></li>
			<li><asp:LinkButton ID="lbBack" runat="server" OnClientClick="return exec_submit()" OnClick="lbBack_Click">前のページに戻る</asp:LinkButton></li>
		</ul>
	</div>
</div>
</article>

<!--▽ パンくず ▽-->
<div class="breadArea sp_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				会員情報の入力
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

</asp:Content>