﻿<%--
=========================================================================================================
  Module      : アドレス帳入力画面(UserShippingInput.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_UserShippingInput, App_Web_usershippinginput.aspx.b2a7112d" title="配送先リストの登録 | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<%@ Register TagPrefix="uc" TagName="Layer" Src="~/Form/Common/Layer/SearchResultLayer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />
<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					配送先リストの登録
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<h2>配送先リストの登録</h2>

	<div class="mypageBox">
		<div class="mypageBox_cts">
			<div class="dvContentsInfo">
				<p id="pRegistInfo" runat="server" visible="false" class="leadTxt">
					アドレス帳に新しいお届け先を登録します。<br>
					下のフォームに入力し、「確認する」ボタンを押してください。
				</p>
				<p id="pModifyInfo" runat="server" visible="false" class="leadTxt">
					アドレス帳に登録されているお届け先を編集します。<br>
					下のフォームに入力し、「確認する」ボタンを押してください。
				</p>
			</div>
			<div class="dvUserShippingInfo">
				<%-- UPDATE PANEL開始 --%>
				<asp:UpdatePanel ID="upUpdatePanel" runat="server">
				<ContentTemplate>
				<dl class="cartInput">
						<dt>配送先名<span class="icnKome">※</span></dt>
						<dd>
							<asp:TextBox id="tbName" Runat="server" maxlength="30" CssClass="nameShipping"></asp:TextBox>
							<asp:CustomValidator
								ID="cvName"
								runat="Server"
								ControlToValidate="tbName"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
						<%-- 氏名 --%>
						<dt><%: ReplaceTag("@@User.name.name@@") %><span class="icnKome">※</span></dt>
						<dd>
							<div class="nameFlex">
								<div class="nameFlex_list">
									<% SetMaxLength(this.WtbShippingName1, "@@User.name1.length_max@@"); %>
									<p>姓</p>
									<asp:TextBox id="tbShippingName1" Runat="server" CssClass="nameFirst"></asp:TextBox>
								</div>
								<div class="nameFlex_list">
									<% SetMaxLength(this.WtbShippingName2, "@@User.name2.length_max@@"); %>
									<p>名</p><asp:TextBox id="tbShippingName2" Runat="server" CssClass="nameLast"></asp:TextBox>
								</div>
							</div>
							<asp:CustomValidator
								ID="cvShippingName1"
								runat="Server"
								ControlToValidate="tbShippingName1"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<asp:CustomValidator
								ID="cvShippingName2"
								runat="Server"
								ControlToValidate="tbShippingName2"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
					<% if (this.IsShippingAddrJp) { %>
						<%-- 氏名（かな） --%>
						<dt>カナ<span class="icnKome">※</span></dt>
						<dd>
							<div class="nameFlex">
								<div class="nameFlex_list">
									<% SetMaxLength(this.WtbShippingNameKana1, "@@User.name_kana1.length_max@@"); %>
									<p>セイ</p>
									<asp:TextBox id="tbShippingNameKana1" Runat="server" CssClass="nameFirst"></asp:TextBox>
								</div>
								<div class="nameFlex_list">
									<% SetMaxLength(this.WtbShippingNameKana2, "@@User.name_kana2.length_max@@"); %>
									<p>メイ</p><asp:TextBox id="tbShippingNameKana2" Runat="server" CssClass="nameLast"></asp:TextBox>
								</div>
							</div>
							<asp:CustomValidator
								ID="cvShippingNameKana1"
								runat="Server"
								ControlToValidate="tbShippingNameKana1"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<asp:CustomValidator
								ID="cvShippingNameKana2"
								runat="Server"
								ControlToValidate="tbShippingNameKana2"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
					<% } %>
					<% if (Constants.GLOBAL_OPTION_ENABLE) { %>
						<%-- 国 --%>
						<dt>
							<%: ReplaceTag("@@User.country.name@@", this.ShippingAddrCountryIsoCode) %>
							<span class="icnKome">※</span>
						</dt>
						<dd>
							<asp:DropDownList runat="server" ID="ddlShippingCountry" AutoPostBack="true" OnSelectedIndexChanged="ddlShippingCountry_SelectedIndexChanged"></asp:DropDownList>
							<asp:CustomValidator
								ID="cvShippingCountry"
								runat="Server"
								ControlToValidate="ddlShippingCountry"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								EnableClientScript="false"
								CssClass="error_inline" />
						</dd>
					<% } %>
					<% if (this.IsShippingAddrJp) { %>
						<%-- 郵便番号 --%>
						<dt>
							<%: ReplaceTag("@@User.zip.name@@") %>
							<span class="icnKome">※</span>
						</dt>
						<dd>
							<div class="zipFlex">
								<div class="zipFlex_list">
									<asp:TextBox id="tbShippingZip1" MaxLength="3" Runat="server" CssClass="zipFirst" Type="tel"></asp:TextBox>
								</div>
								<div class="zipFlex_line">
									<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
								</div>
								<div class="zipFlex_list">
									<asp:TextBox id="tbShippingZip2" MaxLength="4" Runat="server" CssClass="zipLast" Type="tel" OnTextChanged="lbSearchShippingAddr_Click"></asp:TextBox>
								</div>
							</div>
							<asp:LinkButton style="display: none;" ID="lbSearchShippingAddr" runat="server" OnClick="lbSearchShippingAddr_Click" class="btn btn-mini" OnClientClick="return false;">住所検索</asp:LinkButton>
							<%--検索結果レイヤー--%>
							<uc:Layer ID="ucLayer" runat="server" />
							<asp:CustomValidator
								ID="cvShippingZip1"
								runat="Server"
								ControlToValidate="tbShippingZip1"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<asp:CustomValidator
								ID="cvShippingZip2"
								runat="Server"
								ControlToValidate="tbShippingZip2"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<%-- エラーメッセージ --%>
							<% if(StringUtility.ToEmpty(this.ZipInputErrorMessage) != "") {%>
							<span style="color :Red" id="addrSearchErrorMessage">
								<%: this.ZipInputErrorMessage %></span>
							<% } %>
						</dd>
						<%-- 都道府県 --%>
						<dt>住所<span class="icnKome">※</span></dt>
						<dd>
							<div class="selectPrefecture">
								<asp:DropDownList id="ddlShippingAddr1" runat="server" DataTextField="Text" DataValueField="Value"></asp:DropDownList>
							</div>
							<asp:CustomValidator
								ID="cvShippingAddr1"
								runat="Server"
								ControlToValidate="ddlShippingAddr1"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
					<% } %>
						<dd>
							<p class="adTtl">
								<%: ReplaceTag("@@User.addr2.name@@", this.ShippingAddrCountryIsoCode) %>
								<span class="icnKome">※</span>
							</p>
							<% if (IsCountryTw(ddlShippingCountry.SelectedValue)) { %>
								<asp:DropDownList runat="server" ID="ddlShippingAddr2" AutoPostBack="true" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="ddlShippingAddr2_SelectedIndexChanged"></asp:DropDownList>
							<% } else { %>
								<% SetMaxLength(this.WtbShippingAddr2, "@@User.addr2.length_max@@"); %>
								<asp:TextBox id="tbShippingAddr2" Runat="server" CssClass="addr"></asp:TextBox>
								<asp:CustomValidator
									ID="cvShippingAddr2"
									runat="Server"
									ControlToValidate="tbShippingAddr2"
									ValidationGroup="UserShippingRegist"
									ValidateEmptyText="true"
									SetFocusOnError="true"
									ClientValidationFunction="ClientValidate"
									CssClass="error_inline" />
							<% } %>
						</dd>
						<dd>
							<p class="adTtl">
								<%: ReplaceTag("@@User.addr3.name@@", this.ShippingAddrCountryIsoCode) %>
								<% if (IsAddress3Necessary(this.ShippingAddrCountryIsoCode)){ %><span class="icnKome">※</span><% } %>
							</p>
							<% if (IsCountryTw(ddlShippingCountry.SelectedValue)) { %>
								<asp:DropDownList runat="server" ID="ddlShippingAddr3" AutoPostBack="true" DataTextField="Key" DataValueField="Value" Width="95"></asp:DropDownList>
							<% } else { %>
								<% SetMaxLength(this.WtbShippingAddr3, "@@User.addr3.length_max@@"); %>
								<asp:TextBox id="tbShippingAddr3" Runat="server" CssClass="addr2"></asp:TextBox>
								<asp:CustomValidator
									ID="cvShippingAddr3"
									runat="Server"
									ControlToValidate="tbShippingAddr3"
									ValidationGroup="UserShippingRegist"
									ValidateEmptyText="true"
									SetFocusOnError="true"
									ClientValidationFunction="ClientValidate"
									CssClass="error_inline" />
							<% } %>
						</dd>
						<dd>
							<p class="adTtl">
								<%: ReplaceTag("@@User.addr4.name@@", this.ShippingAddrCountryIsoCode) %>
							<% if (this.IsShippingAddrJp == false) { %><span class="icnKome">※</span><% } %>
							</p>
							<% SetMaxLength(this.WtbShippingAddr4, "@@User.addr4.length_max@@"); %>
							<asp:TextBox id="tbShippingAddr4" Runat="server" CssClass="addr2"></asp:TextBox>
							<asp:CustomValidator
								ID="cvShippingAddr4"
								runat="Server"
								ControlToValidate="tbShippingAddr4"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
						<%-- ビル・マンション名 --%>
					<% if (this.IsShippingAddrJp == false) { %>
						<%-- 郵便番号（海外向け） --%>
						<dt>
							<%: ReplaceTag("@@User.addr5.name@@", this.ShippingAddrCountryIsoCode) %>
							<% if (this.IsShippingAddrUs) { %><span class="icnKome">※</span><% } %>
						</dt>
						<dd>
							<% if (this.IsShippingAddrUs) { %>
							<asp:DropDownList ID="ddlShippingAddr5" runat="server"></asp:DropDownList>
								<asp:CustomValidator
									ID="cvShippingAddr5"
									runat="Server"
									ControlToValidate="ddlShippingAddr5"
									ValidationGroup="UserShippingRegistGlobal"
									ValidateEmptyText="true"
									SetFocusOnError="true"
									ClientValidationFunction="ClientValidate"
									CssClass="error_inline" />
							<% } else { %>
							<asp:TextBox ID="tbShippingAddr5" runat="server"></asp:TextBox>
							<% } %>
						</dd>
						<dt>
							<%: ReplaceTag("@@User.zip.name@@", this.ShippingAddrCountryIsoCode) %>
							<% if (this.IsShippingAddrZipNecessary) { %><span class="icnKome">※</span><% } %>
						</dt>
						<dd>
							<asp:TextBox ID="tbShippingZipGlobal" MaxLength="30" runat="server"></asp:TextBox>
							<asp:CustomValidator
								ID="cvShippingZipGlobal"
								runat="Server"
								ControlToValidate="tbShippingZipGlobal"
								ValidationGroup="UserShippingRegistGlobal"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
					<% } %>
					<% if (Constants.DISPLAY_CORPORATION_ENABLED){ %>
						<%-- 企業名 --%>
						<dt>
							<%: ReplaceTag("@@User.company_name.name@@") %>
							<span class="necessary"></span>
						</dt>
						<dd>
							<% SetMaxLength(this.WtbShippingCompanyName, "@@User.company_name.length_max@@"); %>
							<asp:TextBox id="tbShippingCompanyName" Runat="server" CssClass="addr2"></asp:TextBox>
							<asp:CustomValidator
								ID="cvShippingCompanyName"
								runat="Server"
								ControlToValidate="tbShippingCompanyName"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
						<%-- 部署名 --%>
						<dt>
							<%: ReplaceTag("@@User.company_post_name.name@@")%>
							<span class="necessary"></span>
						</dt>
						<dd>
							<% SetMaxLength(this.WtbShippingCompanyPostName, "@@User.company_post_name.length_max@@"); %>
							<asp:TextBox id="tbShippingCompanyPostName" Runat="server" CssClass="addr2"></asp:TextBox>
							<asp:CustomValidator
								ID="cvShippingCompanyPostName"
								runat="Server"
								ControlToValidate="tbShippingCompanyPostName"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
					<%} %>
						<%-- 電話番号 --%>
						<% if (this.IsShippingAddrJp) { %>
						<dt>
							<%: ReplaceTag("@@User.tel1.name@@") %>
							<span class="icnKome">※</span>
						</dt>
						<dd>
							<div class="telFlex">
								<div class="telFlex_list">
									<asp:TextBox id="tbShippingTel1_1" MaxLength="6" Runat="server" CssClass="tel1" Type="tel"></asp:TextBox>
								</div>
								<div class="telFlex_line">
									<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
								</div>
								<div class="telFlex_list">
									<asp:TextBox id="tbShippingTel1_2" MaxLength="4" Runat="server" CssClass="tel2" Type="tel"></asp:TextBox>
								</div>
								<div class="telFlex_line">
									<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_line.svg" alt="">
								</div>
								<div class="telFlex_list">
									<asp:TextBox id="tbShippingTel1_3" MaxLength="4" Runat="server" CssClass="tel3" Type="tel"></asp:TextBox>
								</div>
							</div>
							<asp:CustomValidator
								ID="cvShippingTel1_1"
								runat="Server"
								ControlToValidate="tbShippingTel1_1"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<asp:CustomValidator
								ID="cvShippingTel1_2"
								runat="Server"
								ControlToValidate="tbShippingTel1_2"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
							<asp:CustomValidator
								ID="cvShippingTel1_3"
								runat="Server"
								ControlToValidate="tbShippingTel1_3"
								ValidationGroup="UserShippingRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
						<% } else { %>
						<dt>
							<%: ReplaceTag("@@User.tel1.name@@", this.ShippingAddrCountryIsoCode) %>
							<span class="icnKome">※</span>
						</dt>
						<dd>
							<asp:TextBox ID="tbShippingTel1Global" MaxLength="30" runat="server" Type="tel"></asp:TextBox>
							<asp:CustomValidator
								ID="cvShippingTel1Global"
								runat="Server"
								ControlToValidate="tbShippingTel1Global"
								ValidationGroup="UserShippingRegistGlobal"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						</dd>
						<% } %>
				</dl>
				</ContentTemplate>
				</asp:UpdatePanel>
				<%-- UPDATE PANELここまで --%>
			</div>
			<div class="cartNextbtn">
				<ul>
					<li class="prevBtn"><a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_SHIPPING_LIST) %>">前のページに戻る</a></li>
					<li class="nextBtn"><asp:LinkButton ID="lbConfirm" ValidationGroup="UserShippingRegist" OnClientClick="return exec_submit();" runat="server" OnClick="lbConfirm_Click">確認する</asp:LinkButton></li>
				</ul>
			</div>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					配送先リストの登録
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>

<script type="text/javascript">
<!--
	bindEvent();

	<%-- UpdataPanelの更新時のみ処理を行う --%>
	function bodyPageLoad() {
		if (Sys.WebForms == null) return;
		var isAsyncPostback = Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack();
		if (isAsyncPostback) {
			bindEvent();
		}
	}

	<%-- イベントをバインドする --%>
	function bindEvent() {
		bindExecAutoKana();
		bindZipCodeSearch();
		<% if(Constants.GLOBAL_OPTION_ENABLE) { %>
		bindTwAddressSearch();
		<% } %>
	}

	<%-- 氏名（姓・名）の自動振り仮名変換のイベントをバインドする --%>
	function bindExecAutoKana() {
		execAutoKanaWithKanaType(
			$("#<%= tbShippingName1.ClientID %>"),
			$("#<%= tbShippingNameKana1.ClientID %>"),
			$("#<%= tbShippingName2.ClientID %>"),
			$("#<%= tbShippingNameKana2.ClientID %>"));
	}

	<%-- 郵便番号検索のイベントをバインドする --%>
	function bindZipCodeSearch() {
		$("#<%= tbShippingZip2.ClientID %>").keyup(function (e) {
			if (isValidKeyCodeForKeyEvent(e.keyCode) == false) return;
			checkZipCodeLengthAndExecPostback(
				$("#<%= tbShippingZip1.ClientID %>"),
				$("#<%= tbShippingZip2.ClientID %>"),
				"<%= tbShippingZip2.UniqueID %>",
				'<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_ZIPCODE_SEARCHER_GET_ADDR_JSON %>',
				'#addrSearchErrorMessage'
			);
		});
		$("#<%= lbSearchShippingAddr.ClientID %>").on('click', function () {
			checkZipCodeLengthAndExecPostback(
				$("#<%= tbShippingZip1.ClientID %>"),
				$("#<%= tbShippingZip2.ClientID %>"),
				"<%= lbSearchShippingAddr.UniqueID %>",
				'<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_ZIPCODE_SEARCHER_GET_ADDR_JSON %>',
				'#addrSearchErrorMessage'
			);
		});
	}

	$(document).on('click', '.search-result-layer-close', function () {
		closePopupAndLayer();
	});

	$(document).on('click', '.search-result-layer-addr', function () {
		bindSelectedAddr($('li.search-result-layer-addr').index(this));
	});

	<%-- 複数住所検索結果からの選択値を入力フォームにバインドする --%>
	function bindSelectedAddr(selectedIndex) {
		var selectedAddr = $('.search-result-layer-addrs li').eq(selectedIndex);
		$("#<%= ddlShippingAddr1.ClientID %>").val(selectedAddr.find('.addr').text());
		$("#<%= tbShippingAddr2.ClientID %>").val(selectedAddr.find('.city').text() + selectedAddr.find('.town').text());
		$("#<%= tbShippingAddr3.ClientID %>").focus();

		closePopupAndLayer();
	}

	<% if(Constants.GLOBAL_OPTION_ENABLE) { %>
	<%-- 台湾郵便番号取得関数 --%>
	function bindTwAddressSearch() {
		$('#<%= this.WddlShippingAddr3.ClientID %>').change(function (e) {
			$('#<%= this.WtbShippingZipGlobal.ClientID %>').val(
				$('#<%= this.WddlShippingAddr3.ClientID %>').val().split('|')[0]);
		});
	}
	<% } %>
//-->
</script>

</asp:Content>