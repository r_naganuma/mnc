﻿<%--
=========================================================================================================
  Module      : アドレス帳一覧画面(UserShippingList.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_UserShippingList, App_Web_usershippinglist.aspx.b2a7112d" title="配送先リストページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />
<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					配送先リスト
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<h2>配送先リスト</h2>

	<div class="mypageBox">
		<div class="mypageBox_cts">
			<%-- メッセージ --%>
			<% if (this.IsDelete){%>
				<p class="leadTxt">
					お届け先情報を削除致しました。
				</p>
			<%} %>
			<p class="leadTxt">
				よくご利用になるお届け先を登録する事ができます。
			</p>
			<div class="completeBtn gray">
				<asp:LinkButton id="lbInsert" runat="server" OnClick="lbInsert_Click">お届け先の追加</asp:LinkButton>
			</div>
			<div style="display: none;" id="pagination" class="above clearFix"><%= this.PagerHtml %></div>
			<asp:Repeater id="rUserShippingList" runat="server" ItemType="w2.Domain.UserShipping.UserShippingModel" OnItemCommand="rUserShippingList_ItemCommand">
				<HeaderTemplate>
					<div class="slBox">
				</HeaderTemplate>
				<ItemTemplate>
					<div class="slBox_list">
						<div class="slBox_list--name">
							<p><%# WebSanitizer.HtmlEncode(Item.Name) %></p>
						</div>
						<div class="slBox_list--cts">
							<div runat="server" Visible="<%# IsCountryJp(Item.ShippingCountryIsoCode) %>">
								<p>
									<%#: Item.ShippingName %><%#: "（" + Item.ShippingNameKana + "）" %><br />
									<%# "〒" + WebSanitizer.HtmlEncode(Item.ShippingZip) %><br />
									<%#: Item.ShippingAddr1 %>
									<%#: Item.ShippingAddr2 %><br />
									<%#: Item.ShippingAddr3 %><br />
									<%# (string.IsNullOrEmpty(Item.ShippingAddr4) == false) ? WebSanitizer.HtmlEncode(Item.ShippingAddr4) + "<br />" : string.Empty %>
									<%# (string.IsNullOrEmpty(Item.ShippingAddr5) == false) ? WebSanitizer.HtmlEncode(Item.ShippingAddr5) + "<br />" : string.Empty %>
									<%# (IsCountryJp(Item.ShippingCountryIsoCode) == false) ? WebSanitizer.HtmlEncode(Item.ShippingZip) + "<br />" : string.Empty %>
									<%#: Item.ShippingCountryName %>
								</p>
							</div>
							<div runat="server" Visible="<%# IsCountryTw(Item.ShippingCountryIsoCode) %>">
								<p>
									<%#: Item.ShippingName %>
								</p>
								<div runat="server" visible="<%# Item.ShippingReceivingStoreFlg == Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_OFF %>">
									<p>
										<%#: Item.ShippingAddr2 %><br />
										<%#: Item.ShippingAddr3 %><br />
										<%#: Item.ShippingAddr4 %>
										<%# (string.IsNullOrEmpty(Item.ShippingAddr5) == false) ? WebSanitizer.HtmlEncode(Item.ShippingAddr5) + "<br />" : string.Empty %>
										<%#: Item.ShippingZip %>
									</p>
								</div>
								<div runat="server" visible="<%# Item.ShippingReceivingStoreFlg == Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_ON %>">
									<p>
										<%# (string.IsNullOrEmpty(Item.ShippingAddr4) == false) ? WebSanitizer.HtmlEncode(Item.ShippingAddr4) + "<br />" : string.Empty %>
									</p>
								</div>
								<p>
									<%#: Item.ShippingCountryName %>
								</p>
							</div>
							<div runat="server" Visible="<%# ((IsCountryJp(Item.ShippingCountryIsoCode) == false) && (IsCountryTw(Item.ShippingCountryIsoCode) == false)) %>">
								<p>
									<%#: Item.ShippingName %>&nbsp;様<br />
									<%#: Item.ShippingAddr2 %><br />
									<%# (string.IsNullOrEmpty(Item.ShippingAddr3) == false) ? WebSanitizer.HtmlEncode(Item.ShippingAddr3) + "<br />" : string.Empty %>
									<%#: Item.ShippingAddr4 %>,
									<%#: Item.ShippingAddr5 %>&nbsp;
									<%#: Item.ShippingZip %><br />
									<%#: Item.ShippingCountryName %>
								</p>
							</div>
						</div>
						<div class="slBox_list--btn">
							<asp:LinkButton id="lbUpdate" runat="server" CommandName="Update" CommandArgument="<%# Item.ShippingNo %>" Visible="<%# Item.ShippingReceivingStoreFlg == Constants.FLG_ORDERSHIPPING_SHIPPING_ADDR_KBN_CONVENIENCE_STORE_OFF %>">編集</asp:LinkButton>
							<asp:LinkButton id="lbDelete" runat="server" CommandName="Delete" CommandArgument="<%# Item.ShippingNo %>" OnClientClick="return confirm('削除しますか？');">削除</asp:LinkButton>
						</div>
					</div>
				</ItemTemplate>
				<FooterTemplate>
					</div>
				</FooterTemplate>
			</asp:Repeater>

			<%-- エラーメッセージ --%>
			<% if(StringUtility.ToEmpty(this.ErrorMessage) != ""){ %>
			<p class="leadTxt">
				<%: this.ErrorMessage %>
			</p>
			<% } %>

			<div style="display: none;" id="pagination" class="below clearFix"><%= this.PagerHtml %></div>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					配送先リスト
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
</asp:Content>