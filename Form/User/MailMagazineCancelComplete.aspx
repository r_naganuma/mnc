﻿<%--
=========================================================================================================
  Module      : メールマガジン解除完了画面(MailMagazineCancelComplete.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_MailMagazineCancelComplete, App_Web_mailmagazinecancelcomplete.aspx.b2a7112d" title="メルマガ登録解除 | SIMPLISSE(シンプリス) produced by 山本未奈子" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />

<article id="cartComp">
	
	<div class="inner">
	
		<div class="cartComp_ttl">
			<h2>解除完了</h2>
		</div>
	
		<div class="cartComp_txt">
			<p class="cartComp_txt--cts lh">
				メールマガジンを解除したメールアドレスは<br class="sp_contents">「<%: this.MailAddress %>」です。<br>
				今後とも、「<%: ShopMessage.GetMessage("ShopName") %>」をどうぞ宜しくお願い申し上げます。
			</p>
			<asp:LinkButton ID="lbTopPage" runat="server" OnClick="lbTopPage_Click" class="cartComp_txt--btn">トップページへ</asp:LinkButton>
		</div>
		<p style="display: none;" class="completeInfo">メールマガジンを解除したメールアドレスは「<span><%: this.MailAddress %></span>」です。<br />
		今後とも、「<%: ShopMessage.GetMessage("ShopName") %>」をどうぞ宜しくお願い申し上げます。<br />
		</p>
		<p style="display: none;" class="receptionInfo">
			<%= ShopMessage.GetMessageHtmlEncodeChangeToBr("ContactCenterInfo") %>
		</p>
	</div>
</article>
</asp:Content>
