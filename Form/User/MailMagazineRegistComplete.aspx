﻿<%--
=========================================================================================================
  Module      : メールマガジン登録完了画面(MailMagazineRegistComplete.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2010 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_MailMagazineRegistComplete, App_Web_mailmagazineregistcomplete.aspx.b2a7112d" title="メルマガ登録 | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />

<article id="cartComp">
	<div class="inner">
		<div class="cartComp_ttl">
			<h2>登録完了</h2>
		</div>
	
		<div class="cartComp_txt">
			<p class="cartComp_txt--ttl">
				ご登録いただきありがとうございます。
			</p>
			<p class="cartComp_txt--cts lh">
				メールマガジンを登録したアドレスは<br class="sp_contents">「<%: this.UserMailAddr%>」です。<br>
				登録されたメールアドレスに確認メールを送信しました。
			</p>
			<asp:LinkButton ID="lbTopPage" runat="server" OnClick="lbTopPage_Click" class="cartComp_txt--btn">トップページへ</asp:LinkButton>
		</div>
		<p style="display: none;" class="receptionInfo">
			<%= ShopMessage.GetMessageHtmlEncodeChangeToBr("ContactCenterInfo") %>
		</p>
	</div>
</article>
</asp:Content>