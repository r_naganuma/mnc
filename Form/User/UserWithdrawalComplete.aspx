﻿<%--
=========================================================================================================
  Module      : 会員退会完了画面(UserWithdrawalComplete.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_UserWithdrawalComplete, App_Web_userwithdrawalcomplete.aspx.b2a7112d" title="退会完了ページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />
<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					退会の受付完了
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<h2>退会の受付完了</h2>

	<div class="mypageBox">
		<div class="mypageBox_cts">
			<%-- メッセージ --%>
			<p class="leadTxt">
				退会処理が完了いたしました。<br />
				<%= WebSanitizer.HtmlEncode(ShopMessage.GetMessage("ShopName")) %>をご利用頂き、真に有り難うございました。</p>
			</p>
			<div class="completeBtn">
				<asp:LinkButton ID="lbTopPage" runat="server" OnClick="lbTopPage_Click">トップページへ</asp:LinkButton>
			</div>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					退会の受付完了
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
</asp:Content>