﻿<%--
=========================================================================================================
  Module      : メールマガジン登録入力画面(MailMagazineRegistInput.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2010 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_MailMagazineRegistInput, App_Web_mailmagazineregistinput.aspx.b2a7112d" title="メルマガ登録 | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />
<article id="cart" class="cartPeyment">

<!--▽ パンくず ▽-->
<div class="breadArea pc_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				メールマガジン登録
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

<!-- main -->
<section class="cartTtl second">
	<h2>
		メールマガジン登録
	</h2>
</section>
<!-- // main -->

<div class="cartLead">
	<p>
		SIMPLISSEのメールマガジンは、オンラインストアで開催されるキャンペーン情報・セール情報、特別・限定アイテムの発売情報など、<br class="pc_contents">お得な情報をお届けしています。メールマガジン会員様限定のお得な情報も配信予定ですのでぜひご登録ください！
	</p>
</div>

	<div id="dvMailMagazineRegistInput" class="unit">
			<dl class="cartInputRegist">
					<%-- 氏名 --%>
					<dt style="display: none;"><%: ReplaceTag("@@User.name.name@@") %> <span class="icnKome">※</span></dt>
					<dd style="display: none;">
						<div class="nameFlex">
							<div class="nameFlex_list">
								<% SetMaxLength(WtbUserName1, "@@User.name1.length_max@@"); %>
								<p>姓</p>
								<asp:TextBox id="tbUserName1" Runat="server" CssClass="nameFirst"></asp:TextBox>
							</div>
							<div class="nameFlex_list">
								<% SetMaxLength(WtbUserName2, "@@User.name2.length_max@@"); %>
								<p>名</p><asp:TextBox id="tbUserName2" Runat="server" CssClass="nameLast"></asp:TextBox>
							</div>
						</div>
						<asp:CustomValidator ID="cvUserName1" runat="Server"
							ControlToValidate="tbUserName1"
							ValidationGroup="MailMagazineRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator ID="cvUserName2" runat="Server"
							ControlToValidate="tbUserName2"
							ValidationGroup="MailMagazineRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</dd>
					<%-- 氏名（かな） --%>
					<dt style="display: none;">カナ
						<% if (this.IsJapanese) { %>
							<span class="icnKome">※</span>
						<% } %>
					</dt>
					<dd style="display: none;">
						<div class="nameFlex">
							<div class="nameFlex_list">
								<% SetMaxLength(WtbUserNameKana1, "@@User.name_kana1.length_max@@"); %>
								<p>セイ</p><asp:TextBox id="tbUserNameKana1" Runat="server" CssClass="nameFirst"></asp:TextBox>
							</div>
							<div class="nameFlex_list">
								<% SetMaxLength(WtbUserNameKana2, "@@User.name_kana2.length_max@@"); %>
								<p>メイ</p><asp:TextBox id="tbUserNameKana2" Runat="server" CssClass="nameLast"></asp:TextBox>
							</div>
						</div>
						<% if (this.IsJapanese) { %>
						<asp:CustomValidator ID="cvUserNameKana1" runat="Server"
							ControlToValidate="tbUserNameKana1"
							ValidationGroup="MailMagazineRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator ID="cvUserNameKana2" runat="Server"
							ControlToValidate="tbUserNameKana2"
							ValidationGroup="MailMagazineRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<% } %>
					</dd>
					<dt>
						<%: ReplaceTag("@@User.mail_addr.name@@") %>
						<span class="icnKome">※</span>
					</dt>
					<dd><asp:TextBox ID="tbUserMailAddr" Runat="server" CssClass="mailAddr" MaxLength="256" Type="email"></asp:TextBox>
					<asp:CustomValidator ID="cvUserMailAddr" runat="Server"
						ControlToValidate="tbUserMailAddr"
						ValidationGroup="MailMagazineRegist"
						ValidateEmptyText="true"
						SetFocusOnError="true"
						ClientValidationFunction="ClientValidate"
						CssClass="error_inline" />
					</dd>
					<dt>
						<%: ReplaceTag("@@User.mail_addr.name@@") %>（確認用）
						<span class="icnKome">※</span>
					</dt>
					<dd>
						<asp:TextBox id="tbUserMailAddrConf" Runat="server" MaxLength="256" CssClass="mailAddr" Type="email"></asp:TextBox>
						<asp:CustomValidator ID="cvUserMailAddrConf" runat="Server"
							ControlToValidate="tbUserMailAddrConf"
							ValidationGroup="MailMagazineRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</dd>
			</dl>

			<div class="registBtn">
				<p>送信前に<a href="">プライバシーポリシー</a>をご確認ください。</p>
				<ul>
					<li>
						<asp:LinkButton ID="lbConfirm" ValidationGroup="MailMagazineRegist" OnClientClick="return exec_submit();" runat="server" OnClick="lbConfirm_Click">
							確認する
						</asp:LinkButton>
					</li>
					<li style="display: none;"></li>
				</ul>
			</div>
	</div>

	<div style="display: none;" class="unit">
		<p>メールマガジンを解除される方は、下記のボタンをクリックして、メールマガジン解除画面へ。</p>
		<div class="dvUserBtnBox">
			<p>
				<span><a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_MAILMAGAZINE_CANCEL_INPUT) %>" class="btn btn-large btn-inverse">
					メールマガジン解除</a>
				</span>
			</p>
		</div>
	</div>

</article>

<!--▽ パンくず ▽-->
<div class="breadArea sp_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				メールマガジン登録
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->

<script type="text/javascript">
<!--
	bindExecAutoKana();

	<%-- 氏名（姓・名）の自動振り仮名変換のイベントをバインドする --%>
	function bindExecAutoKana() {
		execAutoKanaWithKanaType(
			$("#<%= tbUserName1.ClientID %>"),
			$("#<%= tbUserNameKana1.ClientID %>"),
			$("#<%= tbUserName2.ClientID %>"),
			$("#<%= tbUserNameKana2.ClientID %>"));
	}
//-->
</script>

</asp:Content>