﻿<%--
=========================================================================================================
  Module      : クーポンBOX画面(UserCouponBox.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2017 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="mypageMenu" Src="~/Page/Parts/Parts000TMPL_011.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_CouponBox, App_Web_usercouponbox.aspx.b2a7112d" title="クーポンBOXページ | SIMPLISSE(シンプリス) produced by 山本未奈子" %>
<%@ Import Namespace="w2.Domain.Coupon.Helper" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" charset="Shift_JIS" src="<%= Constants.PATH_ROOT %>Js/floatingWindow.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%-- UPDATE PANEL開始 --%>
<asp:UpdatePanel ID="upUpdatePanel" runat="server">
<ContentTemplate>
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>Css/user/mypage/style.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" language="javascript">
	function bodyPageLoad() {
		// 検索結果数
		var total = $(".pagination_box--left").html();
		$(".leadTxt").html(total);
	}
</script>
<article id="mypage">

	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					クーポンBOX
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<h2>クーポンBOX</h2>
	<div class="mypageBox">
		<div class="mypageBox_cts">
			<h4>
			<%-- 利用可能クーポンなし--%>
			<% if(StringUtility.ToEmpty(this.AlertMessage) != "") {%>
				<%: this.AlertMessage %>
			<%} %>
			</h4>
			<asp:Repeater ID="rCouponList" ItemType="UserCouponDetailInfo" Runat="server">
				<HeaderTemplate>
				<p class="leadTxt"></p>
				<div class="couponArea">
					<div class="couponArea_th">
						<div class="couponArea_th--list"><p>クーポンコード</p></div>
						<div class="couponArea_th--list"><p>クーポン名</p></div>
						<div class="couponArea_th--list"><p>割引金額/割引率</p></div>
						<div class="couponArea_th--list"><p>利用可能回数</p></div>
						<div class="couponArea_th--list"><p>有効期限</p></div>
					</div>
				</HeaderTemplate>
				<ItemTemplate>
					<div class="couponArea_td">
						<div class="couponArea_td--list">
							<div class="spTtl">
								<p>クーポンコード</p>
							</div>
							<p class="couponTxt">
								<span runat="server" visible="<%# (Item.ExpireEnd < DateTime.Now) %>">
									[有効期限切れ]<br />
								</span>
								<%#: Item.CouponCode %>
							</p>
						</div>
						<div class="couponArea_td--list">
							<div class="spTtl">
								<p>クーポン名</p>
							</div>
							<p class="couponTxt">
								<%#: Item.CouponDispName %>
							</p>
						</div>
						<div class="couponArea_td--list">
							<div class="spTtl">
								<p>割引金額/割引率</p>
							</div>
							<p class="couponTxt">
							<%#: (StringUtility.ToEmpty(Item.DiscountPrice) != "")
									? CurrencyManager.ToPrice(Item.DiscountPrice)
									: (StringUtility.ToEmpty(Item.DiscountRate) != "")
										? StringUtility.ToEmpty(Item.DiscountRate) + "%"
										: "-" %>
							</p>
						</div>
						<div class="couponArea_td--list">
							<div class="spTtl">
								<p>利用可能回数</p>
							</div>
							<p class="couponTxt">
								<%#: GetCouponCount(Item) %>
							</p>
						</div>
						<div class="couponArea_td--list">
							<div class="spTtl">
								<p>有効期限</p>
							</div>
							<p class="couponTxt">
								<%#: DateTimeUtility.ToStringFromRegion(Item.ExpireEnd, DateTimeUtility.FormatType.LongDateHourMinute1Letter) %>
							</p>
						</div>
						<div class="couponArea_td--list">
							<p class="descTxt">
								<%#: Item.CouponDispDiscription %>
							</p>
						</div>
					</div>
				</ItemTemplate>
				<FooterTemplate>
					</div>
				</FooterTemplate>
			</asp:Repeater>
			<%-- ページャ--%>
			<div style="display: none;"><%= this.PagerHtml %></div>
		</div>
		<uc:mypageMenu runat="server" />
	</div>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					クーポンBOX
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>