﻿<%--
=========================================================================================================
  Module      : 領収書ダウンロード画面(ReceiptDownload.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2019 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/DefaultPage.master" autoeventwireup="true" inherits="Form_Receipt_ReceiptDownload, App_Web_receiptdownload.aspx.894f10a4" title="領収書ダウンロード" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
	<%-- ▽編集可能領域：HEAD追加部分▽ --%>
	<meta http-equiv="pragma" content="no-cache" />
	<%-- △編集可能領域△ --%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Css/cart/style.css") %>" rel="stylesheet" type="text/css" media="all" />
<article id="cart" class="cartPeyment">
	
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					領収書ダウンロード
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
	
	<!-- main -->
	<section class="cartTtl second">
		<h2>
			領収書ダウンロード
		</h2>
	</section>
	<!-- // main -->
	<div id="dvReceiptDownload">
		<div style="margin-top: 30px;" class="error-msg" id="dvReceiptError" runat="server" visible="false">
			<asp:Literal ID="lReceiptErrorMessage" runat="server" />
		</div>

		<div class="registBtn">
			<ul>
				<li>
					<asp:LinkButton ID="lbReceiptDownload" runat="server" OnClick="lbReceiptDownload_Click">領収書ダウンロード</asp:LinkButton>
				</li>
				<li style="display: none;"></li>
			</ul>
		</div>
		
	</div>
</article>

<!--▽ パンくず ▽-->
<div class="breadArea sp_contents">
	<ul>
		<li>
			<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
		</li>
		<li>
			<a href="#">
				領収書ダウンロード
			</a>
		</li>
	</ul>
</div>
<!--△ パンくず △-->
</asp:Content>