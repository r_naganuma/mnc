﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="会員特典のご案内 | SIMPLISSE(シンプリス) produced by 山本未奈子" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="shoppingGuide">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					ショッピングガイド
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<section class="ttlArea">
		<h2>ショッピングガイド</h2>
	</section>

	<section class="shoppingGuideArea">
		<div class="shoppingGuideArea_cts">
			<div class="shoppingGuideArea_cts--list">
				<h3>SIMPLISSE MEMBERSHIP　<br class="sp_contents">シンプリス会員特典のご案内</h3>
				<div class="columnArea">
					<p>
						シンプリス オンラインストアではお得なシンプリス会員特典をご用意しております。<br>
						シンプリス会員になるには「会員登録（無料）」をしていただくだけ。登録のその日から全商品を送料無料でお届けします。また、年間のご購入金額に合わせてさまざまな特典がございます。
					</p>
					<a href="<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_REGIST_INPUT + "?" + Constants.REQUEST_KEY_NEXT_URL + "=" + Server.UrlEncode(Request[Constants.REQUEST_KEY_NEXT_URL]) %>" class="btnBlue">
						新規会員登録はこちら
					</a>
					<p>
						特典をお受けいただくため、商品のご注文前には必ずログインを行ってください<br>
						<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "page/terms/") %>">会員規約</a>をご確認ください
					</p>
				</div>
			</div>
			<div class="shoppingGuideArea_cts--list">
				<h3>会員特典について</h3>
				<div class="columnArea">
					<div class="benefitsAbout">
						<div class="benefitsAbout_list">
							<div class="benefitsAbout_list--pic">
								<img src="./images/icn_01.png">
							</div>
							<div class="benefitsAbout_list--txt">
								<p>
									<span>ご優待割引制度</span>
									すべての商品を、会員ランクに応じたご優待割引価格でお買い物いただけます。また、シルバーランク以上の会員様には、年に2回の特別ご優待券をご案内。そのほか会員限定キャンペーンなどもご用意。
								</p>
							</div>
						</div>
						<div class="benefitsAbout_list">
							<div class="benefitsAbout_list--pic">
								<img src="./images/icn_02.png">
							</div>
							<div class="benefitsAbout_list--txt">
								<p>
									<span>いつでも送料無料</span>
									会員の皆さまは、1品より日本全国送料無料でお届けいたします。<br>
									※海外へご配送には通常の料金が発生します。
								</p>
							</div>
						</div>
						<div class="benefitsAbout_list">
							<div class="benefitsAbout_list--pic">
								<img src="./images/icn_03.png">
							</div>
							<div class="benefitsAbout_list--txt">
								<p>
									<span>毎月ランクアップのチャンス！</span>
									ご購入金額に応じたランクアップ判定（ランクの更新）は、毎月行われます。 毎月月末までにランクアップ条件を満たしている場合は、翌月から会員ランクがアップ！
								</p>
							</div>
						</div>
						<div class="benefitsAbout_list">
							<div class="benefitsAbout_list--pic">
								<img src="./images/icn_04.png">
							</div>
							<div class="benefitsAbout_list--txt">
								<p>
									<span>会員ランクは下がりません</span>
									一度獲得された会員ランクは、その後の購入頻度や金額にかかわらず下がることはありません。
								</p>
							</div>
						</div>
					</div>

					<h5>会員ランク条件と特典</h5>
					<p>
						シンプリス オンラインストアでは年間のご購入金額に応じて、レギュラー、シルバー、ゴールド、プラチナ、ダイヤモンドの会員ランクを設け、それぞれに特典をご用意しております。年間のご購入額が一定額を超えると会員ランクがアップし、特典もグレードアップします。
					</p>
					<div class="rankTable">
						<div class="rankTable_row row01">
							<div class="rowList"></div>
							<div class="rowList">
								<p>
									年間購入金額
								</p>
							</div>
							<div class="rowList">
								<p>
									ご優待割引
								</p>
							</div>
							<div class="rowList">
								<p>
									送料無料
								</p>
							</div>
							<div class="rowList">
								<p>
									特別ご優待券<br>
									<span>(年２回)</span>
								</p>
							</div>
						</div>
						<div class="rankTable_row row02">
							<div class="rowList">
								<p>
									レギュラー<br>
									会員
								</p>
							</div>
							<div class="rowList">
								<p>
									3万円未満
								</p>
							</div>
							<div class="rowList">
								<p class="no">
									×
								</p>
							</div>
							<div class="rowList">
								<img src="./images/icn_yes.png">
							</div>
							<div class="rowList">
								<p class="no">
									×
								</p>
							</div>
						</div>
						<div class="rankTable_row row03">
							<div class="rowList">
								<p>
									シルバー<br>
									会員
								</p>
							</div>
							<div class="rowList">
								<p>
									3万円以上
								</p>
							</div>
							<div class="rowList">
								<p>
									3%割引
								</p>
							</div>
							<div class="rowList">
								<img src="./images/icn_yes.png">
							</div>
							<div class="rowList">
								<img src="./images/icn_yes.png">
							</div>
						</div>
						<div class="rankTable_row row04">
							<div class="rowList">
								<p>
									ゴールド<br>
									会員
								</p>
							</div>
							<div class="rowList">
								<p>
									8万円以上
								</p>
							</div>
							<div class="rowList">
								<p>
									5%割引
								</p>
							</div>
							<div class="rowList">
								<img src="./images/icn_yes.png">
							</div>
							<div class="rowList">
								<img src="./images/icn_yes.png">
							</div>
						</div>
						<div class="rankTable_row row05">
							<div class="rowList">
								<p>
									プラチナ<br>
									会員
								</p>
							</div>
							<div class="rowList">
								<p>
									15万円以上
								</p>
							</div>
							<div class="rowList">
								<p>
									7%割引
								</p>
							</div>
							<div class="rowList">
								<img src="./images/icn_yes.png">
							</div>
							<div class="rowList">
								<img src="./images/icn_yes.png">
							</div>
						</div>
						<div class="rankTable_row row06">
							<div class="rowList">
								<p>
									ダイヤモンド<br>
									会員
								</p>
							</div>
							<div class="rowList">
								<p>
									25万円以上
								</p>
							</div>
							<div class="rowList">
								<p>
									10%割引
								</p>
							</div>
							<div class="rowList">
								<img src="./images/icn_yes.png">
							</div>
							<div class="rowList">
								<img src="./images/icn_yes.png">
							</div>
						</div>
					</div>
					<div class="indentTxt">
						<p>
							※ランクアップ判定（ランクの更新）は毎月月末に行います。累計購入金額がそれぞれの会員ランクアップ条件を満たした月の、翌月1日午前2時よりランクが更新されます。
						</p>
						<p>
							※累計購入金額は毎年1月から同年12月までの1年間の金額とし、毎年1月1日にリセットされます。
						</p>
						<p>
							※ご購入金額にはご返送品と未発送品は含まれず、お受け取りいただいたご注文金額で算出されます。
						</p>
						<p>
							※送料無料の特典は日本国内に限ります。配送先が海外の場合の送料はこちらをご参照ください。
						</p>
					</div>

					<h5>バースデー特典のご案内</h5>
					<p>
						会員登録の際にお誕生日を登録いただいた方には、お誕生日月にバースデークーポンをご送付いたします。<br>
						10,000円（税込／送料別）以上お買い物いただくとバースデープレゼントとして5,000円相当の品をお贈りするクーポンです。
					</p>
					<div class="indentTxt">
						<p>
							※シンプリス　オンラインストアで2年以内にご購入履歴がある方に限らせていただきます。
						</p>
						<p>
							※お誕生月の前々月までに会員登録いただいた方が対象となります。
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="shoppingGuideArea_menu">
			<ul>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/#a01") %>">
						会員登録について
					</a>
				</li>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/#a02") %>">
						定期便について
					</a>
				</li>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/#a03") %>">
						ご注文方法について
					</a>
				</li>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/#a04") %>">
						お支払いについて
					</a>
				</li>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/#a05") %>">
						配送について
					</a>
				</li>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/#a06") %>">
						返品・交換について
					</a>
				</li>
			</ul>
			<ul>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/benefits/") %>">
						会員特典のご案内
					</a>
				</li>
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/wrapping/") %>">
						ラッピングについて
					</a>
				</li>
			</ul>
		</div>
	</section>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					ショッピングガイド
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>

