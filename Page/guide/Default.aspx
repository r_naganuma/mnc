﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="ショッピングガイド | SIMPLISSE(シンプリス) produced by 山本未奈子" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="shoppingGuide">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					ショッピングガイド
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<section class="ttlArea">
		<h2>ショッピングガイド</h2>
	</section>

	<section class="shoppingGuideArea">
		<div class="shoppingGuideArea_cts">
			<div class="shoppingGuideArea_cts--list">
				<div class="columnArea">
					<p class="guideMainTtl">
						シンプリス オンラインストアは、<br>
						SIMPLISSE商品をインターネットでご購入いただける公式サイトです。<br>
						24時間いつでもお買い物いただけます。
					</p>
					<p>
						消費税の総額表示義務化に伴い、商品の価格表示は全て税込価格となっております。<br>
						当オンラインストアでは、便利で安心なSSL（Secure Sockets Layer）対応のショッピングカートをご用意しております。
					</p>
				</div>
			</div>
			<div id="a01" class="shoppingGuideArea_cts--list">
				<h3>会員登録について</h3>
				<div class="columnArea">
					<h4>シンプリス オンラインストアではお得で便利な「シンプリス会員特典」をご用意しております。</h4>
					<p>
						会員になるには「会員登録（無料）」をしていただくだけ。登録のその日から全商品を送料無料でお届けします。次回のお買い物より住所などの必須情報が自動入力されるだけでなく、年間のご購入金額に応じてさまざまな特典がございます。<br>
						※会員登録せずにお買い物いただくことも可能です。<br>
						今なら新規会員登録をすると、即日使える500円クーポン進呈中！<br>
						年間のご購入金額に応じてさまざまな特典がございます。
					</p>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/benefits/") %>" class="btnBlue">
						シンプリス会員特典とは
					</a>
					<a href="<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_REGIST_INPUT + "?" + Constants.REQUEST_KEY_NEXT_URL + "=" + Server.UrlEncode(Request[Constants.REQUEST_KEY_NEXT_URL]) %>" class="btnBlue mrgT0">
						新規会員登録はこちら
					</a>
					<p>
						・ログイン時に必要なパスワードをお忘れの場合は、「パスワードをお忘れのお客様」よりご登録時のメールアドレスをご入力ください。メールアドレスにパスワード再設定用のURLを掲載したメールが届きます。<br>URLよりアクセスし、必要事項と新パスワードを設定ください。<br>・会員情報のご確認・ご変更は、ログイン後の「マイページ」よりご確認、ご変更をお願いいたします。<br><br>
					</p>
				</div>
				<div class="columnArea">
					<h4>【重要なお知らせ】</h4>
					<p>
						SIMPLISSE Online Store パスワード再設定のお願い<br>
						2021年1月28日のサイトリニューアル以降、ログインされていない方には、パスワードの再設定をお願いしております。<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/User/PasswordReminderInput.aspx") %>">こちらから</a>
					</p>
				</div>
			</div>

			<div id="a02" class="shoppingGuideArea_cts--list">
				<h3>定期便について</h3>
				<div class="columnArea">
					<h4>シンプリス オンラインストアでは毎日のケアをお得にご使用いただくための、SIMPLISSE 定期便サービスをご用意しております。</h4>
					<p>
						初回は50%OFF、その後は何度でも15%OFFの特別価格にてお安くお届けするほか、ご継続特典など特別なプレゼントもご用意しております。
					</p>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/regularly2205/") %>" class="btnBlue">
						定期便サービスについて
					</a>
				</div>
			</div>

			<div id="a03" class="shoppingGuideArea_cts--list">
				<h3>ご注文方法</h3>
				<div class="columnArea">
					<h4>ご注文方法</h4>
					<p>
						<span class="fwBold">STEP1）商品を探す</span><br>
						商品メニューを選択すると商品一覧が表示されます。 また、商品検索機能によって、該当商品一覧を表示することができます。<br>
						<br>
						<span class="fwBold">STEP2）商品をカートに入れる</span><br>
						ご希望の商品の購入ボタンをクリックしてください。カート内の商品を見たい場合はページ右部の「BAG」からご確認ください。<br>
						<br>
						<span class="fwBold">STEP3）ご注文を確認する</span><br>
						（１）個数を変更する場合は、数量を半角数字で入力してください。<br>（２）商品を削除する場合は、「削除」を押してください。<br>（３）クーポンをご利用の場合はプルダウンからご選択ください。<br>コードをご入力いただく場合は「クーポンコードを入力する」ボタンを押し、ご入力ください。<br>
						<br>
						・ギフトラッピングをご希望の場合は、「カート内容確認」ページにて、「ギフトラッピングサービス」よりお進みいただき、ご希望のサイズを選択の上、併せてご購入ください。ラッピングキットをお届けする「自分でラッピング（330円）」または、商品をラッピングしてお届けする「有料ラッピング （1個につき770円）」をご用意しています。<br>
						<br>
						(４)ご注文の商品・サイズ・個数にお間違えがないか必ずご確認の上、「ご購入手続き」ボタンをクリックしてください。Amazon Payご利用の方は、「Amazonアカウントでお支払い」を選択しお手続きを進めてください。<br>
						<br>
						<span class="fwBold">会員登録をせずにご購入の方は STEP4）へ、</span><br>
						<span class="fwBold">会員の方はログイン後 STEP6）へお進みください。</span><br>
						<br>

						<span class="fwBold">STEP4）注文者情報、配送先情報入力</span><br>
						（１）ご注文者情報を入力します。<br>
						（２）お届け先を選びます。<br><br>
						ご注文者住所へ送る場合は「注文者情報の住所」に、他の住所をご希望の方は配送先をご入力いただくか、予めマイページにてご登録頂いた配送先リストの住所を選択してください。※お届け先の入力は１件ずつとなります。複数へお届けする場合は、1件ずつのご注文をお願いいたします。<br>
						<br>
						<span class="fwBold">STEP5）配送希望日・時間帯・配送パターンを選ぶ</span><br>
						（１）配送希望日・希望時間帯がある場合は、プルダウンから希望するものを選択ください。定期便の方は配送サイクルの指定をします。<br>
						<br>
						<span class="fwBold">STEP6）お支払い方法を選ぶ</span><br>
						（２）「お支払い方法入力へ」ボタンを押し、お支払い方法を「代金引換」「クレジットカード」「NP後払い」のいずれかがお選びいただき必要事項をご入力ください。<br>
						<br>
						<span class="fwBold">STEP7）ご注文内容を確認する</span><br>
						「ご注文内容確認へ」ボタンをクリックし、ご注文の内容を確認してください。間違いがなければ「注文を確定する」ボタンをクリックしてください。<br>
						<br>
						<span class="fwBold">STEP8）ご注文完了</span><br>
						「ご注文完了」の表示がされれば、ご注文のお手続きが完了です。入力いただいたメールアドレス宛に、ご購入確認メールが配信されます。<br>
						<br>
						<span class="fwBold">パンフレットについて</span><br>
						商品パンフレットは原則初回ご購入時のみに同封しております。ご希望の場合は備考欄にご記載ください。<br>
					</p>
				</div>
			</div>
			<div id="a04" class="shoppingGuideArea_cts--list">
				<h3>お支払いについて</h3>
				<div class="columnArea">
					<p>
						お支払いは、クレジットカード決済・代金引換・NP後払い（コンビニ, 郵便局, 銀行）・Amazon Pay の4種類からお選びいただけます。<br>
						<br>
						<span class="fwBold">クレジットカード払いについて</span><br>
						ご利用いただけるクレジットカードは、VISA／Master／JCB／AMEX／Dinersです。<br>
						<br>
						分割払いは「一括／3回／5回／6回／10回／15回／18回／20回／24回／リボ払い」がご利用いただけます。<br>
						</p>
						<img src="./images/logo_card.png" class="logo_card">
						<p>
						<br>
						<span class="fwBold">代引きについて</span><br>
						商品配達時に商品と引き換えに代金をお支払いください。（代引き手数料は無料）<br>
						※ご注文者様の住所とお届け先住所が異なる場合は、代金引換はご利用できません。<br>
						<br>
						<span class="fwBold">後払い（コンビニ・郵便局・銀行）について</span><br>
						請求書は商品に同封されています。（ギフト用でのご注文の場合、請求書は商品に同梱されず、ご購入者さまへお送りいたします）<br>
						<br>
						商品の到着を確認してから、「コンビニ」「郵便局」「銀行」「LINE Pay」で後払いできる安心・簡単な決済方法です。請求書の記載事項に従って発行日から14日以内にお支払いください。（後払い手数料は無料）
					</p>
					<img src="./images/logo_np.png" class="logo_np">
					<p>
						※後払いのご注文には、株式会社ネットプロテクションズの提供する<a href="https://np-atobarai.jp/about/index_wiz.html" target="_blank">NP後払いwizサービス</a>が適用され、サービスの範囲内で個人情報を提供し、代金債権を譲渡します。<br>
						※<span class="notice">ご利用限度額は累計残高で55,000円（税込）迄</span>です。（NP後払いサービスご利用分も含まれます。）詳細はバナーをクリックしてご確認下さい。<br>
						※ご利用者が未成年の場合、法定代理人の利用同意を得てご利用ください。<br>
						※ギフト用でのご注文の場合、請求書は商品に同梱されず、ご購入者さまへお送りいたします。
					</p>
				</div>
				<div class="columnArea">
					<p>
						<span class="fwBold">Amazon payについて</span><br>
						Amazon.co.jpに登録している情報を使って簡単にお支払いができるサービスです。<br>
						決済方法で「Amazon Pay」を選択し、Amazonに登録しているEメールアドレスとパスワードでログインしてクレジットカード情報を選択してください。非会員でのご購入では、Amazonに登録している配送先情報もお選びいただけます。Amazonアカウントに登録された配送先とクレジットカード情報はAmazonによって安全に保管されており、シンプリス オンラインストアがクレジットカード情報を取得することはありません。
					</p>
					<img src="./images/logo_amazon.png" class="logo_amazon">
					<p>※Amazon Payでのお支払いは、一括払いのみとなります。</p>
				</div>
			</div>
			<div id="a05" class="shoppingGuideArea_cts--list">
				<h3>配送について</h3>
				<div class="columnArea">
					<p>
						ご注文を頂いてから通常2営業日以内に速やかに発送いたします。<br>
						※土日･祝日は休業日を頂いております。休業日に頂いたご注文は、翌営業日より発送準備を致します。<br>
						※生産の都合上、商品の納期が変更となる場合もございますのであらかじめご了承ください。<br>
						<br>
						<span class="fwBold">配送・送料について</span><br>
						ご注文した商品は、佐川急便にて配送しております。海外への発送の場合は、EMS（国際スピード郵便）のみとなります。<br>
						<br>
						<span class="fwBold">お届け日時指定について</span><br>
						ご注文手続きの際に、配送希望日をご指定いただけます。 早いご到着をご希望の場合は「指定なし」をご選択いただきますと、最速でご注文日の翌日に発送致しております。交通などの諸事情に よりご希望に添えない場合もございますので、予めご了承ください。<br>
						出荷後の配送状況につきましては、商品発送時にメールでご連絡しております伝票番号をもとに、<a href="http://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp" target="_blank">佐川急便サイト</a>にてご確認ください。<br>
						<br>
						配送時間については、下記の中からご指定いただけます。<br>
						9:00-12:00 / 12:00-1400 / 14:00-16:00 / 16:00-18:00 / 18:00-20:00 / 19:00-21:00<br>
						<br>
						<span class="fwBold">国内への配送・送料について（税込）</span><br>
						地域ごとに送料を設定させていただいております。税込10,000円以上のご購入で送料無料になります。
					</p>
					<div class="columnArea_table table01">
						<div class="th">
							<p>
								東北・関東・中部・関西・四国・九州<br>
								<!--<span class="fontS">(北海道　東北　関東　中部　関西　四国　九州)</span>-->
							</p>
						</div>
						<div class="td">
							<p>
								880円
							</p>
						</div>
						<div class="th">
							<p>
								北海道・沖縄
							</p>
						</div>
						<div class="td">
							<p>
								1,100円
							</p>
						</div>
					</div>
					<div class="columnArea_table table02">
						<div class="th">
							<p>
								送料無料
							</p>
						</div>
						<div class="td">
							<p>
								1回の購入につき合計10,000円以上お買い上げいただいた場合は<span class="notice">送料無料</span>となります。<br>
								なお、お支払い金額が10,000円を下回った場合は、別途送料がかかります。<br>
								会員登録がお済みのお客様（シンプリス会員様）はご購入金額に関わらず<span class="notice">送料無料</span>となります。
							</p>
						</div>
					</div>
					<p>
						<span class="fwBold" id="001">海外への配送・送料について（税込）</a></span>
					</p>
					<!-- <div class="columnArea_table table03">
						<div class="td">
							<p>
								アジア：1,800円<br>
								オセアニア・北米・中米・中近東：2,100円<br>
								ヨーロッパ：2,400円<br>
								南米・アフリカ：2,800円
							</p>
						</div>
					</div> -->
					<div class="columnArea_table table01">
						<div class="th">
							<p>
								中国・韓国・台湾
							</p>
						</div>
						<div class="td">
							<p>
								1,600円
							</p>
						</div>
						<div class="th">
							<p>
								アジア
							</p>
						</div>
						<div class="td">
							<p>
								1,800円
							</p>
						</div>
						<div class="th">
							<p>
								オセアニア・北米・中米・中近東
							</p>
						</div>
						<div class="td">
							<p>
								2,100円
							</p>
						</div>
						<div class="th">
							<p>
								ヨーロッパ
							</p>
						</div>
						<div class="td">
							<p>
								2,400円
							</p>
						</div>
						<div class="th">
							<p>
								南米・アフリカ
							</p>
						</div>
						<div class="td">
							<p>
								2,800円
							</p>
						</div>
					</div>
					<p>※送料は変更となる場合がございます。<br><br></p>
					<div class="columnArea_table table04">
						<div class="th">
							<p>
								配送日時指定
							</p>
						</div>
						<div class="td">
							<p>
								配送希望日、及び、時間帯をご指定頂くことができません。
							</p>
						</div>
						<div class="th">
							<p>
								ご注意事項
							</p>
						</div>
						<div class="td">
							<div class="tdList">
								<div class="tdList_col">
									<p>
										配送期間は、基本的にアジアは３営業日、欧米は７営業日前後になりますが、通関事情、天候などにより遅れる場合もございます。<br>
										<br>
										配送方法は EMS （国際スピード郵便）のみとなります。個人利用のお客様への販売を前提としておりますため、ご配送ラベルには「personal use」と記載いたします。
									</p>
								</div>
								<div class="tdList_col">
									<p>
										ご購入頂きました商品に対し、 お受け取り国での通関時に関税や通関手数料、 その他輸入税等が課せられる場合がございます。 その際はお受け取り人様のご負担となりますので、 ご了承ください。
									</p>
								</div>
								<div class="tdList_col">
									<p>
										ギフトの場合は、備考欄にてその旨をご連絡ください。EMSラベルのギフト欄にチェックを入れて発送させていただきます。
									</p>
								</div>
								<div class="tdList_col">
									<p>
										消費税免税にてご購入いただけますが、システムの都合上、ご注文完了時には消費税が課税されます。免税及び配送料を追加しお支払総額修正後、メールにてご案内いたします。
									</p>
								</div>
							</div>
						</div>
					</div>
					<p>※一部商品は海外配送が承れない場合がございます。</p>
				</div>
			</div>
			<div id="a06" class="shoppingGuideArea_cts--list">
				<h3>返品・交換について</h3>
				<div class="columnArea">
					<p>
						<span class="fwBold">商品不良による返品・交換について</span><br>
						<br>
						お届けした商品に万一、不良や品違いがございましたら、お手数をおかけいたしますが、<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Inquiry/InquiryInput.aspx") %>">お問い合わせフォーム</a> もしくは お電話（0120-370-063）にてご連絡願います。その上で、送料着払いにて当社宛に商品をお送りください。すぐに新しい商品をお送りいたします。<br>
						<br>
						<span class="fwBold">お客様のご都合による返品・交換</span><br>
						注文した商品がお客様のお望みの商品と異なる場合、お客様のご都合による返品、交換をご希望の場合は、未開封・<span class="fwBold">商品到着から７日以内の商品</span>につきましては返品、交換を承っております。その場合、<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Inquiry/InquiryInput.aspx") %>">お問い合わせフォーム</a>よりご連絡をお願いいたします。<br>
						お客様の事情により傷・破損が生じた場合は、配送料、並びに返金に伴う振込手数料はお客様のご負担とさせていただきます。開封、使用した商品ついては、不良品以外は返品できかねますので、予めご了承ください。
					</p>
				</div>
			</div>
			<div class="shoppingGuideArea_cts--list faq">
				<div class="columnArea">
					<h4>よくあるご質問</h4>
					<p>
						お買い物・商品に関するお問い合わせをまとめました。<br class="pc_contents">ご不明な点がございましたら、まずはこちらをご覧ください。
					</p>
					<ul class="faqBox">
						<li>
							<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/faq/") %>">サービスQ&A</a>
						</li>
						<li>
							<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/faq/#jump08") %>">商品Q&A</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="shoppingGuideArea_menu">
			<ul>
				<li>
					<a href="#a01">
						会員登録について
					</a>
				</li>
				<li>
					<a href="#a02">
						定期便について
					</a>
				</li>
				<li>
					<a href="#a03">
						ご注文方法について
					</a>
				</li>
				<li>
					<a href="#a04">
						お支払いについて
					</a>
				</li>
				<li>
					<a href="#a05">
						配送について
					</a>
				</li>
				<li>
					<a href="#a06">
						返品・交換について
					</a>
				</li>
			</ul>
			<ul>
				<!--<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/benefits/") %>">
						会員特典のご案内
					</a>
				</li>-->
				<li>
					<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/wrapping/") %>">
						ラッピングについて
					</a>
				</li>
			</ul>
		</div>
	</section>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					ショッピングガイド
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>

