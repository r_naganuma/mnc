﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="OTHER BRANDS(アザーブランド一覧) | SIMPLISSE(シンプリス) produced by 山本未奈子" MetaDescription="超吸収型生理ショーツ Bé-A(ベア) / 美しいボディへの思いを叶えるラグジュアリースパ DAMAI(ダマイ) / 加圧スタジオ開発 健康で美しい体づくりをサポート solace(ソラーチェ)" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="otherBrand">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					OTHER BRANDS
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

    <section class="listBrandsArea otherBrand">
        <div class="otherBrand__VisuContainer">
            <h2 class="otherBrand__VisuContainer--title">
                OTHER BRANDS
            </h2>
            <div class="otherBrand__VisuContainer--pic">
                <img src="images/01.jpg" alt="">
            </div>
            <div class="otherBrand__VisuContainer--brandLogo">
                <div class="bea">
                    <img src="images/be-a-logo.jpg" alt="">
                    <p class="brandName">
                        Bé-A / ベア
                    </p>
                </div>
            </div>
            <div class="otherBrand__VisuContainer--concept">
                <p>
                    Bé-A（ベア）は、穿くだけで1日を安心して過ごせる、サステナブルな超吸収型サニタリーショーツを開発 。漏れや交換などのストレスを軽減し、“その日であることを忘れるくらいの快適さ”を目指します。悩みを諦めず、もっと自由に、快適に。Girls Be Ambitious
                </p>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=othbea&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                    商品一覧をみる
                </a>
            </div>
        </div>
        <div class="otherBrand__VisuContainer">
            <div class="otherBrand__VisuContainer--pic">
                <img src="images/02.jpg" alt="">
            </div>
            <div class="otherBrand__VisuContainer--brandLogo">
                <div class="damai">
                    <img src="images/damai-logo.jpg" alt="">
                    <p class="brandName">
                        DAMAI / ダマイ
                    </p>
                </div>
            </div>
            <div class="otherBrand__VisuContainer--concept">
                <p>
                    数々の国際的アワード受賞歴をもつ、日本が誇るスパ「DAMAI/ダマイ」。温めること（THERMAL）、植物の力（PHYTO）、高度な技術（THERAPY）の3つをベースにした“サーマル フィト セラピー“という独自メソッドを掲げ、効果重視のスパ発祥のプロダクトが、美しさを引き出します。
                </p>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=othdam&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                    商品一覧をみる
                </a>
            </div>
        </div>
        <div class="otherBrand__VisuContainer">
            <div class="otherBrand__VisuContainer--pic">
                <img src="images/03.jpg" alt="">
            </div>
            <div class="otherBrand__VisuContainer--brandLogo">
                <div class="solace">
                    <img src="images/solace-logo.jpg" alt="">
                    <p class="brandName">
                        solace / ソラーチェ
                    </p>
                </div>
            </div>
            <div class="otherBrand__VisuContainer--concept">
                <p>
                    加圧トレーニングやインディバなどを行う「Solace/ソラーチェ代官山」。注目は、植物性タンパク質を主としたオールインワンドリンク「ソイリーン」。メディカルアドバイザーや管理栄養士を携え、1万件を超えるセッションから導きだしたメソッドにより、健康で美しい体づくりを目指します。
                </p>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=othsol&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                    商品一覧をみる
                </a>
            </div>
        </div>
    </section>

    <!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					OTHER BRANDS
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>
