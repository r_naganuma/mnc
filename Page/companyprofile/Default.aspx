﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="企業情報 | SIMPLISSE(シンプリス) produced by 山本未奈子" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="companyprofile">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					企業情報
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
    <!----->
    <section class="listBrandsArea companyprofile">
        <div class="companyprofile__title">
            <h2>
                企業情報
            </h2>
        </div>
        <div class="companyprofile__wrap">
            <dl class="companyprofile__wrap--Items">
                <dd>
                    会社名
                </dd>
                <dt>
                    MNC New York株式会社 / MNC New York Inc.
                </dt>
            </dl>
            <dl class="companyprofile__wrap--Items">
                <dd>
                    本社
                </dd>
                <dt>
                    東京都渋谷区神宮前5丁目1番7号
                </dt>
            </dl>
            <dl class="companyprofile__wrap--Items">
                <dd>
                    資本金
                </dd>
                <dt>
                    44,500,000円
                </dt>
            </dl>
            <dl class="companyprofile__wrap--Items">
                <dd>
                    代表取締役
                </dd>
                <dt>
                    山本 未奈子
                </dt>
            </dl>
            <dl class="companyprofile__wrap--Items">
                <dd>
                    事業内容
                </dd>
                <dt>
                    シンプリスの製造販売 / 美容に関する講演会
                </dt>
            </dl>
            <dl class="companyprofile__wrap--Items">
                <dd>
                    コーポレートミッション
                </dd>
                <dt>
                    あらゆる女性の可能性を信じ、<br>
                    私たちが情熱を持って生み出しお届けする商品を通じて<br>
                    イノベーションを起こしていく。
                </dt>
            </dl>
            <dl class="companyprofile__wrap--Items">
                <dd>
                    コーポレートサイト
                </dd>
                <dt>
                    https://www.mncny.co.jp/
                </dt>
            </dl>
            <dl class="companyprofile__wrap--Items">
                <dd>
                    主要取引銀行
                </dd>
                <dt>
                    みずほ銀行
                </dt>
            </dl>
        </div>

    </section>
    <!----->

    <div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					企業情報
				</a>
			</li>
		</ul>
	</div>
    <!--△ パンくず △-->

</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>
