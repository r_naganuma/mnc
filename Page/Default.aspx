﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="取り扱い店舗 (shoplist) " MetaDescription="取り扱い店舗 (SHOPLIST) | SIMPLISSE(シンプリス) produced by 山本未奈子" MetaDescription="SIMPLISSE商品は下記の店舗にてお取扱いがあります。店舗により営業時間・店休日・取り扱い商品などが異なりますので、 大変お手数ではございますが、詳しくは店舗へ直接お問い合わせいただきますようお願いいたします。" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="shopList">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					Shop List
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
    <!----->
    <section class="listBrandsArea shopList">
        <div class="shopList__title">
            <h2>
                取り扱い店舗
            </h2>
            <p>
                SIMPLISSE商品は下記の店舗にてお取扱いがあります。<br>
                店舗により営業時間・店休日・取り扱い商品などが異なりますので、 大変お手数ではございますが、詳しくは店舗へ直接お問い合わせいただきますようお願いいたします。
            </p>
        </div>
        <div class="shopList__ankerLink" id="anker">
            <a href="#Tokyo">東京</a>
<!--             <a href="">関東</a> -->
            <a href="#Tokai">東海</a>
            <a href="#Kinki">近畿</a>
            <a href="#Kyushu">九州</a>
            <a href="#Online">オンラインストア</a>
        </div>
        <div class="shopList__listWrapContainer">
            <div class="shopList__listWrapContainer--oneSection" id="Tokyo">
                <h3>
                    東京
                </h3>
                <ul class="flexWrap">
                    <li>
                        <p class="shopName">
                            伊勢丹 新宿店 ビューティアポセカリー
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒160-0022
                            </span>
                            <span>
                                東京都新宿区新宿3-14-1 伊勢丹 新宿店 本館地下2階
                            </span>
                            <span>
                                TEL：03-3352-1111
                            </span>
                        </p>
                        <a href="https://www.isetan.mistore.jp/shinjuku/shops/beauty/apothecary.html">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            伊勢丹 新宿店 婦人雑貨 ISETANSEED
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒160-0022
                            </span>
                            <span>
                                東京都新宿区新宿3-14-1 伊勢丹 新宿店 本館1階
                            </span>
                            <span>
                                TEL：03-3352-1111
                            </span>
                        </p>
                        <a href="https://www.isetan.mistore.jp/shinjuku.html">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            エストネーション 六本木ヒルズ店
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒106-0032
                            </span>
                            <span>
                                東京都港区六本木6-10-2 六本木ヒルズ
                            </span>
                            <span>
                                ヒルサイドけやき坂コンプレックス 1F・2F
                            </span>
                        </p>
                        <a href="http://www.estnation.co.jp/store/roppongi.html">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            イークリニック麻布
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒106-0046
                            </span>
                            <span>
                                東京都港区元麻布1丁目2−13 2階
                            </span>
                            <span>
                                TEL：03-6722-6222
                            </span>
                        </p>
                        <a href="https://dre-cli.com/">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            Riche 麻布十番
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒106-0045
                            </span>
                            <span>
                                東京都港区麻布十番3-3-8 1F
                            </span>
                            <span>
                                TEL：03-3455-8298
                            </span>
                        </p>
                        <a href="http://www.riche.tokyo/">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            二子玉川 蔦屋家電
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒158-0094
                            </span>
                            <span>
                                東京都世田谷区玉川1丁目14番1号 二子玉川ライズ S.C. テラスマーケット
                            </span>
                            <span>
                                TEL：03-5491-8550
                            </span>
                        </p>
                        <a href="https://store.tsite.jp/futakotamagawa/">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            ヴィリーナ 広尾店
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒106-0031
                            </span>
                            <span>
                                東京都港区西麻布3-8-15 麻布YANAGIDAビル１F
                            </span>
                            <span>
                                TEL： 0120-916-442
                            </span>
                        </p>
                        <a href="https://www.virinamaternity.com/index.shtml">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            MS・Style Echika表参道
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒107-0061
                            </span>
                            <span>
                                東京都港区北青山 3-6-12 表参道駅構内
                            </span>
                            <span>
                                TEL/FAX：03-3470-1109
                            </span>
                        </p>
                        <a href="https://www.echika-echikafit.com/shop/omotesando/omt09">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            solace代官山（ソラーチェ代官山）
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒150-0021
                            </span>
                            <span>
                                東京都渋谷区恵比寿西2-20-15 ソルスティス代官山1Ｆ
                            </span>
                            <span>
                                TEL：03-3780-5770
                            </span>
                        </p>
                        <a href="https://www.solace-daikanyama.com/#pagetop">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            メイクアップソリューション 新宿
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒160-8001
                            </span>
                            <span>
                                東京都新宿区西新宿 1-1-3 小田急百貨店本館2F
                            </span>
                            <span>
                                TEL/FAX：03-3344-0026
                            </span>
                        </p>
                        <a href="http://www.odakyu-dept.co.jp/shinjuku/floorguide/02/index.html">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            アーバンコンフォート有楽町
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒100-0006
                            </span>
                            <span>
                                東京都千代田区有楽町 2-7-1 有楽町マルイ5F
                            </span>
                            <span>
                                TEL/FAX：03-6738-3852
                            </span>
                        </p>
                        <a href="http://www.0101.co.jp/index.html">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            MS・Style Esola 池袋
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒170-0014
                            </span>
                            <span>
                                東京都豊島区西池袋1-12-1 Esola池袋 B1F
                            </span>
                            <span>
                                TEL:03-5952-0415
                            </span>
                        </p>
                        <a href="https://www.esola-ikebukuro.com/">
                            official site
                        </a>
                    </li>
                </ul>
            </div>
<!--             <div class="shopList__listWrapContainer--oneSection" id="#Kantou">
                <h3>
                    関東
                </h3>
                <ul class="flexWrap">
                    <li>
                        <p class="shopName">
                            伊勢丹 新宿店 ビューティアポセカリー
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒160-0022
                            </span>
                            <span>
                                東京都新宿区新宿3-14-1 伊勢丹 新宿店 本館地下2階
                            </span>
                            <span>
                                TEL：03-3352-1111
                            </span>
                        </p>
                        <a href="">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            エストネーション 六本木ヒルズ店
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒106-0032
                            </span>
                            <span>
                                東京都港区六本木6-10-2 六本木ヒルズ
                            </span>
                            <span>
                                ヒルサイドけやき坂コンプレックス 1F・2F
                            </span>
                        </p>
                        <a href="">
                            official site
                        </a>
                    </li>
                </ul>
            </div> -->
            <div class="shopList__listWrapContainer--oneSection" id="Tokai">
                <h3>
                    東海
                </h3>
                <ul class="flexWrap">
                    <li>
                        <p class="shopName">
                            メイクアップソリューション 星ヶ丘
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒464-8661
                            </span>
                            <span>
                                愛知県名古屋市千種区星が丘元町14-14 星ヶ丘三越 2F
                            </span>
                            <span>
                                TEL：052-783-0938
                            </span>
                        </p>
                        <a href="https://www.mitsukoshi.mistore.jp/hoshigaoka/shops/floor2.html">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            MONA MACHO
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒460-0008
                            </span>
                            <span>
                                愛知県名古屋市中区栄5丁目28-19 アルティメイトタワー栄V 1･2F
                            </span>
                            <span>
                                052-262-9229
                            </span>
                        </p>
                        <a href="https://www.modix.jp/company/">
                            official site
                        </a>
                    </li>
                </ul>
            </div>
            <div class="shopList__listWrapContainer--oneSection" id="Kinki">
                <h3>
                    近畿
                </h3>
                <ul class="flexWrap">
                    <li>
                        <p class="shopName">
                            メイクアップソリューション 京都
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒600-8520
                            </span>
                            <span>
                                京都府京都市下京区四条通河原町 西入真町52 京都高島屋4F
                            </span>
                            <span>
                                TEL：075-221-3511
                            </span>
                        </p>
                        <a href="https://www.takashimaya.co.jp/kyoto/floor/f04.html">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            メイクアップソリューション 神戸
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒650-0037
                            </span>
                            <span>
                                兵庫県神戸市中央区明石町40番地 大丸神戸店B2F
                            </span>
                            <span>
                                TEL：078-393-3386
                            </span>
                        </p>
                        <a href="https://www.daimaru.co.jp/kobe/floor/b2f.html">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            メイクアップソリューション あべのハルカス
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒545-8545
                            </span>
                            <span>
                                大阪府大阪市阿倍野区阿倍野筋1-1-43 あべのハルカス近鉄本店 ウィング館2F
                            </span>
                            <span>
                                TEL：06-6628-5888
                            </span>
                        </p>
                        <a href="https://abenoharukas.d-kintetsu.co.jp/floor/wing/2">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            メイクアップソリューション なんば
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒542-8510
                            </span>
                            <span>
                                大阪府大阪市中央区難波5-1-5 髙島屋大阪店 5Fエフズクローゼット
                            </span>
                            <span>
                                TEL：06-6636-0195
                            </span>
                        </p>
                        <a href="https://www.takashimaya.co.jp/osaka/floor/f05.html">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            メイクアップソリューション 枚方
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒573-0032
                            </span>
                            <span>
                                大阪府枚方市岡東町19-19 京阪百貨店ひらかた店1F
                            </span>
                            <span>
                                TEL：072-843-2245
                            </span>
                        </p>
                        <a href="https://www.takashimaya.co.jp/osaka/floor/f05.html">
                            official site
                        </a>
                    </li>
                </ul>
            </div>
            <div class="shopList__listWrapContainer--oneSection" id="Kyushu">
                <h3>
                    九州
                </h3>
                <ul class="flexWrap">
                    <li>
                        <p class="shopName">
                            アーバンコンフォート 福岡
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒810-0001
                            </span>
                            <span>
                                福岡県福岡市中央区天神2丁目11-1 福岡パルコ5F
                            </span>
                            <span>
                                TEL/FAX：092-235-7147
                            </span>
                        </p>
                        <a href="http://www.parco-fukuoka.com/page/shop/detail/?id=6101">
                            official site
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            メイクアップソリューション 博多
                        </p>
                        <p class="shopAdress">
                            <span>
                                〒812-0012
                            </span>
                            <span>
                                福岡県福岡市博多区博多駅中央街1-1 博多阪急8F
                            </span>
                            <span>
                                TEL：06-6636-0195
                            </span>
                        </p>
                        <a href="https://www.hankyu-dept.co.jp/hakata/floor/8f.html/">
                            official site
                        </a>
                    </li>
                </ul>
            </div>
            <div class="shopList__listWrapContainer--oneSection" id="Online">
                <h3>
                    オンラインストア
                </h3>
                <ul class="flexWrap">
                    <li>
                        <p class="shopName">
                            Amazon (アマゾン)
                        </p>
                        <a href="https://www.amazon.co.jp/">
                            https://www.amazon.co.jp/
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            SIMPLISSE 楽天市場店
                        </p>
                        <a href="https://www.rakuten.co.jp/simplisse/">
                            https://www.rakuten.co.jp/simplisse/
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            ELLE SHOP (エルショップ)
                        </p>
                        <a href="https://elleshop.jp/web/brand/simplisse/">
                            https://elleshop.jp/web/brand/simplisse/
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            meeco 三越伊勢丹化粧品オンラインストア
                        </p>
                        <a href="https://meeco.mistore.jp/">
                            https://meeco.mistore.jp/
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            二子玉川 蔦屋家電 Yahoo!店
                        </p>
                        <a href="https://shopping.geocities.jp/ftk-tsutayaelectrics/">
                            https://shopping.geocities.jp/ftk-tsutayaelectrics/
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            FLAG SHOP (フラッグショップ)
                        </p>
                        <a href="https://flagshop.jp/fs/shop/r/r21712/">
                            https://flagshop.jp/fs/shop/r/r21712/
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            二子玉川 蔦屋家電 Yahoo!店
                        </p>
                        <a href="https://shopping.geocities.jp/ftk-tsutayaelectrics/">
                            https://shopping.geocities.jp/ftk-tsutayaelectrics/
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            kokode.jp (ココデ)
                        </p>
                        <a href="https://kokode.jp/">
                            https://kokode.jp/
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            三越伊勢丹オンラインストア
                        </p>
                        <a href="https://www.mistore.jp/shopping/">
                            https://www.mistore.jp/shopping/
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            イークリニック オンラインショップ
                        </p>
                        <a href="https://dre-cli.shop/">
                            https://dre-cli.shop/
                        </a>
                    </li>
                    <li>
                        <p class="shopName">
                            VIRINA WEB SHOP
                        </p>
                        <a href="https://www.virinamaternity.com/index.shtml">
                            https://www.virinamaternity.com/index.shtml
                    </li>
                    <li>
                        <p class="shopName">
                            エムコスメスタイル 楽天市場店
                        </p>
                        <a href="https://search.rakuten.co.jp/search/mall/SIMPLISSE/?sid=311104">
                            https://search.rakuten.co.jp/search/mall/SIMPLISSE/?sid=311104
                        </a>
                    </li>
                </ul>
            </div>
<!--             <div class="shopList__bottom">
                <p class="shopList__bottom--title">
                    SIMPLISSE / シンプリスでは、お電話でもご注文を承っております。
                </p>
                <span class="shopList__bottom--text">
                    ・お支払方法は、クレジットカード決済・代金引換・NP後払いからお選び頂けます（手数料無料）。
                </span>
                <span class="shopList__bottom--text">
                    ・配送料は、全国一律525円（沖縄・離島は1,050円）です。ご購入金額が10,000円以上の場合は送料無料となります。
                </span>
                <p class="shopList__bottom--title las">
                    お電話でのお問い合せ
                </p>
                <span class="shopList__bottom--text">
                    フリーダイヤル　0120-370-063（10:00-12:00/13:30-17:00 土日・祝日休業）
                </span>
            </div> -->
        </div>
    </section>

    <div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					Shop List
				</a>
			</li>
		</ul>
	</div>
    <!--△ パンくず △-->

</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>
