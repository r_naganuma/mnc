﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="ネンマク ケア キャンペーン | SIMPLISSE(シンプリス) produced by 山本未奈子" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="renewal">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					NEN-MAKU EARLY SPRING CAMPAIGN
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<section class="kvArea">
		<picture>
			<source media="(max-width: 769px)" srcset="./images/main_sp2.png">
			<img class="main_pic" src="./images/main2.png" alt="">
		</picture>
	</section>

	<div style="background: #ffffff; border: 1px solid #ffffff; margin: 10px; solid #ffffff;"></div>

<div style="text-align:center;">
	<span style="font-size: 22pt; line-height: 35px; letter-spacing: 2px; color: #2e2e2e;">
				潤って、<br class="sp_contents">春の不快感にさようなら。<br><br>
</span>

	<span style="font-size: 12pt; line-height: 35px; letter-spacing: 2px; color: #2e2e2e;">
				暖かな春の訪れが待ち遠しいこの頃。<br class="sp_contents">しかしその前にやってくる花粉の季節…。<br>
				鼻のムズムズ、目の痒み、喉のイガイガ…。<br>
				実はカラダの内側が乾燥していることで、<br class="sp_contents">それらを増長させてしまっている可能性も。<br>
				外と内の間にあり、守ってくれる最後の砦は、<br class="sp_contents">潤っていることがとても大切なのです。<br>
				ネンマク ケアで快適な毎日を。
</span>
		</div>
	
	

	
	

	
	
	
<div style="background: #ffffff; border: 1px solid #ffffff; margin: 10px; solid #ffffff;"></div>

<div style="text-align:center;">
	<span style="font-size: 19pt; line-height: 35px; letter-spacing: 2px; color: #0bb3a4;">
				センシュアル ネンマクケア <br class="sp_contents">2箱ご購入で<br>
				フェミニンエリア専用ウォッシュ現品<br class="sp_contents">PRESENT<br>
				</span>
	
		<span style="font-size: 16pt; line-height: 35px; letter-spacing: 2px; color: #0bb3a4;">
				3/3 17:00まで<br><br>
		</span>
</div>
		<section class="kvArea">
		<picture>
			<source media="(max-width: 769px)" srcset="./images/present_sp.png">
			<img class="main_pic" src="./images/present2.png" alt="">
		</picture>
			<picture><a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001339&cat=wel") %>" class="ctsBtn">
			<source media="(max-width: 769px)" srcset="./images/nenmaku3_sp.png">
			<img class="main_pic" src="./images/nenmaku3.png" alt="">
		</a></picture>
	</section>
	
	
	
	
	
	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					NEN-MAKU EARLY SPRING CAMPAIGN
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>

