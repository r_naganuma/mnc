﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="はじめての方へ10日間のお試しセット(ウェルカムパッケージ) | SIMPLISSE(シンプリス) produced by 山本未奈子" MetaDescription="自然と科学の“いいとこどり”。それぞれの力を融合させたスキンケア、SIMPLISSEを体感いただけるトライアルセットをご用意しました。" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css?12">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="welcome">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					はじめての方へ10日間のお試しセット｜ウェルカムパッケージ
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

    <section class="listBrandsArea welcome">
        <div class="welcome__MainVisuContainer">
            <h2 class="welcome__MainVisuContainer--Title">
                はじめての方へ10日間のお試しセット <span class="sp_noneObj1">|</span><br class="sp_contents"> ウェルカムパッケージ
            </h2>
            <div class="welcome__MainVisuContainer--kv">
                <img src="images/page_kv.jpg" alt="">
            </div>   
        </div>
        <div class="welcome__firstExinner">
            <p class="welcome__firstExinner--title">
                はじめての方に<span>おすすめは美容液</span>
            </p>
            <div class="welcome__firstExinner--explain">
                <p>
                    自然と科学の“いいとこどり”。<br class="pc_contents"><br class="sp_contents">
                    それぞれの力を融合させたスキンケア、<br class="pc_contents"><br class="sp_contents">
                    SIMPLISSEを体感いただける<br class="sp_contents">トライアルセットをご用意しました。
                </p>
                <p>
                    SIMPLISSEのスキンケアは美容液を2つ重ねる<br class="sp_contents">という<br class="pc_contents">シンプルで新しいステップ。<br class="pc_contents"><br class="sp_contents">その短いステップの中にはあらゆる肌悩みに応<br class="sp_contents">える<br class="pc_contents">たくさんの成分が配合されています。<br class="pc_contents"><br class="sp_contents">新たなスキンケアの扉を、SIMPLISSEと共に開<br class="sp_contents">きましょう。
                </p>
            </div>
        </div> 
        <div class="welcome__traialinner">
            <div class="welcome__traialinner--tilte">
                <p>
                    まずは10日間お試しください
                </p>
                <h3>
                    肌が変わる喜びの第一歩を、<br class="sp_contents">SIMPLISSEで
                </h3>
            </div>
            <div class="welcome__traialinner--flex">
                <div class="welcome__traialinner--traialItem">
                    <img src="images/img01.png" alt="">
                    <ul class="welcome__traialinner--traialExBox">
                        <li>
                            10日間分トライアル
                        </li>
                        <li>
                            はじめての方・1点限り
                        </li>
                        <li>
                            新規会員登録で送料無料
                        </li>
                        <li>
                            最短翌日発送
                        </li>
                    </ul>
                </div>
                <div class="welcome__traialinner--traialItemExplain">
                    <p class="title">
                        シンプリス
                        <span>ウェルカムパッケージ</span>
                    </p>
                    <ul class="contentsListItem">
                        <li>
                            シンプリス ブースター モイストアップ ハーバル セラム 
                            <span>＜ブースター/保湿導入美容液＞10g（10日分）</span>
                        </li>
                        <li>
                            シンプリス BC ハーバル インテンシブ セラム 
                            <span>＜ブライトニング クリア美容液＞10g（10日分）</span>
                        </li>
                        <li>
                            シンプリス AC ハーバル アドバンスト セラム
                            <span>＜エイジングケア美容液＞10g（10日分）</span>
                        </li>
                        <li>
                            シンプリス シルバー ポーチ
                        </li>
                        <li>
                            山本未奈子著「本当に知りたかった美肌の教科書」
                        </li>
                    </ul>
                    <div class="welcome__traialinner--specialPrice">
                        <p>
                            はじめての方限定価格
                            <!--<span class="special">4,730円</span>-->
                        </p>
                        <div class="praiceInner">
                            <span>税込</span>
                            <p class="price">
                                4,730円→
                            </p>
                            <span class="col">税込</span>
                            <p class="price col">
                                3,630円
                            </p>
                        </div>
                        <a class="toBuyButton" href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000911&cat=bty") %>">
                            詳細・ご購入はこちら
                        </a>
                        
                    </div>  
                </div>
            </div>
            <div class="welcome__traialinner--bookinner">
                <div class="welcome__traialinner--bookinnerFlex">
                    <div class="book">
                        <img src="images/book.png" alt="">
                    </div>
                    <div class="bookContents">
                        <div class="bookContents__flex">
                            <p class="personName">
                                山本未奈子著
                            </p>
                            <p class="bookExplainContents">
                                <span>本当に知りたかった<br class="sp_contents">美肌の教科書</span><br class="sp_contents">もセットでお届け
                            </p>
                        </div>
                    </div>
                </div>
                <p class="bookExplainContents__second">
                    ダブル洗顔や、コラーゲン信仰、ローラー式美顔器…<br class="pc_contents">
                    その習慣と思い込みを捨てれば、美肌は簡単！日本人の知らない美肌ルール。
                </p>
                
            </div>
        </div>
        <div class="welcome__userVoiceinner">
            <p class="welcome__userVoiceinner--title">
                <span>USER’S VOICE</span>
                多くのお客様にご支持いただいております
            </p>
            <div class="welcome__userVoiceinner--gallery">
                <div class="item">
                    <img src="images/1.jpg" alt="">
                    <p class="itemText">
                        まだまだ乾燥を感じる今の時期、顔の細部にエイジングサインが出やすくなります。<br class="sp_contents">
                        特に目元や口元は皮脂腺がなく、バリア機能が低いのに酷使するパーツです。<br class="sp_contents">
                        見た目の年齢を大きく左右すると言われる部分だからこそ特化した集中ケアアイテムがおすすめです。
                        <span class="name">
                        ・<br class="pc_contents">
                        パーツ用集中ケアセラム
                        </span>
                        <span class="tag">#シンプリスコンセントレートラインセラム</span>
                    </p>
                </div>
                <div class="item">
                    <img src="images/2.jpg" alt="">
                    <p class="itemText">
                        まだまだ乾燥を感じる今の時期、顔の細部にエイジングサインが出やすくなります。<br class="pc_contents"><br class="sp_contents">
                        特に目元や口元は皮脂腺がなく、バリア機能が低いのに酷使するパーツです。<br class="sp_contents">
                        見た目の年齢を大きく左右すると言われる部分だからこそ特化した集中ケアアイテムがおすすめです。
                        <span class="name">
                        ・<br class="pc_contents">
                        パーツ用集中ケアセラム
                        </span>
                        <span class="tag">#シンプリスコンセントレートラインセラム</span>
                    </p>
                </div>
                <div class="item">
                    <img src="images/3.jpg" alt="">
                    <p class="itemText">
                        まだまだ乾燥を感じる今の時期、顔の細部にエイジングサインが出やすくなります。<br class="pc_contents"><br class="sp_contents">
                        特に目元や口元は皮脂腺がなく、バリア機能が低いのに酷使するパーツです。<br class="sp_contents">
                        見た目の年齢を大きく左右すると言われる部分だからこそ特化した集中ケアアイテムがおすすめです。
                        <span class="name">
                        ・<br class="pc_contents">
                        パーツ用集中ケアセラム
                        </span>
                        <span class="tag">#シンプリスコンセントレートラインセラム</span>
                    </p>
                </div>
                <div class="item">
                    <img src="images/4.jpg" alt="">
                    <p class="itemText">
                        まだまだ乾燥を感じる今の時期、顔の細部にエイジングサインが出やすくなります。<br class="pc_contents"><br class="sp_contents">
                        特に目元や口元は皮脂腺がなく、バリア機能が低いのに酷使するパーツです。<br class="sp_contents">
                        見た目の年齢を大きく左右すると言われる部分だからこそ特化した集中ケアアイテムがおすすめです。
                        <span class="name">
                        ・<br class="pc_contents">
                        パーツ用集中ケアセラム
                        </span>
                        <span class="tag">#シンプリスコンセントレートラインセラム</span>
                    </p>
                </div>
            </div>
            <div class="welcome__userVoiceinner--feture">
                <div class="circleinner">
                    <div class="circle">
                        <div class="circleContent">
                            <p class="circleTitle">
                                脂性が改善！！
                                <span>
                                    Y.I　様（28歳）
                                </span>
                            </p>
                            <p class="circleText">
                                いままでオイリーな自分の肌が嫌い
                                でしたが、みるみるうちに改善。
                                いまはつるつる。
                            </p>
                        </div>
                    </div>
                    <div class="circle">
                        <div class="circleContent">
                            <p class="circleTitle">
                                化粧品ジプシー終了
                                <span>
                                    H.N　様（39歳）
                                </span>
                            </p>
                            <p class="circleText">
                                一番肌の状態が安定し、使いごこち
                                も良く、短時間でお手入れを終え
                                られるものはシンプリスでした。
                            </p>
                        </div>
                    </div>
                    <div class="circle">
                        <div class="circleContent">
                            <p class="circleTitle">
                                楽なのに優秀
                                <span>
                                    A.K　様（32歳）
                                </span>
                            </p>
                            <p class="circleText">
                                体調が悪くても赤みが出にくく
                                なったのと、朝起きた時の肌が
                                ツヤツヤしていて驚きました。
                            </p>
                        </div>
                    </div>
                </div>
                <p class="lastText">
                    ※個人の感想であり効果・効能を<br class="sp_contents">示すものではありません。
                </p>
            </div>
        </div>
        <div class="welcome__maxViewinner">
            <picture>
            <source media="(max-width:768px)" srcset="images/06-sp.jpg" alt="">
            <img src="images/06.jpg" alt="">
            </picture>
        </div>
        <div>
            <div class="welcome__firstExinner secondExinner">
                <p class="welcome__firstExinner--title">
                    シンプルで<span>確かなケアを。</span>
                </p>
                <div class="welcome__firstExinner--explain">
                    <p>
                        美容液はスキンケアにおいて一番重要なアイテム。<br class="pc_contents">
                        化粧水よりも美容成分を凝縮して配合できる上、どこへ浸透させるかなど、<br class="pc_contents">
                        粒子の大きさを計算して設計されています。<br class="pc_contents">
                        だからSIMPLISSEは肌に必要な成分を2本の美容液に凝縮しました。
                    </p>
                    <p>
                        美容液のほうが、成分を高濃度で配合できるから化粧水はなし。<br class="pc_contents">
                        美容液のほうが、透過性に優れているからクリームはなし。<br class="pc_contents">
                        「２つの美容液を重ねる」というシンプルな設計が、<br class="pc_contents">
                        美肌を育む最良の方法だと考えました。
                    </p>
                </div>
            </div> 
        </div>
        <div class="welcome__beautyLiquidinner">
            <div class="boxInner">
                <p class="title">
                    第1の美容液
                    <span>
                        朝・夜 2プッシュ
                    </span>
                </p>
                <p class="text">
                    古い角質をそっと取り除き、<br class="pc_contents"><br class="sp_contents">
                    次に使う美容液を受け入れる準備を整えます。
                </p>
                <div class="itemsContent">
                    <img src="images/7.jpg" alt="">
                    <p class="name">
                        ブースター美容液
                    </p>
                    <div class="checkinner">
                        <p>
                            角質
                        </p>
                        <p>
                            キメの乱れ
                        </p>
                        <p>
                            ざらつき
                        </p>
                        <p>
                            導入
                        </p>
                    </div>
                </div>
            </div>
            <div class="boxInner">
                <p class="title">
                    第2の美容液
                    <span>
                        朝・夜 2プッシュ
                    </span>
                </p>
                <p class="textSecond">
                    角質が取り除かれ整った肌に<br class="pc_contents"><br class="sp_contents">
                    たっぷりの潤いと美容成分を届けます。
                </p>
                <div class="itemsContentSecond">
                    <p class="itemsContentSecond__title">
                        肌の悩みや季節に合わせてお選びいただけます。
                    </p>
                    <div class="flex">
                        <div class="flex__box">
                            <img src="images/8.jpg" alt="">
                            <p class="flex__box--text">
                                乾きやくすみ、<br class="pc_contents"><br class="sp_contents">
                                メラミンが気になる肌に。
                            </p>
                            <p class="flex__box--name">
                                ブライトニング クリア美容液
                            </p>
                            <div class="checkinner">
                                <p>
                                    明るさ<sup>*2</sup>
                                </p>
                                <p>
                                    くすみ
                                </p>
                                <p>
                                    透明感<sup>*2</sup>
                                </p>
                                <p>
                                    保湿
                                </p>
                            </div>
                            <p class="lastText">
                                ＊2 角質層が潤うことによる。
                            </p>
                        </div>
                        <div class="flex__box">
                            <img src="images/9.jpg" alt="">
                            <p class="flex__box--text">
                                潤いやハリ感、エイジング<br class="pc_contents">
                                サインが気になる肌に。
                            </p>
                            <p class="flex__box--name">
                                エイジングケア美容液
                            </p>
                            <div class="checkinner">
                                <p>
                                    保湿
                                </p>
                                <p>
                                    弾力<sup>*2</sup>
                                </p>
                                <p>
                                    たるみ<sup>*2</sup>
                                </p>
                                <p>
                                    保湿
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="welcome__midViewinner">
            <picture>
                <source media="(max-width:768px)" srcset="images/09-sp.jpg">
                <img src="images/09.jpg" alt="">
            </picture>
            <div>
                <div class="welcome__midViewinner--Exinner">
                    <p class="title">
                        自然と科学の<span>“いいとこどり”</span>
                    </p>
                    <div class="explain">
                        <p>
                            「自然か、科学か」という二者択一はあえてしません。<br class="pc_contents">
                            古くから親しまれてきた植物のエッセンスには、<br class="pc_contents">
                            人工的には決して作ることのできない 素晴らしい成分があります。<br class="pc_contents">
                            また最先端の科学は、肌のしくみに基づいた有効成分や、<br class="pc_contents">
                            身体のすみずみに届けるための効果的な方法を教えてくれます。<br class="pc_contents">
                            Holistic（自然）の力も、Medical（科学）の力も、<br class="pc_contents">
                            どちらも享受して、最高の結果を出したい。<br class="pc_contents">
                            それが、SIMPLISSEの「ホリディカル美容」です。
                        </p>
                    </div>
                </div> 
                <div class="holdical">
                    <img src="images/11.jpg" alt="">
                </div>
                <div class="welcome__hosricMedicalinner">
                    <picture>
                        <source media="(max-width:768px)" srcset="images/12-sp.jpg">
                        <img src="images/12.jpg" alt="">
                    </picture>
                </div>
            </div>
        </div>
        <div class="welcome__simplisseCeoinner">
            <div class="flex">
                <div class="welcome__simplisseCeoinner--seoBox">
                    <div class="pic">
                        <img src="images/ceo.jpg" alt="">
                    </div>
                </div>
                <div class="welcome__simplisseCeoinner--exBox">
                    <p class="title">
                        肌に悩んだ私から、
                        <span>全世界の女性たちへ</span>
                    </p>
                    <div class="explain">
                        <p>
                            キレイになりたいという思いを、<br class="pc_contents">
                            そのままスキンケアにつなげたい。<br class="pc_contents">
                            そんな願いから生まれたのが、SIMPLISSEです。
                        </p>
                        <p>
                            巷に流布する「美容のウワサ」はすべてリセットしました。<br class="pc_contents">
                            その代わりにベースとしたのは、NY発の最新皮膚科学理論。<br class="pc_contents">
                            現代女性の肌に必要なものだけを、ごくシンプルなスキンケアで<br class="pc_contents">
                            誰でも享受できるようにそんな願いを込めて<br class="pc_contents">
                            誕生した化粧品を、SIMPLISSEと名づけました。
                        </p>
                        <p>
                            お手入れにかけた時間やお金で、<br class="pc_contents">
                            キレイになるわけではありません。<br class="pc_contents">
                            正しい知識や理論に基づいたスキンケアなら、<br class="pc_contents">
                            シンプルなステップで確実に、キレイを育てることができます。<br class="pc_contents">
                            SIMPLISSEと一緒に、そんな“現代の贅沢”を手にいれませんか？
                        </p>
                    </div>
                </div>
            </div>
            <div class="personalEx">
                <p class="names">
                    SIMPLISSE 代表
                    <span>
                        山本未奈子
                    </span>
                </p>
                <p class="license">
                    <span>
                        NY州認定ビューティーセラピスト
                    </span>
                    <span>
                        英国ITEC国際ビューティースペシャリスト
                    </span>
                </p>
                <p class="text">
                    ニューヨーク有数の美容学校で教鞭を執る傍ら、MNCメディカルスパを立ち上げる。それらを経てMNC New York Inc.を設立し、美容ブランド「シンプリス」を発表。著書に、『本当に知りたかった／美肌の教科書』 （講談社＋α文庫）などがある。
                </p>
            </div>
        </div>

        <div class="welcome__traialinner">
            <div class="welcome__traialinner--tilte">
                <p>
                    まずは10日間お試しください
                </p>
                <h3>
                    肌が変わる喜びの第一歩を、SIMPLISSEで
                </h3>
            </div>
            <div class="welcome__traialinner--flex">
                <div class="welcome__traialinner--traialItem">
                    <img src="images/img01.png" alt="">
                    <ul class="welcome__traialinner--traialExBox">
                        <li>
                            10日間分トライアル
                        </li>
                        <li>
                            はじめての方・1点限り
                        </li>
                        <li>
                            新規会員登録で送料無料
                        </li>
                        <li>
                            最短翌日発送
                        </li>
                    </ul>
                </div>
                <div class="welcome__traialinner--traialItemExplain">
                    <p class="title">
                        シンプリス
                        <span>ウェルカムパッケージ</span>
                    </p>
                    <ul class="contentsListItem">
                        <li>
                            シンプリス ブースター モイストアップ ハーバル セラム 
                            <span>＜ブースター/保湿導入美容液＞10g（10日分）</span>
                        </li>
                        <li>
                            シンプリス BC ハーバル インテンシブ セラム 
                            <span>＜ブライトニング クリア美容液＞10g（10日分）</span>
                        </li>
                        <li>
                            シンプリス AC ハーバル アドバンスト セラム
                            <span>＜エイジングケア美容液＞10g（10日分）</span>
                        </li>
                        <li>
                            シンプリス シルバー ポーチ
                        </li>
                        <li>
                            山本未奈子著「本当に知りたかった美肌の教科書」
                        </li>
                    </ul>
                    <div class="welcome__traialinner--specialPrice">
                        <p>
                            はじめての方限定価格
                            <!--<span class="special">4,730円</span>-->
                        </p>
                        <div class="praiceInner">
                            <span>税込</span>
                            <p class="price">
                                4,730円→
                            </p>
                            <span class="col">税込</span>
                            <p class="price col">
                                3,630円
                            </p>
                        </div>
                        <a class="toBuyButton" href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000911&cat=bty") %>">
                            詳細・ご購入はこちら
                        </a>
                        
                    </div>  
                </div>
            </div>
            <div class="welcome__traialinner--bookinner">
                <div class="welcome__traialinner--bookinnerFlex">
                    <div class="book">
                        <img src="images/book.png" alt="">
                    </div>
                    <div class="bookContents">
                        <div class="bookContents__flex">
                            <p class="personName">
                                山本未奈子著
                            </p>
                            <p class="bookExplainContents">
                                <span>本当に知りたかった<br class="sp_contents">美肌の教科書</span>もセットでお届け
                            </p>
                        </div>
                    </div>
                </div>
                <p class="bookExplainContents__second">
                    ダブル洗顔や、コラーゲン信仰、ローラー式美顔器…<br class="pc_contents">
                    その習慣と思い込みを捨てれば、美肌は簡単！日本人の知らない美肌ルール。
                </p>
                
            </div>
        </div>
    </section>

    	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					はじめての方へ10日間のお試しセット｜ウェルカムパッケージ
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>
