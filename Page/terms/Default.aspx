﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="会員規約 | SIMPLISSE(シンプリス) produced by 山本未奈子" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="terms">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					会員規約
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
    <!----->
    <section class="listBrandsArea terms">
        <div class="terms__title">
            <h2>
                会員規約
            </h2>
            <div class="block">
                <p class="block--title">
                    第1条：規約の適応
                </p>
                <p class="block--content">
                    MNC New York株式会社（以下「当社｣といいます）が運営するシンプリス オンラインストア（以下「本サイト」といいます）において「シンプリス オンラインストア会員」として登録された方（以下「会員｣といいます）は「シンプリス オンラインストア会員規約」に基づき当社が本サイト上で会員のみを対象として行う各種提供サービス（以下「本サービス｣といいます）を利用することができるものとします。
                </p>
                <p class="block--content sub">
                    会員は、よりお買い物をお楽しみいただくことを目的としたお客さまサービスの一環として、当社輸入ヘアアクセサリーブランドFrance Luxe／フランスラックス オンラインストア発足時において自動的にVIP会員ランクが引き継がれております。なお、会員ランクは2013年10月時点のものが適用され、その後会員となられた方についてはシンプリス、フランス ラックスのそれぞれへの会員登録が必要となります。フランスラックス オンラインストア会員の退会はフランスラックスのサイト（www.franceluxe.jp）にてお手続きください。
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    第2条：規約の変更
                </p>
                <p class="block--content">
                    当社は、本規約の内容を会員に事前に予告なく変更できるものとします。<br>
                    この場合、会員に対し、変更内容を本サイトまたは当社が定める方法で通知するものとします。
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    第3条：規約の遵守
                </p>
                <p class="block--content">
                    会員は、本サービスを受けるにあたり、本規約を遵守するものとします。
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    第4条：会員登録
                </p>
                <p class="block--content">
                    １．会員として登録を希望される方は、本規約に同意のうえ、本サイトで定める手続きに従って会員登録の申し込みを行うものとします。<br>
                    ２．登録手続きは、前項の申し込みに対して、当社が承諾したときに完了するものとします。<br>
                    ただし、次のいずれかに該当する場合には当社は会員登録申し込みを承諾しないか、あるいは承諾後であっても承諾の取り消しを行うことがあります。<br>
                    （1）申請者が虚偽の事実を申告したとき<br>
                    （2）第7条に違反したとき<br>
                    （3）その他、当社が会員として不適切と判断したとき<br>
                </p>
                <p class="block--content sub">
                    【Macintosh】<br>
                    ブラウザは以下が動作対象となります。<br>
                    ・Safari 最新バージョン<br>
                    ・Google Chrome 最新バージョン
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    第5条：個人情報の取扱い
                </p>
                <p class="block--content">
                    登録された個人情報は、当社「プライバシーポリシー」に基づいて取扱います。
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    第6条：ユーザーIDおよびパスワード
                </p>
                <p class="block--content">
                    ユーザーID（メールアドレス）とパスワードの管理ならびにその使用に関しての責任は全て会員が負うものとし、使用上の過誤または第三者の不正な使用等については当社は一切の責任を負わないものとします。
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    第7条：禁止事項
                </p>
                <p class="block--content">
                    会員は、本サービスを利用するにあたり、次の行為を行わないものとします。<br>
                    （1）自らのユーザーIDとパスワードを公開する行為<br>
                    （2）本サービスで提供される情報、著作物等を自らの個人利用目的以外に利用したり、ネットワークの内外を問わず公衆に再提供する行為<br>
                    （3）ほかの会員もしくは第三者に不利益を与える行為（著作権、知的所有権の侵害、誹謗・中傷など）<br>
                    （4）他人の財産やプライバシーを侵害する行為<br>
                    （5）有害なコンピュータプログラム等を送信、または書き込む行為<br>
                    （6）公序良俗に反する行為<br>
                    （7）本サービスを営利目的で利用する行為<br>
                    （8）本サービスの運営を妨げる行為<br>
                    （9）その他法令に違反し、または違反する恐れのある行為
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    第8条：SC会員特典
                </p>
                <p class="block--content">
                    １．SC会員ランクの決定及び対象条件と特典<br>
                    SC会員ランクは、1月から同年12月の1年間のシンプリスオンラインストア内での累計購入金額によって決まります。なお、原則として、SC会員ランク毎に適応される割引は、当社が行うセール、およびその他クーポンとの併用はできかねます。ただし、当社が併用を認める場合はこの限りではありません。<br>
                    （1）レギュラー会員<br>
                    対象：本サイトで会員登録をされた方 / 特典：送料無料<br>
                    （2）SILVER会員<br>
                    対象：本サイトで年間3万円以上ご購入された方 / 特典：送料無料＋3%off<br>
                    （3）GOLD会員<br>
                    対象：本サイトで年間8万円以上ご購入された方 / 特典：送料無料＋5％off<br>
                    （4）PLATINUM会員<br>
                    対象：本サイトで年間15万円以上ご購入された方 / 特典：送料無料＋7％off<br>
                    （5）DIAMOND会員<br>
                    対象：本サイトで年間25万円以上ご購入された方 / 特典：送料無料＋10％off<br>
                </p>
                <p class="block--content sub">
                    ２．ランクアップ判定時期<br>
                    ランクアップの判定は毎月行うものとします。毎年１月１日よりカウントし、それぞれのランクアップ条件を満たした月の翌月１日より新しいランクが適応されます。一度獲得したSC会員ランクはランクダウンすることはありません。なお、お支払方法が代引交換のご注文の場合、ご購入履歴が反映されるまでにお時間がかかり、毎月月末のランク判定にカウントされない場合がございます。
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    第9条：変更の届け出
                </p>
                <p class="block--content">
                    会員は、会員登録内容に変更が生じた場合はすみやかに本サイト上で定める手続きに従って通知するものとします。
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    第10条：退会
                </p>
                <p class="block--content">
                    会員は、都合により脱会する場合は、本サイトで定める手続きに従っていつでも退会できます。
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    第11条：損害賠償
                </p>
                <p class="block--content">
                    会員が本規約およびその他諸規定等に違反する行為または、不正もしくは違法な行為によって当社に損害を与えた場合には、当該会員に対して当社の被った損害の賠償を請求することができるものとします。
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    第12条：免責事項
                </p>
                <p class="block--content">
                    (1）当社に責任のない事由により発生したトラブルや損失、損害について、当社は一切責任を負いません。<br>
                    (2）本サービスの利用による、会員同士、会員と本サービスにおける情報等提供者もしくは会員と第三者との間で生じた紛議には当社は一切責任を負いません。<br>
                    (3）当社は本サービスの廃止については免責されるものとします。<br>
                    (4）当社は、会員により本サービスを通じて登録、提供された情報が当社の責にきせざる事由により紛失した場合には免責されるものとします。
                </p>
            </div>

        </div>

    </section>

    <div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					会員規約
				</a>
			</li>
		</ul>
	</div>
    <!--△ パンくず △-->

</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>
