﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP Journal" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="journalArea">
    <h2>
        SIMPLISSE <br class="sp_contents">JOURNAL<span>シンプリスのはじめかた</span>
    </h2>
    <div class="journalArea_box">
        <div class="journalArea_box--list">
            <a href="/content/journal/%e8%a1%80%e7%b3%96%e5%80%a4%e3%82%92%e4%b8%8b%e3%81%92%e3%81%a6%e3%80%81%e4%bb%a3%e8%ac%9d%e3%82%92%e4%b8%8a%e3%81%92%e3%82%8b%ef%bc%81-%e6%86%8e%e3%81%84%e8%84%82%e8%82%aa%e3%82%92%e5%8a%b9%e7%8e%87/">
                <img src="/content/wp-content/uploads/2021/01/journal_vol004_01.jpg" alt="">
                <p class="date">2021.1.26</p>
                <p class="cts">
                    血糖値を下げて、代謝を上げる！ 憎い脂肪を効率よく燃やす冬のダイエット必勝法
                </p>
            </a>
        </div>
        <div class="journalArea_box--list">
            <a href="/content/journal/%e3%83%9b%e3%83%ab%e3%83%a2%e3%83%b3%e3%83%90%e3%83%a9%e3%83%b3%e3%82%b9%e3%82%92%e6%95%b4%e3%81%88%e3%81%a6-%e4%b9%be%e7%87%a5%e3%81%8b%e3%82%89%e8%82%8c%e3%82%92%e5%ae%88%e3%82%8b%ef%bc%81-%e5%af%92/">
                <img src="/content/wp-content/uploads/2021/01/journal_vol003_01.jpg" alt="">
                <p class="date">2021.1.26</p>
                <p class="cts">
                    ホルモンバランスを整えて 乾燥から肌を守る！ 寒い冬こその「オイル美容」 
                </p>
            </a>
        </div>
        <div class="journalArea_box--list">
            <a href="/content/journal/%e7%8f%be%e4%bb%a3%e5%a5%b3%e6%80%a7%e3%81%ae%e9%96%93%e3%81%a7%e5%af%86%e3%81%8b%e3%81%ab%e5%a2%97%e5%8a%a0%ef%bc%81-%e5%8e%9f%e5%9b%a0%e3%81%8c%e8%a6%8b%e5%bd%93%e3%81%9f%e3%82%89%e3%81%aa%e3%81%84/">
                <img src="/content/wp-content/uploads/2021/01/journal_vol002_01.jpg" alt="">
                <p class="date">2021.1.26</p>
                <p class="cts">
                    現代女性の間で密かに増加！ 原因が見当たらないその不調、 「新型栄養失調」が原因かも？ 
                </p>
            </a>
        </div>
        <div class="journalArea_box--list">
            <a href="/content/journal/%e4%be%bf%e3%81%8c%e6%ba%9c%e3%81%be%e3%82%8a%e3%82%84%e3%81%99%e3%81%84%e8%a6%81%e5%9b%a0%e3%81%af-%e5%a5%b3%e6%80%a7%e3%83%9b%e3%83%ab%e3%83%a2%e3%83%b3%e3%81%ab%e3%81%82%e3%82%8b%ef%bc%9f-%e8%96%ac/">
                <img src="/content/wp-content/uploads/2021/01/journal_vol001_01.jpg" alt="">
                <p class="date">2021.1.23</p>
                <p class="cts">
                    便が溜まりやすい要因は 女性ホルモンにある？ 薬に頼らず便秘を解消する方法 
                </p>
            </a>
        </div>
    </div>
    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "content/journals/?category=ALL") %>" class="journalArea_btn">VIEW MORE</a>
</section>

<section class="bnrArea_02">
    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/welcome/") %>">
        <picture>
            <source media="(max-width: 769px)" srcset="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/bnr_02_sp.jpg">
            <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/bnr_02.jpg" alt="">
        </picture>
    </a>
</section>
<%-- △編集可能領域△ --%>
