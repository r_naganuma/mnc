﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP Insta" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="instaArea">
    <h2>
        OFFICIAL <br class="sp_contents">INSTAGRAM
    </h2>
    <div class="instaArea_box">
        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/instagram.svg" />
        <a href="https://www.instagram.com/simplisse_official/?hl=ja" target="_blank">@simplisse_official</a>
    </div>
    <div id="instafeed"></div>
</section>
<%-- △編集可能領域△ --%>
