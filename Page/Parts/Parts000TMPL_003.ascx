﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP Link" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="linkArea">
    <div class="linkArea_list">
        <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=bty&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
            <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/pic_beauty.png" />
        </a>
    </div>
    <div class="linkArea_list">
        <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=wel&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
            <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/pic_wellness.png" />
        </a>
    </div>
    <div class="linkArea_list">
        <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=sen&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
            <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/pic_sensual.png" />
        </a>
    </div>
</section>
<%-- △編集可能領域△ --%>
