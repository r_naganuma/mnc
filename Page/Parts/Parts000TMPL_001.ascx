﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP MV" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="mvArea">
    <div class="mvArea_slide">
    <div class="mvArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000888&cat=wel") %>" class="mvPic" onclick="ga('send','event','banner','click','top_fv_banner_lp_uvd', 1);">
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_04.jpg" />
                </div>
            </a>
            <div class="mvTxt">
                <p class="mvTxt_ttl">
                    360度守り抜く<br>
                    飲む紫外線対策ケア
                </p>
                <p class="mvTxt_txt">
                   植物のパワーを2粒にぎゅっと凝縮。<br>太陽も怖くない、全方位ディフェンス。
                </p>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000888&cat=wel") %>" class="mvTxt_btn" onclick="ga('send','event','banner','click','top_fv_banner_lp_uvd', 1);">VIEW DETAILS</a>
            </div>
        </div>
        <div class="mvArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/diet/") %>" class="mvPic" onclick="ga('send','event','banner','click','top_fv_banner_lp_pdplipo', 1);">
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_06.jpg" />
                </div>
            </a>
            <div class="mvTxt">
                <p class="mvTxt_ttl">
                    賢く飲み分けて、<br>
                    キレイを手に入れる<br>
                    ダイエットサプリメント
                </p>
                <p class="mvTxt_txt">
                   食事の気になる「糖質」と「脂質」に。<br>
                   健康的なダイエットをサポート。
                </p>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/diet/") %>" class="mvTxt_btn" onclick="ga('send','event','banner','click','top_fv_banner_lp_pdplipo', 1);">VIEW DETAILS</a>
            </div> 
            </div>
    　　<div class="mvArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/phyto/") %>" class="mvPic" onclick="ga('send','event','banner','click','top_fv_banner_lp_phyto', 1);">
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_03.jpg" />
                </div>
            </a>
            <div class="mvTxt">
                <p class="mvTxt_ttl">
                    すっきりキレイ、<br>
                    発酵の力
                </p>
                <p class="mvTxt_txt">
                   国産の野菜や果物などを丁寧に。<br>自然熟成100%自然の恵みで手軽にクレンズ。
                </p>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/phyto/") %>" class="mvTxt_btn" onclick="ga('send','event','banner','click','top_fv_banner_lp_phyto', 1);">VIEW DETAILS</a>
            </div>
        </div>
    　　<div class="mvArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001141&cat=sen") %>" class="mvPic" onclick="ga('send','event','banner','click','top_fv_banner_lp_nenmaku', 1);">
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_05.jpg" />
                </div>
            </a>
            <div class="mvTxt">
                <p class="mvTxt_ttl">
                    潤って、ニオわない。<br>
                    飲むネンマク ケア
                </p>
                <p class="mvTxt_txt">
                   自分を愛して、心身ともにヘルシーに。<br>セクシャルヘルスを通して豊かな人生を。
                </p>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001141&cat=sen") %>" class="mvTxt_btn" onclick="ga('send','event','banner','click','top_fv_banner_lp_nenmaku', 1);">VIEW DETAILS</a>
            </div>
        </div>        
        <div class="mvArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandConcept/") %>" class="mvPic" onclick="ga('send','event','banner','click','top_fv_banner_lp_behonest', 1);">
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_01.jpg" />
                </div>
                <!-- <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_sub_02.jpg" />
                </div> -->
            </a>
            <div class="mvTxt">
                <p class="mvTxt_ttl">
                    Be HONEST<br>
                    Be BRAVE<br>
                    Be YOU<br>
                    The choice is SIMPLE
                </p>
                <p class="mvTxt_txt">
                    シンプルで確かなケアを提案し、<br>肌・体・心の健やかな美しさを目指します。
                </p>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandConcept/") %>" class="mvTxt_btn" onclick="ga('send','event','banner','click','top_fv_banner_lp_behonest', 1);">VIEW DETAILS</a>
            </div>
        </div>
        <div class="mvArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/welcome/") %>" class="mvPic" onclick="ga('send','event','banner','click','top_fv_banner_lp_wp', 1);">
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_02.jpg" />
                </div>
                <!-- <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_sub_03.jpg" />
                </div> -->
            </a>
            <div class="mvTxt">
                <p class="mvTxt_ttl">
                    はじめての方に。<br>新たなキレイの扉を開く、ウェルカムパッケージ
                </p>
                <p class="mvTxt_txt">
                    肌が変わる喜びの第一歩を。<br>SIMPLSISE 10日間の美容液トライアル。
                </p>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/welcome/") %>" class="mvTxt_btn" onclick="ga('send','event','banner','click','top_fv_banner_lp_wp', 1);">VIEW DETAILS</a>
            </div>
        </div>
    </div>
</section>
<%-- △編集可能領域△ --%>
