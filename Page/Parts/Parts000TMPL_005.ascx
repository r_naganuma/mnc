﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP Lineup" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="lineupArea">
    <div class="lineupArea_list">
        <div class="lineupArea_list--txt">
            <h2>
                BEAUTY
            </h2>
            <p class="lead">
                流されない、<br>
                シンプルで確かなケアを。
            </p>
            <p class="txt">
                美しさに必要なのは、お金や時間ではなく正しい知識。<br>
                情報や常識に惑わされることなく、<br>
                本当に必要なものを、確実に届ける。<br>
                続けることで、肌が持つ本来の美しさを引き出します。
            </p>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=bty&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>" class="lineupBtn">BEAUTY LINEUP</a>
        </div>
        <div class="lineupArea_list--slide">
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000484&cat=bty") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000000484_L.jpg" />
                    <p class="name">
                        シンプリス エイジングケア セット
                    </p>
                </a>
                <p class="cts">
                    ハリ・弾力、毛穴や乾燥へ。リッチな潤いを届ける保湿美容液と、古い角質をそっと取り除く導入美容液。
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000608&cat=bty") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000000608_L.jpg" />
                    <p class="name">
                        シンプリス エンリッチメント オイルセラム
                    </p>
                </a>
                <p class="cts">
                    いつものケアを、スペシャルに変える。組み合わせて“高める”オイル。
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000623&cat=bty") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000000623_L.jpg" />
                    <p class="name">
                        シンプリス ＋ インセンス スキャルプ＆ヘア セット
                    </p>
                </a>
                <p class="cts">
                    美しい髪と頭皮の準備を整えるスキンケア発想のヘアケア シリーズ。
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001033&cat=bty") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000001033_L.jpg" />
                    <p class="name">
                        シンプリス モーニング クレンジング ウォーター
                    </p>
                </a>
                <p class="cts">
                    どんなコンディションの時も、朝はこれ。日中のストレスに立ち向かう“備え”のクレンジング。
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000954&cat=bty") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000000954_L.jpg" />
                    <p class="name">
                        シンプリス ジェントル クレンジング クリーム
                    </p>
                </a>
                <p class="cts">
                    肌に、柔らかな思いやりを。シルクのように、やさしくいたわる“守り”のクレンジング。
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000010&cat=bty") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000000010_L.jpg" />
                    <p class="name">
                        シンプリス UVモイスチャライザー
                    </p>
                </a>
                <p class="cts">
                    365日紫外線と外的環境から守り、みずみずしく明るい肌へ。
                </p>
            </div>
        </div>
    </div>
    
    <div class="lineupArea_list">
        <div class="lineupArea_list--txt">
            <h2>
                WELLNESS
            </h2>
            <p class="lead">
                今の悩みに応える、<br>
                ポジティブな解答を。
            </p>
            <p class="txt">
                現代を生きる女性たちが、<br>
                自分らしく心地よく生きていくために。<br>
                悩みやコンプレックスを手放すきっかけになる<br>
                様々な目的に合わせたソリューションを取り揃えています。
            </p>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=wel&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>" class="lineupBtn">WELLNESS LINEUP</a>
        </div>
        <div class="lineupArea_list--slide">
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000543&cat=wel") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000000543_L.jpg" />
                    <p class="name">
                        シンプリス トータルプログラム27
                    </p>
                </a>
                <p class="cts">
                    27種類のビタミン＆ミネラルで、毎日を健やかに、美しく。
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001141&cat=sen") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000001141_L.jpg" />
                    <p class="name">
                        シンプリス センシュアル ネンマク ケア
                    </p>
                </a>
                <p class="cts">
                    潤って、ニオわない。飲むネンマク ケア。
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001218&cat=wel") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000001218_L.jpg" />
                    <p class="name">
                        シンプリス フィトクレンズ ピュア
                    </p>
                </a>
                <p class="cts">
                    すっきりキレイ、発酵の力。体調管理やファスティング、置き換えダイエット時に。
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000382&cat=wel") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000000382_L.jpg" />
                    <p class="name">
                        シンプリス パーフェクトダイエット プラス+
                    </p>
                </a>
                <p class="cts">
                    美味しいもキレイも手に入れる、健康的なダイエットをサポート。
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000677&cat=wel") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000000677_L.jpg" />
                    <p class="name">
                        シンプリス パーフェクト リポカット
                    </p>
                </a>
                <p class="cts">
                    余分な油を含む食事に、脂質対策サプリメント。
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000605&cat=wel") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000000605_L.jpg" />
                    <p class="name">
                        シンプリス ウィメンズバランス 30＋
                    </p>
                </a>
                <p class="cts">
                    30代からの、心地よくいきる女性たちへ。心とカラダの応援サプリメント。
                </p>
            </div>
        </div>
    </div>
    
    <div class="lineupArea_list">
        <div class="lineupArea_list--txt">
            <h2>
                SENSUAL
            </h2>
            <p class="lead">
                自分の性を大切にできる、<br>
                センシュアルな女性へ。
            </p>
            <p class="txt">
                性という自分の一部を否定することなく、<br>
                ありのままを受け入れて、愛せるように。<br>
                セクシャルヘルスに真摯に向き合い<br>
                安心安全なものや正しい知識で、人生を応援します。
            </p>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=sen&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>" class="lineupBtn">SENSUAL LINEUP</a>
        </div>
        <div class="lineupArea_list--slide">
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001141&cat=sen") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000001141_L.jpg" />
                    <p class="name">
                        シンプリス センシュアル ネンマク ケア
                    </p>
                </a>
                <p class="cts">
                    潤って、ニオわない。飲むネンマク ケア。
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001281&cat=sen") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000001281_L.jpg" />
                    <p class="name">
                        シンプリス センシュアル フェミニン デュオセラム
                    </p>
                </a>
                <p class="cts">
                    カラダもっと心地よく。始めよう、センシュアル美容液
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001293&cat=sen") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000001293_L.jpg" />
                    <p class="name">
                        シンプリス センシュアル フェミニン デオウォッシュ
                    </p>
                </a>
                <p class="cts">
                    弱酸性より、しっかり洗う。フェミニンケアの新提案
                </p>
            </div>
            <div class="slideList">
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001325&cat=sen") %>">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ProductImages/0/40000001325_L.jpg" />
                    <p class="name">
                        シンプリス センシュアル フェミニン ウォッシュ&セラム セット
                    </p>
                </a>
                <p class="cts">
                    フェミニンケア専用。ウォッシュ＆美容液セット。
                </p>
            </div>
        </div>
    </div>
</section>
<%-- △編集可能領域△ --%>
