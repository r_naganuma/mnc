﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP Pickup" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="PickupArea">
    <h2>
        PICK UP
    </h2>
    <div class="PickupArea_box">
        <div class="PickupArea_box--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/anniversary/") %>">
                <div class="pic">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/pick/pick_01.jpg" alt="">
                </div>
                <p class="cts">
                    <span>お試し・まとめ買いにも</span>
                    サプリメントと専用BOXが<br class="pc_contents">セットでスペシャル価格
                </p>
            </a>
        </div>
        <div class="PickupArea_box--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/diet/") %>">
                <div class="pic">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/pick/pick_02.jpg" alt="">
                </div>
                <p class="cts">
                    <span>食事の気になる<br class="sp_contents">「糖質」と「脂質」に</span>
                    健康的なダイエットのための<br class="pc_contents">サポートサプリメント
                </p>
            </a>
        </div>
        <div class="PickupArea_box--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=4582317490676&cat=oth") %>">
                <div class="pic">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/pick/pick_03.jpg" alt="">
                </div>
                <p class="cts">
                    <span>温め成分40%増量して<br class="sp_contents">新登場</span>
                    むくみにフォーカス。<br class="sp_contents">心地よい温感で<br>引き締まったボディの肌へ
                </p>
            </a>
        </div>
        <div class="PickupArea_box--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/welcome/") %>">
                <div class="pic">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/pick/pick_04.jpg" alt="">
                </div>
                <p class="cts">
                    <span>はじめての方限定</span>
                    シンプリスのベストセラー<br>美容液10日間トライアル
                </p>
            </a>
        </div>
    </div>
</section>
<%-- △編集可能領域△ --%>
