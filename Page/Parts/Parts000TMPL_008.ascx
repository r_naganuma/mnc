﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP Info" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="infoMediaArea">
    <section class="infoArea">
        <h2>
            INFORMATION<span>インフォメーション</span>
        </h2>
        <div class="infoArea_box">
            <dl>
                <dt>
                    <p>2021.01.28</p>
                </dt>
                <dd>
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/info/20210128002") %>">
                        【重要】パスワード再設定のお願い
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <p>2021.01.28</p>
                </dt>
                <dd>
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/info/20210128001") %>">
                        オンラインストア リニューアルオープンのお知らせ
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <p>2021.01.28</p>
                </dt>
                <dd>
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/info/20210128003") %>">
                        【更新】新型コロナウィルスの影響による出荷および電話・メール応対につきまして 
                    </a>
                </dd>
            </dl>
        </div>
    </section>
    <section class="mediaArea">
        <h2>
            MEMBERSHIP<span>会員特典のご案内</span>
        </h2>
        <p class="mediaArea_txt">
            シンプリス オンラインストアではお得な<br>シンプリス会員特典をご用意しております。
        </p>
        <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/benefits/") %>" class="mediaArea_box">
            <div class="mediaArea_box--list">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/icn_media_01.png" />
                <p>
                    ご優待<br>
                    割引制度
                </p>
            </div>
            <div class="mediaArea_box--list">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/icn_media_02.png" />
                <p>
                    いつでも<br>
                    送料無料
                </p>
            </div>
            <div class="mediaArea_box--list">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/icn_media_03.png" />
                <p>
                    毎月ランクアップ<br>
                    のチャンス！
                </p>
            </div>
            <div class="mediaArea_box--list">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/icn_media_04.png" />
                <p>
                    会員ランクは<br>
                    下がりません
                </p>
            </div>
        </a>
    </section>
</section>
<%-- △編集可能領域△ --%>
