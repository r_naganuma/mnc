﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP Brand" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<div class="mypageBox_menu">
	<h4></h4>
	<ul class="mypageBox_menu--list">
		<li>
            <h5>
                <a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_MYPAGE) %>">
                    マイページトップ
                </a>
            </h5>
        </li>
    </ul>
    <%if (Constants.FIXEDPURCHASE_OPTION_ENABLED) { %>
	<ul class="mypageBox_menu--list">
		<li>
            <h5>定期便サービス</h5>
            <ul>
                <li>
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_FIXED_PURCHASE_LIST) %>">
                        定期購入情報
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    <%} %>
    <ul class="mypageBox_menu--list">
		<li>
            <h5>お買い物情報</h5>
            <ul>
                <%if (Constants.W2MP_COUPON_OPTION_ENABLED) { %>
                <li>
                    <a href="<%: Constants.PATH_ROOT + Constants.PAGE_FRONT_COUPON_BOX %>">
                        クーポンBOX
                    </a>
                </li>
                <%} %>
                <li>
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_ORDER_HISTORY_LIST) %>">
                        購入履歴一覧（通常・定期）
                    </a>
                </li>
                <li>
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_FAVORITE_LIST) %>">
                        お気に入りリスト
                    </a>
                </li>
                <li>
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_PRODUCT_ARRIVAL_MAIL_LIST) %>">
                        入荷お知らせメール情報
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    <ul class="mypageBox_menu--list">
		<li>
            <h5>会員情報</h5>
            <ul>
                <li>
                    <a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_MODIFY_INPUT) %>">
                        登録情報の変更
                    </a>
                </li>
                <li>
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_SHIPPING_LIST) %>">
                        配送先リスト
                    </a>
                </li>
                <li>
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_WITHDRAWAL_INPUT) %>">
                        会員の退会
                    </a>
                </li>
            </ul>
        </li>
	</ul>
</div>
<%-- △編集可能領域△ --%>
