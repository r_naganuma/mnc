﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP Promotions" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="promoArea">
    <h2>
        PROMOTIONS<span class="ja">いま、注目の特集</span>
    </h2>
    <div class="promoArea_slide">
        <div class="promoArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/nen-makucam2021/") %>">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/promo/promo_210209001.jpg" />
                <p class="date">2021.2.9</p>
                <p class="cts">
                    3/3 17:00まで｜ネンマクケア 2箱ご購入でフェミニンエリア専用ウォッシュ現品PRESENT!
                </p>
            </a>
        </div>
        <div class="promoArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/renewal/") %>">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/promo/promo_210128001.jpg" />
                <p class="date">2021.1.28</p>
                <p class="cts">
                    RENEWAL CAMPAIGN 新ストアでお買い物の皆さまへ！マスクスプレーPRESENT!
                </p>
            </a>
        </div>
        <div class="promoArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001219&cat=wel") %>">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/promo/promo_210204001.jpg" />
                <p class="date">2021.2.4</p>
                <p class="cts">
                    2/12 10:00まで｜MAX ¥2,700 OFF ご好評につき期間延長！フィトのチカラでカラダすっきり。
                </p>
            </a>
        </div>
        <div class="promoArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=othbeapet&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/promo/promo_210205001.jpg" />
                <p class="date">2021.2.5</p>
                <p class="cts">
                    新登場｜「超吸収型サニタリーショーツ」ブランドBé-Aにジュニアサイズが登場！Bé-A Petite／ベア ペティート
                </p>
            </a>
        </div>
        <div class="promoArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=othbea&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/promo/promo_210107002.jpg" />
                <p class="date">2021.1.28</p>
                <p class="cts">
                    新登場｜次世代の「超吸収型サニタリーショーツ」ブランド、Bé-A／ベアがシンプリス オンラインストアに新登場！
                </p>
            </a>
        </div>
        <div class="promoArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=welbrd&dpcnt=&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/promo/promo_201002001.jpg" />
                <p class="date">2020.10.2</p>
                <p class="cts">
                    数量限定｜BORDERS at BALCONYとコラボレーションしたスペシャルキット
                </p>
            </a>
        </div>
        <div class="promoArea_slide--list">
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/anniversary/") %>">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/promo/promo_200731001.jpg" />
                <p class="date">2020.7.31</p>
                <p class="cts">
                    数量限定｜サプリメントと専用BOXがセットでスペシャル価格
                </p>
            </a>
        </div>
    </div>
</section>
<%-- △編集可能領域△ --%>
