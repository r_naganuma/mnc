$(function() {
    $('.column .row a').click(function() {
    var speed = 200;
    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);

    if ($(window).width() > 768) {
    var position = target.offset().top - 50;
    } else {
    var position = target.offset().top - 100;
    }
    $("html, .shopList").animate({
    scrollTop: position
    }, speed, "swing");
    return false;
});
})

$(function() {
    var toggle = $('.toggleBtn');
    var toggleOpenArea = $('.child-items-Answer');

    toggleOpenArea.css('display', 'none');

    toggle.on('click', function(){
        $(this).parent().next().slideToggle(200)
    })
})


$(function(){
    var pageScrollId = $('.column .row a p');
    pageScrollId.click(function(){
        pageScrollId.removeClass('active');
        $(this).addClass('active');
    });
});