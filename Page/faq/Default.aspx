﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="よくある質問 (Q&A) | SIMPLISSE(シンプリス) produced by 山本未奈子" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css">
<script type="text/javascript" charset="Shift_JIS" src="/Js/jquery-1.11.1.min.js"></script>
<script src="js/script.js"></script>
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="faq">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					よくあるご質問
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
    <!----->
    <section class="listBrandsArea faq">
        <h2>よくある質問</h2>
        <div class="faq__containerArea">
            <div class="faq__containerArea--sideMenu">
                <div class="column">
                    <p class="column__title">
                        オンラインストアについて
                    </p>
                    <div class="row">
                        <a href="#jump01">
                            <p class="child-items">
                                ご注文・お支払方法について
                            </p>
                        </a>
                        <a href="#jump02">
                            <p class="child-items">
                                ご配送について
                            </p>
                        </a>
                        <a href="#jump03">
                            <p class="child-items">
                                海外配送について
                            </p>
                        </a>
                        <a href="#jump04">
                            <p class="child-items">
                                返品・交換について
                            </p>
                        </a>
                        <a href="#jump05">
                            <p class="child-items">
                                ギフトラッピングについて
                            </p>
                        </a>
                        <a href="#jump06">
                            <p class="child-items">
                                シンプリス会員特典について
                            </p>
                        </a>
                        <a href="#jump07">
                            <p class="child-items">
                                定期便について
                            </p>
                        </a>
                    </div>
                </div>
                <div class="column">
                    <p class="column__title">
                        BEAUTY商品について
                    </p>
                    <div class="row">
                        <a href="#jump08">
                            <p class="child-items">
                                クレンジング
                            </p>
                        </a>
                        <a href="#jump09">
                            <p class="child-items">
                                美容液
                            </p>
                        </a>
                        <a href="#jump10">
                            <p class="child-items">
                                UV
                            </p>
                        </a>
                        <a href="#jump11">
                            <p class="child-items">
                                スペシャルケア
                            </p>
                        </a>
                        <a href="#jump12">
                            <p class="child-items">
                                ヘアケア
                            </p>
                        </a>
                        <a href="#jump13">
                            <p class="child-items">
                                ボディケア
                            </p>
                        </a>
                        <a href="#jump14">
                            <p class="child-items">
                                メンズケア
                            </p>
                        </a>
                    </div>
                </div>
                <div class="column">
                    <p class="column__title">
                        WELLNESS商品について
                    </p>
                    <div class="row">
                        <a href="#jump15">
                            <p class="child-items">
                                サプリメント
                            </p>
                        </a>
                        <a href="#jump16">
                            <p class="child-items">
                                フード
                            </p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="faq__containerArea--faqContents">
                <div class="column" id="jump01">
                    <p class="title">
                        オンラインストアについて
                    </p>
                    <div class="row">
                        <p class="row__title">  
                            ご注文・お支払方法について
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    注文した商品の追加、変更、もしくはキャンセルは可能ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    発送前であれば商品の追加、変更、キャンセルが可能です。<br>
                                    返品は、未開封・商品到着後７日以内に限り可能です。<br>
                                    ご返品の際は<a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_INQUIRY_INPUT) %>">お問合せフォーム</a>よりご連絡をお願いいたします。その際の送料、並びに返金に伴う振込手数料はお客様のご負担となりますのでご了承ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    どのような支払方法がありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    クレジットカード決済・代金引換・NP後払い（コンビニ, 郵便局, 銀行）・Amazon Payよりお選びいただけます。<br>
                                    （※海外発送については、クレジットカード・Amazon Pay決済のみ承ります）
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    どのクレジットカードが使用できますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    VISA、Master、JCB、AMEX、Dinersに対応しております。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    分割払いはできますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    可能です。 一括払いのほか、リボ払い、分割払いを選択いただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    NP後払いはどのような支払方法ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品の到着を確認してから、「コンビニ」「郵便局」「銀行」「LINE pay」で後払いできる安心・簡単な決済方法です。 請求書はお届けの商品と同梱にてお送りいたします。発行から14日以内にお支払いをお願いします。ご利用限度額は累計残高で55,000円（税込）までとなります。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    領収書は発行してもらえますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    クレジットカード払い・Amazon payの場合は発行可能です。商品購入時に備考欄に領収書発行の旨をご記入ください。<br>
                                    <br>
                                    代引き・後払い（NP）は領収書の発行ができませんことをご了承ください。<br>
                                    支払い方法が「代引き」の場合はお客様が受け取った荷物の送り状控えが領収書となります。<br>
                                    支払い方法が「後払い（NP）」の場合はお支払いいただいた際の、払込受領書または振込み票が領収書となります。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- column end-->
                <div class="column" id="jump02">
                    <div class="row">
                        <p class="row__title">  
                            ご配送について
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    送料はいくらですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    地域ごとに送料を設定しております。下記をご確認ください。<br>
                                    北海道・沖縄：1,100円（税込）<br>
                                    東北・関東・中部・関西・四国・九州：880円（税込）<br>
                                    1回の購入につき合計10,000円（税込）以上お買い上げいただいた場合は送料無料となります。なお、各種割引後の金額が10,000円を下回った場合は、別途送料がかかります。会員登録がお済みのお客様はご購入金額に関わらず送料無料となります。シンプリス会員特典についてはこちらをご覧下さい。<br>国際配送についての料金は、下記「海外発送について」をご参照ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    配達時間帯は指定できますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    可能です。時間帯指定をご希望のお客様は、商品ご注文時に「お届け希望時間帯」欄にてご指定ください。<br>
                                    9:00-12:00 / 12:00-1400 / 14:00-16:00 / 16:00-18:00 / 18:00-20:00 / 19:00-21:00
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    商品は注文してからどれくらいで到着しますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    ご注文を頂いてから通常2営業日以内に速やかに発送させていただきます。<br>
                                    最短発送をご希望の場合は、「配送希望日」指定なしをご選択ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    海外発送はしていますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    お承りしております。 配送方法・配送料金・関税についてなど、海外発送についてよくいただくご質問は下記「海外発送について」にてご確認頂けます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    配送状況は確認できますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品発送時に伝票番号をメールにてご連絡いたします。<br>
                                    伝票番号をもとに、<a href="http://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp" target="_blank" rel="noopenner">佐川急便HP</a>にてご確認頂けます。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- column end-->
                <div class="column" id="jump03">
                    <div class="row">
                        <p class="row__title">  
                            海外発送について
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    どのような支払方法がありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    クレジットカード決済・Amazon Payでのお支払いが可能です。（ VISA / Master / JCB / AMEX / ダイナースがご利用頂けます） ご請求額は各クレジットカード会社の指定為替レートにて、カード発行国の通貨で記載されます 。 デビットカードはご使用いただけません。代金引換えでの対応はできかねますので、ご了承ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    どのような配送方法がありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    配送方法は EMS （国際スピード郵便）のみとなります。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    配送期間はどれくらいかかりますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    ご注文を頂いてから、通常２営業日以内に速やかに発送させていただきます。日時指定、時間指定などは対応できかねますので、ご了承ください。基本的に発送日よりアジアは３日前後、欧米は７日前後になりますが、通関事情、天候などにより遅れる場合もございます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    通関手続き、および関税はかかりますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    ご購入頂きました商品に対し、 お受け取り国での通関時に関税や通関手数料、 その他輸入税等が課せられる場合がございます。その際はお受け取り人様のご負担となりますので、ご了承ください。なお、税関や通関手数料に関する決まりは、 各国によって異なりますので、 詳しくはお受け取り国の税関にお問合せください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    返品/キャンセルはできますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    お客様のご都合による返品は受け付けておりませんので、 ご了承ください。 また、お受け取り人様の長期不在や、税金、通関手数料の未払いにより商品が日本へ返送された場合の送料はお客様のご負担となりますので、ご了承ください。厳重に梱包し発送いたしますが、 万が一お届けした商品に破損があった場合はお問合せフォームよりご連絡をお願いいたします。<br>（場合により税関でのチェック等により、外箱（ ダンボール又は袋） に開封跡などのダメージが残る場合がございますのでご了承ください。）
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    国際配送の料金はいくらになりますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    配送料金は、一律1,800円（税込）となります。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    購入商品数に限りはありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    個人利用の場合、特段の限りを設けておりません。商品合計金額によっては関税などの手数料が課せられる場合がございますので、予めご了承ください。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- column end-->
                <div class="column" id="jump04">
                    <div class="row">
                        <p class="row__title">  
                            返品・交換について
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    開封、使用したものでも返品できますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    開封、使用した商品ついては、不良品以外は返品できかねます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    未開封の商品は返品できますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                   未開封で到着後７日以内の商品につきましては返品、交換を承っております。<a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_INQUIRY_INPUT) %>">お問合せフォーム</a> もしくは お電話（ 0120-370-063）にてご連絡ください。<br>※お客様の送付状態により傷・破損が生じた場合は、配送料、並びに返金に伴う振込手数料はお客様のご負担とさせていただきます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    不良品/誤った商品が届いた場合はどうしたらいいですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    お届けした商品に万一、不良や品違いがございましたら、お手数をおかけいたしますが、<a href="<%= WebSanitizer.HtmlEncode(this.SecurePageProtocolAndHost + Constants.PATH_ROOT + Constants.PAGE_FRONT_INQUIRY_INPUT) %>">お問い合わせフォーム</a> もしくは お電話（ 0120-370-063）にてご連絡願います。その上で、送料着払いにて当社宛に商品をお送りください。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- column end-->
                <div class="column" id="jump05">
                    <div class="row">
                        <p class="row__title">  
                            ギフトラッピングについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    商品のギフトラッピングはできますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    ギフトラッピングを承っております。「カート内容確認」ページに、ラッピングサービスのご案内がございます。ご希望のラッピングをカートに入れてご購入手続きへお進みください。ラッピングキットをお届けする「ご自身でラッピング（300円）」または、商品をラッピングしてお届けする「有料ラッピング （1個につき700円）」をご用意しています。詳細は<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/wrapping/") %>">「ラッピングについて」</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- column end-->
                <div class="column" id="jump06">
                    <div class="row">
                        <p class="row__title">  
                            シンプリス 会員特典について
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    シンプリス 会員特典とは何ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    シンプリス 会員特典は、商品のご購入金額に応じてお得なサービスをお楽しみいただける制度です。会員登録されているお客様であれば自動的にVIP会員ランクが適応となり、特別なお申し込みは不要です。特典内容・ランク条件につきましてはこちらをご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    自分の会員ランクはどこでわかりますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    当オンラインストアにてログインをしていただきますと、マイページにお客様の現在のランクが表示されます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    店舗でシンプリス商品を購入したのですが、シンプリス会員特典は受けれますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                   シンプリス 会員特典は、シンプリス取扱い店舗や他社オンラインショップでのお買い物ではご利用いただけません。<br>
                                   <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/guide/benefits/") %>">シンプリス 会員特典の詳細はこちらをご覧ください。</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- column end-->
                <div class="column" id="jump07">
                    <div class="row">
                        <p class="row__title">  
                            定期便サービスについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    定期便サービスとは何ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    シンプリス 定期便サービスは、ご希望の定期便対象商品を一度のご注文で、定期的にお届けするサービスです。初回は50%OFF、その後は何度でも15%OFFの特別価格にてお安くお届けするほか、ご継続特典など特別なプレゼントもご用意しております。<br>
                                    定期便サービスの詳細は<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/fixedPurchase/") %>">こちら</a>をご覧下さい。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使い切れない時や長期不在時は、定期便を一時中断できますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                     　　マイページ内「定期購入情報詳細」の「次回配送をスキップする」を実行いただくと、次回のお届けをスキップして、その次のお届けから自動的に再開いたします。 また、定期購入設定の「サイクル変更」から定期便のお届けサイクルを変更いただくことも可能です。なお、スキップは「最長2回お届け分」とさせていただきます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    定期便サービスの商品もシンプリス会員割引の対象となりますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                   「定期便サービス」でお申込みいただいた商品は、シンプリス会員ご優待割引対象外となります。何卒ご了承ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    定期便サービスの商品もキャンペーンの対象となりますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    「定期便サービス」でお申込みいただいた商品は、当オンラインストアで行われるキャンペーンの対象外となります。何卒ご了承ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    シンプリス　公式オンラインストアの通常購入商品と、定期便の商品を一緒に届けてもらえますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    当オンラインストアでの通常購入時に、備考欄に「定期便商品同梱希望」の旨をご記入ください。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column" id="jump08">
                    <p class="title">
                        BEAUTY商品について  
                    </p>
                    <div class="row">
                        <p class="row__title">  
                            クレンジングクリームについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    クレンジングクリームの使用方法は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品ページの<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000954&cat=bty") %>">クレンジングクリーム</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用後、洗顔フォームは必要ありませんか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    洗いすぎることによる肌への負担を抑えるため、クレンジングクリームでのシングル洗顔をおすすめしています。洗い残しが気になる場合は、洗浄成分の配合されていない拭き取り化粧水でやさしくお拭き取りください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    ジェントル クレンジング クリーム ・ディープ クレンジング ジェルクリームの違いは何ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    ジェントル クレンジング クリーム は、肌に極めてやさしく、敏感な肌もしっとり柔らかに洗い上げる”守り”のクレンジングです。ディープ クレンジング ジェルクリームは、落ちにくいメイクもするんと落とし、すっきりとした透明感を目指す”攻め”のクレンジングです。その日の肌の乾燥、メイクの濃さなどの状況によって毎日お使い分けください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    まつ毛エクステンションに使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    いずれも一般的なグルーを使用したまつ毛エクステンションにお使いいただけます。擦らないように、やさしくなじませてください。まつ毛エクステンションは、摩擦や生え変わり、ライフスタイルによって自然に取れていきます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用期限はありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    安心して商品をお使いいただける目安として、未開封の場合は３年以内です。開封後は半年を目安にお使いください。高温多湿のところは避けて直射日光の当たらない涼しいところで保管してください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    １本でどのくらいの期間使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    １日あたりさくらんぼ大（約３g）をお使いいただいた場合、約２ヶ月間ご使用いただけます。
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="row__title">  
                            モーニング クレンジングウオーターについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    モーニング クレンジング ウォーターの使用方法は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品ページの<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001033&cat=bty") %>">モーニング クレンジング ウォーター</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    メイク落としとしても使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    洗浄成分が入っていないため、メイクを完全に落とすことはできません。水で落ちるタイプの日焼け止めやパウダーなどを拭き取ることは可能です。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    夜に使っても良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    メイクをしていない日の拭き取りや、洗顔後のベタつきを拭き取る目的など、夜の拭き取りとしてもお使いいただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用期限はありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    安心して商品をお使いいただける目安として、未開封の場合は３年以内です。開封後は半年を目安にお使いください。高温多湿のところは避けて直射日光の当たらない涼しいところで保管してください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    １本でどのくらいの期間使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    1日あたり４プッシュをコットンに含ませてお使いいただいた場合、約2ヶ月間ご使用いただけます。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column" id="jump09">
                    <div class="row">
                        <p class="row__title">  
                            美容液について
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    美容液の使用方法は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品ページの<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000480&cat=btyskncle") %>">ブースター モイストアップ ハーバル セラム・BC ハーバル インテンシブ セラム・AC ハーバル アドバンスト セラム</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    美容液は2本併せて使用したほうが良いですか？（１本だけの使用でも良いですか？）
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    シンプリスの美容液は2本併せてご使用いただくことで、すべてのお手入れが完成するよう開発されました。<br>
                                    ブースター モイストアップ ハーバル セラムには、肌のターンオーバーの促進と、その後に使う化粧品の有効成分を浸透しやすくさせる役割があります。<br>
                                    BC ハーバル インテンシブ セラムとAC ハーバル アドバンスト セラムには、 美肌のために必要な、いわゆるエイジングケア成分が凝縮されています。美容成分を取り込んだ後、最後にうるおいのバリアで閉じ込めます。<br>
                                    そのためブースター モイストアップ ハーバル セラと、BC ハーバル インテンシブ セラム、もしくはAC ハーバル アドバンスト セラムを併せてご使用いただくことでパーフェクトケアが完成します。<br>
                                    1本のみをお使いいただく場合にはそれぞれの効果を考慮してご使用ください。<br>
                                    【ご使用例】<br>
                                    （１）ブースター モイストアップ ハーバル セラム→BC ハーバル インテンシブ セラム<br>
                                    （２）ブースター モイストアップ ハーバル セラム→AC ハーバル アドバンスト セラム
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    化粧水は必要ありませんか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    ブースター モイストアップ ハーバル セラムは、角質層を潤す役割も担っています。そのため、保湿目的の化粧水は必要ありません。拭き取り化粧水はお好みでお使いください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    クリームは必要ありませんか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    BC ハーバル インテンシブ セラム・AC ハーバル アドバンスト セラムには、油膜を張って美容成分を閉じ込め、肌を保護する役割もあります。そのため、美容液のみでスキンケアが完成します。特に乾燥が気になるときや、目元などの気になるパーツを特別にケアをしたい場合は、専用美容液やオイル等を追加してください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用期限はありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    安心して商品をお使いいただける目安として、未開封の場合は３年以内です。開封後は半年を目安にお使いください。高温多湿のところは避けて直射日光の当たらない涼しいところで保管してください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    敏感肌/ニキビ肌でも使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    肌への優しさにこだわって開発された美容液ですので、どなたにもご使用いただけますが、まずは耳のうしろや腕のやわらかいところでお試しいただき、異常のないことを確かめてからのご使用をおすすめします。年齢・体質・生活環境・季節など変化する肌状態に合わせて、使用量を調節しながらお使いください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    １本でどのくらいの期間使用できますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    朝・晩ともに２プッシュをお使いいただいた場合、約２ヶ月間ご使用いただけます。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column" id="jump10">
                    <div class="row">
                        <p class="row__title">  
                            UVモイスチャライザーについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    UVモイスチャライザーの使用方法は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品ページの<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000010&cat=bty") %>">UVモイスチャライザー</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    日焼け止めを塗ると肌が乾燥するのが心配です。シンプリスは乾燥を防げますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    UVモイチャライザーは、肌の上に保護膜を形成します。大気汚染物質やアレルゲン、摩擦などの外的刺激から肌を守りながら、一日中、潤う肌が続きます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    SPF25 PA++では心配です。
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    SPF25 PA++は、紫外線から約8時間、肌を守る数値です。肌へのダメージを抑えるために、日常使いは高SPF値の日焼け止めはおすすめしません。レジャー時など、長時間強い紫外線を浴びることが心配な時は、まずUVモイスチャライザーを下地とし、その上に高SPFの日焼け止めを重ねてください。UVモイスチャライザーの保護膜が、高SPFの日焼け止めによる刺激からも肌を守ります。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    肌が弱くて日焼け止めが苦手です。
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    日焼け止めに含まれる「紫外線吸収剤」は、紫外線から肌を守るものの肌には刺激が強く、乾燥や肌荒れを引き起こす恐れがあります。UVモイスチャライザーは、紫外線吸収剤をシルクタンパクのカプセルに内包し、直接肌に触れさせません。肌の弱いベビー用の日焼け止めにも採用されることもある工夫です。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用期限はありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    安心して商品をお使いいただける目安として、未開封の場合は３年以内です。開封後は半年を目安にお使いください。高温多湿のところは避けて直射日光の当たらない涼しいところで保管してください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    １本でどのくらいの期間使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    1日あたり３プッシュをお使いいただいた場合、約2ヶ月間ご使用いただけます。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="column" id="jump11">
                    <div class="row">
                        <p class="row__title">  
                            コンセントレート ライン セラムについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    コンセントレート ライン セラムの使用方法は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品ページの<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001005&cat=bty") %>">コンセントレート ライン セラム</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用する時の順番は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    シンプリス 美容液とお使いになる場合は、下記の順番でご使用ください。<br>
                                    【ご使用手順】ブースター モイストアップ ハーバル セラム→コンセントレート ライン セラム→BC インテンシブ ハーバル セラム もしくは AC アドバンスト ハーバル セラム<br>他ブランドの化粧品とお使いになる場合は、乳液・クリーム等の油分の含まれた化粧品の前にご使用ください。<br>【ご使用例】化粧水→コンセントレート ライン セラム→乳液・クリーム
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用期限はありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    安心して商品をお使いいただける目安として、未開封の場合は３年以内です。開封後は半年を目安にお使いください。高温多湿のところは避けて直射日光の当たらない涼しいところで保管してください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    どんなパーツに使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    目元・口元・額・眉間など、エイジングサインが気になるパーツにやさしくなじませてお使いください。塗布後、指の腹でやさしく押さえるように温めると、より浸透が高まります。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    １本でどのくらいの期間使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    朝・晩ともに米粒大を5ヶ所にお使いいただいた場合、約1ヶ半ご使用いただけます。
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="row__title">  
                            エンリッチメント オイルセラムについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    エンリッチメント オイルセラムの使用方法は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品ページの<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000608&cat=bty") %>">エンリッチメント オイルセラム</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    メイクの前にも使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    浸透力の高い油分のため、表面はさらりとした仕上がりです。ファンデーションなどメイクもよれにくく、つややかな肌へと仕上がります。心配な場合は、塗布後、時間を置いてからメイクをしてください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    オイル焼けしませんか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    オイル焼けとは、オイル内の不純物が紫外線によって酸化し、色素沈着を起こすことを言います。エンリッチメントオイルセラムは、酸化しにくい油分を選定しており、精製度が高いため、オイル焼けの心配なくお使いいただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用期限はありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    安心して商品をお使いいただける目安として、未開封の場合は３年以内です。開封後は半年を目安にお使いください。高温多湿のところは避けて直射日光の当たらない涼しいところで保管してください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    １本でどのくらいの期間使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    朝・晩ともに５滴ずつの場合は約３ヶ月間、朝晩8滴ずつの場合は約2ヶ月間、ご使用いただけます。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="column" id="jump12">
                    <div class="row">
                        <p class="row__title">  
                            スキャルプ＆ヘア シャンプーについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    スキャルプ＆ヘア シャンプーの使用方法は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品ページの<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000513&cat=bty") %>">スキャルプ＆ヘア シャンプー</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    ノンシリコーンだと泡立ち不足・きしみが不安です。
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    絶妙なバランスでハチミツを用いることで豊かな泡立ちを叶え、すすぎ時にもきしまず、つるんとした指通りにこだわりました。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    カラーリングした髪の毛に使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                   キューティクルを閉じた状態を保ちながら洗うため、ハリ・コシのもととなる髪内部のたんぱく質や、カラー・パーマ剤が抜けにくい仕様です。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用期限はありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    安心して商品をお使いいただける目安として、未開封の場合は３年以内です。開封後は半年を目安にお使いください。高温多湿のところは避けて直射日光の当たらない涼しいところで保管してください。
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="row__title">  
                            スキャルプ＆ヘア トリートメントについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    スキャルプ＆ヘア トリートメントの使用方法は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品ページの<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000000512&cat=bty") %>">スキャルプ＆ヘア トリートメント</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    ノンシリコーンだとまとまらなさそうで不安です。
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    天然のシリコーンといわれる18-MEA誘導体が髪の毛をコーティングするほか、植物由来成分がしなやかでまとまりやすい髪の毛へと整えます。毛先のダメージが気になる場合は、トリートメントを重ね付けするか、タオルドライの後、オイル等で保湿を行ってください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    ボリューム不足で重い仕上がりが苦手です。
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    ふんわりサラサラ、髪が軽くなったような仕上がりです。頭皮が潤い健康になることで、頭頂部は立ち上がり、髪の毛は手触り良くまとまります。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    薄毛・抜け毛に悩んでいます。頭皮マッサージを行っても良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    スキャルプ＆ヘア トリートメントは、髪の毛に馴染ませたのち、頭皮に揉み込んでお使いください。その時、爪を立てずに指の腹で頭皮マッサージをするように揉み込むと、より浸透しやすくなります。耳の後ろからこめかみ、頭頂部を重点的にほぐしましょう。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用期限はありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    安心して商品をお使いいただける目安として、未開封の場合は３年以内です。開封後は半年を目安にお使いください。高温多湿のところは避けて直射日光の当たらない涼しいところで保管してください。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="column" id="jump13">
                    <div class="row">
                        <p class="row__title">  
                            センシュアル フェミニン デオ ウォッシュについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    センシュアル フェミニン デオ ウォッシュの使用方法は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品ページの<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001293&cat=sen") %>">センシュアル フェミニン デオ ウォッシュ</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    生理中でも使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    ご使用いただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    子どもも使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    体の中で最もデリケートなフェミニンエリアにお使いいただけるボディウォッシュですので、肌の弱いお子様にもお役立ていただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    顔や体にも使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    体の中で最もデリケートなフェミニンエリアにお使いいただけるボディウォッシュですので、顔や体にもお役立ていただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用期限はありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    "安心して商品をお使いいただける目安として、未開封の場合は３年以内です。開封後は半年を目安にお使いください。高温多湿のところは避けて直射日光の当たらない涼しいところで保管してください。"
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    １本でどのくらいの期間使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    1日あたり3プッシュをお使いいただいた場合、約2ヶ月間ご使用いただけます。
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="row__title">  
                            センシュアル フェミニン デュオ セラムについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    センシュアル フェミニン デュオ セラムの使用方法は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品ページの<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001281&cat=sen") %>">センシュアル フェミニン デュオ セラム</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    生理中でも使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    ご使用いただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    乳首や脇にも使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    乾燥によるくすみやハリ感のなさが気になるボディのすべての肌にお使いいただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    すぐに下着を穿いて良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    肌表面はすぐにさらっとしますのでべたつきません。待つことなく下着を穿いていただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用期限はありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    安心して商品をお使いいただける目安として、未開封の場合は３年以内です。開封後は半年を目安にお使いください。高温多湿のところは避けて直射日光の当たらない涼しいところで保管してください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    １本でどのくらいの期間使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    1日あたり各2プッシュをお使いいただいた場合、約2ヶ月間ご使用いただけます。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="column" id="jump14">
                    <div class="row">
                        <p class="row__title">  
                            フォーメン 男性用美容液について
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    フォーメン 男性用美容液の使用方法は？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    商品ページの<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001043&cat=bty") %>">フォーメン 男性用美容液</a>をご覧ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    化粧水は必要ありませんか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    フォーメン 男性用美容液には、保湿成分も豊富に含まれています。このほかに化粧水は必要ありません。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    クリームは必要ありませんか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    フォーメン 男性用美容液には、油膜を張って美容成分を閉じ込め、肌を保護する役割もあります。そのため、美容液のみでスキンケアが完成します。このほかにクリームは必要ありません。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    目や口周りのエイジングが気になります。
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    目元などの気になるパーツを特別にケアをしたい時や、特に乾燥が気になる場合は、少量の美容液を重ねることをおすすめいたします。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    使用期限はありますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    "安心して商品をお使いいただける目安として、未開封の場合は３年以内です。開封後は半年を目安にお使いください。高温多湿のところは避けて直射日光の当たらない涼しいところで保管してください。"
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    １本でどのくらいの期間使えますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    朝・晩ともに２プッシュをお使いいただいた場合、約1ヶ月半ご使用いただけます。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="column" id="jump15">
                    <p class="title">
                        WELLNESS商品について
                    </p>
                    <div class="row">
                        <p class="row__title">  
                            トータルプログラム27について
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    妊娠中や授乳中でも飲めますか
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    妊娠中、授乳中の栄養補給にお役立ていただけます。ご心配な方は、かかりつけの医師にご相談ください。 例えば、妊娠中は葉酸400μg・ビタミンC 95mg、授乳期は葉酸280μg・ビタミンC 125mgが一日の目安量とされています。シンプリスには、葉酸400μg・しっかり届くビタミンC 600mgを。その他、胎児や乳児に欠かせないオメガ３（DHA/EPA）や、毎日の美容と健康のために必要な栄養素もバランスよく配合しています。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    いつ飲むのが良いですか。
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    毎日お飲み忘れなく続けていただくために、食後など決まった時間にお飲みになることをおすすめいたします。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    商品は安全ですか。
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    国内のGMP基準（医薬品製造グレード）の工場で高品質の原材料、高い安全性にこだわって製造しています。また、合成着色料・香料・保存料など一切無添加です。<br>
                                    ※GMP基準とは・・・品質の良い優れた製品を製造するために原材料の受け入れ―製造—試験—出荷判定にいたる工程全ての遵守事項を定めたもので、医薬品GMP規定の骨子に沿った内容となる。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    ビタミンCのカプセルがピンク色がかって見えるのは何ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    胃酸で壊れやすいビタミンCを守るため、トウモロコシ由来の腸溶性コーティングを施しています。コーティング液が中身の粉末に触れるとピンク色になる場合がございますが、品質に問題はありませんのでご安心ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    サプリメントを飲んでから尿が黄色くなることがあります。なぜですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    水溶性ビタミンには蛍光性の強い黄色のものがあります。吸収されたビタミン類が体内で満たされると、過剰な分が尿中に排出されるため黄色くなることがあります。 健康上の問題はありませんのでご安心ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    他のサプリメントと併用しても良いですか。
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    基本的には問題ありませんが、同じ成分を過剰に摂取すると体の負担となる場合がありますので、成分をご確認の上お飲みいただくようご注意ください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    病院の薬を飲んでいますが併用は大丈夫ですか。
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    安心してお飲みいただくために、かかりつけの医師、薬剤師にご相談の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="row__title">  
                            パーフェクトダイエット プラス＋について
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    いつ飲むのが良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    サプリメントは食品ですのでお飲みいただく時間に指定はございませんが、忘れない習慣となるように、お食事やデザートなどカロリーやボリュームが気になるお食事の前など、決まったタイミングでお飲みいただくことをおすすめいたします。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    妊娠中や授乳中でも飲めますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    妊娠中の方および授乳中の方は、本品の摂取をお控えください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    病院の薬を飲んでいますが併用は大丈夫ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    通院中の方は、かかりつけの医師、薬剤師にご相談の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    一日何袋まで飲んで良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    一日1袋（4粒）が目安となりますが、食事の毎に飲んでいただいても構いません。ご体調や生活リズムに合わせてお飲みください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                   １袋を数回に分けて飲んでも良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    1袋（4粒）中に含む栄養素はまとめて飲むことを前提に開発されています。一回のお食事につき1袋をお飲みいただくことをおすすめいたします。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    他のサプリメントと併用しても良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    SIMPLISSEのサプリメントとの併用は問題ありません。 また、本品は食品ですので他社のサプリメントとの併用も基本的には問題ありませんが、 同じ成分を過剰に摂取すると体の負担となる場合がありますので、 成分をご確認の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    商品は安全ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    日本国内のGMP基準（医薬品製造グレード）の工場で高品質の原材料、高い安全性にこだわって製造しています。また、合成着色料・香料・保存料など一切無添加です。<br>
                                     ※GMPとは・・・品質の良い優れた製品を製造するために原材料の受入ー製造ー試験ー出荷判定にいたる工程全ての遵守事項を定めたもので、医薬品GMP規定の骨子に沿った内容となる。
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="row__title">  
                            パーフェクト リポカットについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    いつ飲むのが良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    サプリメントは食品ですのでお飲みいただく時間に指定はございませんが、忘れない習慣となるように、余分な油が気になるお食事の前など、決まったタイミングでお飲みいただくことをおすすめいたします。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    妊娠中や授乳中でも飲めますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    妊娠中の方および授乳中の方は、本品の摂取をお控えください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    病院の薬を飲んでいますが併用は大丈夫ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    通院中の方は、かかりつけの医師、薬剤師にご相談の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                   一日何袋まで飲んで良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    一日1袋（5粒）が目安となります。食事の油は適度に取り入れる必要がありますので、食事の毎にお飲みいただくことはおすすめいたしません。特に、オメガ３などの良質な油を含む食事は、そのままお召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    １袋を数回に分けて飲んでも良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    1袋（5粒）中に含む栄養素はまとめて飲むことを前提に開発されています。一回のお食事につき1袋をお飲みいただくことをおすすめいたします。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    他のサプリメントと併用しても良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    SIMPLISSEのサプリメントとの併用は問題ありません。 また、本品は食品ですので他社のサプリメントとの併用も基本的には問題ありませんが、 同じ成分を過剰に摂取すると体の負担となる場合がありますので、 成分をご確認の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    商品は安全ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    日本国内のGMP基準（医薬品製造グレード）の工場で高品質の原材料、高い安全性にこだわって製造しています。また、合成着色料・香料・保存料など一切無添加です。<br> ※GMPとは・・・品質の良い優れた製品を製造するために原材料の受入ー製造ー試験ー出荷判定にいたる工程全ての遵守事項を定めたもので、医薬品GMP規定の骨子に沿った内容となる。
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="row__title">  
                            UVディフェンス プロについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    いつ飲むのが良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    サプリメントは食品ですのでお飲みいただく時間に指定はございませんが、毎日の習慣となるように、毎朝など決まった時間にお飲みいただくことをおすすめいたします。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    妊娠中や授乳中でも飲めますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    妊娠中の方および授乳中の方は、本品の摂取をお控えください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    病院の薬を飲んでいますが併用は大丈夫ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                   通院中の方は、かかりつけの医師、薬剤師にご相談の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                   年齢制限はありますか？子どもでも飲めますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    年齢に制限はございません。塗る日焼け止めが合わない方や、男性を含め皆さまにお飲みいただけます。 カプセルの形状となりますので喉に詰まる恐れがある年齢のお子さまにはおすすめしておりません。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    他のサプリメントと併用しても良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    SIMPLISSEのサプリメントとの併用は問題ありません。 また、本品は食品ですので他社のサプリメントとの併用も基本的には問題ありませんが、 同じ成分を過剰に摂取すると体の負担となる場合がありますので、 成分をご確認の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    商品は安全ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    日本国内のGMP基準（医薬品製造グレード）の工場で高品質の原材料、高い安全性にこだわって製造しています。また、合成着色料・香料・保存料など一切無添加です。<br> ※GMPとは・・・品質の良い優れた製品を製造するために原材料の受入ー製造ー試験ー出荷判定にいたる工程全ての遵守事項を定めたもので、医薬品GMP規定の骨子に沿った内容となる。
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="row__title">  
                            ウィメンズバランス 30＋について
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    いつ飲むのが良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    サプリメントは食品ですのでお飲みいただく時間に指定はございませんが、毎日の習慣となるように、心身ともにリラックスする就寝前など決まった時間にお飲みいただくことをおすすめいたします。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    妊娠中や授乳中でも飲めますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    妊娠中の方および授乳中の方は、本品の摂取をお控えください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    病院の薬を飲んでいますが併用は大丈夫ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    通院中の方は、かかりつけの医師、薬剤師にご相談の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    なぜイソフラボンが入っていないのですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    厚生労働省が定めるイソフラボンの安全な摂取上限量／日は70mg程度とされています。例えば、納豆1パック（50g）に約36mg含まれるため、通常の食事でも十分にイソフラボンを摂ることができます。過剰摂取とならないために、SIMPLISSEはイソフラボンを配合していません。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    他のサプリメントと併用しても良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    SIMPLISSEのサプリメントとの併用は問題ありません。 また、本品は食品ですので他社のサプリメントとの併用も基本的には問題ありませんが、 同じ成分を過剰に摂取すると体の負担となる場合がありますので、 成分をご確認の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    商品は安全ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    日本国内のGMP基準（医薬品製造グレード）の工場で高品質の原材料、高い安全性にこだわって製造しています。また、合成着色料・香料・保存料など一切無添加です。<br> ※GMPとは・・・品質の良い優れた製品を製造するために原材料の受入ー製造ー試験ー出荷判定にいたる工程全ての遵守事項を定めたもので、医薬品GMP規定の骨子に沿った内容となる。
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="row__title">  
                            ネンマク ケアについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    いつ飲むのが良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    サプリメントは食品ですのでお飲みいただく時間に指定はございませんが、毎日の習慣となるように、心身ともにリラックスする就寝前など決まった時間にお飲みいただくことをおすすめいたします。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    妊娠中や授乳中でも飲めますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                   デリケートな時期ですので、 かかりつけの医師、薬剤師にご相談の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    病院の薬を飲んでいますが併用は大丈夫ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    通院中の方は、かかりつけの医師、薬剤師にご相談の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    男性が飲んでも良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    男性にもお飲みいただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    便が赤くなった気がします。なぜですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    アスタキサンチンという赤い色素の栄養素のうち、吸収されなかった分が便に混ざり、赤くなることがございます。痛みなどその他症状がなければ特段問題ないものと存じます。ご心配な方は、かかりつけの医師、薬剤師にご相談の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    他のサプリメントと併用しても良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    SIMPLISSEのサプリメントとの併用は問題ありません。 また、本品は食品ですので他社のサプリメントとの併用も基本的には問題ありませんが、 同じ成分を過剰に摂取すると体の負担となる場合がありますので、 成分をご確認の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    商品は安全ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    日本国内のGMP基準（医薬品製造グレード）の工場で高品質の原材料、高い安全性にこだわって製造しています。また、合成着色料・香料・保存料など一切無添加です。<br> ※GMPとは・・・品質の良い優れた製品を製造するために原材料の受入ー製造ー試験ー出荷判定にいたる工程全ての遵守事項を定めたもので、医薬品GMP規定の骨子に沿った内容となる。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column" id="jump16">
                    <div class="row">
                        <p class="row__title">  
                            フィトクレンズ ピュア について
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    いつ飲むのが良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    サプリメントは食品ですのでお飲みいただく時間に指定はございませんが、忘れない習慣となるように、お食事やデザートなどカロリーやボリュームが気になるお食事の前など、決まったタイミングでお飲みいただくことをおすすめいたします。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    妊娠中や授乳中でも飲めますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    天然の食材のみ使用した健康ドリンクですので安心してお召し上がりいただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    病院の薬を飲んでいますが併用は大丈夫ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    通院中の方は、念のため、かかりつけの医師、薬剤師にご相談の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    何歳から飲めますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    幼児からご高齢の方まで、幅広い年齢の方にお召し上がりいただけます。お子様が初めてお召し上がりになる際は、大人よりもやや薄めに希釈してください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    温めて飲んでも良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                   50度以下のお湯で希釈してお召し上がりいただけます。電子レンジのご利用はお控えください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    ファスティング以外に、毎日飲んでも良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                   植物性発酵飲料は、ファスティングや置き換えダイエットのほか、毎日の健康管理のためにもお役立ていただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    他のサプリメントと併用しても良いですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                   SIMPLISSEのサプリメントとの併用は問題ありません。 また、本品は食品ですので他社のサプリメントとの併用も基本的には問題ありませんが、ご心配な場合は販売元へお問い合わせください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    商品は安全ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                   日本国内の工場にて、急性毒性試験・農薬分析(ポジティブリスト制に基づく一斉残留農薬検査226項目)・重金属・.細菌を用いる復帰突然変異試験、の4項目に関する安全性試験の結果、安全性の高い食品であることを確認しています。合成着色料。香料・保存料などを一切使用せず、天然原料の味わいを大切に製造しています。
                                </p>
                            </div>
                        </div>                        
                    </div>

                    <div class="row">
                        <p class="row__title">  
                            ハーブティーについて
                        </p>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    カフェインは含まれますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    全てのハーブティーにカフェインは含まれておりません。夜のリラックスタイムにもお召し上がりいただけます。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    妊娠中や授乳中でも飲めますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    妊娠中の方および授乳中の方は、かかりつけの医師、薬剤師にご相談の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    病院の薬を飲んでいますが併用は大丈夫ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    通院中の方は、かかりつけの医師、薬剤師にご相談の上お召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    水出しできますか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    全てのハーブティーは水出しができかねます。ティーバッグを入れたマグカップに温かいお湯を注ぎ、2〜3分待ってからお召し上がりください。煮出す必要はありません。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    それぞれのハーブティーの用途を教えてください。
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    クレンズティーは、溜め込みや重さをスッキリさせたい時に。濃く出すとハーブの力も強く感じることができます。ダイエットティーは、食事やスイーツのお供として、余分を避けたい時に。ホワイトニングティーは、強い日差しが気になる時に。赤いクチナシの実から抽出した色素が沈澱することもありますが、この成分もぜひ残さずお召し上がりください。
                                </p>
                            </div>
                        </div>
                        <div class="par">
                            <div class="child-items">
                                <p>
                                    商品は安全ですか？
                                </p>
                                <div class="toggleBtn">
                                    <img src="images/open.png" alt="">
                                </div>
                            </div>
                            <div class="child-items-Answer">
                                <p>
                                    日本国内の、GMP基準（医薬品製造グレード）、食品安全の国際規格FSSC22000の工場で高品質の原材料、高い安全性にこだわって製造しています。また、合成着色料・香料・保存料など一切無添加です。<br> ※GMPとは・・・品質の良い優れた製品を製造するために原材料の受入ー製造ー試験ー出荷判定にいたる工程全ての遵守事項を定めたもので、医薬品GMP規定の骨子に沿った内容となる。<br>※FSSC220000とは・・・世界規模で食品安全レベルを一定以上に確保することを目的とした認証システムで、消費者に安全な食品を提供することを目的としている。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!----->
    <div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					よくあるご質問
				</a>
			</li>
		</ul>
	</div>
    <!--△ パンくず △-->


</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>
