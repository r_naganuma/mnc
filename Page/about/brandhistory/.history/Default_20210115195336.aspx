﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="WELLNESSについて" MetaDescription="WELLNESSについて" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="about">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					BRAND HISTORY
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<section class="listBrandsArea wellness">
		<div class="catBox">
			<div class="catPic">
				<img src="<%= Constants.PATH_ROOT %>Contents/Html/Category/images/002/main_welness.jpg" />
			</div>
			<div class="catTxt">
				<p class="catTxt_brand">WELLNESS</p>
				<p class="catTxt_lead">
					今の悩みに応える、<br class="sp_contents">ポジティブな解答を。
				</p>
				<p class="catTxt_cts">
					現代を生きる女性たちが、<br>
					自分らしく心地よく生きていくために。<br>
					悩みやコンプレックスを手放すきっかけになる<br>
					様々な目的に合わせたソリューションを<br>
					取り揃えています。
				</p>
			</div>
		</div>
	</section>

	<section class="ctsArea wellness">
		<div class="ctsArea_box">
			<div class="ctsArea_box--list">
				<p class="ctsTtl">実感につながる豊富な配合量</p>
				<p class="ctsTxt">
					どんなにいい成分でも、<br>
					適切な量が入っていなければ結果は出ません。<br>
					変化を実感してもらいたいから、<br class="sp_contents">上質な成分を、惜しみなくたっぷりと。<br>
					シンプリスは、質だけでなく、<br class="sp_contents">量にもこだわります。
				</p>
			</div>
			<div class="ctsArea_box--list">
				<p class="ctsTtl">合成着色料、香料、保存料フリー</p>
				<p class="ctsTxt">
					体の中に入れるものに、不要な添加物は入れない。<br>
					例えばサプリメントは、 色や香りに手を加えず、<br>
					腸まで確実に届けてくれるカプセルを採用。<br>
					特殊フィルムによる個包装で劣化を防ぎ、<br class="sp_contents">フレッシュな栄養を閉じ込めました。
				</p>
			</div>
			<div class="ctsArea_box--list">
				<p class="ctsTtl">医療レベルの生産ライン</p>
				<p class="ctsTxt">
					毎日安心して続けていただくために、<br class="sp_contents">厳しい管理基準をクリアした<br>
					信頼できる工場のみでの生産を徹底しています。
				</p>
			</div>
		</div>
		<div class="ctsArea_bnr">
			<a href="">
				<picture>
					<source media="(max-width: 769px)" srcset="./images/bnr_sp.jpg">
						<img src="./images/bnr_pc.jpg" />
				</picture>
			</a>
		</div>
	</section>

	<section class="itemArea">
		<!-- <ul>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001001&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_01.jpg" />
					<p><span>SKIN CARE</span>スキンケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001002&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_02.jpg" />
					<p><span>HAIR CARE</span>ヘアケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001003&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_03.jpg" />
					<p><span>BODY CARE</span>ボディケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001004&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_04.jpg" />
					<p><span>MEN’S CARE</span>メンズケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001006&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_05.jpg" />
					<p><span>OTHER</span>その他</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001005&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_06.jpg" />
					<p><span>TRIAL KIT</span>トライアルキット</p>
				</a>
            </li>
		</ul> -->
		<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=wel&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>" class="itemArea_btn">ALL PRODUCTS</a>
	</section>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					WELLNESSについて
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>

