﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="Brand history" MetaDescription="Brand history" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css?12">
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="../../../Js/jquery.inview.min.js"></script>
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="history">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
            </li>
            <li>
				<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandConcept/") %>">ABOUT</a>
			</li>
			<li>
				<a href="#">
					BRAND HISTORY
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

    <!------>
    <section class="listBrandsArea history">
        <div class="history__secTopArea">
            <p class="history__secTopArea--text">
                SIMPLISSEは、こうして生まれました
            </p>
            <div class="history__secTopArea--title">
                <img src="images/01.png" alt="HOW WE STARTED">
            </div>
            <p class="history__secTopArea--titleSubText">
                Our history begins back in 2009 <br class="sp_contents">New York.<br class="pc_contents"><br class="sp_contents">
                This is the story of SIMPLISSE,<br class="pc_contents"><br class="sp_contents">
                a beauty wellness brand founded <br class="sp_contents">by Minako Yamamoto.
            </p>
            <div class="history__secTopArea--visu">
                <div>
                    <img src="images/02.png" alt="">
                </div>
                <div class="">
                    <img src="images/03.png" alt="">
                </div>
            </div>
        </div>
        <div class="moveTo">
            <div class="history__secflex">
                <div class="ill">
                    <img src="images/04.png" alt="">
                </div>
                <div class="contents">
                    <p>
                        <span>
                            “どうして私の肌は、キレイにならないんだろう？”
                        </span>
                        いろいろな化粧品を試しては、途方にくれる毎日。<br class="pc_contents">
                        シンプリス開発者・山本未奈子は当時、<br class="pc_contents">
                        しつこい肌荒れに悩まされていました。
                    </p>
                </div>
            </div>
            <div class="history__secflex">
                <div class="ill">
                    <img src="images/05.png" alt="">
                </div>
                <div class="contents">
                    <p>
                        <span>
                            世の中にある化粧品に頼れないのなら、<br class="pc_contents">
                            いっそのこと、自分の力で解決しようー
                        </span>
                        そう思い立った山本が向かったのは、<br class="pc_contents">
                        最新の情報と最高の講師が集う、ニューヨークの著名美容学校。<br class="pc_contents">
                        正しい知識を学ぶ面白さに<br class="pc_contents">
                        夢中になっているうちに、主席で卒業することに。<br class="pc_contents">
                        そして気づけば、悩みの肌荒れも解消していました。
                    </p>
                </div>
            </div>
            <div class="history__secflex">
                <div class="ill">
                    <img src="images/06.png" alt="">
                </div>
                <div class="contents">
                    <p>
                        <span>
                            “美肌を阻んでるのは、<br class="pc_contents">
                            私の勘違いや思い込みだったー”
                        </span>
                        その知識と熱意を評価され、山本は教える側に回ることに。<br class="pc_contents">
                        皮膚のメカニズムを見つめた知識は<br class="pc_contents">
                        ますます確かなものとなり、<br class="pc_contents">
                        皮膚科学から細胞学・細菌学・スパの技術まで、<br class="pc_contents">
                        さらに広がっていきました。
                    </p>
                </div>
            </div>
            <div class="history__secflex">
                <div class="ill">
                    <img src="images/07.png" alt="">
                </div>
                <div class="contents">
                    <p>
                        <span>
                            “必要なものをきちんと届ける。<br class="pc_contents">
                            お手入れの基本は、実はシンプル”
                        </span>
                        何年もニューヨークで美容に携わったのち、<br class="pc_contents">
                        帰国した山本が取り組んだのは、<br class="pc_contents">
                        念願の「納得できる美容液」づくりでした。<br class="pc_contents">
                        それは決して、簡単な道のりではありません。<br class="pc_contents">
                        原料選びから成分のチョイス、製造環境まで<br class="pc_contents">
                        「想いを形にする」ためにはたくさんのハードルがありました。
                    </p>
                </div>
            </div>
            <div class="history__secflex">
                <div class="ill ill-mr ill-xs">
                    <img src="images/img-story-06-pc.jpg" alt="">
                </div>
                <div class="contents">
                    <p>
                        <span>
                            何度も試行錯誤を重ねて誕生したのが、<br class="pc_contents">
                            シンプリスのロングセラー商品であり一番人気である<br class="pc_contents">美容液シリーズです。
                        </span>
                        NYの皮膚科学と経験を詰め込んだ自信作は<br class="pc_contents">
                        まだ無名だったにも関わらずたくさんの注文をいただき、<br class="pc_contents">
                        嬉しい悲鳴をあげたほど。<br class="pc_contents">
                        想いを込めて、１つずつ発送しました。
                    </p>
                </div>
            </div>
            <div class="history__secflex">
                <div class="ill ill-xs2">
                    <img src="images/img-story-07-pc.jpg" alt="">
                </div>
                <div class="contents">
                    <p>
                        <span>
                            “内側から、揺るがないキレイを。<br class="pc_contents">
                            だって、肌は食べたものでできているから”
                        </span>
                        美容液がヒットしたら、次はクリーム？<br class="pc_contents">
                        とんでもない。山本が取り組んだのは、<br class="pc_contents">
                        インナーケアのためのプログラムでした。<br class="pc_contents">
                        なぜなら、表面だけでは“本当の美しさ”は<br class="pc_contents">
                        手に入れられないと考えたから。<br class="pc_contents">
                        こだわり抜いて開発したサプリメントは、<br class="pc_contents">
                        キレイになりたい、けれど多忙でストレスの多い<br class="pc_contents">
                        現代女性たちの間で大人気となりました。
                    </p>
                </div>
            </div>
        </div>
        <div class="history__lastBox">
            <p class="history__lastBox--text">
                たくさんの商品を生み出してきたシンプリスですが、<br class="pc_contents">
                現在も、理念はまったく変わりません。<br class="pc_contents">
                本当に必要なものを、シンプルな形で届けること。<br class="pc_contents">
                正しい知識に基づいて、確かなキレイを育むこと。<br class="pc_contents">
                ブランドの顔である美容液と同じように、<br class="pc_contents">
                すべての商品に想いを込めて送り出しています。
            </p>
            <div>
                <img src="images/img-story-26-pc.jpg" alt="">
            </div>
            <div class="history__lastBox--text">
                <p>
                    これからも、私たちの挑戦は続きます。<br class="pc_contents">
                    女性たちの笑顔が溢れるように。<br class="pc_contents">
                </p>
                <p>
                    あなたが、あなたらしくあること。<br class="pc_contents">
                    それは誰のためでもありません。<br class="pc_contents">
                    大切なのは、自分にとって心地よい自分でいられること。<br class="pc_contents">
                    そんな本質的な美しさに、SIMPLISSEは今日も<br class="pc_contents">
                    寄り添っていきたいと願っています。
                </p>
                <span class="illCredit">
                    illustration by Uca
                </span>
            </div>
        </div>
    </section>  

    <section>
        <div class="history__aboutUsinner">
            <p class="history__aboutUsinner--aboutUs">
                WANT TO KNOW MORE
                <span>
                    ABOUT US?
                </span>
            </p>
            <div class="history__aboutUsinner--itemTableConteiner" id="tableParent">
                <dl class="history__aboutUsinner--itemTable">
                    <dt>
                        BRAND CONCEPT
                    </dt>
                    <dd>
                        Be HONEST Be BRAVE Be YOU The choice is SIMPLE
                        <p>
                            自分が正直にあるために。変化を恐れず、勇敢であるために。<br>
                            あなたが、あなたらしくあるために。<br>
                            本当にいいもの、必要なもの、続けられるものを求めたら答えはシンプルでした。
                        </p>
                        <div class="arrow">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandConcept/") %>">
                                <img src="images/right-arrow.png" alt="">
                            </a>
                        </div>
                    </dd>
                </dl>
                <dl class="history__aboutUsinner--itemTable">
                    <dt>
                        <p class="hide">BRAND HISTORY</p>
                    </dt>
                    <dd>
                        <div class="hide">
                            シンプリスは、こうして生まれました
                            <p>
                                たくさんの商品を生み出してきたシンプリスですが、現在も、理念はまったく変わりません。ブランドの顔である美容液と同じように、すべての商品に想いを込めて送り出しています。
                            </p>
                            <div class="arrow">
                                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandhistory/") %>">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                        </div>
                    </dd>
                </dl>
                <dl class="history__aboutUsinner--itemTable">
                    <dt> 
                        MINAKO YAMAMOYO
                    </dt>
                    <dd>
                        肌に悩んだ私から、全世界の女性たちへ
                            <p>
                                もっと肌をきれいにしたい。美しくなりたい。それは全ての女性に共通する願いです。けれど、
                                これほど技術が進化した現代にあって多くの女性が肌トラブルやエイジングに悩んでいます。
                            </p>
                            <div class="arrow">
                                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/yamamotominako/") %>">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                    </dd>
                </dl>
                <dl class="history__aboutUsinner--itemTable">
                    <dt> 
                        CSR REPORT
                    </dt>
                    <dd>
                        すべての女性へ、健やかで心地よい毎日を
                        <p>
                            MNC New Yorkは、すべての女性が健やかに、心地よく生きていける社会を目指すことを目的とし、社会や女性に貢献する活動を展開していきます。
                        </p>
                        <div class="arrow">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/csr-report/") %>">
                                <img src="images/right-arrow.png" alt="">
                            </a>
                        </div>
                    </dd>
                </dl>
            </div>
        </div>
    </section>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
            </li>
            <li>
				<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandConcept/") %>">ABOUT</a>
			</li>
			<li>
				<a href="#">
					BRAND HISTORY
				</a>
			</li>
		</ul>
	</div>
    <!--△ パンくず △-->
    
    <script>
        // history__aboutUsinner--itemTable の最後の段にボーダーを追加
        $(function() {
        var parent = $('#tableParent .history__aboutUsinner--itemTable:last');
        parent.find('dt').css('border-bottom', 'solid 1px #CCCCCC');
        parent.find('dd').css('border-bottom', 'solid 1px #CCCCCC');

            
        var $win = $(window);

        $win.on('load resize', function(){
        var windowSize = window.innerWidth;
        
        // bp768 以下で上記のcss(dtのみ)非表示
        if (windowSize < 768) {
            parent.find('dt').css('border-bottom', 'none');
            }
        });            
    });

    $(function(){
        // .history__secflex(親)　を奇数偶数で取得
        var animateDefalt = $('.history__lastBox');
        var animateObjectOdd = $('.moveTo .history__secflex:nth-child(odd)');
        var animateObjectEven = $('.moveTo .history__secflex:nth-child(even)');

        // .history__secflexのテキスト(子)を奇数偶数で取得 
        var animateDelayTitle = $('.moveTo .history__secflex .contents span') ;
        var animateDelayText = $('.moveTo .history__secflex .contents p') ;

        // アニメーション実行前
        // .history__secflexに付与する
        var defalt_props = {
            "transform" : "translateY(60px)",
            "opacity" : "0"
        }

        var odd_props = {
            "transform" : "translateX(-270px)",
            "opacity" : "0"
        }
        var even_props = {
            "transform" : "translateX(270px)",
            "opacity" : "0"
        }

        // .history__secflex テキスト(子)に付与する
        var delay_props = {
            "transition-property" : "all",
            "transition-duration" : "30s",
            "opacity" : "1",
            "transition" : "4s"
        }
        
        // 初期時にcssを付与
        animateDefalt.css(defalt_props);
        animateObjectOdd.css(odd_props);
        animateObjectEven.css(even_props);
        animateDelayText.css('opacity','0');
    

        // アニメーション実行時 の表示位置初期化css
        var after_props = {
            "transform" : "translateX(0px)",
            "transform" : "translateY(0px)",
            "opacity" : "1",    
            "transition" : "3s"
        }

        // スクロール
        $('.moveTo').css("opacity","0");
        $(window).scroll(function() {
            var scrollMove = $(this).scrollTop();
                console.log(scrollMove);

                var scrollSize = 0;

                if ( scrollSize < 150 ) {
                    $('.moveTo').css({
                        "opacity" : "1",
                        "transition" : "0.5s",
                    });
                }
                
        });

        animateDefalt.on('inview', function(event,isInview){
            if (isInview){
                $(this).css(after_props);
                $(this).find(animateDelayText).css(delay_props);
            } else {
                $(this).css(defalt_props);
                $(this).find(animateDelayText).css("opacity", "0");
            }
        });
        animateObjectOdd.on('inview', function(event,isInview){
            if (isInview){
                $(this).css(after_props);
                $(this).find(animateDelayTitle.add(animateDelayText) ).css(delay_props);
            } else {
                $(this).css(odd_props);
                $(this).find(animateDelayText).css("opacity", "0");
            }
        });
        animateObjectEven.on('inview', function(event,isInview){
            if (isInview){
                $(this).css(after_props);
                $(this).find(animateDelayTitle.add(animateDelayText) ).css(delay_props);
            } else {
                $(this).css(even_props);
                $(this).find(animateDelayText).css("opacity", "0");
            }
        });
    })
</script>
<!------>

</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>

