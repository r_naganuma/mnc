﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="SIMPLISSE(シンプリス)について | SIMPLISSE(シンプリス) produced by 山本未奈子" MetaDescription="自分に正直であるために。変化を怖れず、勇敢であるために。あなたが、あなたらしくあるために。本当にいいもの、必要なもの、続けられるものを求めたら答えはシンプルでした。いろんな選択肢がある現代だからこそ、シンプルで誠実なものを提供し、あなたの求める美しさをサポートします。" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link rel="stylesheet" href="css/style.css?12">
<link href="../css/style.css" rel="stylesheet" type="text/css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>



<!-- about -->
<article id="brandConcept">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
            </li>
			<li>
				<a href="#">
					ABOUT (SIMPLISSEについて)
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

    <!------>
    <section class="listBrandsArea brandConcept">
        <h2 class="brandConcept__concept--title">
            BRAND CONCEPT
        </h2>
        <div class="brandConcept__conceptCointainerArea">
            <div class="brandConcept__conceptCointainerArea--img">
                <img src="images/01.jpg" alt="">
            </div>
            <h3 class="brandConcept__conceptCointainerArea--title">
                <span>
                    Be HONEST
                </span>
                <span>
                    Be BRAVE
                </span>
                <span>
                    Be YOU
                </span>
                <span>
                    The choice is SIMPLE
                </span>
            </h3>
            <div class="brandConcept__conceptCointainerArea--text"> 
                <p>
                    自分に正直であるために。<br>
                    変化を怖れず、勇敢であるために。<br>
                    あなたが、あなたらしくあるために。<br>
                    本当にいいもの、必要なもの、続けられるものを<br>
                    求めたら答えはシンプルでした。
                </p>
                <p>
                    いろんな選択肢がある現代だからこそ、<br>
                    シンプルで誠実なものを提供し、<br>
                    あなたの求める美しさをサポートします。
                </p>
            </div>
        </div>

        <div class="brandConcept__productCointainerArea">
            <div class="brandConcept__productCointainerArea--img">
                <img src="images/02.jpg" alt="">
            </div>
            <div class="brandConcept__productCointainerArea--content">
                <p class="feture">
                    PRODUCT CONCEPT
                </p>
                <p class="title">
                    自然と科学の“いいとこどり”<br>
                    それがホリディカル美容
                </p>
                <p class="text">
                    「自然か、科学か」という二者択一はあえてしません。<br class="pc_contents">
                    古くから親しまれてきた植物のエッセンスには、<br class="pc_contents">
                    人工的には決して作ることのできない<br>
                    素晴らしい成分があります。<br>
                    また最先端の科学は、肌のしくみに基づいた有効成分や、<br class="pc_contents">
                    身体のすみずみに届けるための効果的な方法を教えてくれます。<br>
                    Holistic（自然）の力も、Medical（科学）の力も、<br class="pc_contents">
                    どちらも享受して、最高の結果を出したい。<br class="pc_contents">
                    それが、SIMPLISSEの「ホリディカル美容」です。
                </p>
            </div>
        </div>

        <div class="brandConcept__holidicalCointainerArea">
            <p class="brandConcept__holidicalCointainerArea--text">
                ホリディカル美容に基づき、<br>
                BEAUTYとWELLNESSの両軸から、<br>
                肌、体、心が美しく健やかで<br class="sp_contents">あることを目指します。
            </p>    
            <div class="brandConcept__holidicalCointainerArea--img">
                <img src="images/03.jpg" alt="">
            </div>
        </div>
    </section>

    <section>
        <div class="brandConcept__aboutUsinner">
            <p class="brandConcept__aboutUsinner--aboutUs">
                WANT TO KNOW MORE
                <span>
                    ABOUT US?
                </span>
            </p>
            <div class="brandConcept__aboutUsinner--itemTableConteiner" id="tableParent">
                <dl class="brandConcept__aboutUsinner--itemTable">
                    <dt>
                        <p class="hide">BRAND CONCEPT</p>
                    </dt>
                    <dd>
                        <div class="hide">
                            Be HONEST Be BRAVE Be YOU The choice is SIMPLE
                            <p>
                                自分が正直にあるために。変化を恐れず、勇敢であるために。<br>
                                あなたが、あなたらしくあるために。<br>
                                本当にいいもの、必要なもの、続けられるものを求めたら答えはシンプルでした。
                            </p>
                            <div class="arrow">
                                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandConcept/") %>">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                        </div>
                    </dd>
                </dl>
                <dl class="brandConcept__aboutUsinner--itemTable">
                    <dt>
                        BRAND HISTORY
                    </dt>
                    <dd>
                            シンプリスは、こうして生まれました
                            <p>
                                たくさんの商品を生み出してきたシンプリスですが、現在も、理念はまったく変わりません。ブランドの顔である美容液と同じように、すべての商品に想いを込めて送り出しています。
                            </p>
                            <div class="arrow">
                                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandhistory/") %>">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                    </dd>
                </dl>
                <dl class="brandConcept__aboutUsinner--itemTable">
                    <dt> 
                        MINAKO YAMAMOYO
                    </dt>
                    <dd>
                        肌に悩んだ私から、全世界の女性たちへ
                            <p>
                                もっと肌をきれいにしたい。美しくなりたい。それは全ての女性に共通する願いです。けれど、
                                これほど技術が進化した現代にあって多くの女性が肌トラブルやエイジングに悩んでいます。
                            </p>
                            <div class="arrow">
                                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/yamamotominako/") %>">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                    </dd>
                </dl>
                <dl class="brandConcept__aboutUsinner--itemTable">
                    <dt> 
                        CSR REPORT
                    </dt>
                    <dd>
                        すべての女性へ、健やかで心地よい毎日を
                        <p>
                            MNC New Yorkは、すべての女性が健やかに、心地よく生きていける社会を目指すことを目的とし、社会や女性に貢献する活動を展開していきます。
                        </p>
                        <div class="arrow">
                            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/csr-report/") %>">
                                <img src="images/right-arrow.png" alt="">
                            </a>
                        </div>
                    </dd>
                </dl>
            </div>
        </div>
    </section>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
            </li>
			<li>
				<a href="#">
					ABOUT (SIMPLISSEについて)
				</a>
			</li>
		</ul>
	</div>
    <!--△ パンくず △-->
    
    <script>
        // brandConcept__aboutUsinner--itemTable の最後の段にボーダーを追加
        $(function() {
        var parent = $('#tableParent .brandConcept__aboutUsinner--itemTable:nth-last-child(1)');
        parent.find('dt').css('border-bottom', 'solid 1px #CCCCCC');
        parent.find('dd').css('border-bottom', 'solid 1px #CCCCCC');

            
        var $win = $(window);

        $win.on('load resize', function(){
        var windowSize = window.innerWidth;
        
        // bp768 以下で上記のcss(dtのみ)非表示
        if (windowSize < 768) {
            parent.find('dt').css('border-bottom', 'none');
            }
        });            
    });
</script>
<!------>

</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>

