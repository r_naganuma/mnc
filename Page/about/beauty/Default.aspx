﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="BEAUTY(ビューティー)について | SIMPLISSE(シンプリス) produced by 山本未奈子" MetaDescription="美しさに必要なのは、お金や時間ではなく正しい知識。情報や常識に惑わされることなく、本当に必要なものを、確実に届ける。続けることで、肌が持つ本来の美しさを引き出します。" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="about">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					BEAUTYについて
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<section class="listBrandsArea beauty">
		<div class="catBox">
			<div class="catPic">
				<img src="<%= Constants.PATH_ROOT %>Contents/Html/Category/images/001/main_beauty.jpg" />
			</div>
			<div class="catTxt">
				<p class="catTxt_brand">BEAUTY</p>
				<p class="catTxt_lead">
					流されない、<br class="sp_contents">シンプルで確かなケアを。
				</p>
				<p class="catTxt_cts">
					美しさに必要なのは、<br class="sp_contents">お金や時間ではなく正しい知識。<br>
					情報や常識に惑わされることなく、<br>
					本当に必要なものを、確実に届ける。<br>
					続けることで、肌が持つ本来の美しさを<br class="sp_contents">引き出します。
				</p>
			</div>
		</div>
	</section>

	<section class="ctsArea beauty">
		<div class="ctsArea_box">
			<div class="ctsArea_box--list">
				<p class="ctsTtl">シンプルでハイクオリティ</p>
				<p class="ctsTxt">
					スキンケアは、お金をかければかけるほど、<br>
					時間をかければかけるほど<br class="sp_contents">いいというものではありません。<br>
					必要なものを、過不足なく、確実に届ける。<br>
					<br>
					一切の無駄を省きながらも、<br class="sp_contents">質の高いケアこそが、<br>
					肌が本当に求めるものなのです。
				</p>
			</div>
			<div class="ctsArea_box--list">
				<p class="ctsTtl">コンディションに耳を傾ける</p>
				<p class="ctsTxt">
					肌は、毎日同じ状態ではありません。<br>
					自分の肌をよく観察して、<br>
					その日のコンディションに合った<br class="sp_contents">ケアを選ぶことが大切。<br>
					日々の肌の要求に柔軟に応える<br class="sp_contents">ラインナップをご用意しています。
				</p>
			</div>
			<div class="ctsArea_box--list">
				<p class="ctsTtl">機能美を追求したプロダクト</p>
				<p class="ctsTxt">
					毎日使うものだから、<br class="sp_contents">機能性にもこだわりました。<br>
					例えば美容液は、<br class="sp_contents">エアレスボトルを採用することで、<br>
					余計な成分は入れず、フレッシュなまま。<br>
					キャップレスで、スマートに時短を叶えます。
				</p>
			</div>
		</div>
		<div class="ctsArea_bnr">
			<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/regularly/") %>">
				<picture>
					<source media="(max-width: 769px)" srcset="./images/bnr_sp.jpg">
						<img src="./images/bnr_pc.jpg" />
				</picture>
			</a>
		</div>
	</section>

	<section class="itemArea">
		<ul>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btyskn&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btyskn.png" />
					<p><span>SKIN CARE</span>スキンケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btyhai&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btyhai.png" />
					<p><span>HAIR CARE</span>ヘアケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btybod&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btybod.png" />
					<p><span>BODY CARE</span>ボディケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btymen&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btymen.png" />
					<p><span>MEN’S CARE</span>メンズケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btyoth&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btyoth.png" />
					<p><span>OTHER</span>その他</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=btytry&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/menu/btytry.png" />
					<p><span>TRIAL KIT</span>トライアルキット</p>
				</a>
            </li>
		</ul>
		<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=bty&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>" class="itemArea_btn">ALL PRODUCTS</a>
	</section>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					BEAUTYについて
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>

