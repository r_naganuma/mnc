﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="SENSUALについて" MetaDescription="SENSUALについて" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="about">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					SENSUALについて
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<section class="listBrandsArea sensual">
		<div class="catBox">
			<div class="catPic">
				<img src="<%= Constants.PATH_ROOT %>Contents/Html/Category/images/003/main_sensual.jpg" />
			</div>
			<div class="catTxt">
				<p class="catTxt_brand">SENSUAL</p>
				<p class="catTxt_lead">
					自分の性を大切にできる、<br>
					センシュアルな女性へ。
				</p>
			</div>
		</div>
	</section>

	<section class="ctsArea sensual">
		<div class="ctsArea_box">
			<div class="ctsArea_box--list">
				<p class="ctsTxt">
					自分の性について、誰もがちょっとずつ、<br>
					悩みや気後れ、コンプレックスを<br class="sp_contents">抱えているのが現状です。<br>
					性とは自分を構成する大切な要素。<br>
					自分の一部を否定することなく、<br class="sp_contents">ありのままを受け入れて<br>
					愛することができれば、<br class="sp_contents">よりヘルシーで美しくいられるはず。<br>
					<br>
					SIMPLISSE「センシュアル ライン」は、<br>
					セクシャルヘルスに真摯に向き合い<br>
					安心安全なものや正しい知識を<br class="sp_contents">届けるために生まれました。<br>
					<br>
					自分自身を大切に扱い、<br>
					もっと豊かに、もっと心地よく生きるために。<br>
					SIMPLISSEは女性の人生を応援します。
				</p>
			</div>
		</div>
		<div class="ctsArea_bnr">
			<a href="">
				<picture>
					<source media="(max-width: 769px)" srcset="./images/bnr_sp.jpg">
						<img src="./images/bnr_pc.jpg" />
				</picture>
			</a>
		</div>
	</section>

	<section class="itemArea">
		<!-- <ul>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001001&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_01.jpg" />
					<p><span>SKIN CARE</span>スキンケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001002&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_02.jpg" />
					<p><span>HAIR CARE</span>ヘアケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001003&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_03.jpg" />
					<p><span>BODY CARE</span>ボディケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001004&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_04.jpg" />
					<p><span>MEN’S CARE</span>メンズケア</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001006&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_05.jpg" />
					<p><span>OTHER</span>その他</p>
				</a>
            </li>
            <li>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=001005&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>">
					<img src="./images/item_06.jpg" />
					<p><span>TRIAL KIT</span>トライアルキット</p>
				</a>
            </li>
		</ul> -->
		<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=sen&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>" class="itemArea_btn">ALL PRODUCTS</a>
	</section>

	<section class="talkArea">
		<div class="talkArea_list">
			<h2>開発背景<span>SEXUAL HEALTH TALK</span></h2>
			<div class="talkArea_list--box">
				<div class="talkTxt">
					<h3>
						女性の人生をもっと豊かに、<br>
						もっと心地よいものへ
					</h3>
					<p>
						<span class="ttl">SIMPLISSEがセクシャルヘルスにチャレンジする理由</span>
						「あらゆる女性の可能性を信じ、私たちが情熱を持って生み出す商品・サービス・情報を通じてイノベーションを起こしていく。」これが私たちの会社のミッションです。<br>
						<br>
						SIMPLISSEは、これまで美容と健康について、女性の人生をサポートしたいと前進してきました。しかし、適切なスキンケアにより肌トラブルがなく、インナーケアにより健康的であったとしても、性にまつわる悩みを抱えている場合、自分を認め、自由に自分らしく幸せに生きていくことは時に困難です。<br>
						<br>
						女性が性について話題にすることは、はしたない。性の悩みについて声に出すことは恥ずかしいこと。いまだそんな風潮があるあまり、オープンに話をするどころか、悩みを相談することすらはばかられる現状があります。<br>
						“自分らしい美しさ”をサポートすることに専念してきたビューティーブランドとして、今私たちが行動すべきだと確信しました。<br>
						<br>
						女性が抱えるコンプレックスときちんと向き合い、私たちが正しい知識と安全な商品を提供することで、悩みを解消するお手伝いがしたい。<br>
						SIMPLISSEのセクシャルヘルスへの取り組み「SENSUAL/センシュアル」ラインには、そんな願いが込められています。<br>
						セクシャルヘルスを真摯に考え、女性の人生をもっと豊かに、もっと心地よいものへと後押しするために。SIMPLISSEはセクシャルヘルスにチャレンジします。
					</p>
				</div>
				<div class="talkPic">
					<div class="talkPic_pic">
						<img src="./images/pic_01.jpg" />
					</div>
					<div class="talkPic_txt">
						<h4>
							<span class="sub01">SIMPLISSE 代表</span>
							山本未奈子
							<span class="sub02">
								NY州認定ビューティーセラピスト<br>
								英国ITEC国際ビューティースペシャリスト
							</span>
						</h4>
						<p>
							NYの美容学校 L’atelier Esthetiqueを卒業後、同校で教鞭を執る。拠点を日本に移し、2009年にMNC New York Inc.を設立。「日本で一番女性を幸せにする会社」をビジョンに掲げ、女性の活躍をサポートするため、ビューティーブランド「SIMPLISSE／シンプリス」のほか、フェムテック事業やファッションブランドを展開する。また美容記事の監修や講演など美容の専門家としても多方面で活躍中。著書多数。
						</p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="talkArea_list">
			<h2>医師と語るセクシャルヘルス<span>SEXUAL HEALTH TALK</span></h2>
			<div class="talkArea_list--box">
				<div class="talkPic_pic sp_contents">
					<img src="./images/pic_02.jpg" />
				</div>
				<div class="talkTxt">
					<h3>
						全ての女性たちに<br>
						女性であることの幸せを<br>
						感じてほしい
					</h3>
					<p>
						産婦人科医として医師人生をスタートした私は、自分の性器にコンプレックスを抱いている女性たちの声に耳を傾けてきました。診察室に入ったとたんに、その場で泣き崩れるほど、深く傷つきながら長い間思い悩まれていた女性が、そのコンプレックスを治療することでいきいきと美しく生まれ変わっていかれる姿を数多く見てきました。髪型やメークを変えたわけではない、ダイエットや整形手術をしたわけでもない。でも、女性器コンプレックスを治療したことで、彼女たちは輝きを取り戻したのです。<br>
						<br>
						女性器について、日本では「恥部」「陰部」と呼ぶことからも、話題にすることや興味を持つことさえも、恥ずかしく悪いことのように思えてしまうということもあるでしょう。こうして、セクシャルヘルスについての正確な情報は遮断され、悩みは極めて個人的で秘められたものとなり、解決までの道のりを遠く複雑にしていきます。<br>
						<br>
						しかし、セクシャルヘルスについての理解を深めることができれば、もっと自分の体をいつくしみ、もっと自分を愛することができるはずだと、私は確信します。全ての女性たちに、セクシャルヘルスについて正しい情報を得て、一人で悩み続けることなく女性であることの幸せを感じていただきたい。そう願っています。
					</p>
				</div>
				<div class="talkPic">
					<div class="talkPic_pic pc_contents">
						<img src="./images/pic_02.jpg" />
					</div>
					<div class="talkPic_txt">
						<h4>
							<span class="sub01">なおえビューティークリニック 院長</span>
							喜田 直江　医師
						</h4>
						<p>
							京都府立医科大学卒業。産婦人科、形成外科、美容外科・皮膚科の経験を経て、平成23年、婦人科形成専門のなおえビューティークリニックを開院。
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					SENSUALについて
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>

