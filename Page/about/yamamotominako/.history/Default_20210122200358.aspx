﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="山本未奈子(MINAKO YAMAMOTO)について | SIMPLISSE(シンプリス) produced by 山本未奈子" MetaDescription="「NYの美容学校 L’atelier Esthetiqueを卒業後、同校で教鞭を執る。拠点を日本に移し、2009年にMNC New York Inc.を設立。「日本で一番女性を幸せにする会社」をビジョンに掲げ、女性の活躍をサポートするため、ビューティーブランド「SIMPLISSE／シンプリス」のほか、フェムテック事業やファッションブランドを展開する。また美容記事の監修や講演など美容の専門家としても多方面で活躍中。著書多数。」" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link rel="stylesheet" href="css/style.css?12">
<link href="../css/style.css" rel="stylesheet" type="text/css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>



<!-- about -->
<article id="yamamotominako">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
            </li>
            <li>
				<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/yamamotominako/") %>">about</a>
			</li>
			<li>
				<a href="#">
					山本未奈子について
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

    <!------>
    <section class="listBrandsArea yamamotominako">
        <div class="yamamotominako__bk">
            <div class="yamamotominako__simplisseCeoinner">
                <div class="flex">
                    <div class="yamamotominako__simplisseCeoinner--seoBox">
                        <div class="pic">
                            <img src="images/yamamotominako.jpg" alt="">
                        </div>
                    </div>
                    <div class="yamamotominako__simplisseCeoinner--exBox">
                        <p class="title">
                            肌に悩んだ私から、
                            <span>全世界の女性たちへ</span>
                        </p>
                        <div class="explain">
                            <p>
                                もっと肌をきれいにしたい。美しくなりたい。<br class="pc_contents"><br class="sp_contents">
                                それは全ての女性に共通する願いです。<br class="pc_contents"><br class="sp_contents">
                                けれど、これほど技術が進化した現代にあって<br class="pc_contents"><br class="sp_contents">
                                多くの女性が肌トラブルやエイジングに悩んでいます。<br class="pc_contents"><br class="sp_contents">
                                実は、その原因は自分自身にあることがほとんど。<br class="pc_contents"><br class="sp_contents">
                                よかれと思い込んでいるケアが間違っていて、<br class="pc_contents"><br class="sp_contents">
                                目指すキレイに近づけない人が少なくありません。
                            </p>
                            <p>
                                キレイになりたいという思いを、そのままスキンケアにつなげたい。<br class="pc_contents"><br class="sp_contents">
                                そんな願いから生まれたのが、シンプリスです。<br class="pc_contents"><br class="sp_contents">
                                だから、巷に流布する「美容のウワサ」はすべてリセットしました。<br class="pc_contents"><br class="sp_contents">
                                その代わりにベースとしたのは、NY発の最新皮膚科学理論。<br class="pc_contents"><br class="sp_contents">
                                現代女性の肌に必要なものだけを、ごくシンプルなスキンケアで<br class="pc_contents"><br class="sp_contents">
                                誰でも享受できるようにそんな願いを込めて<br class="pc_contents"><br class="sp_contents">
                                誕生した化粧品を、シンプリスと名づけました。
                            </p>
                            <p>
                                お手入れにかけた時間やお金で、キレイになるわけではありません。<br class="pc_contents"><br class="sp_contents">
                                正しい知識や理論に基づいたスキンケアなら、<br class="pc_contents"><br class="sp_contents">
                                シンプルなステップで確実に、キレイを育てることができます
                            </p>
                            <p>
                                誰でも簡単に、そして確実にキレイを手に入れられる。<br class="pc_contents"><br class="sp_contents">
                                シンプリスと一緒に、そんな“現代の贅沢”を手にいれませんか？
                            </p>
                        </div>
                    </div>
                </div>
                <div class="personalEx">
                    <p class="names">
                        SIMPLISSE 代表
                        <span>
                            山本未奈子
                        </span>
                    </p>
                    <p class="license">
                        <span>
                            NY州認定ビューティーセラピスト
                        </span>
                        <span>
                            英国ITEC国際ビューティースペシャリスト
                        </span>
                    </p>
                </div>
            </div>
            <div class="yamamotominako__Careerinner">
                <div class="yamamotominako__Careerinner--picBox">
                    <div class="pic">
                        <img src="images/01.jpg" alt="">
                    </div>
                    <div class="pic">
                        <img src="images/02.jpg" alt="">
                    </div>
                </div>
                <p class="yamamotominako__Careerinner--carrerEx">
                    「NYの美容学校 L’atelier Esthetiqueを卒業後、同校で教鞭を執る。拠点を日本に移し、2009年にMNC New York Inc.を設立。「日本で一番女性を幸せにする会社」をビジョンに掲げ、女性の活躍をサポートするため、ビューティーブランド「SIMPLISSE／シンプリス」のほか、フェムテック事業やファッションブランドを展開する。また美容記事の監修や講演など美容の専門家としても多方面で活躍中。著書多数。」
                </p>
            </div>
            <div class="yamamotominako__publishingBooksinner">
                <div class="yamamotominako__publishingBooksinner--title">
                    <p>
                        2009.10  著書「今まで誰も教えてくれなかった 極上美肌論」（武田ランダムハウスジャパン）出版。
                    </p>
                    <p>
                        2010.12  著書「輝いている女性は秘かにやっている NY発 全身美肌術」（マガジンハウス）出版。
                    </p>
                    <p>
                        2011.04  著書「美人になる食べ方」（幻冬舎）出版。
                    </p>
                    <p>
                        2011.08  山本未奈子・田村マナ共著「頭皮ケアで始める美髪バイブル」（講談社）出版。
                    </p>
                    <p>
                        2012.08  著書「毛穴レス美肌バイブル」（マイナビ）出版。
                    </p>
                    <p>
                        2013.08  著書「本当に知りたかった 美肌の教科書」（文庫／講談社）出版。
                    </p>
                    <p>
                        2016.06  著書「35歳からの「もう太らない自分」の作り方」（講談社）出版。
                    </p>
                </div>
            </div>
            <div class="yamamotominako__aboutUsinner">
                <p class="yamamotominako__aboutUsinner--aboutUs">
                    WANT TO KNOW MORE
                    <span>
                        ABOUT US?
                    </span>
                </p>
                <div class="yamamotominako__aboutUsinner--itemTableConteiner" id="tableParent">
                    <dl class="yamamotominako__aboutUsinner--itemTable">
                        <dt>
                            BRAND CONCEPT
                        </dt>
                        <dd>
                            自然と科学の“いいとこどり”それがホリディカル美容
                            <p>
                                Holistic（自然）の力も、Medical（科学）の力も、どちらも享受して、最高の結果を出したい。それが、SIMPLISSEの「ホリディカル美容」です。
                            </p>
                            <div class="arrow">
                                <a href="">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                        </dd>
                    </dl>
                    <dl class="yamamotominako__aboutUsinner--itemTable">
                        <dt>
                            BRAND HISTORY
                        </dt>
                        <dd>
                            シンプリスは、こうして生まれました
                            <p>
                                たくさんの商品を生み出してきたシンプリスですが、現在も、理念はまったく変わりません。ブランドの顔である美容液と同じように、すべての商品に想いを込めて送り出しています。
                            </p>
                            <div class="arrow">
                                <a href="">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                        </dd>
                    </dl>
                    <dl class="yamamotominako__aboutUsinner--itemTable">
                        <dt> 
                            CSR REPORT
                        </dt>
                        <dd>
                            すべての女性へ、健やかで心地よい毎日を
                            <p>
                                MNC New Yorkは、すべての女性が健やかに、心地よく生きていける社会を目指すことを目的とし、社会や女性に貢献する活動を展開していきます。
                            </p>
                            <div class="arrow">
                                <a href="">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </section>  

    
    <script>
        // yamamotominako__aboutUsinner--itemTable の最後の段にボーダーを追加
        $(function() {
            var parent = $('#tableParent .yamamotominako__aboutUsinner--itemTable:last');
            parent.find('dt').css('border-bottom', 'solid 1px #CCCCCC');
            parent.find('dd').css('border-bottom', 'solid 1px #CCCCCC');

                
            var $win = $(window);

            $win.on('load resize', function(){
            var windowSize = window.innerWidth;
            
            // bp768 以下で上記のcss(dtのみ)非表示
            if (windowSize < 768) {
                parent.find('dt').css('border-bottom', 'none');
                }
            })            
        })

        
        
    </script>
    <!------>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					山本未奈子について
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>

