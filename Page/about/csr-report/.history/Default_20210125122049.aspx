﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="CSR REPORT シンプリスの取り組み | SIMPLISSE(シンプリス) produced by 山本未奈子" MetaDescription="すべての女性へ、健やかで心地よい毎日を SIMPLISSEは、すべての女性が健やかに、心地よく生きていける社会を目指すことを目的とし、社会や女性に貢献する活動を展開していきます。" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css?12">
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="../../../Js/jquery.inview.min.js"></script>
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="brandConcept">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
            </li>
            <li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
            </li>
			<li>
				<a href="#">
					CSR REPORT シンプリスの取り組み
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

    <!------>
    <!------>
    <section class="listBrandsArea csrReport">
        <h2 class="csrReport__title">
            CSR REPORT <br class="sp_contents">シンプリスの取り組み
        </h2>
        <div class="csrReport__contentBox">
            <p class="csrReport__contentBox--title">
                ティール&ホワイトリボンプロジェクト　支援活動
            </p>
            <p class="csrReport__contentBox--text">
                <span>
                    すべての女性へ、健やかで心地よい毎日を
                </span>
                SIMPLISSEは、すべての女性が健やかに、心地よく生きていける社会を目指すことを目的とし、<br class="pc_contents">社会や女性に貢献する活動を展開していきます。<br class="pc_contents">その一環として、子宮頸がん領域での普及・啓発活動を行うNPO法人キャンサーネットジャパンの啓発プロジェクト「ティール&ホワイトリボンプロジェクト」に賛同し、子宮頸がんの予防・検診・治療に対する正しい知識の普及を支援します。
            </p>
            <div class="csrReport__contentBox--wrapArea">
                <p>
                    2018年、シンプリス商品の売上げの一部を寄付致しました。
                </p>
                <p>
                    <span>・寄付金額 </span>
                    ： 450,000円<br class="pc_contents">
                    引き続き、女性をサポートするサプリメント「シンプリス ウィメンズバランス 30+」の売上げの一部を寄付する活動を継続します。
                </p>
            </div>
        </div>
        <div class="csrReport__contentBox">
            <p class="csrReport__contentBox--title">
                ティール&ホワイトリボンプロジェクト　支援活動
            </p>
            <p class="csrReport__contentBox--text">
                <span>
                    すべての女性へ、健やかで心地よい毎日を
                </span>
                SIMPLISSEは、すべての女性へ、健やかで心地よい毎日をSIMPLISSEは、すべての女性が健やかに、心地よく生きていける社会を目指すことを目的とし、<br class="pc_contents">社会や女性に貢献する活動を展開していきます。<br class="pc_contents">その一環として、子宮頸がん領域での普及・啓発活動を行うNPO法人キャンサーネットジャパンの啓発プロジェクト「ティール&ホワイトリボンプロジェクト」に賛同し、子宮頸がんの予防・検診・治療に対する正しい知識の普及を支援します。
            </p>
            <div class="csrReport__contentBox--wrapArea">
                <p>
                    2017年、シンプリス商品の売上げの一部を寄付致しました。
                </p>
                <p>
                    <span>・寄付金額 </span>
                    ： 50,000円<br class="pc_contents">
                    引き続き、女性をサポートするサプリメント「シンプリス ウィメンズバランス 30+」の売上げの一部を寄付する活動を継続します。
                </p>
            </div>
        </div>
        <div class="csrReport__contentBox">
            <p class="csrReport__contentBox--title">
                熊本地震災害　支援活動
            </p>
            <p class="csrReport__contentBox--text">
                熊本地震により多くの方の尊い命が失われたことに、深い哀悼の意を捧げます。<br class="pc_contents">
                同時に被災された皆様に対しまして心よりお見舞い申し上げます。<br class="pc_contents">
                SIMPLISSEでは社員一同出来る限りの支援を行い、被災地の復興に少しでも貢献していきたいと考えております。
            </p>
            <div class="csrReport__contentBox--wrapArea">
                <p>
                    お客様の善意とお力をお借りして、シンプリス オンラインストアの売上げの一部を熊本地震災害義援金として<br class="pc_contents">
                    熊本市に寄付致しました。
                </p>
                <p>
                    <span>・実施日 </span>
                    ： 2016年5月19日<br class="pc_contents">
                    <span>・寄付金額 </span>
                    ： 1,000,000円<br class="pc_contents">
                </p>
            </div>
        </div>
        <div class="csrReport__contentBox">
            <p class="csrReport__contentBox--title">
                熊本地震災害　支援活動
            </p>
            <p class="csrReport__contentBox--text">
                東日本大震災により多くの方の尊い命が失われたことに、深い哀悼の意を捧げます。同時に被災された皆様に対しまして心よりお見舞い申し上げます。SIMPLISSEでは社員一同出来る限りの支援を行い、被災地の復興に少しでも貢献していきたいと考え支援企画をしております。
                <span>
                    東日本大震災復興支援企画／シンプリス チャリティコフレの販売
                </span>
            </p>
            <div class="csrReport__contentBox--wrapArea">
                <p>
                    お客様の善意とお力をお借りして、シンプリス オンラインストアにてチャリティコフレの販売を行い、売り上げ全額と共に、弊社よりマッ チング寄付という形にて売り上げの同額をを上乗せした金額を日本赤十字社へ寄付いたしました。
                </p>
                <p>
                    <span>・実施日 </span>
                    ： 2011年3月24日〜4月26日
                    <br class="pc_contents">
                    <span>・販売商品 </span>
                    ： シンプリス チャリティコフレ<br class="pc_contents">
                    <span>・商品販売金額（税抜／オンラインVIP会員割引有り） </span>
                    ： 1,008,457円<br class="pc_contents">
                    <span>・SIMPLISSEマッチング金額 </span>
                    ： 1,008,457円<br class="pc_contents">
                    <span>・寄付金額 </span>
                    ： 2,016,914円<br class="pc_contents">
                </p>
            </div>
            <div class="csrReport__contentBox--wrapArea">
                <p>
                    お客様の善意とお力をお借りして、チャリティコフレの再販を行い、売り上げ全額を「あしなが育英会の東日本大地震・津波遺児支援 遺児 のケア「東北レンボーハウス」（仮称）建設」,「Coffret Project」の2団体へ寄付いたしました。
                </p>
                <p>
                    <span>・実施日 </span>
                    ：2011年6月20日〜7月26日
                    <br class="pc_contents">
                    <span>・販売商品 </span>
                    ： シンプリス チャリティコフレ＜第2弾＞<br class="pc_contents">
                    <span>・商品販売金額（税抜／オンラインVIP会員割引有り） </span>
                    ： 394,970円<br class="pc_contents">
                    <span>・ 寄付金額（あしなが育英会東日本大地震・津波遺児支援遺児のケア「東北レンボーハウス」（仮称）建設 </span>
                    ： 197,485円<br class="pc_contents">
                    <span>寄付金額（Coffret Project） </span>
                    ： 197,485円<br class="pc_contents">
                </p>
            </div>
            <p class="csrReport__contentBox--singleTitle">
                寄付
            </p>
            <div class="csrReport__contentBox--wrapArea">
                <p>
                    山本未奈子著「美人になる食べ方」（幻冬舎）の印税の一部は、東北関東大震災の復興支援のため日本赤十字社及び、あしなが育英会に寄付 いたします。 日本赤十字社 , あしなが育英会
                </p>
            </div>
        </div>
        <div class="csrReport__roomToReadArea">
            <p class="csrReport__contentBox--title">
                Room to Read　サポート活動
            </p>    
            <div class="innerSecond">
                <div class="innerSecond__title">
                    <img src="images/logo.png" alt="">
                    <p class="text">
                        <span>
                            Room to Readとは
                        </span>
                        <span>
                            ルーム・トゥ・リードは毎年約100万人の子どもたちの未来に投資しています。
                        </span>
                    </p>
                </div>
                <div class="innerSecond__contents">
                    <p>
                        ルーム・トゥ・リードは、質の高い教育によってすべての子どもたちが自分の可能
                        性を最大限に発揮し、地域社会や世界に貢献できる世界を目指すNPO法人です。
                    </p>
                    <p>
                        教育における識字と男女平等に焦点を当てることで、低所得層のコミュニティに住む何百万人もの子どもたちの生活を変えようとしています。
                    </p>
                    <p>
                        地域社会、パートナー組織、政府との協力のもと、初等教育における子どもたちの識字能力と読書習慣を育成するとともに、少女たちが学業における成功と、卒業後の豊かな人生のための必要なライフスキルを身につけ、中等教育を修了できるように支援します。
                    </p>
                    <p class="innerSecond__contents--quote">
                        —Room to Readサイトより抜粋
                        <span>
                            <a href="https://japan.roomtoread.org/" target="_blank" rel="noopenner">
                                —Room to Readサイトへ
                            </a>
                        </span>
                    </p>
                </div>
            </div> 
            <p class="csrReport__roomToReadArea--carrer">
                <span>
                    実績
                </span>
                ・2010年、Room to Read チャリティガラ　ブロンズスポンサー<br>
                ・Room to Readイベント委員会メンバーとして活動中<br>
                ・山本未奈子著「今まで誰も教えてくれなかった　極上美肌論」（武田ランダムハウス）、「輝いている女性が秘かにやっている　NY発　全身美肌術」 （マガジンハウス）の印税の一部を寄付。<br>
                ・2012年、Room to Readチャリティガラ　プレミアレベルスポンサー<br>
                ・2013年、Room to Readチャリティガラ　プレミアレベルスポンサー
            </p>
            <div class="intro">
                <div class="inner">
                        <p>
                            『社会貢献』という言葉。 日本にいるときはあまり気にも留めてませんでした。 ニューヨークではほとんどの人が何らかの形で社会貢献してい ます。ボランティア活動をしていることは特に偉いことではな く、当たり前のことなのです。私も何かしたい、そう思ったと き出会ったのが、Room to Readの創立者であるジョン・ウッ ド氏の著書「マイクロソフトでは出会えなかった天職」でした。
                            <span class="p-next">
                                本を読んで、涙もろい私は何度も泣きました。そして心動かさ れました。日本でチャリティーやボランティア活動というとま だまだ、敷居の高い感じがします。でもこの本を読んで自分に 合ったやり方で、何かできることをすればいいんだ、というこ とに気付かされました。
                            </span>
                        </p>
                        <div class="pic">
                            <img src="images/01.png" alt="">
                        </div>
                </div>
                <div class="inner">
                    <p>
                        ルームトゥリードは、ジョン・ウッド氏率いる非利益の 団体で、教育を受ける機会のない子供たちのために学校を設立したり、本を提供するなどの活動を行っています。<br>この世の中 の８億人が字の読み書きができない、ってご存知ですか？現在、そのうちの１億人の子供たちが字を学ぶための施設も機会も本もないのです。私にも子供の三人がいます。毎晩、本を読んであげながらこの恵まれた環境にとても感謝するようになりました。そして、世界中の子供たちが、同じように学ぶ機会に恵まれるよう祈っています。
                        <span class="ceoIntro">
                            山本未奈子
                            <span>
                                山本未奈子のブログでもチャリティの様子をお伝えしています。
                            </span>
                        </span>
                    </p>
                    <div class="pic">
                        <img src="images/02.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>  

    <section>
        <div class="csrReport__aboutUsinner">
            <p class="csrReport__aboutUsinner--aboutUs">
                WANT TO KNOW MORE
                <span>
                    ABOUT US?
                </span>
            </p>
            <div class="csrReport__aboutUsinner--itemTableConteiner" id="tableParent">
                <dl class="csrReport__aboutUsinner--itemTable">
                    <dt>
                        BRAND CONCEPT
                    </dt>
                    <dd>
                        
                            Be HONEST Be BRAVE Be YOU The choice is SIMPLE
                            <p>
                                自分が正直にあるために。変化を恐れず、勇敢であるために。<br>
                                あなたが、あなたらしくあるために。<br>
                                本当にいいもの、必要なもの、続けられるものを求めたら答えはシンプルでした。
                            </p>
                            <div class="arrow">
                                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandConcept/") %>">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                    </dd>
                </dl>
                <dl class="csrReport__aboutUsinner--itemTable">
                    <dt>
                        BRAND HISTORY
                    </dt>
                    <dd>
                            シンプリスは、こうして生まれました
                            <p>
                                たくさんの商品を生み出してきたシンプリスですが、現在も、理念はまったく変わりません。ブランドの顔である美容液と同じように、すべての商品に想いを込めて送り出しています。
                            </p>
                            <div class="arrow">
                                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/brandhistory/") %>">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                    </dd>
                </dl>
                <dl class="csrReport__aboutUsinner--itemTable">
                    <dt> 
                        MINAKO YAMAMOYO
                    </dt>
                    <dd>
                        肌に悩んだ私から、全世界の女性たちへ
                            <p>
                                もっと肌をきれいにしたい。美しくなりたい。それは全ての女性に共通する願いです。けれど、
                                これほど技術が進化した現代にあって多くの女性が肌トラブルやエイジングに悩んでいます。
                            </p>
                            <div class="arrow">
                                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/yamamotominako/") %>">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                    </dd>
                </dl>
                <dl class="csrReport__aboutUsinner--itemTable">
                    <dt> 
                        <p class="hide">CSR REPORT</p>
                    </dt>
                    <dd>
                        <div class="hide">
                            すべての女性へ、健やかで心地よい毎日を
                            <p>
                                MNC New Yorkは、すべての女性が健やかに、心地よく生きていける社会を目指すことを目的とし、社会や女性に貢献する活動を展開していきます。
                            </p>
                            <div class="arrow">
                                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/about/csr-report/") %>">
                                    <img src="images/right-arrow.png" alt="">
                                </a>
                            </div>
                        </div>
                    </dd>
                </dl>
            </div>
        </div>
    </section>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
            </li>
            <li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
            </li>
			<li>
				<a href="#">
					CSR REPORT シンプリスの取り組み
				</a>
			</li>
		</ul>
	</div>
    <!--△ パンくず △-->
    
    <script>
        // csrReport__aboutUsinner--itemTable の最後の段にボーダーを追加
        $(function() {
        var parent = $('#tableParent .csrReport__aboutUsinner--itemTable：last');
        parent.find('dt').css('border-bottom', 'solid 1px #CCCCCC');
        parent.find('dd').css('border-bottom', 'solid 1px #CCCCCC');

            
        var $win = $(window);

        $win.on('load resize', function(){
        var windowSize = window.innerWidth;
        
        // bp768 以下で上記のcss(dtのみ)非表示
        if (windowSize < 768) {
            parent.find('dt').css('border-bottom', 'none');
            }
        });            
    });
</script>
<!------>

</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>

