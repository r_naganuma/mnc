﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="【重要】パスワード再設定のお願い | SIMPLISSE(シンプリス) produced by 山本未奈子" MetaDescription="" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="info">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					【重要】パスワード再設定のお願い
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
    <!----->
    <section class="listBrandsArea info">
        <div class="info__title">
            <h2>
                【重要】パスワード再設定のお願い
            </h2>
            <p class="info__title--date">
                2021.01.28
            </p>
            <div class="block">
                <p class="block--content">
                    いつもシンプリスオンラインストアをご利用いただき誠にありがとうございます。<br>
                    2021年1月28日、 オンラインストア リニューアルに伴い、お客様のログインパスワードがリセットされます。<br>
                    誠に恐れ入りますが、リニューアル後、初めてご利用いただく際には、パスワードの再設定をお願いいたします。<br>
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    パスワード再設定の手順
                </p>
                <p class="block--content">
                    下記「パスワード再設定」ページへアクセスし、ご登録時のメールアドレスを入力してください。
                    <a href="https://www.simplisse.jp/Form/User/PasswordReminderInput.aspx">https://www.simplisse.jp/Form/User/PasswordReminderInput.aspx</a>
                </p>
            </div>

            <div class="block">
                <p class="block--content">
                    ▼
                    パスワード再設定用のURLを掲載したメールが届きます。                    
                </p>
                <p class="block--content">
                    ▼
                    URLよりアクセスし、新パスワードを設定してください。設定完了の画面が表示されたら完了です。                                      
                </p>
                <p class="block--content">
                    お手数をおかけいたしますが、ご理解のほど何卒宜しくお願い申し上げます。                                  
                </p>
            </div>
            <div class="block">
                <p class="block--content">
                    SIMPLISSE Online Store
                </p>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Inquiry/InquiryInput.aspx") %>">
                    <p class="block--content formTitle">
                        お問い合わせフォーム
                    </p>
                </a>
                <p class="formTitle__tel">
                    <span>
                        0120-370-063
                    </span>
                    （通話料無料）
                </p>
                <p class="formTitle__detail">
                    <span>
                        10:00-12:00 / 13:30-16:00 土日・祝日は休業
                    </span>
                    <span>
                        （通常は 10:00-12:00 / 13:30-17:00）
                    </span>
                </p>
            </div>

        </div>

    </section>
    <!----->

    <div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					【重要】パスワード再設定のお願い
				</a>
			</li>
		</ul>
	</div>
    <!--△ パンくず △-->

</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>
