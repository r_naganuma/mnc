﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="オンラインストア リニューアルオープンのお知らせ | SIMPLISSE(シンプリス) produced by 山本未奈子" MetaDescription="" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="info">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					オンラインストア リニューアルオープンのお知らせ
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
    <!----->
    <section class="listBrandsArea info">
        <div class="info__title">
            <h2>
                オンラインストア リニューアルオープンのお知らせ
            </h2>
            <p class="info__title--date">
                2021.01.28
            </p>
            <div class="block">
                <p class="block--title">
                    この度、シンプリス オンラインストアは、新たな機能を追加し、より便利でお買い物を<br>
                    お楽しみいただけるストアとなってリニューアルオープンいたしました。
                </p>
            </div>
            <div class="block">
                <p class="block--title">
                    ■「定期便」は、さらにお得に。便利で続けやすい内容へ
                </p>
                <p class="block--content">
                    初回お申し込みは50％OFF！2回目以降も15%OFF！特典多数でお得が続きます。<br>
                    また、お届けサイクルが増え、ご都合に合わせてご自身での変更も可能になりました。<br>
                    お届け日の変更や、定期便のお休みも、マイページ内「定期便購入情報」よりお手続きいただけます。
                </p>
            </div>

            <div class="block">
                <p class="block--title">
                    ■ 新機能「クーポンBOX」で、今使えるクーポンを一度にチェックできるように
                </p>
                <p class="block--content">
                    マイページ内の「クーポンBOX」で、お手持ちのクーポンをチェック。使い忘れなく、<br>
                    お得にお買い物をお楽しみいただけます。また、お買い物の履歴は、<br>マイページ内の「購入履歴一覧」からご確認いただけます。<br>
                    引き続き、お買い物金額に応じた会員特典をご利用いただけます。
                </p>
            </div>

            <div class="block">
                <p class="block--title">
                    ■ 読んでキレイを手に入れる、「JOURNAL」をお届けします
                </p>
                <p class="block--content">
                    より良いケアをするために、より深くキレイを知るために。肌や体、健康について、有益な情報をお届けします。
                </p>
            </div>

            <div class="block">
                <p class="block--title">
                    ■ 厳選した「OTHER BRAND」を揃えました
                </p>
                <p class="block--content">
                    ご好評いただいておりましたCHANCEコーナーは終了いたしました。ご愛顧誠にありがとうございました。<br>
                    今後は、「OTHER BRAND」として、より厳選したアイテムを展開いたします。
                </p>
            </div>

            <div class="block">
                <p class="block--content">
                    上記のほか、各ページの見やすさ、使いやすさを追求し、ページ構成やデザインなども刷新しています。<br>
                    これからも、皆さまのライフスタイルに寄り添い、人生を豊かにするために、<br>高品質な商品・情報をお届けするブランドとして進化してまいります。<br>
                    今後ともSIMPLISSEを何卒宜しくお願い申し上げます。
                </p>
            </div>
            <div class="block">
                <p class="block--content">
                    SIMPLISSE Online Store
                </p>
                <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Inquiry/InquiryInput.aspx") %>">
                    <p class="block--content formTitle">
                        お問い合わせフォーム
                    </p>
                </a>
                <p class="formTitle__tel">
                    <span>
                        0120-370-063
                    </span>
                    （通話料無料）
                </p>
                <p class="formTitle__detail">
                    <span>
                        10:00-12:00 / 13:30-16:00 土日・祝日は休業
                    </span>
                    <span>
                        （通常は 10:00-12:00 / 13:30-17:00）
                    </span>
                </p>
            </div>

        </div>

    </section>
    <!----->

    <div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					オンラインストア リニューアルオープンのお知らせ
				</a>
			</li>
		</ul>
	</div>
    <!--△ パンくず △-->

</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>
