﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="【更新】新型コロナウィルスの影響による出荷および電話・メール応対につきまして | SIMPLISSE(シンプリス) produced by 山本未奈子" MetaDescription="" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="info">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					【更新】新型コロナウィルスの影響による出荷および電話・メール応対につきまして
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

        <!----->
        <section class="listBrandsArea info">
            <div class="info__title">
                <h2>
                    【更新】新型コロナウィルスの影響による出荷および電話・メール応対につきまして
                </h2>
                <p class="info__title--date">
                    2021.01.28
                </p>
                <div class="block">
                    <p class="block--content">
                        新型コロナウイルスにより罹患された皆様および関係の皆様に心よりお見舞い申し上げます。<br>
                        現在、感染拡大に備え、従業員の健康と安全確保を考慮しながら、出荷およびお客様のサポートをさせていただいております。<br>
                        状況を鑑み、お問い合わせ受付時間を変更いたしました。<br>
                        今後も引き続き、以下の通り対応させていただきますので何卒ご理解のほど宜しくお願い申し上げます。
                    </p>
                </div>
                <div class="block">
                    <p class="block--title">
                        ■ 商品のお届けにつきまして
                    </p>
                    <p class="block--content">
                        出荷業務を行う提携倉庫の従業員におきましても、時差出勤の実施など、感染拡大の予防を講じております。<br>
                        極力お約束の日時にお届けするよう努めてまいりますが、<br>
                        お届けに遅延が発生する可能性がございますこと予めご了承くださいますよう何卒お願い申し上げます。
                    </p>
                </div>
    
                <div class="block">
                    <p class="block--title">
                        ■ フリーダイヤル受付時間につきまして
                    </p>
                    <p class="block--content">
                        引き続き、受付時間を短縮して対応させていただきます。<br>
                        また、通常よりもお問い合わせメールへのご回答に時間を要しますこと、ご容赦いただきたくお願い申し上げます。                    
                    </p>
                    <p class="block--content sub">
                        【お電話でのお問い合わせ】<br>
                        0120-370-063（通話料無料）<br>
                        平日10:00-12:00／13:30-16:00 土日・祝日は休業<br>
                        （通常は10:00-12:00／13:30-17:00）
                    </p>
                    <p class="block--content">
                        お客様にはご不便とご迷惑をおかけいたしますが、ご理解賜れますと幸いです。<br>
                        通常運用に戻す際には改めてご案内申し上げます。
                    </p>
                    <p class="block--content">
                        今後とも弊社並びにSIMPLISSEを何卒宜しくお願い申し上げます。
                    </p>
                </div>
    
    
                <div class="block">
                    <p class="block--content">
                        SIMPLISSE Online Store
                    </p>
                    <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Inquiry/InquiryInput.aspx") %>">
                        <p class="block--content formTitle">
                            お問い合わせフォーム
                        </p>
                    </a>
                </div>
    
            </div>
    
        </section>
        <!----->

    <div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					【更新】新型コロナウィルスの影響による出荷および電話・メール応対につきまして
				</a>
			</li>
		</ul>
	</div>
    <!--△ パンくず △-->

</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>
