/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/ts/main.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/ts/anniversaryCampaign.ts":
/*!***************************************!*\
  !*** ./src/ts/anniversaryCampaign.ts ***!
  \***************************************/
/*! exports provided: anniversaryCampaign_common, anniversaryCampaign_pc, anniversaryCampaign_sp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "anniversaryCampaign_common", function() { return anniversaryCampaign_common; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "anniversaryCampaign_pc", function() { return anniversaryCampaign_pc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "anniversaryCampaign_sp", function() { return anniversaryCampaign_sp; });
var anniversaryCampaign_common = function () {
};
var anniversaryCampaign_pc = function () {
    var urlHash = location.hash;
    var campaign01_Offset = $('.campaign--01').offset().top - 80;
    var campaign02_Offset = $('.campaign--02').offset().top - 80;
    if (urlHash == "#campaign01") {
        $('body, html').animate({ scrollTop: campaign01_Offset }, 1000);
    }
    if (urlHash == "#campaign02") {
        $('body, html').animate({ scrollTop: campaign02_Offset }, 1000);
    }
};
var anniversaryCampaign_sp = function () {
    var urlHash = location.hash;
    var campaign01_Offset = $('.campaign--01').offset().top - 65;
    var campaign02_Offset = $('.campaign--02').offset().top - 65;
    if (urlHash == "#campaign01") {
        $('body, html').animate({ scrollTop: campaign01_Offset }, 1000);
    }
    if (urlHash == "#campaign02") {
        $('body, html').animate({ scrollTop: campaign02_Offset }, 1000);
    }
};


/***/ }),

/***/ "./src/ts/common.ts":
/*!**************************!*\
  !*** ./src/ts/common.ts ***!
  \**************************/
/*! exports provided: menuHover_pc, menuOn_sp, common_pc, common_sp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "menuHover_pc", function() { return menuHover_pc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "menuOn_sp", function() { return menuOn_sp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "common_pc", function() { return common_pc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "common_sp", function() { return common_sp; });
var menuHover_pc = function () {
    $('.pcMenuLists > li:eq(0)').hover(function () {
        $(this).find('.headerSubmenuContainerOuter').stop(true, true).fadeIn(500);
        $('.pcMenuBack').stop(true, true).fadeIn();
    }, function () {
        $(this).find('.headerSubmenuContainerOuter').stop(true, true).fadeOut(100);
        $('.pcMenuBack').stop(true, true).fadeOut();
    });
    $('.pcMenuLists > li:eq(1)').hover(function () {
        $(this).find('.headerSubmenuContainerOuter').stop(true, true).fadeIn(500);
        $('.pcMenuBack').stop(true, true).fadeIn();
    }, function () {
        $(this).find('.headerSubmenuContainerOuter').stop(true, true).fadeOut(100);
        $('.pcMenuBack').stop(true, true).fadeOut();
    });
};
var menuOn_sp = function () {
    var animation = bodymovin.loadAnimation({
        container: document.getElementById('spMenuBack'),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        path: 'data.json',
        rendererSettings: {
            className: 'spMenuBackAnime',
            preserveAspectRatio: 'none'
        }
    });
    var menuIconToogle = false;
    $('.menuIcon').click(function () {
        //console.log("メニューを出す");
        $(this).toggleClass('menuIconClicked');
        $('body').toggleClass('bodyFixed');
        $('.spMuneInner').toggleClass('spMuneInnerPointerEventsOff');
        if (!menuIconToogle) {
            animation.setDirection(1);
            animation.goToAndPlay(5, 1);
            $('.spMenu li').each(function (index, element) {
                $(element).stop(true, true).delay((index + 2) * 100).fadeIn();
            });
            $('#spMenuOnBackGround').stop(true, true).fadeIn();
            $('.menuLogo').stop(true, true).delay(500).fadeIn(800);
            $('.menuLogo').stop(true, true).delay(500).fadeIn(800);
            var menuLogoOn = anime({
                targets: '.menuLogo',
                right: '80px',
                duration: 800,
                delay: 500,
                loop: false,
                easing: 'easeOutSine'
            });
            menuIconToogle = !menuIconToogle;
        }
        else {
            //console.log("メニューをしまう");
            animation.setDirection(-1);
            animation.play();
            $('.spMenu li').stop(true, true).fadeOut();
            $('#spMenuOnBackGround').stop(true, true).fadeOut();
            $('.menuLogo').stop(true, true).fadeOut();
            var menuLogoOn = anime({
                targets: '.menuLogo',
                right: '100px',
                duration: 800,
                delay: 500,
                loop: false,
                easing: 'easeOutSine'
            });
            menuIconToogle = !menuIconToogle;
        }
    });
};
var common_pc = function () {
    /*
      let controllerCommon = new ScrollMagic.Controller();
    
      let controllerCommon__01 = new ScrollMagic.Scene({triggerElement: ".pageTopTrigger", triggerHook: 0.5, offset:800})
        .setVelocity(".bottom_btn", {opacity:1}, {duration: 800 ,delay:0 ,ease: Sine.easeInOut})
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controllerCommon);
    */
    var pagetop = $('.bottom_btn');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            pagetop.fadeIn();
        }
        else {
            pagetop.fadeOut();
        }
    });
    $('.page-top a').click(function () {
        $('body, html').animate({ scrollTop: 0 }, 500);
        return false;
    });
};
var common_sp = function () {
    var pagetop = $('.bottom_btn');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            pagetop.fadeIn();
        }
        else {
            pagetop.fadeOut();
        }
    });
    $('.page-top a').click(function () {
        $('body, html').animate({ scrollTop: 0 }, 500);
        return false;
    });
};


/***/ }),

/***/ "./src/ts/main.ts":
/*!************************!*\
  !*** ./src/ts/main.ts ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ "./src/ts/common.ts");
/* harmony import */ var _top__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./top */ "./src/ts/top.ts");
/* harmony import */ var _simplisseStory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./simplisseStory */ "./src/ts/simplisseStory.ts");
/* harmony import */ var _minakosMessage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./minakosMessage */ "./src/ts/minakosMessage.ts");
/* harmony import */ var _anniversaryCampaign__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./anniversaryCampaign */ "./src/ts/anniversaryCampaign.ts");
/* harmony import */ var _newProjectTop__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./newProjectTop */ "./src/ts/newProjectTop.ts");
/* harmony import */ var _talk__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./talk */ "./src/ts/talk.ts");







var loading = false;
window.onload = function () {
    //読み込み終わり
    loading = !loading;
    // SPのメニューの出方アニメーション設定
    _common__WEBPACK_IMPORTED_MODULE_0__["menuOn_sp"]();
    // PCのメニューのhover
    _common__WEBPACK_IMPORTED_MODULE_0__["menuHover_pc"]();
    var mediaQueryList = matchMedia('(min-width:768px)');
    mediaQueryList.addListener(onMediaQueryChange);
    function onMediaQueryChange(mediaQueryList) {
        if (mediaQueryList.matches == true) {
            //console.log("PC")
            _common__WEBPACK_IMPORTED_MODULE_0__["common_pc"]();
            if ($('body').hasClass('top') & loading) {
                //console.log('ここはトップページですの')
                //topページの処理
                _top__WEBPACK_IMPORTED_MODULE_1__["mainVisualAnimeStart_pc"]();
            }
            else if ($('body').hasClass('minakosMessage')) {
                //console.log('ここはminakosMessageページですの')
                _minakosMessage__WEBPACK_IMPORTED_MODULE_3__["minakosMessage_pc"]();
            }
            else if ($('body').hasClass('simplisseStory')) {
                //console.log('ここはsimplisseStoryページですの')
                _simplisseStory__WEBPACK_IMPORTED_MODULE_2__["simplisseStory_pc"]();
            }
            else if ($('body').hasClass('anniversaryCampaign')) {
                //anniversaryCampaignページ
                _anniversaryCampaign__WEBPACK_IMPORTED_MODULE_4__["anniversaryCampaign_pc"]();
            }
            else if ($('body').hasClass('newProjectTop')) {
                //newProjectページ
                _newProjectTop__WEBPACK_IMPORTED_MODULE_5__["newProjectTop_pc"]();
            }
            else if ($('body').hasClass('talkBody')) {
                //talkページ
                _talk__WEBPACK_IMPORTED_MODULE_6__["talk_pc"]();
            }
        }
        else {
            //console.log("SP")
            _common__WEBPACK_IMPORTED_MODULE_0__["common_sp"]();
            if ($('body').hasClass('top') & loading) {
                //topページの処理
                _top__WEBPACK_IMPORTED_MODULE_1__["mainVisualAnimeStart_sp"]();
            }
            else if ($('body').hasClass('minakosMessage')) {
                //minakosMessageページの処理
                _minakosMessage__WEBPACK_IMPORTED_MODULE_3__["minakosMessage_sp"]();
            }
            else if ($('body').hasClass('simplisseStory')) {
                //console.log('ここはsimplisseStoryページですの')
                _simplisseStory__WEBPACK_IMPORTED_MODULE_2__["simplisseStory_sp"]();
            }
            else if ($('body').hasClass('anniversaryCampaign')) {
                _anniversaryCampaign__WEBPACK_IMPORTED_MODULE_4__["anniversaryCampaign_sp"]();
            }
            else if ($('body').hasClass('newProjectTop')) {
                _newProjectTop__WEBPACK_IMPORTED_MODULE_5__["newProjectTop_sp"]();
            }
        }
    }
    onMediaQueryChange(mediaQueryList);
};


/***/ }),

/***/ "./src/ts/minakosMessage.ts":
/*!**********************************!*\
  !*** ./src/ts/minakosMessage.ts ***!
  \**********************************/
/*! exports provided: minakosMessage_common, minakosMessage_pc, minakosMessage_sp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "minakosMessage_common", function() { return minakosMessage_common; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "minakosMessage_pc", function() { return minakosMessage_pc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "minakosMessage_sp", function() { return minakosMessage_sp; });
var minakosMessage_common = function () {
};
var minakosMessage_pc = function () {
    var mainPhoto = anime({
        targets: '.img--01',
        left: '0px',
        opacity: 1,
        duration: 2000,
        easing: 'easeOutSine'
    });
    var colType02 = anime({
        targets: '.colType--02',
        opacity: 1,
        duration: 2000,
        easing: 'easeOutSine'
    });
    var text01 = anime({
        targets: '.text--01',
        opacity: 1,
        duration: 3000,
        easing: 'easeOutSine'
    });
    /*
      $('.text--02').textillate({
         initialDelay: 0,
         in: {
            effect: 'fadeIn',
            delay: 40
         }
      });
    */
};
var minakosMessage_sp = function () {
    var mainPhoto = anime({
        targets: '.img--01',
        top: '0px',
        opacity: 1,
        duration: 2000,
        easing: 'easeOutSine'
    });
};


/***/ }),

/***/ "./src/ts/newProjectTop.ts":
/*!*********************************!*\
  !*** ./src/ts/newProjectTop.ts ***!
  \*********************************/
/*! exports provided: newProjectTop_common, newProjectTop_pc, newProjectTop_sp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newProjectTop_common", function() { return newProjectTop_common; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newProjectTop_pc", function() { return newProjectTop_pc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newProjectTop_sp", function() { return newProjectTop_sp; });
var newProjectTop_common = function () {
};
var newProjectTop_pc = function () {
    var contents01Height = $('.contents--01Wrappper').height();
    $('.contents--01Wrappper').css('height', '512px');
    $('.seeMore').click(function () {
        $('.contents--01Wrappper').css('height', contents01Height);
        $(this).fadeOut(150);
        $('.pWrapper--01').addClass('p--open');
    });
};
var newProjectTop_sp = function () {
    var contents01Height = $('.contents--01Wrappper').height();
    $('.contents--01Wrappper').css('height', '410px');
    $('.seeMore').click(function () {
        $('.contents--01Wrappper').css('height', contents01Height);
        $(this).fadeOut(150);
        $('.pWrapper--01').addClass('p--open');
    });
};


/***/ }),

/***/ "./src/ts/simplisseStory.ts":
/*!**********************************!*\
  !*** ./src/ts/simplisseStory.ts ***!
  \**********************************/
/*! exports provided: simplisseStory_common, simplisseStory_pc, simplisseStory_sp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "simplisseStory_common", function() { return simplisseStory_common; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "simplisseStory_pc", function() { return simplisseStory_pc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "simplisseStory_sp", function() { return simplisseStory_sp; });
var simplisseStory_common = function () {
};
var simplisseStory_pc = function () {
    var index;
    $('.clickMeImage').hover(function () {
        index = $('.clickMeImage').index(this);
        //$('.clickMeImageTextContainer').removeClass('clickMeImageTextContainerOn');
        //$('.clickMeImageTextContainer:eq('+index+')').toggleClass("clickMeImageTextContainerOn");
        $('.clickMeImageTextContainer:eq(' + index + ')').stop(true, true).fadeToggle();
    });
    var controller = new ScrollMagic.Controller();
    var anime__01 = new ScrollMagic.Scene({ triggerElement: ".animeEle--01", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--01", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__01__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--01", triggerHook: 0.5, offset: 100 })
        .setVelocity(".animeEle--01--02", { opacity: 1 }, { duration: 800, delay: 200, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--02", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--02", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__02__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--02", triggerHook: 0.5, offset: 100 })
        .setVelocity(".animeEle--02--02", { opacity: 1 }, { duration: 800, delay: 200, ease: Sine.easeInOut })
        .addTo(controller);
    var anime__03 = new ScrollMagic.Scene({ triggerElement: ".animeEle--03", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--03", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__03__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--03", triggerHook: 0.5, offset: 100 })
        .setVelocity(".animeEle--03--02", { opacity: 1 }, { duration: 800, delay: 200, ease: Sine.easeInOut })
        .addTo(controller);
    var anime__04 = new ScrollMagic.Scene({ triggerElement: ".animeEle--04", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--04", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__04__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--04", triggerHook: 0.5, offset: 100 })
        .setVelocity(".animeEle--04--02", { opacity: 1 }, { duration: 800, delay: 200, ease: Sine.easeInOut })
        .addTo(controller);
    var anime__05 = new ScrollMagic.Scene({ triggerElement: ".animeEle--05", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--05", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__05__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--05", triggerHook: 0.5, offset: 100 })
        .setVelocity(".animeEle--05--02", { opacity: 1 }, { duration: 800, delay: 200, ease: Sine.easeInOut })
        .addTo(controller);
    var anime__06 = new ScrollMagic.Scene({ triggerElement: ".animeEle--06", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--06", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__06__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--06", triggerHook: 0.5, offset: 100 })
        .setVelocity(".animeEle--06--02", { opacity: 1 }, { duration: 800, delay: 200, ease: Sine.easeInOut })
        .addTo(controller);
    var anime__07 = new ScrollMagic.Scene({ triggerElement: ".animeEle--07", triggerHook: 0.5, offset: -150 })
        .setVelocity(".animeEle--07", { opacity: 1, top: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__08 = new ScrollMagic.Scene({ triggerElement: ".animeEle--08", triggerHook: 0.5, offset: -50 })
        .setVelocity(".animeEle--08", { opacity: 1, top: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__09 = new ScrollMagic.Scene({ triggerElement: ".animeEle--09", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--09", { opacity: 1, top: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
};
var simplisseStory_sp = function () {
    var index;
    var currentIndex;
    $('.clickMeImage').click(function () {
        index = $('.clickMeImage').index(this);
        $('.clickMeImageTextContainer:eq(' + index + ')').siblings('.clickMeImageTextContainer').fadeOut();
        $('.clickMeImageTextContainer:eq(' + index + ')').stop(true, true).fadeToggle();
    });
    var controller = new ScrollMagic.Controller();
    var anime__01 = new ScrollMagic.Scene({ triggerElement: ".animeEle--01", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--01", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__01_02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--01", triggerHook: 0.5, offset: 200 })
        .setVelocity(".animeEle--01--02", { opacity: 1 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        .addTo(controller);
    var anime__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--02", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--02", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__02_02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--02", triggerHook: 0.5, offset: 250 })
        .setVelocity(".animeEle--02--02", { opacity: 1 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        .addTo(controller);
    var anime__03 = new ScrollMagic.Scene({ triggerElement: ".animeEle--03", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--03", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__03__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--03", triggerHook: 0.5, offset: 250 })
        .setVelocity(".animeEle--03--02", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        .addTo(controller);
    var anime__04 = new ScrollMagic.Scene({ triggerElement: ".animeEle--04", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--04", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__04__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--04", triggerHook: 0.5, offset: 300 })
        .setVelocity(".animeEle--04--02", { opacity: 1 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        .addTo(controller);
    var anime__05 = new ScrollMagic.Scene({ triggerElement: ".animeEle--05", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--05", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__05__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--05", triggerHook: 0.5, offset: 250 })
        .setVelocity(".animeEle--05--02", { opacity: 1 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        .addTo(controller);
    var anime__06 = new ScrollMagic.Scene({ triggerElement: ".animeEle--06", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--06", { opacity: 1, left: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__06__02 = new ScrollMagic.Scene({ triggerElement: ".animeEle--06", triggerHook: 0.5, offset: 250 })
        .setVelocity(".animeEle--06--02", { opacity: 1 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        .addTo(controller);
    var anime__07 = new ScrollMagic.Scene({ triggerElement: ".animeEle--07", triggerHook: 0.5, offset: -150 })
        .setVelocity(".animeEle--07", { opacity: 1, top: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__08 = new ScrollMagic.Scene({ triggerElement: ".animeEle--08", triggerHook: 0.5, offset: -50 })
        .setVelocity(".animeEle--08", { opacity: 1, top: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
    var anime__09 = new ScrollMagic.Scene({ triggerElement: ".animeEle--09", triggerHook: 0.5, offset: 0 })
        .setVelocity(".animeEle--09", { opacity: 1, top: 0 }, { duration: 800, delay: 0, ease: Sine.easeInOut })
        //.addIndicators( {name: ".animeEle--01"})
        .addTo(controller);
};


/***/ }),

/***/ "./src/ts/talk.ts":
/*!************************!*\
  !*** ./src/ts/talk.ts ***!
  \************************/
/*! exports provided: talk_common, talk_pc, talk_sp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "talk_common", function() { return talk_common; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "talk_pc", function() { return talk_pc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "talk_sp", function() { return talk_sp; });
var talk_common = function () {
};
var talk_pc = function () {
    var urlHash = location.hash;
    var talk_01Area_Offset = $('#talk--01Area').offset().top - 80;
    var talk_02Area_Offset = $('#talk--02Area').offset().top - 80;
    if (urlHash == "#talk--01Area") {
        $('body, html').animate({ scrollTop: talk_01Area_Offset }, 500);
    }
    else if (urlHash == "#talk--02Area") {
        $('body, html').animate({ scrollTop: talk_02Area_Offset }, 500);
    }
};
var talk_sp = function () {
    var urlHash = location.hash;
    var campaign01_Offset = $('.campaign--01').offset().top - 65;
    if (urlHash == "#campaign01") {
        $('body, html').animate({ scrollTop: campaign01_Offset }, 500);
    }
};


/***/ }),

/***/ "./src/ts/top.ts":
/*!***********************!*\
  !*** ./src/ts/top.ts ***!
  \***********************/
/*! exports provided: mainVisualAnimeStart_pc, mainVisualAnimeStart_sp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mainVisualAnimeStart_pc", function() { return mainVisualAnimeStart_pc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mainVisualAnimeStart_sp", function() { return mainVisualAnimeStart_sp; });
// トップのローディング画面解除 → トップページのコンテンツのアニメーション開始
var mainVisualAnimeStart_pc = function () {
    if ($('svg').hasClass('animationTopLogo')) {
    }
    else {
        var animationTopLogo = bodymovin.loadAnimation({
            container: document.getElementById('topOnlyLogo'),
            renderer: 'svg',
            loop: false,
            autoplay: false,
            path: 'topLogo.json',
            rendererSettings: {
                className: 'animationTopLogo',
                preserveAspectRatio: 'none'
            }
        });
    }
    $('#loadingContainer').delay(500).fadeOut(300, function () {
        $('body').removeClass('bodyFixed');
        animationTopLogo.play();
        var mainVisualLeft = anime({
            targets: '.mainVisual__left .rect--01',
            top: '100%',
            duration: 800,
            delay: 200,
            loop: false,
            easing: 'easeInOutCubic'
        });
        var mainVisualRight = anime({
            targets: '.mainVisual__right',
            marginTop: '0px',
            opacity: 1,
            duration: 800,
            delay: 200,
            loop: false,
            easing: 'easeInOutCubic'
        });
        var contents01Container = anime({
            targets: '.contents01Container',
            opacity: 1,
            duration: 800,
            delay: 800,
            loop: false,
            easing: 'easeInOutCubic'
        });
    });
    var scrollTarget = $('.contents01Container').offset();
    var scrollTargetTop = scrollTarget.top - 115;
    $('.icon_S_Container').click(function () {
        $('body, html').stop(true, true).animate({ scrollTop: scrollTargetTop }, 500);
        return false;
    });
};
var mainVisualAnimeStart_sp = function () {
    if ($('svg').hasClass('animationTopLogoSp')) {
    }
    else {
        var animationTopLogo = bodymovin.loadAnimation({
            container: document.getElementById('topOnlyLogoSp'),
            renderer: 'svg',
            loop: false,
            autoplay: false,
            path: 'topLogo.json',
            rendererSettings: {
                className: 'animationTopLogoSp',
                preserveAspectRatio: 'none'
            }
        });
    }
    $('#loadingContainer').delay(500).fadeOut(300, function () {
        $('body').removeClass('bodyFixed');
        animationTopLogo.play();
        var mainVisualLeft = anime({
            targets: '.mainVisual__left .rect--01',
            top: '100%',
            duration: 600,
            loop: false,
            easing: 'easeInOutQuad'
        });
        var mainVisualRight_01 = anime({
            targets: '.mainVisual__right',
            opacity: 1,
            duration: 800,
            delay: 500,
            loop: false,
            easing: 'easeInCubic'
        });
        var mainVisualRight_02 = anime({
            targets: '.mainVisual__right',
            marginTop: '-30px',
            duration: 800,
            delay: 400,
            loop: false,
            easing: 'easeInOutQuad'
        });
        var contents01Container = anime({
            targets: '.contents01Container',
            opacity: 1,
            duration: 800,
            delay: 800,
            loop: false,
            easing: 'easeInOutCubic'
        });
    });
    var scrollTarget = $('.contents01Container').offset();
    var scrollTargetTop = scrollTarget.top - 95;
    $('.icon_S_Container').click(function () {
        $('body, html').stop(true, true).animate({ scrollTop: scrollTargetTop }, 500);
        return false;
    });
};


/***/ })

/******/ });
//# sourceMappingURL=app.js.map