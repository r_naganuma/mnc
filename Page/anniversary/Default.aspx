﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="サプリメントBOX | SIMPLISSE(シンプリス) produced by 山本未奈子" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link rel="stylesheet" href="./css/style.css?20210727" />
<link rel="stylesheet" href="./css/secondCommon.css?20200213" />
<link rel="stylesheet" href="./css/anniversaryCampaign.css?20210727" />
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>


<div class="anniversaryCampaign">
    <div class="wrapper font-feature-settings">
        <!-- <header>
    <div class="pcOnly headerMenu--pcContainer">
        <div class="headerMenu--pcContainer__inner">
        <h1><a href="../"><img src="./imgs/img-header-logo-pc.svg" alt="SIMPLISSE 10TH ANNIVERSARY"></a></h1>
        <div id="topOnlyLogo"></div>
        <div class="h1Logo"></div>
        <div class="pcMenuListsContainer">
            <nav>
            <ul class="pcMenuLists">
                <li class="font--gothamScreenSmart__light message"><a href="#"><span class="textillate">MESSAGE</span></a>
                <div class="headerSubmenuContainerOuter">
                    <ul class="headerSubmenuContainer">
                    <li class="font--gothamScreenSmart__light"><a href="../minakos-message/"><span class="textillate">MINAKO’S MESSAGE</span></a></li>
                    <li class="font--gothamScreenSmart__light"><a href="../voice/"><span class="textillate">VOICE</span></a></li>
                    </ul>
                </div>
                <div class="currentIcon"></div>
                </li>
                <li class="font--gothamScreenSmart__light brand"><a href="#"><span class="textillate">BRAND</span></a>
                <div class="headerSubmenuContainerOuter">
                    <ul class="headerSubmenuContainer">
                    <li class="font--gothamScreenSmart__light"><a href="../simplisse-story/"><span class="textillate">SIMPLISSE STORY</span></a></li>
                    <li class="font--gothamScreenSmart__light"><a href="../about-simplisse/"><span class="textillate">ABOUT SIMPLISSE</span></a></li>
                    </ul>
                </div>
                <div class="currentIcon"></div>
                </li>
                <li class="font--gothamScreenSmart__light anniversaryCampaign"><a href="../anniversary-campaign/"><span class="textillate">ANNIVERSARY<br>CAMPAIGN</span></a><div class="currentIcon"></div></li>

                <li class="font--gothamScreenSmart__light newProject"><a href="../sensual/"><span class="textillate">NEW<br>PROJECT</span></a><div class="currentIcon"></div></li>
                <li class="font--gothamScreenSmart__light onlyMeSupplement"><span class="textillate">ONLY ME<br>SUPPLEMENT</span><div class="currentIcon"></div></li>


                <li class="font--gothamScreenSmart__bold"><a href="https://www.simplisse.jp/"><span class="textillate">ONLINE<br>STORE</span></a><div class="currentIcon"></div></li>
            </ul>
            </nav>
        </div>
        </div>
    </div>

    <div class="pcMenuBack"></div>

    <div class="headerMenu--spContainer spOnly">
        <div class="menuIcon"><span></span><span></span><span></span></div>
        <div class="space"></div>
        <div class="menuLogo"><img src="./imgs/img-header-logo-sp.svg" alt=""></div>
        <div class="spMuneInner">
        <ul class="spMenu">
            <li class="font--gothamScreenSmart__bold"><a href=".././">TOP</a></li>
            <li class="font--gothamScreenSmart__bold"><a href="#">MESSAGE</a>
            <ul>
                <li class="font--gothamScreenSmart__light"><a href="../minakos-message/">- MINAKO’S MESSAGE</a></li>
                <li class="font--gothamScreenSmart__light"><a href="../voice/">- VOICE</a></li>
            </ul>
            </li class="font--gothamScreenSmart__bold">
            <li class="font--gothamScreenSmart__bold"><a href="">BRAND</a>
            <ul>
                <li class="font--gothamScreenSmart__light"><a href="../simplisse-story/">- SIMPLISSE STORY</a></li>
                <li class="font--gothamScreenSmart__light"><a href="../about-simplisse/">- ABOUT SIMPLISSE</a></li>
            </ul>
            </li>
            <li class="font--gothamScreenSmart__bold"><a href="../anniversary-campaign/">ANNIVERSARY CAMPAIGN</a></li>

            <li class="font--gothamScreenSmart__bold"><a href="../sensual/">NEW PROJECT</a></li>
            <li class="font--gothamScreenSmart__bold onlyMeSupplement">ONLY ME SUPPLEMENT</li>


            <li class="font--gothamScreenSmart__bold"><a href="https://www.simplisse.jp/">ONLINE STORE</a></li>
        </ul>
        </div>
        <div class="headerLogoSp"><a href=".././"><img src="./imgs/img-header-logo-pc.svg" alt="SIMPLISSE 10TH ANNIVERSARY"></a></div>
        <div id="topOnlyLogoSp"></div>
        <div class="font--gothamScreenSmart__medium headerMenuOnlineStore"><a href="https://www.simplisse.jp/">ONLINE<br>STORE</a></div>


        <div id="spMenuBack" class="spOnly"></div>
    </div>
    </header> -->
    <div id="headerWrap"></div>
    <div class="pageTopTrigger"></div>

    <main>
    <section class="campaign--03">
        <div class="contentsContainer--01">
        <div class="mainVisualOutter">
            <div class="mainVisualInner">
            <div class="kvImgWrapper">
                <div class="kvImg__01"></div>
                <div class="kvImg__02"></div>
            </div>
            <div class="maruWrapper">
                <div></div>
            </div>
            <div class="titleWrapper">
                <!--
                <h2 class="text--01 font--gothamScreenSmart__bold">ANNIVERSARY CAMPAIGN #03</h2>
                <p class="text--02 font--gothamScreenSmart__bold">
                A BOX FULL OF <br>
                HEALTH & BEAUTY!
                </p>
-->
                <h2 class="text--02 font--gothamScreenSmart__bold">
                A BOX FULL OF <br>
                HEALTH & BEAUTY!
                </h2>
                <div class="newSetAdd">
                <p class="font--TBGothic--M">新セット<br>追加！</p>
                <!-- <img src="imgs/newSetAdd.svg" alt="新セット追加！"> -->
                </div>

            </div>
            </div>
        </div>
        </div>

        <div class="contentsContainer--02">
        <div class="textWrapper">
            <h3 class="text--03 font--TBGothic--M">専用BOXで、<br class="spOnly">いつもの場所に、いつものサプリ。</h3>
            <p class="text--04">毎日の健康を支えるサプリメントをもっと身近に、習慣にしてほしい。<br class="pcOnly">
            そんな想いでサプリメントのための収納BOXを作りました。<br class="pcOnly">
            コンパクトに収められ、使いやすい場所に置いておけば<br class="pcOnly">
            飲み忘れもなくなるかも。<br class="pcOnly">
            そんなサプリメントBOXを、10周年記念のプレミアムなセットにしました。<br class="pcOnly">
            気になっていたあのサプリメントをたっぷり試せるチャンスです。
            </p>
        </div>

        <div class="imgwrapper">
            <div class="areaA">
            <img src="imgs/cam03/campain03-img-01@2x.png" alt="">
            </div>
            <div class="areaB">
            <img src="imgs/cam03/campain03-img-02@2x.png" alt="">
            </div>
        </div>
        </div>

        <div class="contentsContainer--03">
        <h3 class="text--03 font--gothamScreenSmart__medium">PREMIUM SUPPLEMENT SET</h3>
        <p class="text--04 font--TBGothic--M">新たに3種のセレクション<span
            class="font--gothamScreenSmart__medium">BOX</span>が新登場！</p>

        <div class="productWrapper selectionBox selectionBox--A">
            <picture>
            <source srcset="imgs/cam03/selection-box-A-sp.png" media="(max-width: 767px)">
            <img src="imgs/cam03/selection-box-A-pc@2x.png" alt="シンプリス サプリメント セレクションBOX-A">
            </picture>
            <p class="title font--TBGothic--M">シンプリス サプリメント セレクションBOX-A<br>22,000円+税</p>
            <p class="text--01">(通常合計価格 27,500円+税の品)<br>
            ・サプリメントBOX<br>
            ・シンプリス センシュアル ネンマク ケア 1箱<br>
            ・シンプリス パーフェクトダイエット プラス＋ 1箱<br>
            ・シンプリス UVディフェンス プロ 1箱</p>
            <a class="linkTypeCommon linkType--01 font--TBGothic--M"
            href="https://www.simplisse.jp/Form/Product/ProductDetail.aspx?shop=0&pid=40000001283&cat=wel") %>ご購入はこちら</a>
        </div>

        <div class="productWrapper selectionBox selectionBox--B">
            <picture>
            <source srcset="imgs/cam03/selection-box-B-sp.png" media="(max-width: 767px)">
            <img src="imgs/cam03/selection-box-B-pc@2x.png" alt="シンプリス サプリメント セレクションBOX-B">
            </picture>
            <p class="title font--TBGothic--M">シンプリス サプリメント セレクションBOX-B<br>40,000円+税</p>
            <p class="text--01">(通常合計価格 51,900円+税の品)<br>
            ・サプリメントBOX<br>
            ・シンプリス センシュアル ネンマク ケア 3箱<br>
            ・シンプリス UVディフェンス プロ 3箱<br>
            </p>
            <a class="linkTypeCommon linkType--01 font--TBGothic--M"
            href="https://www.simplisse.jp/Form/Product/ProductDetail.aspx?shop=0&pid=40000001284&cat=wel") %>ご購入はこちら</a>
        </div>

        <div class="productWrapper selectionBox selectionBox--C">
            <picture>
            <source srcset="imgs/cam03/selection-box-C-sp.png" media="(max-width: 767px)">
            <img src="imgs/cam03/selection-box-C-pc@2x.png" alt="シンプリス サプリメント セレクションBOX-C">
            </picture>
            <p class="title font--TBGothic--M">シンプリス サプリメント セレクションBOX-C<br>45,000円+税</p>
            <p class="text--01">(通常合計価格 62,000円+税の品)<br>
            ・サプリメントBOX<br>
            ・シンプリス トータルプログラム27 2箱<br>
            ・シンプリス パーフェクトダイエット プラス＋ 2箱<br>
            ・シンプリス パーフェクト リポカット 2箱
            </p>
            <a class="linkTypeCommon linkType--01 font--TBGothic--M"
            href="https://www.simplisse.jp/Form/Product/ProductDetail.aspx?shop=0&pid=40000001285&cat=wel") %>ご購入はこちら</a>
        </div>



        <div class="productWrapper productWrapper--02">
            <figure>
            <img class="pcOnly" src="imgs/cam03/campain03-img-03-pc@2x.png" alt="サプリメントBOX & シンプリス サプリメント全6種 各1箱">
            <img class="spOnly" src="imgs/cam03/campain03-img-03-sp.png" alt="">
            </figure>
            <p class="title font--TBGothic--M">シンプリス サプリメント プレミアム フルBOX<br>45,000円+税</p>
            <p class="text--01">(通常合計価格 60,900円+税の品)<br>サプリメントBOX & シンプリス サプリメント全6種 各1箱</p>
            <a class="linkTypeCommon linkType--01 font--TBGothic--M"
            href="https://www.simplisse.jp/Form/Product/ProductDetail.aspx?shop=0&pid=40000001201&cat=wel") %>ご購入はこちら</a>
        </div>

        <div class="productWrapper productWrapper--02">
            <figure>
            <img class="pcOnly" src="imgs/cam03/campain03-img-04-pc@2x.png" alt="サプリメントBOX & シンプリス サプリメント全6種 各10袋">
            <img class="spOnly" src="imgs/cam03/campain03-img-04-sp.png" alt="">
            </figure>
            <p class="title font--TBGothic--M">シンプリス サプリメント アソートBOX<br>15,000円+税</p>
            <p class="text--01"> （通常合計価格 18,000円+税相当）<br>サプリメントBOX & シンプリス サプリメント全6種 各10袋</p>
            <a class="linkTypeCommon linkType--01 font--TBGothic--M"
            href="https://www.simplisse.jp/Form/Product/ProductDetail.aspx?shop=0&pid=40000001202&cat=wel") %>ご購入はこちら</a>
            <p class="text--02">※数に限りがございますため、予告なく終了させていただく場合がございます。</p>
        </div>
        </div>

        <div class="contentsContainer--04">
        <h3 class="text--03 font--TBGothic--M"><span
            class="font--gothamScreenSmart__medium">SIMPLISSE</span>のサプリメントは<br class="spOnly">すべて飲み合わせ<span
            class="font--gothamScreenSmart__medium">OK!</span><br>悩みや食事に合わせて、カスタマイズ。</h3>
        <p class="text--01">シンプリスのサプリメントは合わせて飲んでも成分の過剰摂取にならないようレシピを組んでいます。<br>
            だから、シンプリスのサプリメント同士ならすべて飲み合わせ可能です。<br class="pcOnly">
            たとえば、1日の中で、こんなふうに飲み合わせてもOK！</p>
        <p class="text--02">※シンプリスのサプリメント以外と併用する場合は、同じ成分の過剰摂取になる可能性があります。<br>
            成分をご確認の上お飲みいただくようご注意ください。 </p>
        <figure>
            <img class="pcOnly" src="imgs/cam03/campain03-img-05-pc@2x.png" alt="24H SUPPLEMENT!">
            <img class="spOnly" src="imgs/cam03/campain03-img-05-sp.png" alt="">
            <figcaption>※飲む際の一例であり、使用方法ではありません。</figcaption>
        </figure>
        <!--
        <div class="andMoreWrapper">
            <p class="text--08 font--gothamScreenSmart__bold">AND MORE</p>
            <h3 class="text--09 font--TBGothic--M">会員登録なしでも、皆さま送料無料<br>
            &amp;<br>
            会員登録で即日使える<br>
            1,000円OFFクーポンPRESENT
            </h3>
            <a class="linkTypeCommon linkType--02" href="https://www.simplisse.jp/index.php?option=com_content&view=article&id=2491&Itemid=313#3">詳しくはオンラインストアへ</a>
        </div>
-->
        </div>

    </section>


    <section class="campaign--02">
        <div class="contentsContainer--01">
        <div class="mainVisualOutter">
            <div class="mainVisualInner">
            <h2 class="text--01 font--gothamScreenSmart__bold">ANNIVERSARY CAMPAIGN #02</h2>
            <p class="text--02 font--gothamScreenSmart__bold">
                COLORFUL &amp;<br>
                HEALTHFUL HABITS<br>
                with
            </p>
            <img class="img--01" src="imgs/img-logo.png" alt="FIJI water × SIMPLISSE">
            <img class="spOnly" src="imgs/img-campaign-02-01-sp.jpg" alt="">
            </div>
        </div>
        </div>

        <section class="contentsContainer--02">
        <div class="contentsContainer--02__inner">
            <div class="contents--01">
            <h3 class="font--TBGothic--M">あなたのライフスタイルに、<br class="spOnly">サプリメントを。</h3>
            <p>忙しい現代を生きる私たちに、サプリメントは必要なものだとSIMPLISSEは考えます。<br>
                SIMPLISSEなら、あなたのライフスタイルや<br class="pcOnly">
                日々の悩みに寄り添うサプリメントがきっと見つかるはず。<br class="pcOnly"><br class="spOnly">
                今なら、南国フィジー生まれのミネラルたっぷり「フィジーウォーター 500ml」と<br class="pcOnly">
                ネオプレン素材の色鮮やかな「オリジナル ボトルホルダー」をプレゼント。<br>
                ミネラルたっぷりの水でサプリメントをチャージして、内から健やかなカラダへ。<br class="pcOnly">
                さあ、今日からサプリメントライフをはじめましょう！
            </p>
            </div>
            <div class="contents--02">
            <img src="imgs/img-campaign-02-05-pc.png" alt="フィジーウォーター500ml&オリジナル ボトルフォルダー">
            </div>
        </div>

        <p class="text--13 font--TBGothic--M">
            <span style="color: #DA006C;">ご好評につき、本キャンペーンは終了いたしました。</span><br><br>
            プレゼントコードご入力 &amp; <br class="spOnly">SIMPLISSEのサプリメント<br>
            いずれか1点以上ご購入でPRESENT!</p>

        <div class="img--02">
            <img class="pcOnly" src="imgs/img-campaign-02-04-pc.png" alt="SIMPLISSEのサプリメント">
            <img class="spOnly" src="imgs/img-campaign-02-04-sp.png" alt="SIMPLISSEのサプリメント">
        </div>
        <p class="annotation--01">※パーフェクトダイエット セット、サプリメント プレミアム BOX各種は<br class="spOnly">対象外となります。</p>
        </section>

        <section>
        <div class="contentsContainerCommon contentsContainer--03">
            <div class="contents--02">
            <h3 class="text--12 font--TBGothic--M"><span class="font--gothamScreenSmart__light">FIJI</span> ウォーターとは？
            </h3>
            <p class="text--07">
                フィジーウォーターは、南太平洋の楽園フィジー生まれのミネラルウォーター。天然のシリカを93mg/Lと豊富に含んだ、飲みやすくておいしい軟水のミネラルウォーターです。多くの海外セレブに愛飲されています。</p>
            </div>
            <div class="contents--01"><img src="imgs/img-campaign-02-02-pc.png" alt="FIJI ウォーター"></div>
        </div>
        </section>

        <!--//////////////////////////
プレゼントコード▽▽▽
-->
        <!-- 
        <section>
        <div class="wrapperPresentCode">
            <h3 class="font--TBGothic--M">サプリメントご購入時に<br>
            プレゼントコード入力欄に下記コードを<br class="spOnly">ご入力ください。
            </h3>
            <div class="contentsPresentCode">
            <div class="areaA">
                <div class="imgWrapper"><img src="imgs/img-presentCode-01.png" alt=""></div>
            </div>
            <div class="areaB font--TBGothic--M">プレゼントコード <br><span
                class="codeNum font--gothamScreenSmart__bold">SUPFB</span></div>
            <div class="areaC">※定期便は対象外になります。※お一人様につき、1点となります。<br>
                ※コードのご使用は、ご本人様に限り1回のみ有効となります。<br>
                ※コードを入力せずご購入した場合、後からのキャンペーン適用は致し兼ねます。<br>
                ※なくなり次第終了となります。</div>
            </div>
            <a class="linkTypeCommon linkType--01 font--TBGothic--M"
            href="https://www.simplisse.jp/index.php?option=com_content&view=article&id=3023&Itemid=313">サプリメントご購入はこちら</a>
        </div>
        </section>
-->
        <!--
プレゼントコード△△△
//////////////////////////-->

        <!--
        <section>
        <div class="contentsContainer--05">
            <p class="text--08 font--gothamScreenSmart__bold">AND MORE</p>
            <h3 class="text--09 font--TBGothic--M">会員登録なしでも、皆さま送料無料<br>
            &amp;<br>
            会員登録で即日使える<br>
            1,000円OFFクーポンPRESENT
            </h3>

            <a class="linkTypeCommon linkType--02" href="https://www.simplisse.jp/index.php?option=com_content&view=article&id=2491&Itemid=313">詳しくはオンラインストアへ</a>

            <a class="linkTypeCommon linkType--02" href="https://www.simplisse.jp/index.php?option=com_content&view=article&id=2491&Itemid=313#3">詳しくはオンラインストアへ</a>
        </div>
        </section>
-->

    </section>

    <section id="campaign01" class="campaign--01">
        <div class="contentsContainer--01">
        <div class="mainVisualOutter">
            <div class="mainVisualInner">
            <h2 class="text--01 font--gothamScreenSmart__bold">ANNIVERSARY CAMPAIGN #01</h2>
            <p class="text--02 font--gothamScreenSmart__bold">10 YEARS<br>
                STORY<br>
                OF SERUM
            </p>
            <img class="spOnly" src="imgs/img-campaign-01-01-sp.jpg" alt="">
            </div>
        </div>

        <h3 class="text--03 font--TBGothic--M">10年の想い、生まれた原点をのせて。<br>アニバーサリー限定パッケージ。</h3>
        <p class="text--04">女性たちの笑顔溢れる日々に寄り添いたい、<br class="pcOnly">
            そんな想いと共に、シンプリスの歴史は美容液から始まりました。<br class="pcOnly"><br class="pcOnly">
            累計販売本数が最多のベストセラー「美容液シリーズ」と<br class="pcOnly">
            2019年に誕生した「コンセントレート ライン セラム」を、<br class="pcOnly">
            10年間の歩みをオリジナルイラストにした<br class="pcOnly">
            アニバーサリー限定パッケージでお届けいたします。<br><br class="pcOnly">
            ぜひお手元で、シンプリスの世界観をご体感ください。
        </p>
        </div>

        <section>
        <div class="contentsContainerCommon contentsContainer--02">
            <div class="contents--01"><img src="imgs/img-campaign-01-02-pc.jpg" alt="美容液シリーズ 写真"></div>
            <div class="contents--02">
            <p class="text--05 font--gothamScreenSmart__bold">SPECIAL 01</p>
            <h3 class="text--06 font--TBGothic--M">進化し続ける美容液シリーズが<br><span
                class="font--gothamScreenSmart__light">SPECIAL SETに。</span></h3>
            <p class="text--07">シンプリスがスキンケアで最も重要だと考えるアイテム「美容液」。３種類全てが入った「シンプリス 10周年 スペシャル セラム
                セット」を特別にご用意いたしました。3つを揃え、朝晩の使い分けや、肌状態の使い分けで充実のスキンケアを。</p>
            <p class="text--07"><span style="color: #DA006C;">ご好評につき、本キャンペーンは終了いたしました。</span></p>

            <!--
            <a class="linkTypeCommon linkType--01" href="https://www.simplisse.jp/index.php?option=com_content&view=article&id=2491&Itemid=313">詳しくはオンラインストアへ</a>
-->
            </div>
        </div>
        </section>

        <section>
        <div class="contentsContainerCommon contentsContainer--03">
            <div class="contents--02"><img src="imgs/img-campaign-01-03-pc.jpg" alt="美容液シリーズ 写真"></div>
            <div class="contents--01">
            <p class="text--05 font--gothamScreenSmart__bold">SPECIAL 02</p>
            <h3 class="text--06 font--TBGothic--M">2019年生まれ、ラインセラムを<br><span
                class="font--gothamScreenSmart__light">SPECIAL GIFTに。</span></h3>
            <p class="text--07">10周年を記念して、人気のパーツケア集中アイテム「シンプリス コンセントレート ラインセラム （11,250円＋税の品）」を、シンプリス オンライン
                ストア内で２万円以上ご購入の方にもれなくプレゼント。<br class="spOnly">目元や口元にリッチな潤いと柔らかさを。</p>
            <p class="text--07"><span style="color: #DA006C;">ご好評につき、本キャンペーンは終了いたしました。</span></p>
            <!--
            <a class="linkTypeCommon linkType--01" href="https://www.simplisse.jp/index.php?option=com_content&view=article&id=2491&Itemid=313#2">詳しくはオンラインストアへ</a>
-->
            </div>
        </div>
        </section>

        <!--
        <section>
        <div class="contentsContainer--04">
            <p class="text--08 font--gothamScreenSmart__bold">AND MORE</p>
            <h3 class="text--09 font--TBGothic--M">会員登録なしでも、皆さま送料無料<br>
            &amp;<br>
            会員登録で即日使える<br>
            1,000円OFFクーポンPRESENT
            </h3>

            <a class="linkTypeCommon linkType--02" href="https://www.simplisse.jp/index.php?option=com_content&view=article&id=2491&Itemid=313">詳しくはオンラインストアへ</a>

            <a class="linkTypeCommon linkType--02" href="https://www.simplisse.jp/index.php?option=com_content&view=article&id=2491&Itemid=313#3">詳しくはオンラインストアへ</a>

        </div>
        </section>
-->

        <div class="contentsContainer--05">
        <p class="text--10">オリジナルイラストでつづった<br class="spOnly">SIMPLISSE STORYはこちら</p>
        <a class="linkType--03" href="https://www.simplisse.jp/Page/about/brandhistory/">
            <p class="text--11">SIMPLISSEが生まれるまで</p>
            <img src="imgs/img-campaign-04-pc.jpg" alt="">
        </a>
        </div>

    </section>
    </main>

    <div id="footerWrap"></div>

        <!-- <footer>
    <div id="footer">
            <div class="fo_back pcOnly">
                <ul class="font--bold">
                <li><a href="/index.php?option=com_content&amp;view=article&amp;id=18&amp;Itemid=171">会社概要</a></li>
                <li><a href="/index.php?option=com_content&amp;view=article&amp;id=30&amp;Itemid=172">特定商取引法に基づく表記</a></li>
                <li><a href="/index.php?option=com_content&amp;view=article&amp;id=31&amp;Itemid=173">ご利用規約</a></li>
                <li><a href="/index.php?option=com_content&amp;view=article&amp;id=32&amp;Itemid=174">プライバシーポリシー</a></li>
                <li class="pc_only"><a href="/index.php?option=com_content&amp;view=article&amp;id=45&amp;Itemid=175">Q&amp;A</a></li>
                <li><a href="/index.php?option=com_formmaker&amp;view=formmaker&amp;id=12&amp;Itemid=290
    ">お問い合わせ</a></li>
                </ul>
            <p class="font--gothamScreenSmart__black">Copyright©2018 MNC New York Inc. All Rights Reserved.</p>
            </div>
            <div class="fo_back spOnly">
            <p><a href="/index.php?option=com_content&amp;view=article&amp;id=18&amp;Itemid=171">会社概要</a><a href="/index.php?option=com_content&amp;view=article&amp;id=30&amp;Itemid=172">特定商取引法に基づく表記</a> <a href="/index.php?option=com_content&amp;view=article&amp;id=31&amp;Itemid=173">ご利用規約</a></p>
            <p><a href="/index.php?option=com_content&amp;view=article&amp;id=32&amp;Itemid=174">プライバシーポリシー</a><a href="/index.php?option=com_formmaker&amp;view=formmaker&amp;id=12&amp;Itemid=290">お問い合わせ</a></p>
            <p class="font--gothamScreenSmart__light">Copyright©2018 MNC New York Inc. All Rights Reserved.</p>
            </div>
            </div>
    </footer> -->

    <div class="bottom_btn">
    <div class="page-top">
        <a href="#top">page top</a>
    </div>
    </div>
</div>

<div id="spMenuBack" class="spOnly"></div>
<div id="spMenuOnBackGround" class="spOnly"></div>

<script src="./js/app.js?2020_0214"></script>


<!-- <script>
    $(function(){
        $("html").css("font-size","inherit")
    });
</script> -->

</div>
<%-- △編集可能領域△ --%>
</asp:Content>
