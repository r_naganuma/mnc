﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="特定商取引法に基づく表記 | SIMPLISSE(シンプリス) produced by 山本未奈子" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/style.css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="termsofuse">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					特定商取引法に基づく表示
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
    <!----->
    <section class="listBrandsArea termsofuse">
        <div class="termsofuse__title">
            <h2>
                特定商取引法に基づく表示
            </h2>
        </div>
        <div class="termsofuse__wrap">
            <dl class="termsofuse__wrap--Items">
                <dd>
                    ショップ名
                </dd>
                <dt>
                    SIMPLISSE オンラインストア
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    事業者名
                </dd>
                <dt>
                    MNC New York株式会社
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    販売責任者
                </dd>
                <dt>
                    山本　未奈子
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    住所
                </dd>
                <dt>
                    〒150-0001 東京都 渋谷区 神宮前5丁目1番7号
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    電話番号
                </dd>
                <dt>
                    03-3473-3939
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    FAX番号
                </dd>
                <dt>
                    03-3478-3933
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    メールアドレス
                </dd>
                <dt>
                    info@simplisse.jp
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    ホームページ
                </dd>
                <dt>
                    https://www.simplisse.jp/
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    取扱商品
                </dd>
                <dt>
                    スキンケア商品、ビューティーサプリメント・健康食品、美容機器
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    許認可・資格
                </dd>
                <dt>
                    必要なし
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    販売数量
                </dd>
                <dt>
                    商品は在庫限りとさせていただきます。ご注文いただきました商品が在庫切れの場合はメールにてご連絡差し上げます。
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    商品代金
                </dd>
                <dt>
                    商品毎に設定。商品ページで販売価格を表示しております。
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    商品代金以外の必要料金
                </dd>
                <dt>
                    送料<br>
                    880円（北海道・東北・関東・中部・関西・四国・九州）<br>
                    1,100円（沖縄・離島）<br>
                    1,800円（海外配送）<br>
                    消費税 10%
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    申込の有効期限
                </dd>
                <dt>
                    商品到着日より７日以内に、メールまたは、サイト内お問合せ窓口からご連絡ください。これらの方法でご連絡いただけなかった場合は、返品、交換は承れません。
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    引き渡し時期
                </dd>
                <dt>
                    ご注文を頂いてから、３営業日以内に速やかに発送させていただきます。土日祝日の発送はお休みとなります。
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    中途解約
                </dd>
                <dt>
                    定期購入のご解約は、次回商品発送の7日前までに電話・メールにてご連絡をお願いいたします。期の途中で解約する場合は、お届け済み商品につきまして定価との差額をご請求させていただきます。
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    返品期限・条件
                </dd>
                <dt>
                    未開封で到着から7日以内の商品につきましては返品、交換を承っております。その場合、お問合せフォームよりご連絡ください。
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    返品送料
                </dd>
                <dt>
                    返品に伴う配送料はお客様負担とさせていただきます。
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    不良品
                </dd>
                <dt>
                    万一不良品等がございましたら、お手数をおかけいたしますが、弊社のお問合せフォーム info@simplisse.jpにご連絡の上、着払いでご返送ください。すぐに新しい商品をお送りいたします。
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    お支払方法
                </dd>
                <dt>
                    代引き、クレジットカード払、NP後払い（コンビニ支払、郵便振替）、Amazon Payを用意しております。ご希望にあわせて、各種ご利用ください。
                </dt>
            </dl>
            <dl class="termsofuse__wrap--Items">
                <dd>
                    お支払期限
                </dd>
                <dt>
                    代引き：商品引渡時<br>
                    クレジットカード：ご利用のクレジットカード会社により異なります。詳しくは、各クレジット会社にお問い合わせください。<br>
                    NP後払い：お客様に郵送の専用振込用紙にて、最寄のコンビニエンスストア、郵便局で、伝票発行後14日以内にお支払いください。
                </dt>
            </dl>
        </div>

    </section>
    <!----->

    <div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					特定商取引法に基づく表示
				</a>
			</li>
		</ul>
	</div>
    <!--△ パンくず △-->

</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>
