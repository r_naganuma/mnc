﻿<%--
=========================================================================================================
  Module      : カスタムページテンプレート画面(CustomPageTemplate.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Page Title="リニューアル記念キャンペーン | SIMPLISSE(シンプリス) produced by 山本未奈子" Language="C#" Inherits="ContentsPage" MasterPageFile="~/Form/Common/DefaultPage.master" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="システム管理者" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<%-- △編集可能領域△ --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%-- ▽編集可能領域：コンテンツ▽ --%>

<!-- about -->
<article id="renewal">
	<!--▽ パンくず ▽-->
	<div class="breadArea pc_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					リニューアルオープン記念
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->

	<section class="kvArea">
		<picture>
			<source media="(max-width: 769px)" srcset="./images/kv_sp.png">
			<img class="main_pic" src="./images/kv.png" alt="">
		</picture>
		<div class="kvArea_txt">
			<h2>
				RENEWAL<br>
				OPEN
			</h2>
			<p>
				価値ある情報を充実させ、<br class="sp_contents">もっと便利で快適に。<br>
				シンプリス オンラインサイト <br class="sp_contents">リニューアルオープン！
			</p>
		</div>
	</section>

	<section class="bnrArea">
		<picture>
			<source media="(max-width: 769px)" srcset="./images/bnr_sp.png">
			<img class="main_pic" src="./images/bnr.png" alt="">
		</picture>
	</section>

	<section class="ctsArea cts01">
		<h3>CAMPAIGN #1</h3>
		<div class="ctsInner">
			<h4>
				オンラインストアでお買い物で<br>
				オリジナル マスクスプレー<span>(非売品)</span><br class="sp_contents"> PRESENT
			</h4>
			<p class="ctsTxt">
				期間中に、シンプリス オンラインストアで、<br>
				3,000円以上*の商品をご購入の<br class="sp_contents">皆さまにもれなくプレゼント！<br>
				マスクにシュッとひと吹き、<br class="sp_contents">爽やかな香りが広がってリフレッシュ。<br>
				除菌もできるスプレーです。
			</p>
			<ul>
				<li>
					除菌
				</li>
				<li>
					リフレッシュ
				</li>
				<li>
					ライム&レモン&ミントの香り
				</li>
			</ul>
			<p class="ctsNote">
				※税込・送料別・各種割引適用後、3,000円以上のご購入時にプレゼントいたします。<br>
				※お一人様1本とさせていただきます。<br>
				※なくなり次第終了となります。
			</p>
		</div>
		<div class="ctsNum">
			<p>数量限定</p>
		</div>
	</section>

	<section class="ctsArea cts02">
		<h3>CAMPAIGN #2</h3>
		<div class="ctsInner">
			<h4>
				「ベア シグネチャー ショーツ」<br class="sp_contents">3枚購入で、<br class="pc_contents">血液専用粉洗剤 <br class="sp_contents">PRESENT
			</h4>
			<p class="ctsTxt">
				昨年7月にデビューした「Bé-A / ベア」が、<br class="sp_contents">シンプリス オンラインストアに初登場！<br class="sp_contents">注目アイテムは、次世代の「超吸収型サニタリーショーツ」。ショーツ1 枚で、<br class="sp_contents">タンポン約12本分* の液体を吸収し、<br>
				穿くだけで1日中安心して過ごせます。<br>
				洗い替えも含めて3枚揃えるのがおすすめ。<br>
				ご購入は今がチャンス。<br>
				<span>*公的検査機関のデータを元に算出</span>
			</p>
			<div class="ctsPic">
				<picture>
					<source media="(max-width: 769px)" srcset="./images/item_02_sp.png">
					<img class="main_pic" src="./images/item_02.png" alt="">
				</picture>
			</div>
			<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=othbea&dpcnt=15&img=1&sort=07&udns=2&fpfl=0&pno=1") %>" class="ctsBtn">ご購入はこちら</a>
		</div>
	</section>

	<section class="ctsArea cts03">
		<h3>CAMPAIGN #3</h3>
		<div class="ctsInner">
			<h4>
				人気の「シンプリス フィト<br class="sp_contents">クレンズピュア」<br class="pc_contents">10本購入で<br class="sp_contents">2本PRESENT
			</h4>
			<p class="ctsTxt">
				ファスティングや置き換えダイエット、<br class="sp_contents">日々の健康管理にと、<br class="pc_contents">リピーター続出の<br class="sp_contents">人気発酵飲料。<br class="pc_contents">キレイを続ける方に、<br class="sp_contents">2本無料のビックプレゼントです。<br>
				シンプリス フィトクレンズ ピュア <br class="sp_contents">10本 税込 86,400円 <br class="sp_contents">(2本 税込 17,280円の品プレゼント)
			</p>
			<div class="ctsPic">
				<picture>
					<source media="(max-width: 769px)" srcset="./images/item_03_sp.png">
					<img class="main_pic" src="./images/item_03.png" alt="">
				</picture>
			</div>
			<a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductDetail.aspx?shop=0&pid=40000001339&cat=wel") %>" class="ctsBtn">ご購入はこちら</a>
		</div>
	</section>

	<!--▽ パンくず ▽-->
	<div class="breadArea sp_contents">
		<ul>
			<li>
				<a href="<%= WebSanitizer.UrlAttrHtmlEncode(this.UnsecurePageProtocolAndHost + Constants.PATH_ROOT) %>">HOME</a>
			</li>
			<li>
				<a href="#">
					リニューアルオープン記念
				</a>
			</li>
		</ul>
	</div>
	<!--△ パンくず △-->
</article>
<!-- // about -->
<%-- △編集可能領域△ --%>
</asp:Content>

