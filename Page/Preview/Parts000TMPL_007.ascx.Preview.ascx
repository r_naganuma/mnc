﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP Message" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="messageArea">
    <div class="messageArea_pic">
        <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/pic_profile.jpg" />
    </div>
    <div class="messageArea_txt">
        <div class="messageArea_txt--inner">
            <p class="messageArea_txt--ttl">
                シンプルに、美しく<br>
                あなたらしい人生のために。
            </p>
            <p class="messageArea_txt--txt">
                心地よく豊かな「キレイ」を育み、<br>
                誰もがなりたい自分に近づけるように。<br>
                それが私たちの願いです。
            </p>
            <p class="messageArea_txt--name">
                <span>SIMPLISSE 代表</span>
                山本未奈子
            </p>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Page/profile/") %>" class="messageArea_txt--btn">PROFILE</a>
        </div>
    </div>
</section>
<%-- △編集可能領域△ --%>
