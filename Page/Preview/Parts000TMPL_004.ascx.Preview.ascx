﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP Keywords" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="keywordsArea">
    <h2>
        KEYWORDS<span>注目キーワード</span>
    </h2>
    <ul class="keywordsArea_list">
        <li>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=&dpcnt=15&img=2&sort=07&udns=2&fpfl=0&_key=ダイエット&pno=1") %>">ダイエット</a>
        </li>
        <li>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=&dpcnt=15&img=2&sort=07&udns=2&fpfl=0&_key=口コミ人気&pno=1") %>">口コミ人気</a>
        </li>
        <li>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=&dpcnt=15&img=2&sort=07&udns=2&fpfl=0&_key=定期便&pno=1") %>">定期便</a>
        </li>
        <li>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=&dpcnt=15&img=2&sort=07&udns=2&fpfl=0&_key=エイジングケア&pno=1") %>">エイジングケア</a>
        </li>
        <li>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=&dpcnt=15&img=2&sort=07&udns=2&fpfl=0&_key=センシュアル&pno=1") %>">センシュアル</a>
        </li>
        <li>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=&dpcnt=15&img=2&sort=07&udns=2&fpfl=0&_key=飲む日焼け止め&pno=1") %>">飲む日焼け止め</a>
        </li>
        <li>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=&dpcnt=15&img=2&sort=07&udns=2&fpfl=0&_key=ファスティング&pno=1") %>">ファスティング</a>
        </li>
        <li>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=&dpcnt=15&img=2&sort=07&udns=2&fpfl=0&_key=粘膜&pno=1") %>">粘膜</a>
        </li>
        <li>
            <a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + "Form/Product/ProductList.aspx?shop=0&cat=&dpcnt=15&img=2&sort=07&udns=2&fpfl=0&_key=初回お試し&pno=1") %>">初回お試し</a>
        </li>
    </ul>
    <div class="keywordsArea_search">
        <div class="keywordsArea_search--input">
            <input name="ctl00$BodyHeaderMain$tbSearchWord2" type="text" maxlength="168" id="ctl00_BodyHeaderMain_tbSearchWord2" placeholder="商品名、キーワードで検索">
        </div>
        <div class="keywordsArea_search--btn">
            <a id="searchBtnArea">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/commons/icn_search.png" />
            </a>
        </div>
    </div>
</section>

<section class="bnrArea_01">
    <a href="">
        <picture>
            <source media="(max-width: 769px)" srcset="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/bnr_01_sp.jpg">
            <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/bnr_01.jpg" alt="">
        </picture>
    </a>
</section>

<script>
$(function(){    
    $("#searchBtnArea").click(function () {
        $("#ctl00_BodyHeaderMain_tbSearchWord").attr("value", $("#ctl00_BodyHeaderMain_tbSearchWord2").val());
        __doPostBack('ctl00$BodyHeaderMain$lbSearch', '');
    });
    $("#ctl00_BodyHeaderMain_tbSearchWord2").keypress(function(e){
        if(e.which == 13){
            $("#searchBtnArea").click();
        }
    });
});
</script>
<%-- △編集可能領域△ --%>
