﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP Brand" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="brandsArea">
    <h2>
        OTHER BRANDS
    </h2>
    <div class="brandsArea_box">
        <div class="brandsArea_box--list">
            <a href="">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/brand_bea.jpg" />
            </a>
        </div>
        <div class="brandsArea_box--list">
            <a href="">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/brand_damai.jpg" />
            </a>
        </div>
        <div class="brandsArea_box--list">
            <a href="">
                <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/brand_solace.jpg" />
            </a>
        </div>
    </div>
</section>
<%-- △編集可能領域△ --%>
