﻿<%--
=========================================================================================================
  Module      : カスタムパーツテンプレート画面(CustomPartsTemplate.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" Inherits="BaseUserControl" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="TOP MV" %>
<%@ FileInfo LastChanged="MNCNewYork" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<section class="mvArea">
    <div class="mvArea_slide">
        <div class="mvArea_slide--list">
            <a href="" class="mvPic">
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_main_01.jpg" />
                </div>
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_sub_01.jpg" />
                </div>
            </a>
            <div class="mvTxt">
                <p class="mvTxt_ttl">
                    RENEWAL<br>
                    OPEN
                </p>
                <p class="mvTxt_txt">
                    価値ある情報を充実させ、<br>
                    もっと便利で快適に。
                </p>
                <a href="" class="mvTxt_btn">VIEW DETAILS</a>
            </div>
        </div>

        <div class="mvArea_slide--list">
            <a href="" class="mvPic">
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_main_02.jpg" />
                </div>
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_sub_02.jpg" />
                </div>
            </a>
            <div class="mvTxt">
                <p class="mvTxt_ttl">
                    Be HONEST<br>
                    Be BRAVE Be YOU<br>
                    The choice is SIMPLE
                </p>
                <p class="mvTxt_txt">
                    本当にいいもの、必要なもの、続けられるものを求めたら答えはシンプルでした。
                </p>
                <a href="" class="mvTxt_btn">VIEW DETAILS</a>
            </div>
        </div>
        <div class="mvArea_slide--list">
            <a href="" class="mvPic">
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_main_03.jpg" />
                </div>
                <div class="mvPic_list">
                    <img src="<%= Constants.PATH_ROOT %>Contents/ImagesPkg/top/mv_sub_03.jpg" />
                </div>
            </a>
            <div class="mvTxt">
                <p class="mvTxt_ttl">
                    WELCOME TO<br>
                    A NEW WORLD<br>
                    OF BEAUTY
                </p>
                <p class="mvTxt_txt">
                    はじめての方へ10日間のお試しセット<br>
                    ウェルカムパッケージ
                </p>
                <a href="" class="mvTxt_btn">VIEW DETAILS</a>
            </div>
        </div>
    </div>
</section>
<%-- △編集可能領域△ --%>
