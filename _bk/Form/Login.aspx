﻿<%--
=========================================================================================================
  Module      : ログイン画面(Login.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_Login, App_Web_login.aspx.b129f0c2" title="ログインページ" %>
<%@ Register Src="~/Form/Common/PaypalScriptsForm.ascx" TagPrefix="uc" TagName="PaypalScriptsForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="dvUserContents">
	<h2>ログインページ</h2>
	<div id="dvLogin" class="unit clearFix">
			<div id="dvMessages" runat="server" class="contentsInfo"><p><%= WebSanitizer.HtmlEncodeChangeToBr(this.ErrorMessage) %></p></div>
		<div id="dvLoginWrap">
			<div class="dvLoginLogin">
				<h3>ログイン</h3>
				<p>会員登録がお済みの方は以下よりログイン下さい。</p>
				<ul>
					<%if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED) { %>
					<li>
						<%: ReplaceTag("@@User.mail_addr.name@@") %><br />
						<asp:TextBox ID="tbLoginIdInMailAddr" Runat="server" CssClass="loginIdInMailAddr" MaxLength="256" Type="email"></asp:TextBox>
					</li>
					<%} else { %>
					<li><%: ReplaceTag("@@User.login_id.name@@") %><br />
						<asp:TextBox ID="tbLoginId" Runat="server" CssClass="loginId" MaxLength="15"></asp:TextBox></li>
					<%} %>
					<li><%: ReplaceTag("@@User.password.name@@") %><br />
						<asp:TextBox ID="tbPassword" Runat="server" TextMode="Password" autocomplete="off" CssClass="loginPass" MaxLength="15"></asp:TextBox></li>
					<li><asp:CheckBox ID="cbAutoCompleteLoginIdFlg" runat="server" Text="ログインIDを記憶する" /><br />（共有パソコンの場合は解除がオススメです）</li>
				</ul>
				<p>
				<asp:LinkButton ID="lbLogin" runat="server" onclick="lbLogin_Click" class="btn-org btn-large btn-org-blk">ログイン</asp:LinkButton>
				</p>
			</div>
			<div class="dvLoginReminder">
				<h3>パスワードを忘れた方</h3>
				<p><a href="<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_PASSWORD_REMINDER_INPUT %>" class="btn-org btn-large btn-org-blk">パスワードを忘れた方はこちら</a></p>
			</div>
		</div>
		<div class="dvLoginRegist">
			<h3>会員登録</h3>
				
			<p>会員登録がお済みで無い方はこちらから登録をお願いいたします。</p>
			<p><a href="<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_REGIST_REGULATION + "?" + Constants.REQUEST_KEY_NEXT_URL + "=" + Server.UrlEncode(Request[Constants.REQUEST_KEY_NEXT_URL]) %>" class="btn-org btn-large btn-org-blk">会員登録をする</a></p>
			<p><a href="<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_EASY_REGIST_INPUT %>" class="btn-org btn-large btn-org-blk">かんたん会員登録をする</a></p>
			<% if (Constants.COMMON_SOCIAL_LOGIN_ENABLED) { %>
			<div style="margin-top:30px;">
				<h3>ソーシャルログイン</h3>
				<div style="padding: 0 1em;">
					<ul>
						<% if (Constants.SOCIAL_LOGIN_ENABLED) { %>
						<%-- Facebook --%>
						<li>
							<a style="width: 185px;background-color: #305097;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 13px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
								href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
										w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Facebook,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
										true,
										Request.Url.Authority) %>">Facebookで新規登録/ログイン</a>
						</li>
						<%-- Twitter --%>
						<li>
							<a style="width: 185px;background-color: #1da1f2;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 13px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
								href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
										w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Twitter,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
										true,
										Request.Url.Authority) %>">Twitterで新規登録/ログイン</a>
						</li>
						<%-- Yahoo --%>
						<li>
							<a style="width: 185px;background-color: #FF0020;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 13px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
								href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
										w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Yahoo,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
										true,
										Request.Url.Authority) %>">Yahoo!で新規登録/ログイン</a>
						</li>
						<%-- LINE --%>
						<li>
							<a style="width: 185px;background-color: #00c300;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 13px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
								href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
										w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Line,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_LOGIN_CALLBACK,
										true,
										Request.Url.Authority) %>">LINEで新規登録/ログイン</a>
							<p style="margin:3px 0 4px;">※LINE連携時に友だち追加します</p>
						</li>
						<% } %>
						<%-- AmazonPay --%>
						<% if (Constants.AMAZON_LOGIN_OPTION_ENABLED) { %>
						<li>
							<%--▼▼Amazonログインボタンウィジェット▼▼--%>
							<div id="AmazonLoginButton" style="margin-bottom:5px;"></div>
							<%--▲▲Amazonログインボタンウィジェット▲▲--%>
						</li>
						<% } %>
						<%-- PayPal --%>
						<%if (Constants.PAYPAL_LOGINPAYMENT_ENABLED) {%>
						<li style="margin :10px 0 10px 0">
							<%
								ucPaypalScriptsForm.LogoDesign = "Login";
								ucPaypalScriptsForm.AuthCompleteActionControl = lbPayPalAuthComplete;
								ucPaypalScriptsForm.GetShippingAddress = (this.IsLoggedIn == false);
							%>
							<uc:PaypalScriptsForm ID="ucPaypalScriptsForm" runat="server" />
							<div>
							<div id="paypal-button" style="width: 236px;"></div>
							<% if (SessionManager.PayPalCooperationInfo != null) {%>
								※<%: SessionManager.PayPalCooperationInfo.AccountEMail %> 連携済
							<% } else {%>
								※PayPalで新規登録/ログインします
							<%} %>
							</div>
							<asp:LinkButton ID="lbPayPalAuthComplete" runat="server" OnClick="lbPayPalAuthComplete_Click"></asp:LinkButton>
						</li>
						<% } %>
						<%-- 楽天Connect --%>
						<% if (Constants.RAKUTEN_LOGIN_ENABLED) { %>
						<li>
							<asp:LinkButton ID="lbRakutenIdConnectRequestAuthForUserRegister" runat="server" OnClick="lbRakutenIdConnectRequestAuth_Click">
								<img src="https://checkout.rakuten.co.jp/p/common/img/btn_idconnect_03.gif" style="width: 237px;"/></asp:LinkButton>
							<p style="margin: 4px 0;">
								楽天会員のお客様は、楽天IDに登録している情報を利用して、<br/>
								「新規会員登録/ログイン」が可能です。
							</p>
						</li>
						<% } %>
					</ul>
				</div>
			</div>
			<% } %>
		</div>
	</div>
</div>
<%--▼▼Amazonウィジェット用スクリプト▼▼--%>
<% if (Constants.AMAZON_LOGIN_OPTION_ENABLED) { %>
<script type='text/javascript'>
	window.onAmazonLoginReady = function () {
		amazon.Login.setClientId('<%=Constants.PAYMENT_AMAZON_CLIENTID %>');
	};
	window.onAmazonPaymentsReady = function () {
		if ($('#AmazonLoginButton').length) showButton();
	};

	<%--Amazonボタン表示ウィジェット--%>
	function showButton() {
		var authRequest;
		OffAmazonPayments.Button("AmazonLoginButton", "<%=Constants.PAYMENT_AMAZON_SELLERID %>", {
			type: "LwA",
			color: "Gold",
			size: "large",
			authorization: function () {
				loginOptions = {
					scope: "payments:shipping_address payments:widget profile",
					popup: true
				};
				authRequest = amazon.Login.authorize(loginOptions, "<%=w2.App.Common.Amazon.Util.AmazonUtil.CreateCallbackUrl(Constants.PAGE_FRONT_AMAZON_LOGIN_CALLBACK, this.NextUrl)%>");
			},
			onError: function (error) {
				alert(error.getErrorMessage());
			}
		});
		$('#OffAmazonPaymentsWidgets0').css({ 'height': '44px', 'width': '237px' });
	}
</script>
<script async="async" type="text/javascript" charset="utf-8" src="<%=Constants.PAYMENT_AMAZON_WIDGETSSCRIPT %>"></script>
<% } %>
<%-- ▲▲Amazonウィジェット用スクリプト▲▲ --%>
</asp:Content>