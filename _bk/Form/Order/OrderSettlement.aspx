﻿<%--
=========================================================================================================
Module      : 注文決済画面(OrderSettlement.aspx)
･･･････････････････････････････････････････････････････････････････････････････････････････････････････
Copyright   : Copyright w2solution Co.,Ltd. 2011 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ page language="C#" masterpagefile="~/Form/Common/OrderPage.master" autoeventwireup="true" inherits="Form_Order_OrderSettlement, App_Web_ordersettlement.aspx.bf558b1b" title="注文決済ページ" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="最終更新者" %>

--%>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<%-- △編集可能領域△ --%>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table id="tblLayout">
<tr>
<td>
<%-- ▽レイアウト領域：レフトエリア▽ --%>
<%-- △レイアウト領域△ --%>
</td>
<td>
<div id="divTopArea">
<%-- ▽レイアウト領域：トップエリア▽ --%>
<%-- △レイアウト領域△ --%>
</div>

<%-- ▽編集可能領域：コンテンツ▽ --%>
<h2 class="ttlC">外部サイト決済</h2>
<p class="pdg_bottomA">
<span style="color:Red;">
外部サイト決済が必要な注文があります。<br />
「決済する」ボタンをクリックして決済を行ってください。
</span>
</p>

<asp:Repeater ID="rOrder" OnItemCommand="rOrder_ItemCommand" runat="server">
<ItemTemplate>
	<div class="orderSettlement">
	<div class="background">
	<div class="bottom">
	<h3>ご注文明細</h3>
	
	<div class="orderDetail">
	<div class="suborderDetail">
	<em>注文番号：&nbsp;<asp:Literal ID="lOrderId" Text="<%# ((CartObject)Container.DataItem).OrderId %>" runat="server"></asp:Literal></em>
	<div class="productList">
	<asp:Repeater DataSource="<%# ((CartObject)Container.DataItem).Items %>" runat="server">
	<ItemTemplate>
		<%-- 通常商品 --%>
		<div visible="<%# ((CartProduct)Container.DataItem).IsSetItem == false %>" runat="server">
		<div>
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
			<td class="name"><%# WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductName) %></td>
			<td class="quantity">数量&nbsp;&nbsp;&nbsp;<%# WebSanitizer.HtmlEncode(StringUtility.ToNumeric(((CartProduct)Container.DataItem).Count)) %></td>
			</tr>
			</table>
		</div>
		<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
		</div>
		<%-- セット商品 --%>
		<div visible="<%# (((CartProduct)Container.DataItem).IsSetItem) && (((CartProduct)Container.DataItem).ProductSetItemNo == 1) %>" runat="server">
		<div>
		<asp:Repeater DataSource="<%# (((CartProduct)Container.DataItem).ProductSet != null) ? ((CartProduct)Container.DataItem).ProductSet.Items : null %>" runat="server">
		<HeaderTemplate>
			<table cellpadding="0" cellspacing="0" width="100%">
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
			<td class="name"><%# WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) %> x <%# WebSanitizer.HtmlEncode(StringUtility.ToNumeric(((CartProduct)Container.DataItem).CountSingle)) %></td>
			<td rowspan="<%# ((CartProduct)Container.DataItem).ProductSet.Items.Count %>" visible="<%# ((CartProduct)Container.DataItem).ProductSetItemNo == 1 %>" class="quantity" runat="server">
				数量&nbsp;&nbsp;&nbsp;<%# WebSanitizer.HtmlEncode(GetProductSetCount((CartProduct)Container.DataItem))%></td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</table>
		</FooterTemplate>
		</asp:Repeater>
		</div>
		<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
		</div>
	</ItemTemplate>
	</asp:Repeater>
	</div>

	<div class="exec">
	合計金額：&nbsp;<%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceTotal) %><br />
	<div class="status" visible='<%# GetCanSettlement(((CartObject)Container.DataItem).OrderId) %>' runat="server">
		<span class="btn_settlement">
		<asp:LinkButton id="lbSettlement" runat="server" CommandName="settlement" CommandArgument="<%# Container.ItemIndex %>" ><small>決済する</small></asp:LinkButton></span>
	</div>
	<div class="status" visible='<%# GetSettlementStatus(((CartObject)Container.DataItem).OrderId) == "success" %>' runat="server">注文完了</div>
	<div class="status" visible='<%# GetSettlementStatus(((CartObject)Container.DataItem).OrderId) == "incomplete_docomo" %>' runat="server">注文完了</div>
	<div class="status" visible='<%# GetSettlementStatus(((CartObject)Container.DataItem).OrderId) == "failure" %>' runat="server">注文失敗</div>
	<div class="status" visible='<%# GetSettlementStatus(((CartObject)Container.DataItem).OrderId) == "cancel" %>' runat="server">キャンセル済</div>
	</div>
	<br class="clr" />
	</div>
	</div><!--orderDetail-->
	</div><!--bottom-->
	</div><!--background-->
	</div><!--orderSettlement-->

</ItemTemplate>
</asp:Repeater>
<%-- △編集可能領域△ --%>

<div id="divBottomArea">
<%-- ▽レイアウト領域：ボトムエリア▽ --%>
<%-- △レイアウト領域△ --%>
</div>

</td>
<td>
<%-- ▽レイアウト領域：ライトエリア▽ --%>
<%-- △レイアウト領域△ --%>
</td>
</tr>
</table>
</asp:Content>
