﻿<%--
=========================================================================================================
  Module      : カート一覧画面(CartList.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Register TagPrefix="uc" TagName="BodyAnnounceFreeShipping" Src="~/Form/Common/BodyAnnounceFreeShipping.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyRecommendFreeShipping" Src="~/Form/Common/BodyRecommendFreeShipping.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductRecommendByRecommendEngine" Src="~/Form/Common/Product/BodyProductRecommendByRecommendEngine.ascx" %>
<%@ Register TagPrefix="uc" TagName="Criteo" Src="~/Form/Common/Criteo.ascx" %>
<%@ Register Src="~/Form/Common/PayPalScriptsForm.ascx" TagPrefix="uc" TagName="PayPalScriptsForm" %>
<%@ Register TagPrefix="uc" TagName="AffiliateTag" Src="~/Form/Common/AffiliateTag.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/OrderPage.master" autoeventwireup="true" inherits="Form_Order_CartList, App_Web_cartlist.aspx.bf558b1b" title="ショッピングカートページ" %>
<%@ Import Namespace="w2.Domain.Coupon.Helper" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="ｗ２ユーザー" %>

--%>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<%-- △編集可能領域△ --%>
</asp:Content>
<%-- ▽▽Amazonペイメントを使う場合はウィジェットを配置するページは必ずSSLでなければいけない▽▽ --%>
<script runat="server">
	public override PageAccessTypes PageAccessType { get { return PageAccessTypes.Https; } }
</script>
<%-- △△Amazonペイメントを使う場合はウィジェットを配置するページは必ずSSLでなければいけない△△ --%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table id="tblLayout">
<tr>
<td>
<%-- ▽レイアウト領域：レフトエリア▽ --%>
<%-- △レイアウト領域△ --%>
</td>
<td>
<div id="divTopArea">
<%-- ▽レイアウト領域：トップエリア▽ --%>
<%-- △レイアウト領域△ --%>
</div>

<%-- ▽編集可能領域：コンテンツ▽ --%>
<script type="text/javascript">

	function bodyPageLoad() {
		if (Sys.WebForms == null) return;
		var isAsyncPostback = Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack();
		if (isAsyncPostback) {
			try {
				<%-- Ajax動いてカート内容操作後にウィジェット消える場合のエラーを回避 --%>
				if ($('#AmazonPayButton').length) showButton();
			}
			catch (e) { }
		}
	}
</script>
<%-- UPDATE PANEL開始 --%>
<asp:UpdatePanel ID="upUpdatePanel" runat="server">
<ContentTemplate>

<uc:AffiliateTag ID="AffiliateTagFree" Location="free" runat="server"/>

<p id="CartFlow"><img src="../../Contents/ImagesPkg/order/cart_step00.gif" alt="カート内容確認 " width="781" height="58" /></p>

<div class="btmbtn above cartstep">
<h2 class="ttlB">
	カート内容確認
</h2>
<ul style="display:<%= (this.CartList.Items.Count != 0) ? "block" : "none" %>">
	<%-- UPDATE PANELの外のイベントを呼び出す --%>
	<%-- ▼PayPalログインここから▼ --%>
	<%if (Constants.PAYPAL_LOGINPAYMENT_ENABLED) {%>
	<%if (this.DispPayPalShortCut) {%>
	<li style="float: left; width: 200px; margin: 0;">
		<%
			ucPaypalScriptsForm.LogoDesign = "Cart";
			ucPaypalScriptsForm.AuthCompleteActionControl = lbPayPalAuthComplete;
			ucPaypalScriptsForm.GetShippingAddress = (this.IsLoggedIn == false);
		%>
		<uc:PaypalScriptsForm ID="ucPaypalScriptsForm" runat="server" />
		<div id="paypal-button"></div>
		<div style="font-size: 9pt; text-align: center;margin: 3px">
		<%if (SessionManager.PayPalCooperationInfo != null) {%>
			<%: (SessionManager.PayPalCooperationInfo != null) ? SessionManager.PayPalCooperationInfo.AccountEMail : "" %> 連携済
		<%} %>
		<asp:LinkButton ID="lbPayPalAuthComplete" runat="server" OnClick="lbPayPalAuthComplete_Click"></asp:LinkButton>
		</div>
	</li>
	<%} %>
	<%} %>
	<%-- ▲PayPalログインここまで▲ --%>
	<li>
		<%if ( this.CanUseAmazonPayment()) { %>
		<%-- ▼▼Amazonお支払いボタンウィジェット▼▼ --%>
			<div id="AmazonPayButton" style="display:inline" title="Amazonアカウントでお支払いの場合はコチラから"></div>
		<%-- ▲▲Amazonお支払いボタンウィジェット▲▲ --%>
		<% } %>
	</li>
	<li><a href="<%= WebSanitizer.HtmlEncode(this.NextEvent) %>" class="btn btn-success">ご購入手続き</a></li>
</ul>
</div>

<%if (this.CartList.Items.Count != 0) {%>
	下の内容をご確認のうえ、「ご購入手続き」ボタンを押してください。
<%} else { %>
	カートに商品がありません。
<%} %>

<%-- 次へイベント用リンクボタン（イベント作成用。通常はUpdatePanel内部に設置する） --%>
<asp:LinkButton ID="lbNext" OnClick="lbNext_Click" ValidationGroup="OrderShipping" runat="server"></asp:LinkButton>

<div id="CartList">
<% if (string.IsNullOrEmpty(this.DispErrorMessage) == false) { %>
<span style="color:red"><%: this.DispErrorMessage %></span>
<% } %>
<p class="sum"><img src="../../Contents/ImagesPkg/cartlist/ttl_sum.gif" alt="総合計" width="48" height="16" /><strong><%: CurrencyManager.ToPrice(this.CartList.PriceCartListTotal) %></strong></p>

<%if (this.CartList.Items.Count != 0) {%>
<asp:Repeater id="rCartList" Runat="server" OnItemCommand="rCartList_ItemCommand">
<ItemTemplate>

	<%if (Constants.ProductOrderLimitKbn.ProductOrderLimitOn == Constants.PRODUCT_ORDER_LIMIT_KBN_CAN_BUY) { %>
	<div runat="server" visible="<%# ((CartObject)Container.DataItem).HasNotFirstTimeOrderIdList %>" style="text-align:left;position:relative;bottom:30px;">
	</div>
	<%} %>

	<%if (Constants.CARTCOPY_OPTION_ENABLED){ %>
	<%-- ▼カート削除完了メッセージ▼ --%>
	<div runat="server" visible="<%# ((CartObject)Container.DataItem).IsCartDeleteCompleteMesseges %>" style="text-align:left;position:relative;bottom:30px;">
		<span>カートの削除が完了しました。</span>
	</div>
	<%-- ▲カート削除完了メッセージ▲ --%>
	<%} %>
	
<%if (this.HasOrderCombinedReturned) { %>
	<%-- ▼注文同梱解除メッセージ▼ --%>
	<div runat="server" style="text-align:left;position:relative;bottom:30px;">
		<span>注文同梱を解除しました。</span>
	</div>
	<%-- ▲注文同梱解除メッセージ▲ --%>
<% } %>

	<div class="productList">
	<div class="background">

	<%if (Constants.CARTCOPY_OPTION_ENABLED){ %>
	<%-- ▼カートコピー完了メッセージ▼ --%>
	<div runat="server" visible="<%# ((CartObject)Container.DataItem).IsCartCopyCompleteMesseges %>" style="text-align:right">
		<span>カートのコピーが完了しました。</span>
	</div>
	<%-- ▲カートコピー完了メッセージ▲ --%>
	<%} %>

	<h3>カート番号 <%# Container.ItemIndex + 1 %><%# WebSanitizer.HtmlEncode(DispCartDecolationString(Container.DataItem, "（ギフト）", "（デジタルコンテンツ）")) %>のご注文内容<%if (Constants.CARTCOPY_OPTION_ENABLED){ %><span style="float:right"><asp:LinkButton ID="lbCopyCart" runat="server" Text="カートコピー" CommandArgument="<%# Container.ItemIndex %>" OnClick="lbCopyCart_Click" style="color:white;"></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbDeleteCart" runat="server" Text="カート削除" CommandArgument="<%# Container.ItemIndex %>" OnClick="lbDeleteCart_Click" style="color:white"></asp:LinkButton></span><%} %></h3>
	<div class="list" style="background-color: #f1f1f1;">
	<p class="ttl">
		<span style="padding-left: 120px; padding-right: 203px;">商品名</span>
		<span style="padding-left: 40px;padding-right: 24px;">単価（税込）</span>
		<span style="padding-left: 30px;padding-right: 24px;">注文数</span>
		<span style="padding-left: 10px;padding-right: 18px;">消費税率</span>
		<span style="padding-left: 10px;padding-right: 24px;">小計（税込）</span>
	</p>
	<asp:Repeater id="rCart" runat="server" DataSource='<%# (CartObject)Container.DataItem %>' OnItemCommand="rCartList_ItemCommand">
	<ItemTemplate>
		<%-- 通常商品 --%>
		<div class="product" style="background-color: white;" visible="<%# ((CartProduct)Container.DataItem).IsSetItem == false && ((CartProduct)Container.DataItem).QuantitiyUnallocatedToSet != 0 %>" runat="server">
		<%-- 隠し値 --%>
		<asp:HiddenField ID="hfShopId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ShopId %>" />
		<asp:HiddenField ID="hfProductId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductId %>" />
		<asp:HiddenField ID="hfVariationId" runat="server" Value="<%# ((CartProduct)Container.DataItem).VariationId %>" />
		<asp:HiddenField ID="hfIsFixedPurchase" runat="server" Value="<%# ((CartProduct)Container.DataItem).IsFixedPurchase %>" />
		<asp:HiddenField ID="hfAddCartKbn" runat="server" Value="<%# ((CartProduct)Container.DataItem).AddCartKbn %>" />
		<asp:HiddenField ID="hfProductSaleId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductSaleId %>" />
		<asp:HiddenField ID="hfProductOptionValue" runat="server" Value='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList.GetDisplayProductOptionSettingSelectValues() %>' />
		<asp:HiddenField ID="hfUnallocatedQuantity" runat="server" Value='<%# ((CartProduct)Container.DataItem).QuantitiyUnallocatedToSet %>' />
		<div>
		<dl class="name">
		<dt>
			<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
			<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" /></a>
			<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false %>" />
		</dt>
		<dd style="padding-top: 25px">
			<span>
				<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
					<%# WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) %></a>
				<%# (((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false) ? WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) : "" %>
				<%# (((CartProduct)Container.DataItem).GetProductTag("tag_cart_product_message").Length != 0) ? "<p class=\"message\">" + WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).GetProductTag("tag_cart_product_message")) + "</p>" : "" %></span></dd>
		<dd visible='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList.IsSelectedProductOptionValueAll %>' runat="server">
			<asp:Repeater ID="rProductOptionSettings" DataSource='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList %>' runat="server">
				<ItemTemplate>
					<%# WebSanitizer.HtmlEncode(((ProductOptionSetting)Container.DataItem).GetDisplayProductOptionSettingSelectValue()) %>
					<%# (((ProductOptionSetting)Container.DataItem).GetDisplayProductOptionSettingSelectValue() != "") ? "<br />" : "" %>
				</ItemTemplate>
			</asp:Repeater>
		</dd>
		<dd>
			<asp:Repeater ID="rSetPromotion" DataSource="<%# GetSetPromotionByProduct((CartProduct)Container.DataItem) %>" runat="server">
				<ItemTemplate>
					<span class="setpromotion" visible='<%# ((SetPromotionModel)Container.DataItem).Url != "" %>' runat="server">
						「<a href="<%# WebSanitizer.HtmlEncode(Constants.PATH_ROOT + ((SetPromotionModel)Container.DataItem).Url) %>"><%# WebSanitizer.HtmlEncode(((SetPromotionModel)Container.DataItem).SetpromotionDispName) %></a>」対象商品
					</span>
					<span class="setpromotion" visible='<%# ((SetPromotionModel)Container.DataItem).Url == "" %>' runat="server">
						「<%# WebSanitizer.HtmlEncode(((SetPromotionModel)Container.DataItem).SetpromotionDispName) %>」対象商品
					</span>
				</ItemTemplate>
			</asp:Repeater>
		</dd>
		</dl>
		<p class="price"><%#: CurrencyManager.ToPrice(((CartProduct)Container.DataItem).Price) %> (<%#: this.ProductPriceTextPrefix %>)</p>
		<p class="quantity">
			<span visible='<%# IsChangeProductCount((CartObject)((RepeaterItem)Container.Parent.Parent).DataItem, (CartProduct)Container.DataItem) %>' runat="server">
			<asp:TextBox ID="tbProductCount" Runat="server" Text='<%# ((CartProduct)Container.DataItem).QuantitiyUnallocatedToSet %>' MaxLength="3"></asp:TextBox>
			</span>
			<span visible='<%# IsChangeProductCount((CartObject)((RepeaterItem)Container.Parent.Parent).DataItem, (CartProduct)Container.DataItem) == false %>' runat="server">
				<%#  StringUtility.ToNumeric(((CartProduct)Container.DataItem).QuantitiyUnallocatedToSet)%>
			</span>
		</p>
		<p class="taxRate"><%#: TaxCalculationUtility.GetTaxRateForDIsplay(((CartProduct)Container.DataItem).TaxRate) %>%</p>
			<p class="subtotal"><%#: CurrencyManager.ToPrice(((CartProduct)Container.DataItem).Price * ((CartProduct)Container.DataItem).QuantitiyUnallocatedToSet) %> (<%#: this.ProductPriceTextPrefix %>)</p>
		<p class="delete"><asp:LinkButton ID="lbDeleteProduct" CommandName="DeleteProduct" Runat="server">削除</asp:LinkButton></p>
		</div>
		<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
		<small class="fred pdg_leftA" visible="<%# this.ErrorMessages.HasMessages(((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex) %>" runat="server">
			<%# WebSanitizer.HtmlEncode(this.ErrorMessages.Get(((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex)) %>
		</small>
		</div><!--product-->
		
		<%-- セット商品 --%>
		<div class="product" visible="<%# (((CartProduct)Container.DataItem).IsSetItem) && (((CartProduct)Container.DataItem).ProductSetItemNo == 1) %>" runat="server">
		<%-- 隠し値 --%>
		<asp:HiddenField ID="hfIsSetItem" runat="server" Value="<%# ((CartProduct)Container.DataItem).IsSetItem %>" />
		<asp:HiddenField ID="hfProductSetId" runat="server" Value="<%# GetProductSetId((CartProduct)Container.DataItem) %>" />
		<asp:HiddenField ID="hfProductSetNo" runat="server" Value="<%# GetProductSetNo((CartProduct)Container.DataItem) %>" />
		<asp:HiddenField ID="hfProductSetItemNo" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductSetItemNo %>" />
		<div>
		<asp:Repeater id="rProductSet" DataSource="<%# (((CartProduct)Container.DataItem).ProductSet != null) ? ((CartProduct)Container.DataItem).ProductSet.Items : null %>" OnItemCommand="rCartList_ItemCommand" runat="server">
		<HeaderTemplate>
			<table cellpadding="0" cellspacing="0" width="950" summary="ショッピングカート">
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
			<td class="name">
			<dl>
			<dt>
				<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
					<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" /></a>
				<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false %>" />
			</dt>
			<dd>
				<span>
					<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
						<%# WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) %> x <%# WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).CountSingle) %></a>
					<%# (((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false) ? WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) + " x " + WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).CountSingle) : ""%>
					<%# (((CartProduct)Container.DataItem).GetProductTag("tag_cart_product_message").Length != 0) ? "<br/><p class=\"message\">" + WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).GetProductTag("tag_cart_product_message")) + "</p>" : "" %>
				</span>
			</dd>
			</dl>
			<p class="price"><%#: CurrencyManager.ToPrice(((CartProduct)Container.DataItem).Price) %> (<%#: this.ProductPriceTextPrefix %>)</p></td>
			<td visible="<%# (((CartProduct)Container.DataItem).ProductSetItemNo == 1) %>" rowspan="<%# (((CartProduct)Container.DataItem).ProductSet != null) ? ((CartProduct)Container.DataItem).ProductSet.Items.Count : 1 %>" class="quantity" runat="server">
				<asp:TextBox ID="tbProductSetCount" Runat="server" Text='<%# GetProductSetCount((CartProduct)Container.DataItem) %>' MaxLength="3" CssClass="orderCount"></asp:TextBox></td>
			<td class="taxRate" runat="server">
				<%#: TaxCalculationUtility.GetTaxRateForDIsplay(((CartProduct)Container.DataItem).TaxRate) %>%</td>
			<td visible="<%# (((CartProduct)Container.DataItem).ProductSetItemNo == 1) %>" rowspan="<%# (((CartProduct)Container.DataItem).ProductSet != null) ? ((CartProduct)Container.DataItem).ProductSet.Items.Count : 1 %>" class="subtotal" runat="server">
				<%#: CurrencyManager.ToPrice(GetProductSetPriceSubtotal((CartProduct)Container.DataItem)) %> (<%#: this.ProductPriceTextPrefix %>)</td>
			<td visible="<%# (((CartProduct)Container.DataItem).ProductSetItemNo == 1) %>" rowspan="<%# (((CartProduct)Container.DataItem).ProductSet != null) ? ((CartProduct)Container.DataItem).ProductSet.Items.Count : 1 %>" class="delete" runat="server">
				<asp:LinkButton ID="lbDeleteProductSet" CommandName="DeleteProductSet" CommandArgument='' Runat="server">削除</asp:LinkButton></td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</table>
		</FooterTemplate>
		</asp:Repeater>
		</div>
		<small class="fred pdg_leftA" visible="<%# this.ErrorMessages.HasMessages(((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex) %>" runat="server">
			<%# WebSanitizer.HtmlEncode(this.ErrorMessages.Get(((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex)) %>
		</small>
		</div><!--product-->
		
	</ItemTemplate>
	</asp:Repeater>
	<!-- ▽セットプロモーション商品▽ -->
	<asp:Repeater ID="rCartSetPromotion" DataSource="<%# ((CartObject)Container.DataItem).SetPromotions %>" runat="server">
	<ItemTemplate>
		<asp:HiddenField ID="hfCartSetPromotionNo" runat="server" Value="<%# ((CartSetPromotion)Container.DataItem).CartSetPromotionNo %>" />
		<div class="product">
			<div>
				<asp:Repeater ID="rCartSetPromotionItem" DataSource="<%# ((CartSetPromotion)Container.DataItem).Items %>" OnItemCommand="rCartList_ItemCommand" runat="server">
				<HeaderTemplate>
					<table cellpadding="0" cellspacing="0" summary="ショッピングカート">
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td class="name">
							<dl>
							<dt>
								<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
									<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" /></a>
								<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false %>" />
							</dt>
							<dd>
								<span>
									<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
										<%# WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName)%></a>
									<%# (((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false) ? WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) : "" %>
								</span>
							</dd>
							<dd visible='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList.IsSelectedProductOptionValueAll %>' runat="server">
							<asp:Repeater ID="rProductOptionSettings" DataSource='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList %>' runat="server">
								<ItemTemplate>
									<%# WebSanitizer.HtmlEncode(((ProductOptionSetting)Container.DataItem).GetDisplayProductOptionSettingSelectValue()) %>
									<%# (((ProductOptionSetting)Container.DataItem).GetDisplayProductOptionSettingSelectValue() != "") ? "<br />" : "" %>
								</ItemTemplate>
							</asp:Repeater>
							</dd>
							</dl>
							<p class="price"><%#: CurrencyManager.ToPrice(((CartProduct)Container.DataItem).Price) %> (<%#: this.ProductPriceTextPrefix %>)</p>
						</td>
						<td class="quantity">
							<span visible="<%# ((CartObject)((RepeaterItem)Container.Parent.Parent.Parent.Parent).DataItem).IsGift == false %>" runat="server">
								<asp:TextBox ID="tbSetPromotionItemCount" Runat="server" Text="<%# ((CartProduct)Container.DataItem).QuantityAllocatedToSet[((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).CartSetPromotionNo] %>" MaxLength="3" CssClass="orderCount"></asp:TextBox><br />
								<small class="fred" visible="<%# this.ErrorMessages.HasMessages(((RepeaterItem)Container.Parent.Parent.Parent.Parent).ItemIndex, ((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex) %>" runat="server">
									<%# WebSanitizer.HtmlEncode(this.ErrorMessages.Get(((RepeaterItem)Container.Parent.Parent.Parent.Parent).ItemIndex, ((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex)) %>
								</small>
							</span>
							<span visible="<%# ((CartObject)((RepeaterItem)Container.Parent.Parent.Parent.Parent).DataItem).IsGift %>" runat="server">
								<%# StringUtility.ToNumeric(((CartProduct)Container.DataItem).QuantityAllocatedToSet[((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).CartSetPromotionNo]) %>
							</span>
						</td>
						<td class="quantity">
							<%# WebSanitizer.HtmlEncode(TaxCalculationUtility.GetTaxRateForDIsplay(((CartProduct)Container.DataItem).TaxRate)) %>%
						</td>
						<td visible="<%# (Container.ItemIndex == 0) %>" rowspan="<%# ((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).Items.Count %>" class="subtotal" runat="server">
							<span visible="<%# ((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).IsDiscountTypeProductDiscount %>" runat="server">
								<strike><%#: CurrencyManager.ToPrice(((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).UndiscountedProductSubtotal) %> (税込)</strike><br />
							</span>
							<%#: CurrencyManager.ToPrice(((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).UndiscountedProductSubtotal - ((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).ProductDiscountAmount) %> (税込)<br />
							<%# WebSanitizer.HtmlEncode(((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).SetpromotionDispName) %>
						</td>
						<td class="delete">
							<asp:LinkButton ID="lbDeleteProduct" CommandName="DeleteProduct" CommandArgument='' Runat="server">削除</asp:LinkButton>
							<%-- 隠し値 --%>
							<asp:HiddenField ID="hfShopId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ShopId %>" />
							<asp:HiddenField ID="hfProductId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductId %>" />
							<asp:HiddenField ID="hfVariationId" runat="server" Value="<%# ((CartProduct)Container.DataItem).VariationId %>" />
							<asp:HiddenField ID="hfIsFixedPurchase" runat="server" Value="<%# ((CartProduct)Container.DataItem).IsFixedPurchase %>" />
							<asp:HiddenField ID="hfAddCartKbn" runat="server" Value="<%# ((CartProduct)Container.DataItem).AddCartKbn %>" />
							<asp:HiddenField ID="hfProductSaleId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductSaleId %>" />
							<asp:HiddenField ID="hfProductOptionValue" runat="server" Value='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList.GetDisplayProductOptionSettingSelectValues() %>' />
							<asp:HiddenField ID="hfAllocatedQuantity" runat="server" Value='<%# ((CartProduct)Container.DataItem).QuantityAllocatedToSet[((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).CartSetPromotionNo] %>' />
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					</table>
				</FooterTemplate>
				</asp:Repeater>
			</div>
		</div>
	</ItemTemplate>
	</asp:Repeater>
	<!-- △セットプロモーション商品△ -->

	<%-- ▽ノベルティ▽ --%>
	<asp:Repeater ID="rNoveltyList" runat="server" DataSource="<%# GetCartNovelty(((CartObject)Container.DataItem).CartId) %>" Visible="<%# GetCartNovelty(((CartObject)Container.DataItem).CartId).Length != 0 %>">
		<HeaderTemplate>
		</HeaderTemplate>
		<ItemTemplate>
			<div class="novelty clearFix">
			<h4 class="title">
				<%# WebSanitizer.HtmlEncode(((CartNovelty)Container.DataItem).NoveltyDispName) %>を追加してください。
			</h4>
			<p runat="server" visible="<%#((CartNovelty)Container.DataItem).GrantItemList.Length == 0 %>">
				ただいま付与できるノベルティはございません。
			</p>
			<asp:Repeater ID="rNoveltyItem" runat="server" DataSource="<%# ((CartNovelty)Container.DataItem).GrantItemList %>" OnItemCommand="rCartList_ItemCommand">
				<ItemTemplate>
					<div class="plist">
						<p class="image">
							<w2c:ProductImage ProductMaster="<%# ((CartNoveltyGrantItem)Container.DataItem).ProductInfo %>" IsVariation="true" ImageSize="M" runat="server" />
						</p>
						<p class="name"><%# WebSanitizer.HtmlEncode(((CartNoveltyGrantItem)Container.DataItem).JointName) %></p>
						<p class="price"><%#: CurrencyManager.ToPrice(((CartNoveltyGrantItem)Container.DataItem).Price) %>(<%#: this.ProductPriceTextPrefix %>)</p>
						<p class="add">
							<asp:LinkButton ID="lbAddNovelty" runat="server" CommandName="AddNovelty" CommandArgument='<%#  string.Format("{0},{1}", ((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex) %>' class="btn btn-mini">カートに追加</asp:LinkButton>
						</p>
						<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1" /></p>
					</div><!--product-->
				</ItemTemplate>
			</asp:Repeater>
			</div><!--novelty-->
		</ItemTemplate>
		<FooterTemplate>
		</FooterTemplate>
	</asp:Repeater>
	<%-- △ノベルティ△ --%>

	</div><!--list-->

	<uc:BodyAnnounceFreeShipping runat="server" TargetCart="<%# ((CartObject)Container.DataItem) %>" />

	<div class="cartOrder">
	<div class="subcartOrder">
	
	<div class="pointBox" visible="<%# Constants.W2MP_POINT_OPTION_ENABLED && this.IsLoggedIn %>" runat="server">
	<div class="box">
	<p><img src="../../Contents/ImagesPkg/common/ttl_point.gif" alt="ポイントを使う" width="262" height="23" /></p>
	<div class="boxbtm">
	<div>
	<div>
	<dl>
	<dt>今回合計 <%# WebSanitizer.HtmlEncode(GetNumeric(this.LoginUserPointUsable))%> ポイントまでご利用いただけます
	<span>※1<%= Constants.CONST_UNIT_POINT_PT %> = <%: CurrencyManager.ToPrice(1m) %></span>
	</dt>
	<dd><asp:TextBox ID="tbOrderPointUse" Runat="server" Text="<%# ((CartObject)Container.DataItem).UsePoint %>" MaxLength="6"></asp:TextBox>&nbsp;&nbsp;<%= Constants.CONST_UNIT_POINT_PT %></dd>
	</dl>
	<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
	</div>
	<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
	</div>
	<span class="fred" visible="<%# this.ErrorMessages.HasMessages(Container.ItemIndex, CartErrorMessages.ErrorKbn.Point) %>" runat="server">
		<%# WebSanitizer.HtmlEncode(this.ErrorMessages.Get(Container.ItemIndex, CartErrorMessages.ErrorKbn.Point)) %>
	</span>
	</div><!--boxbtm-->
	</div><!--box-->
	</div><!--pointBox-->
	<div class="couponBox" visible="<%# Constants.W2MP_COUPON_OPTION_ENABLED %>" runat="server">
	<div class="box">
	<p><img src="../../Contents/ImagesPkg/common/ttl_coupon.gif" alt="クーポンを使う" width="262" height="23" /></p>
	<div id="divCouponInputMethod" runat="server"
		style="font-size: 10px; padding: 10px 10px 0px 10px; font-family: 'Lucida Grande','メイリオ',Meiryo,'Hiragino Kaku Gothic ProN', sans-serif; color: #333;">
		<asp:RadioButtonList runat="server" AutoPostBack="true" ID="rblCouponInputMethod"
			OnSelectedIndexChanged="rblCouponInputMethod_SelectedIndexChanged" OnDataBinding="rblCouponInputMethod_DataBinding"
			DataSource="<%# GetCouponInputMethod() %>" DataTextField="Text" DataValueField="Value" RepeatColumns="2" RepeatDirection="Horizontal"></asp:RadioButtonList>
	</div>
	<div class="boxbtm">
	<div>
	<div id="hgcCouponSelect" runat="server">
		<asp:DropDownList CssClass="input_border" style="width: 240px" ID="ddlCouponList" runat="server" DataTextField="Text" DataValueField="Value" OnTextChanged="ddlCouponList_TextChanged" AutoPostBack="true"></asp:DropDownList>
	</div>
	<div>
	<dl>
	<dl id="hgcCouponCodeInputArea" runat="server">
	<dt><span>クーポンコード</span></dt>
	<dd><asp:TextBox ID="tbCouponCode" runat="server" Text="<%# GetCouponCode(((CartObject)Container.DataItem).Coupon) %>" MaxLength="30" autocomplete="off"></asp:TextBox></dd>
	</dl>
	<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
	</div>
	<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
	</div>
	<span class="fred" visible="<%# this.ErrorMessages.HasMessages(Container.ItemIndex, CartErrorMessages.ErrorKbn.Coupon) %>" runat="server">
		<%# WebSanitizer.HtmlEncode(this.ErrorMessages.Get(Container.ItemIndex, CartErrorMessages.ErrorKbn.Coupon)) %>
	</span>
	<asp:LinkButton runat="server" ID="lbShowCouponBox" Text="クーポンBOX" 
		style="color: #ffffff !important; background-color: #000 !important;
																																																																																																border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25); text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25); display: inline-block;
																																																																																																padding: 4px 10px 4px; margin-bottom: 0; font-size: 13px; line-height: 18px; text-align: center; vertical-align: middle; cursor: pointer;
																																																																																																border: 1px solid #cccccc; border-radius: 4px; box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05); white-space: nowrap;"
		OnClick="lbShowCouponBox_Click" ></asp:LinkButton>
	</div><!--boxbtm-->
	</div><!--box-->
	<div runat="server" id="hgcCouponBox" style="z-index: 1; top: 0; left: 0; width: 100%; height: 120%; position: fixed; background-color: rgba(128, 128, 128, 0.75);"
		Visible='<%# ((CartObject)Container.DataItem).CouponBoxVisible %>'>
	<div id="hgcCouponList" style="width: 800px; height: 500px; top: 50%; left: 50%; text-align: center; border: 2px solid #aaa; background: #fff; position: fixed; z-index: 2; margin:-250px 0 0 -400px;">
	<h2 style="height: 20px; color: #fff; background-color: #000; font-size: 16px; padding: 3px 0px; border-bottom: solid 1px #ccc; ">クーポンBOX</h2>
	<div style="height: 400px; overflow: auto;">
	<asp:Repeater ID="rCouponList" ItemType="UserCouponDetailInfo" Runat="server" DataSource="<%# GetUsableCoupons((CartObject)Container.DataItem) %>">
		<HeaderTemplate>
		<table>
			<tr>
				<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:150px;">クーポンコード</th>
				<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:230px;">クーポン名</th>
				<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:100px;">割引金額<br />/割引率</th>
				<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:70px;">利用可能回数</th>
				<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:350px;">有効期限</th>
				<th style="border-bottom-style:solid; border-bottom-width:1px; background-color:#ececec; padding:10px; text-align:center;width:100px;"></th>
			</tr>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:150px; background-color: white;">
					<%#: Item.CouponCode %><br />
					<asp:HiddenField runat="server" ID="hfCouponBoxCouponCode" Value="<%# Item.CouponCode %>" />
				</td>
				<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:230px; background-color: white;"
					title="<%#: Item.CouponDispDiscription %>">
					<%#: Item.CouponDispName %>
				</td>
				<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:70px; background-color: white;">
					<%#: (StringUtility.ToEmpty(Item.DiscountPrice) != "")
							? CurrencyManager.ToPrice(Item.DiscountPrice)
							: (StringUtility.ToEmpty(Item.DiscountRate) != "")
								? StringUtility.ToEmpty(Item.DiscountRate) + "%"
								: "-" %>
				</td>
				<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:70px; background-color: white;">
					<%#: GetCouponCount(Item) %>
				</td>
				<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:350px; background-color: white;">
					<%#: DateTimeUtility.ToStringFromRegion(Item.ExpireEnd, DateTimeUtility.FormatType.LongDateHourMinute1Letter) %>
				</td>
				<td style="border-bottom-style:solid; border-bottom-width:1px; padding:10px 8px; text-align:left; text-align:center;width:100px; background-color: white;">
					<asp:LinkButton runat="server" id="lbCouponSelect" OnClick="lbCouponSelect_Click" style="color: #ffffff !important; background-color: #000 !important;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25); text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25); display: inline-block;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														padding: 4px 10px 4px; margin-bottom: 0; font-size: 13px; line-height: 18px; text-align: center; vertical-align: middle; cursor: pointer;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														border: 1px solid #cccccc; border-radius: 4px; box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05); white-space: nowrap;">このクーポンを使う</asp:LinkButton>
				</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</table>
		</FooterTemplate>
	</asp:Repeater>
	</div>
	<div style="width: 100%; height: 50px; display: block; z-index: 3">
		<asp:LinkButton ID="lbCouponBoxClose" OnClick="lbCouponBoxClose_Click" runat="server"
			style="padding: 8px 12px; font-size: 14px; color: #333; text-decoration: none; border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																					display: inline-block; line-height: 18px; color: #333333; text-align: center; vertical-align: middle; border-radius: 5px; cursor: pointer; background-color: #f5f5f5;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																					border: 1px solid #cccccc; box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05); text-decoration: none; background-image: none; margin: 5px auto">クーポンを利用しない</asp:LinkButton>
	</div>
	</div>
	</div>
	</div><!--couponBox-->

	<div class="priceList">
	<div>
	<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
	<dt>小計(<%#: this.ProductPriceTextPrefix %>)</dt>
	<dd><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceSubtotal) %></dd>
	</dl>
	<%if (this.ProductIncludedTaxFlg == false) { %>
		<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
			<dt>消費税額</dt>
			<dd><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceSubtotalTax) %></dd>
		</dl>
	<%} %>
	<%-- セットプロモーション(商品割引) --%>
	<asp:Repeater DataSource="<%# ((CartObject)Container.DataItem).SetPromotions %>" runat="server">
	<ItemTemplate>
		<span visible="<%# ((CartSetPromotion)Container.DataItem).IsDiscountTypeProductDiscount %>" runat="server">
		<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
		<dt><%# WebSanitizer.HtmlEncode(((CartSetPromotion)Container.DataItem).SetpromotionDispName) %></dt>
		<dd class='<%# (((CartSetPromotion)Container.DataItem).ProductDiscountAmount > 0) ? "minus" : "" %>'><%# (((CartSetPromotion)Container.DataItem).ProductDiscountAmount > 0) ? "-" : ""%><%#: CurrencyManager.ToPrice(((CartSetPromotion)Container.DataItem).ProductDiscountAmount) %></dd>
		</dl>
		</span>
	</ItemTemplate>
	</asp:Repeater>
	<%if (Constants.MEMBER_RANK_OPTION_ENABLED && this.IsLoggedIn){ %>
	<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
	<dt>会員ランク割引額</dt>
	<dd class='<%# (((CartObject)Container.DataItem).MemberRankDiscount > 0) ? "minus" : "" %>'><%# (((CartObject)Container.DataItem).MemberRankDiscount > 0) ? "-" : ""%><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).MemberRankDiscount * ((((CartObject)Container.DataItem).MemberRankDiscount < 0) ? -1 : 1)) %></dd>
	</dl>
	<%} %>
	<%if (Constants.MEMBER_RANK_OPTION_ENABLED && Constants.FIXEDPURCHASE_OPTION_ENABLED && this.IsLoggedIn){ %>
	<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
	<dt>定期会員割引額</dt>
	<dd class='<%# (((CartObject)Container.DataItem).FixedPurchaseMemberDiscountAmount > 0) ? "minus" : "" %>'><%# (((CartObject)Container.DataItem).FixedPurchaseMemberDiscountAmount > 0) ? "-" : "" %><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).FixedPurchaseMemberDiscountAmount * ((((CartObject)Container.DataItem).FixedPurchaseMemberDiscountAmount < 0) ? -1 : 1)) %></dd>
	</dl>
	<%} %>
	<%if (Constants.FIXEDPURCHASE_OPTION_ENABLED){ %>
	<span runat="server" visible="<%# (((CartObject)Container.DataItem).HasFixedPurchase) %>">
	<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
	<dt>定期購入割引額</dt>
	<dd class='<%# (((CartObject)Container.DataItem).FixedPurchaseDiscount > 0) ? "minus" : "" %>'><%#: (((CartObject)Container.DataItem).FixedPurchaseDiscount > 0) ? "-" : "" %><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).FixedPurchaseDiscount * ((((CartObject)Container.DataItem).FixedPurchaseDiscount < 0) ? -1 : 1)) %></dd>
	</dl>
	</span>
	<%} %>
	<%if (Constants.W2MP_COUPON_OPTION_ENABLED){ %>
	<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
	<dt>クーポン割引額</dt>
	<dd class='<%# (((CartObject)Container.DataItem).UseCouponPrice > 0) ? "minus" : "" %>'><%# (((CartObject)Container.DataItem).UseCouponPrice > 0) ? "-" : "" %><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).UseCouponPrice * ((((CartObject)Container.DataItem).UseCouponPrice < 0) ? -1 : 1)) %></dd>
	</dl>
	<%} %>
	<%if (Constants.W2MP_POINT_OPTION_ENABLED && this.IsLoggedIn){ %>
	<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
	<dt>ポイント利用額</dt>
	<dd class='<%# (((CartObject)Container.DataItem).UsePointPrice > 0) ? "minus" : "" %>'><%# (((CartObject)Container.DataItem).UsePointPrice > 0) ? "-" : "" %><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).UsePointPrice * ((((CartObject)Container.DataItem).UsePointPrice < 0) ? -1 : 1)) %></dd>
	</dl>
	<%} %>
	<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
	<dt>配送料金</dt>
	<dd runat="server" style='<%# (((CartObject)Container.DataItem).ShippingPriceSeparateEstimateFlg) ? "display:none;" : ""%>'>
		<%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceShipping) %></dd>
	<dd runat="server" style='<%# (((CartObject)Container.DataItem).ShippingPriceSeparateEstimateFlg == false) ? "display:none;" : ""%>'>
		<%# WebSanitizer.HtmlEncode(((CartObject)Container.DataItem).ShippingPriceSeparateEstimateMessage)%></dd>
	</dl>
	<%-- セットプロモーション(配送料割引) --%>
	<asp:Repeater DataSource="<%# ((CartObject)Container.DataItem).SetPromotions %>" runat="server">
	<ItemTemplate>
		<span visible="<%# ((CartSetPromotion)Container.DataItem).IsDiscountTypeShippingChargeFree %>" runat="server">
		<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
		<dt><%# WebSanitizer.HtmlEncode(((CartSetPromotion)Container.DataItem).SetpromotionDispName) %>(送料割引)</dt>
		<dd class='<%# (((CartSetPromotion)Container.DataItem).ShippingChargeDiscountAmount > 0) ? "minus" : "" %>'><%# (((CartSetPromotion)Container.DataItem).ShippingChargeDiscountAmount > 0) ? "-" : ""%><%#: CurrencyManager.ToPrice(((CartSetPromotion)Container.DataItem).ShippingChargeDiscountAmount) %></dd>
		</dl>
		</span>
	</ItemTemplate>
	</asp:Repeater>
	</dl>
	</div>
	<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
	<div>
	<dl class="result">
	<dt>合計(税込)</dt>
	<dd><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceCartTotalWithoutPaymentPrice) %></dd>
	</dl>
	</div>
	</div><!--priceList-->
	<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
	</div><!--subcartOrder-->
	</div><!--cartOrder-->
	</div><!--background-->
	</div><!--productList-->
	
	<%-- 隠し値：カートID --%>
	<asp:HiddenField ID="hfCartId" runat="server" Value="<%# ((CartObject)Container.DataItem).CartId %>" />
	<%-- 隠し再計算ボタン --%>
	<asp:LinkButton id="lbRecalculateCart" runat="server" CommandArgument="<%# Container.ItemIndex %>" onclick="lbRecalculate_Click"></asp:LinkButton>
</ItemTemplate>
</asp:Repeater>
<%} %>

<%if (this.CartList.Items.Count != 0) {%>
<p class="sum"><img src="../../Contents/ImagesPkg/cartlist/ttl_sum.gif" alt="総合計" width="48" height="16" /><strong><%: CurrencyManager.ToPrice(this.CartList.PriceCartListTotal) %></strong></p>
<%} %>

<div class="btmbtn below">
<ul>
	<li><a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>" class="btn btn-large btn-gry">買い物を続ける</a></li>
	<%if (this.CartList.Items.Count != 0) {%>
	<%-- UPDATE PANELの外のイベントを呼び出す --%>
	<li><a href="<%= WebSanitizer.HtmlEncode(this.NextEvent) %>" class="btn btn-large btn-success">ご購入手続き</a></li>
	<%} %>
</ul>
<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
</div><!--btmbtn-->
</div>

</div><!--CartList-->

</ContentTemplate>
</asp:UpdatePanel>
<%-- UPDATE PANELここまで --%>
<div>
	<!-- シルバーエッグ連携時使用 -->
	<uc:BodyProductRecommendByRecommendEngine runat="server" RecommendCode="pc413" RecommendTitle="おすすめ商品一覧" MaxDispCount="6" RecommendProductId="<%# GetCartProductsForSilveregg() %>" DispCategoryId="" NotDispCategoryId="" NotDispRecommendProductId="" />
</div>
<%-- CRITEOタグ（引数：カート情報） --%>
<uc:Criteo ID="criteo" runat="server" Datas="<%# this.CartList %>" />

<%--▼▼Amazonウィジェット用スクリプト▼▼--%>
<script type="text/javascript">
	
	window.onAmazonLoginReady = function () {
		amazon.Login.setClientId('<%=Constants.PAYMENT_AMAZON_CLIENTID %>');
	};
	window.onAmazonPaymentsReady = function () {
		if ($('#AmazonPayButton').length) showButton();
	};

	<%-- Amazonボタン表示ウィジェット --%>
	function showButton() {
		var authRequest;
		OffAmazonPayments.Button("AmazonPayButton", "<%=Constants.PAYMENT_AMAZON_SELLERID %>", {
			type: "PwA",
			color: "Gold",
			size: "medium",
			authorization: function () {
				loginOptions = {
					scope: "payments:widget payments:shipping_address profile",
					popup: true
				};
				authRequest = amazon.Login.authorize(loginOptions, "<%=w2.App.Common.Amazon.Util.AmazonUtil.CreateCallbackUrl(Constants.PAGE_FRONT_AMAZON_ORDER_CALLBACK) %>");
			},
			onError: function (error) {
				alert(error.getErrorMessage());
			}
		});
	};
</script>
<script async="async" type="text/javascript" charset="utf-8" src="<%=Constants.PAYMENT_AMAZON_WIDGETSSCRIPT %>"></script>
<%-- ▲▲Amazonウィジェット用スクリプト▲▲ --%>

<%-- △編集可能領域△ --%>

<div id="divBottomArea">
<%-- ▽レイアウト領域：ボトムエリア▽ --%>
<%-- △レイアウト領域△ --%>
</div>

</td>
<td>
<%-- ▽レイアウト領域：ライトエリア▽ --%>
<%-- △レイアウト領域△ --%>
</td>
</tr>
</table>
</asp:Content>
