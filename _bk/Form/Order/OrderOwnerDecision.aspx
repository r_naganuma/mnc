﻿<%--
=========================================================================================================
  Module      : 注文者決定画面(OrderOwnerDecision.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ page language="C#" masterpagefile="~/Form/Common/OrderPage.master" autoeventwireup="true" inherits="Form_Order_OrderOwnerDecision, App_Web_orderownerdecision.aspx.bf558b1b" title="注文者決定ページ" maintainscrollpositiononpostback="true" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="最終更新者" %>

--%>
<%-- ▽▽Amazonペイメントを使う場合はウィジェットを配置するページは必ずSSLでなければいけない▽▽ --%>
<script runat="server">
	public override PageAccessTypes PageAccessType { get { return PageAccessTypes.Https; } }
</script>
<%-- △△Amazonペイメントを使う場合はウィジェットを配置するページは必ずSSLでなければいけない△△ --%>
<%@ Register TagPrefix="uc" TagName="PaypalScriptsForm" Src="~/Form/Common/PayPalScriptsForm.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<%-- △編集可能領域△ --%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table id="tblLayout">
<tr>
<td>
<%-- ▽レイアウト領域：レフトエリア▽ --%>
<%-- △レイアウト領域△ --%>
</td>
<td>
<div id="divTopArea">
<%-- ▽レイアウト領域：トップエリア▽ --%>
<%-- △レイアウト領域△ --%>
</div>

<%-- ▽編集可能領域：コンテンツ▽ --%>
<div id="dvUserBox" class="clearFix">
	<div id="dvUserContents">
		<h2>注文者の決定</h2>
		<div id="dvLogin" class="unit clearFix">
			<div id="dvLoginWrap">
				<div class="dvLoginLogin">
					<h3>会員の方</h3> 
					<p >ログインIDをお持ちの方は、こちらからログインを行ってください。</p>
					<div id="LoginBox">
					<div class="top">
					<div class="bottom">
					<div>
					<dl>
					<%if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED) { %>
					<dt>
						<%: ReplaceTag("@@User.mail_addr.name@@") %>
					</dt>
					<dd><asp:TextBox ID="tbLoginIdInMailAddr" runat="server" CssClass="input_widthC input_border" MaxLength="256" Type="email"></asp:TextBox></dd>
					<%} else { %>
					<dt>
						<%: ReplaceTag("@@User.login_id.name@@") %>
					</dt>
					<dd><asp:TextBox ID="tbLoginId" runat="server" CssClass="input_widthC input_border" MaxLength="15" Type="email"></asp:TextBox></dd>
					<%} %>
					</dl>
					<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
					</div>
					<div>
					<dl>
					<dt>
						<%: ReplaceTag("@@User.password.name@@") %>
					</dt>
					<dd><asp:TextBox ID="tbPassword" TextMode="Password" autocomplete="off" runat="server" CssClass="input_widthC input_border" MaxLength="15"></asp:TextBox></dd>
					</dl>
					<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
					</div>
					<small id="dLoginErrorMessage" class="fred" runat="server"></small>
					<span><asp:CheckBox ID="cbAutoCompleteLoginIdFlg" runat="server" Text="ログインIDを記憶する" /></span>
					<div class="alignR mb15"><asp:LinkButton ID="lbLogin" runat="server" onclick="lbLogin_Click" class="btn btn-large btn-success">ログイン</asp:LinkButton></div>
					<span><a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_PASSWORD_REMINDER_INPUT) %>">パスワードを忘れた方はこちら</a></span>
					</div><!--bottom-->
					</div><!--top-->
					</div><!--LoginBox-->
				</div><!--dvLoginLogin-->
			</div><!--dvLoginWrap-->
			<div class="dvLoginRegist">
				<h3>会員でない方</h3>
				<div>
				<%if (this.CartList.HasFixedPurchase) { %>
				<p>定期購入商品は会員様のみがご購入頂けるようになっております。</p>
				<p>ログインIDをお持ちでないお客様はこちらから会員登録を行ってください。</p>
				<% }else{ %>
				<p>会員登録がお済みで無い方はこちらから登録をお願いいたします。</p>
				<% } %>
				<div class="alignC" style="width:450px"><asp:LinkButton ID="lbUserRegist" runat="server" OnClick="lbUserRegist_Click" class="btn btn-large btn-inverse">新規会員登録</asp:LinkButton></div>
				</div>
				<p style="margin:3px 0 4px;"></p>
				<div class="alignC" style="width:450px">
					<asp:LinkButton ID="lbUserEasyRegist" runat="server" OnClick="lbUserEasyRegist_Click" class="btn-org btn-large btn-org-blk">かんたん会員登録する</asp:LinkButton>
				</div>
				<%if (this.CartList.HasFixedPurchase == false) { %>
				<div style="margin-top:30px;">
				<h3>会員登録せずに購入</h3>
				<p>会員でないお客様はこちらから購入者情報をご記入ください。</p>
				<div class="alignC" style="width:450px"><asp:LinkButton ID="lbOrderShipping" runat="server" 
						onclick="lbOrderShipping_Click" class="btn btn-large btn-success">ご購入手続き</asp:LinkButton></div>
				</div>
				</div>
				<%} %>
				<% if (Constants.COMMON_SOCIAL_LOGIN_ENABLED) { %>
				<br class="clr" />
				<div style="margin-top:20px;" class="dvSocialLoginCooperation">
				<h3>ソーシャルログイン</h3>
					<p>ソーシャルログイン連携によるログインは、こちらから行ってください。</p>
					<ul style="display:flex;margin-bottom:20px;flex-wrap:wrap">
						<% if (Constants.SOCIAL_LOGIN_ENABLED) { %>
						<%-- Facebook --%>
						<li style="padding: 0 0.5em;">
							<a style="width: 170px;background-color: #305097;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 12px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
								href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
										w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Facebook,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
										true,
										Request.Url.Authority) %>">Facebookで新規登録/ログイン</a>
						</li>
						<%-- Twitter --%>
						<li style="padding: 0 0.5em;">
							<a style="width: 170px;background-color: #1da1f2;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 12px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
								href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
										w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Twitter,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
										true,
										Request.Url.Authority) %>">Twitterで新規登録/ログイン</a>
						</li>
						<%-- Yahoo --%>
						<li style="padding: 0 0.5em;">
							<a style="width: 170px;background-color:#ff0020;border:none;border-radius:5px;color:white;padding:1em 2em;text-align:center;text-decoration:none;display: inline-block;font-size:12px;font-family:'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
								href="<%= w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
										w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Yahoo,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
										true,
										Request.Url.Authority) %>">Yahoo!で新規登録/ログイン</a>
						</li>
						<%-- LINE --%>
						<li style="padding: 0 0.5em;">
							<a style="width: 170px;background-color: #00c300;border: none;border-radius:5px;color: white;padding: 1em 2em;text-align: center;text-decoration: none;display: inline-block;font-size: 12px;font-family: 'ヒラギノ角ゴ Pro W3', 'Hiragino Kaku Gothic Pro', 'メイリオ', Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;"
								href="<%=w2.App.Common.User.SocialLogin.Util.SocialLoginUtil.GetAuthenticateUrl(
										w2.App.Common.User.SocialLogin.Helper.SocialLoginApiProviderType.Line,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
										Constants.PAGE_FRONT_SOCIAL_LOGIN_ORDER_CALLBACK,
										true,
										Request.Url.Authority) %>">LINEで新規登録/ログイン</a>
							<p style="margin:3px 0 4px;">※LINE連携時に友だち追加します</p>
						</li>
						<% } %>
						<%-- Amazon --%>
						<% if (this.IsVisibleAmazonPayButton || this.IsVisibleAmazonLoginButton) { %>
						<li style="padding: 0 0.5em;">
							<%-- ▼▼Amazonボタンウィジェット▼▼ --%>
							<div id="AmazonPayButton" style="margin-bottom:5px;"></div>
							<%-- ▲▲Amazonボタンウィジェット▲▲ --%>
						</li>
						<% } %>
						<%-- PayPal --%>
						<%if (Constants.PAYPAL_LOGINPAYMENT_ENABLED) {%>
							<li>
								<%
									ucPaypalScriptsForm.LogoDesign = "Login";
									ucPaypalScriptsForm.AuthCompleteActionControl = lbPayPalAuthComplete;
									ucPaypalScriptsForm.GetShippingAddress = (this.IsLoggedIn == false);
								%>
								<div style="width: 218px;margin: 0px 8px 0px 6px">
								<uc:PaypalScriptsForm ID="ucPaypalScriptsForm" runat="server" />
								<div id="paypal-button"></div>
								<% if (SessionManager.PayPalCooperationInfo != null) {%>
									※<%: SessionManager.PayPalCooperationInfo.AccountEMail %> 連携済
								<% } else {%>
									※PayPalで新規登録/ログインします<br />
								<%} %>
								</div>
								<asp:LinkButton ID="lbPayPalAuthComplete" runat="server" OnClick="lbPayPalAuthComplete_Click"></asp:LinkButton>
							</li>
						<% } %>
						<%-- 楽天Connect --%>
						<% if (Constants.RAKUTEN_LOGIN_ENABLED) { %>
							<li style="padding: 0 0.5em;">
								<asp:LinkButton ID="lbRakutenIdConnectRequestAuthForUserRegister" runat="server" OnClick="lbRakutenIdConnectRequestAuth_Click">
									<img src="https://checkout.rakuten.co.jp/p/common/img/btn_idconnect_03.gif" style="width: 218px;"/></asp:LinkButton>
								<p style="margin: 4px 0;">
									楽天会員のお客様は、楽天IDに登録している情報を利用して、<br/>
									「新規会員登録/ログイン」が可能です。
								</p>
							</li>
						<% } %>
					</ul>
				</div>
				<% } %>
			
		</div>
	</div>
</div>
<%--▼▼Amazonウィジェット用スクリプト▼▼--%>
<script type="text/javascript">
	
	window.onAmazonLoginReady = function () {
		amazon.Login.setClientId('<%=Constants.PAYMENT_AMAZON_CLIENTID %>');
	};
	window.onAmazonPaymentsReady = function () {
		if ($('#AmazonPayButton').length) showButton();
	};

	<%-- Amazonボタン表示ウィジェット --%>
	function showButton() {
		var authRequest;
		OffAmazonPayments.Button("AmazonPayButton", "<%=Constants.PAYMENT_AMAZON_SELLERID %>", {
			type: "<%= (this.IsVisibleAmazonPayButton) ? "PwA" : ((this.IsVisibleAmazonLoginButton) ? "LwA" : "") %>",
			color: "Gold",
			size: "large",
			authorization: function () {
				loginOptions = {
					scope: "payments:widget payments:shipping_address profile",
					popup: true,
					state: '<%= Request.RawUrl %>'
				};
				authRequest = amazon.Login.authorize(loginOptions, '<%= this.AmazonCallBackUrl %>');
			},
			onError: function (error) {
				alert(error.getErrorMessage());
			}
		});
		$('#OffAmazonPaymentsWidgets0').css({ 'height': '44px', 'width': '218px' });
	};
</script>
<script async="async" type="text/javascript" charset="utf-8" src="<%=Constants.PAYMENT_AMAZON_WIDGETSSCRIPT %>"></script>
<%-- ▲▲Amazonウィジェット用スクリプト▲▲ --%>
<%-- △編集可能領域△ --%>

<div id="divBottomArea">
<%-- ▽レイアウト領域：ボトムエリア▽ --%>
<%-- △レイアウト領域△ --%>
</div>

</td>
<td>
<%-- ▽レイアウト領域：ライトエリア▽ --%>
<%-- △レイアウト領域△ --%>
</td>
</tr>
</table>
</asp:Content>