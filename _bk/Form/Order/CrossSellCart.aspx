﻿<%--
=========================================================================================================
  Module      : クロスセルカート画面(CrossSellCart.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2010 All Rights Reserved.
=========================================================================================================
--%>
<%-- ▽ユーザーコントロール宣言領域▽ --%>
<%-- △ユーザーコントロール宣言領域△ --%>
<%@ Register TagPrefix="uc" TagName="BodyAnnounceFreeShipping" Src="~/Form/Common/BodyAnnounceFreeShipping.ascx" %>
<%@ Register TagPrefix="uc" TagName="BodyProductRecommendByRecommendEngine" Src="~/Form/Common/Product/BodyProductRecommendByRecommendEngine.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/OrderPage.master" autoeventwireup="true" inherits="Form_Order_CrossSellCart, App_Web_crosssellcart.aspx.bf558b1b" title="ショッピングカートページ" %>
<%@ Register TagPrefix="uc" TagName="PaypalScriptsForm" Src="~/Form/Common/PayPalScriptsForm.ascx" %>
<%@ Register TagPrefix="uc" TagName="AffiliateTag" Src="~/Form/Common/AffiliateTag.ascx" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
<%@ FileInfo LayoutName="Default" %><%@ FileInfo LastChanged="ｗ２ユーザー" %>

--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- ▽編集可能領域：HEAD追加部分▽ --%>
<%-- △編集可能領域△ --%>
</asp:Content>
<%-- ▽▽Amazonペイメントを使う場合はウィジェットを配置するページは必ずSSLでなければいけない▽▽ --%>
<script runat="server">
	public override PageAccessTypes PageAccessType { get { return PageAccessTypes.Https; } }
</script>
<%-- △△Amazonペイメントを使う場合はウィジェットを配置するページは必ずSSLでなければいけない△△ --%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table id="tblLayout">
<tr>
<td>
<%-- ▽レイアウト領域：レフトエリア▽ --%>
<%-- △レイアウト領域△ --%>
</td>
<td>
<div id="divTopArea">
<%-- ▽レイアウト領域：トップエリア▽ --%>
<%-- △レイアウト領域△ --%>
</div>

<%-- ▽編集可能領域：コンテンツ▽ --%>
<%-- 次へイベント用リンクボタン --%>
<asp:LinkButton ID="lbNext" OnClick="lbNext_Click" ValidationGroup="OrderPayment" runat="server"></asp:LinkButton>
<div class="main">
<div class="submain">

	<div class="column">
		<em class=""><a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT) %>" class="btn btn-large">買い物を続ける</a></em>
		<br class="clr" />
		<uc:BodyProductRecommendByRecommendEngine runat="server" RecommendCode="pc413" RecommendTitle="おすすめ商品一覧" MaxDispCount="4"
												RecommendProductId="<%# GetCartProductsForSilveregg() %>" DispCategoryId="" NotDispCategoryId="" NotDispRecommendProductId="" />
	</div><!--column-->

<%-- UPDATE PANEL開始 --%>
<asp:UpdatePanel runat="server">
<ContentTemplate>

<uc:AffiliateTag ID="AffiliateTagFree" Location="free" runat="server"/>

<div class="columnRight">
<h2>
	<img src="../../Contents/ImagesPkg/common/ttl_shopping_cart.gif" alt="ショッピングカート" width="141" height="16" />
	<div style="display:<%= (this.CartList.Items.Count != 0) ? "block" : "none" %>;">
	<%-- ▼PayPalログインここから▼ --%>
	<%if (Constants.PAYPAL_LOGINPAYMENT_ENABLED) {%>
	<%if (this.DispPayPalShortCut) {%>
		<div style="width: 190px; margin: 5px 5px 5px 150px;vertical-align: middle">
			<%
				ucPaypalScriptsForm.LogoDesign = "Cart";
				ucPaypalScriptsForm.AuthCompleteActionControl = lbPayPalAuthComplete;
				ucPaypalScriptsForm.GetShippingAddress = (this.IsLoggedIn == false);
			%>
			<uc:PaypalScriptsForm ID="ucPaypalScriptsForm" runat="server" />
			<div id="paypal-button"></div>
			<div style="font-size: 9pt;text-align: center">
				<%if (SessionManager.PayPalCooperationInfo != null) {%>
					<%: (SessionManager.PayPalCooperationInfo != null) ? SessionManager.PayPalCooperationInfo.AccountEMail : "" %> 連携済
				<%} %>
				<asp:LinkButton ID="lbPayPalAuthComplete" runat="server" OnClick="lbPayPalAuthComplete_Click"></asp:LinkButton>
			</div>
		</div>
	<%} %>
	<%} %>
	<%-- ▲PayPalログインここまで▲ --%>
	<%-- ▼▼Amazonお支払いボタンウィジェット▼▼ --%>
	<%if ( this.CanUseAmazonPayment()) { %>
		<div id="AmazonPayButton" style="margin: 5px 5px 5px 150px;" title="Amazonアカウントでお支払いの場合はコチラから"></div>
	<% } %>
	<%-- ▲▲Amazonお支払いボタンウィジェット▲▲ --%>
	<p class="right"><a href="<%= WebSanitizer.HtmlEncode(this.NextEvent) %>" class="btn btn-large btn-success">レジに進む</a></p>
	</div>
</h2>

<%if (this.CartList.Items.Count == 0) {%>
	カートに商品がありません。
<%} %>
<% if (string.IsNullOrEmpty(this.DispErrorMessage) == false) { %>
<span style="color:red;"><%: this.DispErrorMessage %></span>
<% } %>

<asp:Repeater id="rCartList" runat="server" OnItemCommand="rCartList_ItemCommand">
	<ItemTemplate>
		<%-- ▼カート情報▼ --%>
		<div class="shoppingCart">
			<div visible="<%# (Container.ItemIndex == 0) %>" runat="server">
				<div class="sumBox mrg_topA">
					<div class="subSumBoxB">
						<p><img src="../../Contents/ImagesPkg/common/ttl_sum.gif" alt="総合計" width="52" height="16" /><strong><%#: CurrencyManager.ToPrice(this.CartList.PriceCartListTotal) %></strong></p>
					</div>
				</div><!--sum-->
			</div>

			<div class="subCartList">
				<div class="bottom">
					<h3><div class="cartNo">カート番号<%# Container.ItemIndex + 1 %><%# WebSanitizer.HtmlEncode(DispCartDecolationString(Container.DataItem, "（ギフト）", "（デジタルコンテンツ）")) %></div>
						<div class="cartLink"><a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_CART_LIST) %>">カートを見る</a></div></h3>
					<div class="block">

						<asp:Repeater ID="rCart" DataSource="<%# ((CartObject)Container.DataItem).Items %>" runat="server" OnItemCommand="rCartList_ItemCommand">
							<ItemTemplate>
								<%-- 通常商品 --%>
								<div class="singleProduct" visible="<%# ((CartProduct)Container.DataItem).IsSetItem == false && ((CartProduct)Container.DataItem).QuantitiyUnallocatedToSet != 0 %>" runat="server">
									<%-- 隠し値 --%>
									<asp:HiddenField ID="hfShopId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ShopId %>" />
									<asp:HiddenField ID="hfProductId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductId %>" />
									<asp:HiddenField ID="hfVariationId" runat="server" Value="<%# ((CartProduct)Container.DataItem).VariationId %>" />
									<asp:HiddenField ID="hfIsFixedPurchase" runat="server" Value="<%# ((CartProduct)Container.DataItem).IsFixedPurchase %>" />
									<asp:HiddenField ID="hfAddCartKbn" runat="server" Value="<%# ((CartProduct)Container.DataItem).AddCartKbn %>" />
									<asp:HiddenField ID="hfProductSaleId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductSaleId %>" />
									<asp:HiddenField ID="hfProductOptionValue" runat="server" Value='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList.GetDisplayProductOptionSettingSelectValues() %>' />
									<asp:HiddenField ID="hfUnallocatedQuantity" runat="server" Value='<%# ((CartProduct)Container.DataItem).QuantitiyUnallocatedToSet %>' />
									<div>
										<dl>
											<dt>
												<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
													<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" /></a>
												<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false %>" />
											</dt>
											<dd>
												<strong>
													<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
														<%# WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) %></a>
													<%# (((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false) ? WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) : "" %>
												</strong>
												<%# (((CartProduct)Container.DataItem).GetProductTag("tag_cart_product_message").Length != 0) ? "<small>" + WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).GetProductTag("tag_cart_product_message")) + "</small>" : "" %>
												<p visible='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList.IsSelectedProductOptionValueAll %>' runat="server">
													<asp:Repeater ID="rProductOptionSettings" DataSource='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList %>' runat="server">
														<ItemTemplate>
															<%# (((ProductOptionSetting)Container.DataItem).GetDisplayProductOptionSettingSelectValue() != "") ? "<strong>" : "" %>
															<%# WebSanitizer.HtmlEncode(((ProductOptionSetting)Container.DataItem).GetDisplayProductOptionSettingSelectValue()) %>
															<%# (((ProductOptionSetting)Container.DataItem).GetDisplayProductOptionSettingSelectValue() != "") ? "</strong>" : "" %>
														</ItemTemplate>
													</asp:Repeater>
												</p>
												<p visible='<%# IsChangeProductCount((CartObject)((RepeaterItem)Container.Parent.Parent).DataItem, (CartProduct)Container.DataItem) %>' runat="server">数量：<asp:TextBox ID="tbProductCount" Runat="server" Text="<%# ((CartProduct)Container.DataItem).QuantitiyUnallocatedToSet %>" MaxLength="3"></asp:TextBox></p>
												<p visible='<%# IsChangeProductCount((CartObject)((RepeaterItem)Container.Parent.Parent).DataItem, (CartProduct)Container.DataItem) == false %>' runat="server">数量：<%# StringUtility.ToNumeric(((CartProduct)Container.DataItem).QuantitiyUnallocatedToSet) %></p>
												<p><%#: CurrencyManager.ToPrice(((CartProduct)Container.DataItem).Price) %> (<%#: this.ProductPriceTextPrefix %>)</p>
												<p class="fred" visible="<%# this.ErrorMessages.HasMessages(((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex) %>" runat="server">
													<%# WebSanitizer.HtmlEncode(this.ErrorMessages.Get(((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex)) %>
												</p>
											</dd>
										</dl>
										<dl>
											<p class="delete"><asp:LinkButton ID="lbDeleteProduct" CommandName="DeleteProduct" Runat="server">削除</asp:LinkButton></p>
										</dl>
									</div>
								</div><!--singleProduct-->

								<%-- セット商品 --%>
								<div class="multiProduct" visible="<%# (((CartProduct)Container.DataItem).IsSetItem) && (((CartProduct)Container.DataItem).ProductSetItemNo == 1) %>" runat="server">
									<%-- 隠し値 --%>
									<asp:HiddenField ID="hfIsSetItem" runat="server" Value="<%# ((CartProduct)Container.DataItem).IsSetItem %>" />
									<asp:HiddenField ID="hfProductSetId" runat="server" Value="<%# GetProductSetId((CartProduct)Container.DataItem) %>" />
									<asp:HiddenField ID="hfProductSetNo" runat="server" Value="<%# GetProductSetNo((CartProduct)Container.DataItem) %>" />
									<asp:HiddenField ID="hfProductSetItemNo" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductSetItemNo %>" />

									<asp:Repeater id="rProductSet" DataSource="<%# (((CartProduct)Container.DataItem).ProductSet != null) ? ((CartProduct)Container.DataItem).ProductSet.Items : null %>" runat="server">
										<ItemTemplate>
											<div>
											<dl>
												<dt>
													<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
														<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" /></a>
													<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false %>" />
												</dt>
												<dd>
													<strong>
														<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
															<%# WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) %></a>
														<%# (((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false) ? WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) : "" %>
													</strong>
												<%# (((CartProduct)Container.DataItem).GetProductTag("tag_cart_product_message").Length != 0) ? "<small>" + WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).GetProductTag("tag_cart_product_message")) + "</small>" : "" %>
													<p><%#: CurrencyManager.ToPrice(((CartProduct)Container.DataItem).Price) %> (<%#: this.ProductPriceTextPrefix %>)&nbsp;&nbsp;x&nbsp;&nbsp;<%# WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).CountSingle) %></p></dd>
											</dl>
											</div>
											<table visible="<%# (((CartProduct)Container.DataItem).ProductSetItemNo == ((CartProduct)Container.DataItem).ProductSet.Items.Count) %>" width="297" cellpadding="0" cellspacing="0" class="clr" runat="server">
												<tr>
													<th width="38">セット：</th>
													<th width="50"><asp:TextBox ID="TextBox1" Runat="server" Text="<%# GetProductSetCount((CartProduct)Container.DataItem) %>" MaxLength="3"></asp:TextBox></th>
													<th width="146"><%#: CurrencyManager.ToPrice(GetProductSetPriceSubtotal((CartProduct)Container.DataItem)) %> (<%#: this.ProductPriceTextPrefix %>)</th>
													<th visible="<%# (((CartProduct)Container.DataItem).ProductSetItemNo == 1) %>" rowspan="<%# (((CartProduct)Container.DataItem).ProductSet != null) ? ((CartProduct)Container.DataItem).ProductSet.Items.Count : 1 %>" class="delete" runat="server">
														<asp:LinkButton ID="lbDeleteProductSet" CommandName="DeleteProductSet" CommandArgument='' Runat="server">削除</asp:LinkButton></th>
													<td width="61"></td>
												</tr>
											</table>
										</ItemTemplate>
									</asp:Repeater>
								</div><!--multiProduct-->
							</ItemTemplate>
						</asp:Repeater>

						<%-- セットプロモーション商品 --%>
						<asp:Repeater ID="rCartSetPromotion" DataSource="<%# ((CartObject)Container.DataItem).SetPromotions %>" runat="server">
							<ItemTemplate>
								<asp:HiddenField ID="hfCartSetPromotionNo" runat="server" Value="<%# ((CartSetPromotion)Container.DataItem).CartSetPromotionNo %>" />
								<div class="multiProduct">
									<asp:Repeater ID="rCartSetPromotionItem" DataSource="<%# ((CartSetPromotion)Container.DataItem).Items %>" OnItemCommand="rCartList_ItemCommand" runat="server">
										<ItemTemplate>
											<%-- 隠し値 --%>
											<asp:HiddenField ID="hfShopId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ShopId %>" />
											<asp:HiddenField ID="hfProductId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductId %>" />
											<asp:HiddenField ID="hfVariationId" runat="server" Value="<%# ((CartProduct)Container.DataItem).VariationId %>" />
											<asp:HiddenField ID="hfIsFixedPurchase" runat="server" Value="<%# ((CartProduct)Container.DataItem).IsFixedPurchase %>" />
											<asp:HiddenField ID="hfAddCartKbn" runat="server" Value="<%# ((CartProduct)Container.DataItem).AddCartKbn %>" />
											<asp:HiddenField ID="hfProductSaleId" runat="server" Value="<%# ((CartProduct)Container.DataItem).ProductSaleId %>" />
											<asp:HiddenField ID="hfProductOptionValue" runat="server" Value='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList.GetDisplayProductOptionSettingSelectValues() %>' />
											<asp:HiddenField ID="hfAllocatedQuantity" runat="server" Value='<%# ((CartProduct)Container.DataItem).QuantityAllocatedToSet[((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).CartSetPromotionNo] %>' />
											<div>
												<dl>
													<dt>
														<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
															<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" /></a>
														<w2c:ProductImage ProductMaster="<%# Container.DataItem %>" ImageSize="M" runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false %>" />
													</dt>
													<dd>
														<strong>
															<a href='<%# WebSanitizer.UrlAttrHtmlEncode(((CartProduct)Container.DataItem).CreateProductDetailUrl()) %>' runat="server" Visible="<%# ((CartProduct)Container.DataItem).IsProductDetailLinkValid() %>">
																<%# WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) %></a>
															<%# (((CartProduct)Container.DataItem).IsProductDetailLinkValid() == false) ? WebSanitizer.HtmlEncode(((CartProduct)Container.DataItem).ProductJointName) : "" %>
														</strong>
														<p visible='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList.IsSelectedProductOptionValueAll %>' runat="server">
															<asp:Repeater ID="rProductOptionSettings" DataSource='<%# ((CartProduct)Container.DataItem).ProductOptionSettingList %>' runat="server">
																<ItemTemplate>
																<%# (((ProductOptionSetting)Container.DataItem).GetDisplayProductOptionSettingSelectValue() != "") ? "<strong>" : "" %>
																<%# WebSanitizer.HtmlEncode(((ProductOptionSetting)Container.DataItem).GetDisplayProductOptionSettingSelectValue()) %>
																<%# (((ProductOptionSetting)Container.DataItem).GetDisplayProductOptionSettingSelectValue() != "") ? "</strong>" : "" %>
																</ItemTemplate>
															</asp:Repeater>
														</p>
														<p visible="<%# ((CartObject)((RepeaterItem)Container.Parent.Parent.Parent.Parent).DataItem).IsGift == false %>" runat="server">
															数量：<asp:TextBox ID="tbSetPromotionItemCount" Runat="server" Text="<%# ((CartProduct)Container.DataItem).QuantityAllocatedToSet[((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).CartSetPromotionNo] %>" MaxLength="3"></asp:TextBox>
														</p>
														<p visible="<%# ((CartObject)((RepeaterItem)Container.Parent.Parent.Parent.Parent).DataItem).IsGift %>" runat="server">
															数量：<%#  StringUtility.ToNumeric(((CartProduct)Container.DataItem).QuantityAllocatedToSet[((CartSetPromotion)((RepeaterItem)Container.Parent.Parent).DataItem).CartSetPromotionNo]) %>
														</p>
														<p><%#: CurrencyManager.ToPrice(((CartProduct)Container.DataItem).Price) %> (<%#: this.ProductPriceTextPrefix %>)</p>
														<p class="fred" visible="<%# this.ErrorMessages.HasMessages(((RepeaterItem)Container.Parent.Parent.Parent.Parent).ItemIndex, ((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex) %>" runat="server">
															<%# WebSanitizer.HtmlEncode(this.ErrorMessages.Get(((RepeaterItem)Container.Parent.Parent.Parent.Parent).ItemIndex, ((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex)) %>
														</p>
													</dd>
												</dl>
												<dl>
													<p class="delete"><asp:LinkButton ID="lbDeleteProduct" CommandName="DeleteProduct" Runat="server">削除</asp:LinkButton></p>
												</dl>
											</div>
										</ItemTemplate>
									</asp:Repeater>
									<dl class="setpromotion">
										<dt><%# WebSanitizer.HtmlEncode(((CartSetPromotion)Container.DataItem).SetpromotionDispName) %></dt>
										<dd>
											<span visible="<%# ((CartSetPromotion)Container.DataItem).IsDiscountTypeProductDiscount %>" runat="server">
												<strike><%#: CurrencyManager.ToPrice(((CartSetPromotion)Container.DataItem).UndiscountedProductSubtotal) %> (<%#: this.ProductPriceTextPrefix %>)</strike><br />
											</span>
											<span><%#: CurrencyManager.ToPrice(((CartSetPromotion)Container.DataItem).UndiscountedProductSubtotal - ((CartSetPromotion)Container.DataItem).ProductDiscountAmount) %> (<%#: this.ProductPriceTextPrefix %>)</span>
											<%#: CurrencyManager.ToPrice(((CartSetPromotion)Container.DataItem).UndiscountedProductSubtotal - ((CartSetPromotion)Container.DataItem).ProductDiscountAmount) %> (<%#: this.ProductPriceTextPrefix %>)
										</dd>
									</dl>
								</div>
							</ItemTemplate>
						</asp:Repeater>

						<%-- ▽ノベルティ▽ --%>
						<asp:Repeater ID="rNoveltyList" runat="server" DataSource="<%# GetCartNovelty(((CartObject)Container.DataItem).CartId) %>" Visible="<%# GetCartNovelty(((CartObject)Container.DataItem).CartId).Length != 0 %>">
							<HeaderTemplate>
								<div class="novelty clearFix">
							</HeaderTemplate>
							<ItemTemplate>
								<h4 class="title" runat="server">
									<%# WebSanitizer.HtmlEncode(((CartNovelty)Container.DataItem).NoveltyDispName)%>を追加してください。
								</h4>
								<p runat="server" visible="<%#((CartNovelty)Container.DataItem).GrantItemList.Length == 0 %>">
									ただいま付与できるノベルティはございません。
								</p>
								<asp:Repeater ID="rNoveltyItem" runat="server" DataSource="<%# ((CartNovelty)Container.DataItem).GrantItemList %>" OnItemCommand="rCartList_ItemCommand">
									<ItemTemplate>
										<div class="product">
											<p class="image" style="margin-right: 5px">
												<w2c:ProductImage ProductMaster="<%# ((CartNoveltyGrantItem)Container.DataItem).ProductInfo %>" IsVariation="true" ImageSize="M" runat="server" />
											</p>
											<p class="name"><%# WebSanitizer.HtmlEncode(((CartNoveltyGrantItem)Container.DataItem).JointName) %></p>
											<p class="price"><%#: CurrencyManager.ToPrice(((CartNoveltyGrantItem)Container.DataItem).Price) %>(<%#: this.ProductPriceTextPrefix %>)</p>
											<p class="add">
												<asp:LinkButton ID="lbAddNovelty" runat="server" CommandName="AddNovelty" CommandArgument='<%#  string.Format("{0},{1}", ((RepeaterItem)Container.Parent.Parent).ItemIndex, Container.ItemIndex) %>' class="btn btn-mini">カートへ追加</asp:LinkButton>
											</p>
											<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1" /></p>
										</div><!--product-->
									</ItemTemplate>
								</asp:Repeater>
							</ItemTemplate>
							<FooterTemplate>
								</div><!--novelty-->
							</FooterTemplate>
						</asp:Repeater>
						<%-- △ノベルティ△ --%>

						<% if (Constants.W2MP_POINT_OPTION_ENABLED && this.IsLoggedIn) { %>
							<div class="pointBox" >
								<asp:LinkButton ID="lbUsePoint" Visible="<%# IsPointInputOpened(Container.ItemIndex) == false %>" OnClick="lbUsePoint_Click" runat="server">ポイントを使う</asp:LinkButton>
								<div class="box" id="divPointBox" visible="<%# IsPointInputOpened(Container.ItemIndex) %>" runat="server">
									<p><img src="../../Contents/ImagesPkg/common/ttl_point.gif" alt="ポイントを使う" width="262" height="23" /></p>
									<div class="boxbtm">
										<div>
											<dl>
												<dt>合計 <%= GetNumeric(this.LoginUserPointUsable) %> ポイント<span>までご利用いただけます</span></dt>
												<dd><asp:TextBox ID="tbOrderPointUse" Runat="server" Text="<%# ((CartObject)Container.DataItem).UsePoint %>" MaxLength="6"></asp:TextBox>&nbsp;&nbsp;pt</dd>
											</dl>
											<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
										</div>
										<span class="fred" visible="<%# this.ErrorMessages.HasMessages(Container.ItemIndex, CartErrorMessages.ErrorKbn.Point) %>" runat="server">
											<%# WebSanitizer.HtmlEncode(this.ErrorMessages.Get(Container.ItemIndex, CartErrorMessages.ErrorKbn.Point))%></span>
									</div><!--boxbtm-->
								</div><!--box-->
							</div><!--pointBox-->
						<% } %>

						<% if (Constants.W2MP_COUPON_OPTION_ENABLED) { %>
							<div class="couponBox">
								<asp:LinkButton ID="lbUseCoupon" Visible="<%# IsCoupontInputOpened(Container.ItemIndex) == false %>" OnClick="lbUseCoupon_Click" runat="server">クーポンを使う</asp:LinkButton>
								<div id="divCouponBox" class="box" visible="<%# IsCoupontInputOpened(Container.ItemIndex) %>" runat="server">
									<p><img src="../../Contents/ImagesPkg/common/ttl_coupon.gif" alt="クーポンを使う" width="262" height="23" /></p>
									<div class="boxbtm">
										<div>
											<dl>
												<dt><span>クーポンコード</span></dt>
												<dd><asp:TextBox ID="tbCouponCode" runat="server" Text="<%# GetCouponCode(((CartObject)Container.DataItem).Coupon) %>" MaxLength="30" autocomplete="off"></asp:TextBox></dd>
											</dl>
											<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
										</div>
										<span class="fred" visible="<%# this.ErrorMessages.HasMessages(Container.ItemIndex, CartErrorMessages.ErrorKbn.Coupon) %>" runat="server">
											<%# WebSanitizer.HtmlEncode(this.ErrorMessages.Get(Container.ItemIndex, CartErrorMessages.ErrorKbn.Coupon)) %></span>
									</div><!--boxbtm-->
								</div><!--box-->
							</div><!--couponBox-->
						<% } %>

						<div class="priceList">
							<div>
								<dl class="bgc">
									<dt>小計(<%#: this.ProductPriceTextPrefix %>)</dt>
									<dd><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceSubtotal) %></dd>
								</dl>
								<%if (this.ProductIncludedTaxFlg == false) { %>
									<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
										<dt>消費税</dt>
										<dd><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceSubtotalTax) %></dd>
									</dl>
								<%} %>
								<%-- セットプロモーション割引額(商品割引) --%>
								<asp:Repeater ID="rSetPromotion" DataSource="<%# ((CartObject)Container.DataItem).SetPromotions %>" runat="server">
								<ItemTemplate>
									<span visible="<%# ((CartSetPromotion)Container.DataItem).IsDiscountTypeProductDiscount %>" runat="server">
									<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
										<dt><%# WebSanitizer.HtmlEncode(((CartSetPromotion)Container.DataItem).SetpromotionDispName) %></dt>
										<dd class='<%# (((CartSetPromotion)Container.DataItem).ProductDiscountAmount > 0) ? "minus" : "" %>'><%# (((CartSetPromotion)Container.DataItem).ProductDiscountAmount > 0) ? "-" : ""%><%#: CurrencyManager.ToPrice(((CartSetPromotion)Container.DataItem).ProductDiscountAmount) %></dd>
									</dl>
									</span>
								</ItemTemplate>
								</asp:Repeater>
								<%if (Constants.MEMBER_RANK_OPTION_ENABLED && this.IsLoggedIn){ %>
									<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
										<dt>会員ランク割引額</dt>
										<dd class='<%# (((CartObject)Container.DataItem).MemberRankDiscount > 0) ? "minus" : "" %>'><%# (((CartObject)Container.DataItem).MemberRankDiscount > 0) ? "-" : "" %><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).MemberRankDiscount * ((((CartObject)Container.DataItem).MemberRankDiscount < 0) ? -1 : 1)) %></dd>
									</dl>
								<%} %>
								<%if (Constants.FIXEDPURCHASE_OPTION_ENABLED) { %>
									<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
										<dt>定期購入割引額</dt>
										<dd class='<%# (((CartObject)((RepeaterItem)Container).DataItem).FixedPurchaseDiscount > 0) ? "minus" : "" %>'><%# (((CartObject)((RepeaterItem)Container).DataItem).FixedPurchaseDiscount > 0) ? "-" : "" %><%#: CurrencyManager.ToPrice(((CartObject)((RepeaterItem)Container).DataItem).FixedPurchaseDiscount* ((((CartObject)((RepeaterItem)Container).DataItem).FixedPurchaseDiscount < 0) ? -1 : 1)) %></dd>
									</dl>
								<% } %>
								<%if (Constants.W2MP_POINT_OPTION_ENABLED && this.IsLoggedIn){ %>
									<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
										<dt>ポイント利用額</dt>
										<dd class='<%# (((CartObject)Container.DataItem).UsePointPrice > 0) ? "minus" : "" %>'><%# (((CartObject)Container.DataItem).UsePointPrice > 0) ? "-" : "" %><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).UsePointPrice * ((((CartObject)Container.DataItem).UsePointPrice < 0) ? -1 : 1)) %></dd>
									</dl>
								<%} %>
								<%if (Constants.W2MP_COUPON_OPTION_ENABLED){ %>
									<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
										<dt>クーポン割引額</dt>
										<dd class='<%# (((CartObject)Container.DataItem).UseCouponPrice > 0) ? "minus" : "" %>'><%# (((CartObject)Container.DataItem).UseCouponPrice > 0) ? "-" : "" %><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).UseCouponPrice * ((((CartObject)Container.DataItem).UseCouponPrice < 0) ? -1 : 1)) %></dd>
									</dl>
								<%} %>
								<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
									<dt>配送料金</dt>
									<dd runat="server" style='<%# (((CartObject)Container.DataItem).ShippingPriceSeparateEstimateFlg) ? "display:none;" : ""%>'>
										<%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceShipping) %></dd>
									<dd runat="server" style='<%# (((CartObject)Container.DataItem).ShippingPriceSeparateEstimateFlg == false) ? "display:none;" : ""%>'>
										<%# WebSanitizer.HtmlEncode(((CartObject)Container.DataItem).ShippingPriceSeparateEstimateMessage)%></dd>
								</dl>
								<%-- セットプロモーション割引額(配送料割引) --%>
								<asp:Repeater DataSource="<%# ((CartObject)Container.DataItem).SetPromotions %>" runat="server">
								<ItemTemplate>
									<span visible="<%# ((CartSetPromotion)Container.DataItem).IsDiscountTypeShippingChargeFree %>" runat="server">
									<dl class='<%= (this.DispNum++ % 2 == 0) ? "" : "bgc" %>'>
										<dt><%# WebSanitizer.HtmlEncode(((CartSetPromotion)Container.DataItem).SetpromotionDispName) %>(送料割引)</dt>
										<dd class='<%# (((CartSetPromotion)Container.DataItem).ShippingChargeDiscountAmount > 0) ? "minus" : "" %>'><%# (((CartSetPromotion)Container.DataItem).ShippingChargeDiscountAmount > 0) ? "-" : ""%><%#: CurrencyManager.ToPrice(((CartSetPromotion)Container.DataItem).ShippingChargeDiscountAmount) %></dd>
									</dl>
									</span>
								</ItemTemplate>
								</asp:Repeater>
							</div>
							<p class="clr"><img src="../../Contents/ImagesPkg/common/clear.gif" alt="" width="1" height="1"  /></p>
							<div>
								<dl class="result">
									<dt>合計(税込)</dt>
									<dd><%#: CurrencyManager.ToPrice(((CartObject)Container.DataItem).PriceCartTotalWithoutPaymentPrice) %></dd>
								</dl>
							</div>
						</div><!--priceList-->

						<uc:BodyAnnounceFreeShipping runat="server" TargetCart="<%# ((CartObject)Container.DataItem) %>" />
					</div><!--block-->
				</div><!--bottom-->
			</div><!--subCartList-->

			<div visible="<%# ((CartObjectList)((Repeater)Container.Parent).DataSource).Items.Count == Container.ItemIndex + 1 %>" runat="server">
				<div class="sumBox">
					<div class="subSumBox">
						<p><img src="../../Contents/ImagesPkg/common/ttl_sum.gif" alt="総合計" width="52" height="16" />
							<strong><%#: CurrencyManager.ToPrice(this.CartList.PriceCartListTotal) %></strong></p>
					</div>
					<%if (Constants.W2MP_POINT_OPTION_ENABLED && this.IsLoggedIn) { %>
						<dl>
							<dt Visible="<%# ((CartObject)Container.DataItem).FirstBuyPoint != 0 %>" runat="server">初回購入獲得ポイント</dt>
							<dd Visible="<%# ((CartObject)Container.DataItem).FirstBuyPoint != 0 %>" runat="server"><%# WebSanitizer.HtmlEncode(GetNumeric(((CartObjectList)((Repeater)Container.Parent).DataSource).TotalFirstBuyPoint)) %>pt</dd>
							<dt>購入後獲得ポイント</dt>
							<dd><%# WebSanitizer.HtmlEncode(GetNumeric(((CartObjectList)((Repeater)Container.Parent).DataSource).TotalBuyPoint)) %>pt</dd>
						</dl>
						<small>※ 1pt = <%: CurrencyManager.ToPrice(1m) %></small>
					<%} %>
				</div><!--sumBox-->
				<p class="btmbtn right"><a href="<%= this.NextEvent %>" class="btn btn-large btn-success">レジに進む</a></p>

			</div>
	
		</div><!--shoppingCart-->
		<%-- ▲カート情報▲ --%>
	
		<%-- 隠し値：カートID --%>
		<asp:HiddenField ID="hfCartId" runat="server" Value="<%# ((CartObject)Container.DataItem).CartId %>" />
		<%-- 隠し再計算ボタン --%>
		<asp:LinkButton id="lbRecalculateCart" runat="server" CommandArgument="<%# Container.ItemIndex %>" onclick="lbRecalculate_Click"></asp:LinkButton>
	</ItemTemplate>
</asp:Repeater>
</div><!--columnRight-->
</ContentTemplate>
</asp:UpdatePanel>
<%-- UPDATE PANELここまで --%>
<br class="clr" />
</div><!--submain-->
</div><!--main-->

<script type="text/javascript">
	function bodyPageLoad() {
		if (Sys.WebForms == null) return;
		var isAsyncPostback = Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack();
		if (isAsyncPostback) {
			try {
				<%-- Ajax動いてカート内容操作後にウィジェット消える場合のエラーを回避 --%>
				if ($('#AmazonPayButton').length) showButton();
			}
			catch (e) { }
		}
	}
	<%--▼▼Amazonウィジェット用スクリプト▼▼--%>
	window.onAmazonLoginReady = function () {
		amazon.Login.setClientId('<%=Constants.PAYMENT_AMAZON_CLIENTID %>');
	};
	window.onAmazonPaymentsReady = function () {
		if ($('#AmazonPayButton').length) showButton();
	};

	<%-- Amazonボタン表示ウィジェット --%>
	function showButton() {
		var authRequest;
		OffAmazonPayments.Button("AmazonPayButton", "<%=Constants.PAYMENT_AMAZON_SELLERID %>", {
			type: "PwA",
			color: "Gold",
			size: "medium",
			authorization: function () {
				loginOptions = {
					scope: "payments:widget payments:shipping_address profile",
					popup: true
				};
				authRequest = amazon.Login.authorize(loginOptions, "<%=w2.App.Common.Amazon.Util.AmazonUtil.CreateCallbackUrl(Constants.PAGE_FRONT_AMAZON_ORDER_CALLBACK) %>");
			},
			onError: function (error) {
				alert(error.getErrorMessage());
			}
		});
	};
	<%-- ▲▲Amazonウィジェット用スクリプト▲▲ --%>
</script>
<script async="async" type="text/javascript" charset="utf-8" src="<%=Constants.PAYMENT_AMAZON_WIDGETSSCRIPT %>"></script>
<%-- △編集可能領域△ --%>

<div id="divBottomArea">
<%-- ▽レイアウト領域：ボトムエリア▽ --%>
<%-- △レイアウト領域△ --%>
</div>

</td>
<td>
<%-- ▽レイアウト領域：ライトエリア▽ --%>
<%-- △レイアウト領域△ --%>
</td>
</tr>
</table>
</asp:Content>