﻿<%--
=========================================================================================================
  Module      : 会員登録入力画面(UserRegistInput.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ Register TagPrefix="uc" TagName="BodyUserExtendRegist" Src="~/Form/Common/User/BodyUserExtendRegist.ascx" %>
<%@ Register TagPrefix="uc" TagName="Layer" Src="~/Form/Common/Layer/SearchResultLayer.ascx" %>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_UserRegistInput, App_Web_userregistinput.aspx.b2a7112d" title="会員新規登録入力ページ" %>
<%@ Register TagPrefix="uc" TagName="PaypalScriptsForm" Src="~/Form/Common/PayPalScriptsForm.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="dvUserContents">
	<%-- 会員登録系パンくず --%>
	<div id="dvHeaderRegistClumbs">
		<p><img src="../../Contents/ImagesPkg/user/clumbs_regist_2.gif" alt="会員情報の入力" /></p>
	</div>

	<h2>会員情報の入力</h2>

	<div id="dvUserRegistInput" class="unit">
		
		<%-- メッセージ --%>
		<div class="dvContentsInfo">
			<p>
				<%= WebSanitizer.HtmlEncode(ShopMessage.GetMessage("ShopName")) %>会員へのお申込みにあたっては、以下の項目にご入力が必要です。<br />
				下記の項目に入力の上、「確認する」ボタンを押して下さい。</p>
		</div>
		
		<% if ((Constants.RAKUTEN_LOGIN_ENABLED && (this.IsAfterOrder == false)) || Constants.PAYPAL_LOGINPAYMENT_ENABLED) { %>
		<%-- ソーシャルログイン連携 --%>
		<div class="dvSocialLoginCooperation">
			<h3>ソーシャルログイン連携</h3>
			<ul style="display:flex;margin-bottom:20px;flex-wrap:wrap">
			<%-- PayPal --%>
			<% if (Constants.PAYPAL_LOGINPAYMENT_ENABLED) { %>
				<%-- PayPal --%>
				<li style="padding: 0 0.5em;">
					<%-- ▼PayPalここから▼ --%>
					<%
						ucPaypalScriptsForm.LogoDesign = "Login";
						ucPaypalScriptsForm.AuthCompleteActionControl = lbPayPalAuthComplete;
						ucPaypalScriptsForm.GetShippingAddress = (this.IsLoggedIn == false);
					%>
					<uc:PaypalScriptsForm ID="ucPaypalScriptsForm" runat="server" />
					<div style="width: 218px;">
					<div id="paypal-button"></div>
					<% if (SessionManager.PayPalCooperationInfo != null) { %>
						※<%: SessionManager.PayPalCooperationInfo.AccountEMail %> 連携済
					<% } else { %>
						<p style="padding-top:7px;">※PayPalで新規登録/ログインします</p>
					<% } %>
					</div>
					<asp:LinkButton ID="lbPayPalAuthComplete" runat="server" OnClick="lbPayPalAuthComplete_Click"></asp:LinkButton>
					<%-- ▲PayPalここまで▲ --%>
				</li>
			<% } %>
			<%-- ▼楽天Connect▼ --%>
			<% if (Constants.RAKUTEN_LOGIN_ENABLED && (this.IsAfterOrder == false)) { %>
				<li>
					<p style="text-align: center;">
						<asp:LinkButton runat="server" ID="lbRakutenIdConnectRequestAuth" OnClick="lbRakutenIdConnectRequestAuth_OnClick">
							<img src="https://checkout.rakuten.co.jp/p/common/img/btn_idconnect_05.gif" alt="楽天IDで新規登録" style="width: 237px;" />
						</asp:LinkButton>
						<% if (this.IsRakutenIdConnectUserRegister) { %>
							<br/>※<%: SessionManager.RakutenIdConnectActionInfo.RakutenIdConnectUserInfoResponseData.Email %> 連携済<br/>
						<% } %>
					</p>
				</li>
			<% } %>
			<%-- ▲楽天Connect▲ --%>
			</ul>
		</div>
		<% } %>

		<%-- お客様情報入力フォーム --%>
		<div class="dvUserInfo">	
			<h3>お客様情報</h3>
			<ins><span class="necessary">*</span>は必須入力となります。</ins>

		<%-- UPDATE PANEL開始 --%>
		<asp:UpdatePanel ID="upUpdatePanel" runat="server">
		<ContentTemplate>
			<table cellspacing="0">
				<tr>
					<%-- 氏名 --%>
					<th>
						<%: ReplaceTag("@@User.name.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_name"/>
					</th>
					<td>
						<table cellspacing="0">
							<tr>
								<td>
									<% SetMaxLength(this.WtbUserName1, "@@User.name1.length_max@@"); %>
									<span class="fname">姓</span><asp:TextBox id="tbUserName1" Runat="server" CssClass="nameFirst"></asp:TextBox></td>
								<td>
									<% SetMaxLength(this.WtbUserName2, "@@User.name2.length_max@@"); %>
									<span class="lname">名</span><asp:TextBox id="tbUserName2" Runat="server" CssClass="nameLast"></asp:TextBox><span class="notes">※全角入力</span></td>
							</tr>
							<tr>
								<td><span class="notes">例：山田</span></td>
								<td><span class="notes">太郎</span></td>
							</tr>
						</table>
						<asp:CustomValidator
							ID="cvUserName1"
							runat="Server"
							ControlToValidate="tbUserName1"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserName2"
							runat="Server"
							ControlToValidate="tbUserName2"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% if (this.IsUserAddrJp) { %>
				<tr>
					<%-- 氏名（かな） --%>
					<th>
						<%: ReplaceTag("@@User.name_kana.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_kana"/>
					</th>
					<td>
						<table cellspacing="0">
							<tr>
								<td>
									<% SetMaxLength(this.WtbUserNameKana1, "@@User.name_kana1.length_max@@"); %>
									<span class="fname">姓</span><asp:TextBox id="tbUserNameKana1" Runat="server" CssClass="nameFirst"></asp:TextBox></td>
								<td>
									<% SetMaxLength(this.WtbUserNameKana2, "@@User.name_kana2.length_max@@"); %>
									<span class="lname">名</span><asp:TextBox id="tbUserNameKana2" Runat="server" CssClass="nameLast"></asp:TextBox><span class="notes">※全角ひらがな入力</span></td>
							</tr>
							<tr>
								<td><span class="notes">例：やまだ</span></td>
								<td><span class="notes">たろう</span></td>
							</tr>
						</table>
						<asp:CustomValidator
							ID="cvUserNameKana1"
							runat="Server"
							ControlToValidate="tbUserNameKana1"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserNameKana2"
							runat="Server"
							ControlToValidate="tbUserNameKana2"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<%if (Constants.PRODUCTREVIEW_ENABLED) { %>
				<tr>
					<%-- ニックネーム --%>
					<th>
						<%: ReplaceTag("@@User.nickname.name@@") %>
					</th>
					<td>
						<asp:TextBox id="tbUserNickName" runat="server" MaxLength="20" CssClass="nickname"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserNickName"
							runat="Server"
							ControlToValidate="tbUserNickName"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<%} %>
				<tr>
					<%-- 生年月日 --%>
					<th>
						<%: ReplaceTag("@@User.birth.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_birth"/>
					</th>
					<td>
						<asp:DropDownList id="ddlUserBirthYear" runat="server" CssClass="year"></asp:DropDownList>年
						<asp:DropDownList id="ddlUserBirthMonth" runat="server" CssClass="month"></asp:DropDownList>月
						<asp:DropDownList id="ddlUserBirthDay" runat="server" CssClass="date"></asp:DropDownList>日
						<asp:CustomValidator
							ID="cvUserBirthYear"
							runat="Server"
							ControlToValidate="ddlUserBirthYear"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							EnableClientScript="false"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserBirthMonth"
							runat="Server"
							ControlToValidate="ddlUserBirthMonth"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							EnableClientScript="false"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserBirthDay"
							runat="Server"
							ControlToValidate="ddlUserBirthDay"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							EnableClientScript="false"
							CssClass="error_inline" />
					</td>
				</tr>
				<tr>
					<%-- 性別 --%>
					<th>
						<%: ReplaceTag("@@User.sex.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_sex"/>
					</th>
					<td>
						<asp:RadioButtonList ID="rblUserSex" runat="server" RepeatDirection="Horizontal" CellSpacing="0" RepeatLayout="Flow" CssClass="radioBtn"></asp:RadioButtonList>
						<asp:CustomValidator
							ID="cvUserSex"
							runat="Server"
							ControlToValidate="rblUserSex"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							EnableClientScript="false"
							CssClass="error_inline" />
					</td>
				</tr>
				<tr>
					<%-- PCメールアドレス --%>
					<th>
						<%: ReplaceTag("@@User.mail_addr.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_mail_addr"/>
					</th>
					<td>
						<asp:TextBox id="tbUserMailAddr" Runat="server" MaxLength="256" CssClass="mailAddr" Type="email"></asp:TextBox>
						<%if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED) {%>
						<span class="notes">※ログイン時に利用します</span>
						<%} %>
						<asp:CustomValidator
							ID="cvUserMailAddr"
							runat="Server"
							ControlToValidate="tbUserMailAddr"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserMailAddrForCheck"
							runat="Server"
							ControlToValidate="tbUserMailAddr"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							CssClass="error_inline" />
					</td>
				</tr>
				<tr>
					<%-- PCメールアドレス（確認用） --%>
					<th>
						<%: ReplaceTag("@@User.mail_addr.name@@") %>（確認用）
						<span class="necessary">*</span><span id="efo_sign_mail_addr_conf"/>
					</th>
					<td>
						<asp:TextBox id="tbUserMailAddrConf" Runat="server" MaxLength="256" CssClass="mailAddr" Type="email"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserMailAddrConf"
							runat="Server"
							ControlToValidate="tbUserMailAddrConf"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% if (Constants.DISPLAYMOBILEDATAS_OPTION_ENABLED) { %>
				<tr>
					<%-- モバイルメールアドレス --%>
					<th>
						<%: ReplaceTag("@@User.mail_addr2.name@@") %>
						<span class="necessary" style="display:<%= (Constants.DISPLAYMOBILEDATAS_OPTION_ENABLED) ? "" : "none" %>">*</span>
					</th>
					<td>
						<asp:TextBox id="tbUserMailAddr2" Runat="server" MaxLength="256" CssClass="mailAddr" Type="email"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserMailAddr2"
							runat="Server"
							ValidationGroup="UserRegist"
							ControlToValidate="tbUserMailAddr2"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<tr>
					<%-- モバイルメールアドレス（確認用） --%>
					<th>
						<%: ReplaceTag("@@User.mail_addr2.name@@") %>（確認用）
						<span class="necessary" style="display:<%= (Constants.DISPLAYMOBILEDATAS_OPTION_ENABLED) ? "" : "none" %>">*</span>
					</th>
					<td>
						<asp:TextBox id="tbUserMailAddr2Conf" Runat="server" MaxLength="256" CssClass="mailAddr" Type="email"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserMailAddr2Conf"
							runat="Server"
							ControlToValidate="tbUserMailAddr2Conf"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<% if (Constants.GLOBAL_OPTION_ENABLE) { %>
				<tr>
					<%-- 国 --%>
					<th>
						<%: ReplaceTag("@@User.country.name@@", this.UserAddrCountryIsoCode) %>
					<span class="necessary">*</span>
					</th>
					<td>
						<asp:DropDownList id="ddlUserCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserAddrCountry_SelectedIndexChanged"/>
						<asp:CustomValidator
							ID="cvUserCountry"
							runat="Server"
							ControlToValidate="ddlUserCountry"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							EnableClientScript="false"
							CssClass="error_inline" />
						<span id="countryAlertMessage" class="notes" runat="server" Visible='false'>※Amazonログイン連携では国はJapan以外選択できません。</span>
					</td>
				</tr>
				<% } %>
				<% if (this.IsAmazonLoggedIn && Constants.AMAZON_LOGIN_OPTION_ENABLED) { %>
					<tr>
						<th>
							<%: ReplaceTag("@@User.addr.name@@") %>
						</th>
						<td>
							<%--▼▼Amazonアドレス帳ウィジェット▼▼--%>
							<div id="addressBookWidgetDiv" style="width:100%;height:300px;"></div>
							<div id="addressBookErrorMessage" style="color:red;padding:5px" ClientIDMode="Static" runat="server"></div>
							<%--▲▲Amazonアドレス帳ウィジェット▲▲--%>
							<%--▼▼AmazonリファレンスID格納▼▼--%>
							<asp:HiddenField runat="server" ID="hfAmazonOrderRefID" />
							<%--▲▲AmazonリファレンスID格納▲▲--%>
						</td>
					</tr>
				<% } else { %>
				<% if (this.IsUserAddrJp) { %>
				<tr>
					<%-- 郵便番号 --%>
					<th>
						<%: ReplaceTag("@@User.zip.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_zip"/>
					</th>
					<td valign="middle">
						<asp:TextBox id="tbUserZip1" Runat="server" MaxLength="3" CssClass="zipFirst" Type="tel"></asp:TextBox>-
						<asp:TextBox id="tbUserZip2" Runat="server" MaxLength="4" CssClass="zipLast" Type="tel" OnTextChanged="lbSearchAddr_Click"></asp:TextBox>
						<asp:LinkButton ID="lbSearchAddr" runat="server" OnClick="lbSearchAddr_Click" class="btn btn-mini" OnClientClick="return false;">
							住所検索</asp:LinkButton><br/>
						<%--検索結果レイヤー--%>
						<uc:Layer ID="ucLayer" runat="server" />
						<asp:CustomValidator
							ID="cvUserZip1"
							runat="Server"
							ValidationGroup="UserRegist"
							ControlToValidate="tbUserZip1"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserZip2"
							runat="Server"
							ControlToValidate="tbUserZip2"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<span style="color :Red" id="addrSearchErrorMessage">
							<%: this.ZipInputErrorMessage %></span>
					</td>
				</tr>

				<tr>
					<%-- 都道府県 --%>
					<th>
						<%: ReplaceTag("@@User.addr1.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_addr1"/>
					</th>
					<td>
						<asp:DropDownList id="ddlUserAddr1" runat="server" CssClass="district"></asp:DropDownList>
						<asp:CustomValidator
							ID="cvUserAddr1"
							runat="Server"
							ControlToValidate="ddlUserAddr1"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<tr>
					<%-- 市区町村 --%>
					<th>
						<%: ReplaceTag("@@User.addr2.name@@", this.UserAddrCountryIsoCode) %>
						<span class="necessary">*</span><% if (this.IsUserAddrJp) { %><span id="efo_sign_addr2"/><% } %>
					</th>
					<td>
						<% if (IsCountryTw(this.UserAddrCountryIsoCode)) { %>
							<asp:DropDownList runat="server" ID="ddlUserAddr2" AutoPostBack="true" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="ddlUserAddr2_SelectedIndexChanged"></asp:DropDownList>
						<% } else { %>
							<% SetMaxLength(this.WtbUserAddr2, "@@User.addr2.length_max@@"); %>
							<asp:TextBox id="tbUserAddr2" Runat="server" CssClass="addr"></asp:TextBox><span class="notes">※全角入力</span>
							<asp:CustomValidator
								ID="cvUserAddr2"
								runat="Server"
								ControlToValidate="tbUserAddr2"
								ValidationGroup="UserRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						<% } %>
					</td>
				</tr>
				<tr>
					<%-- 番地 --%>
					<th>
						<%: ReplaceTag("@@User.addr3.name@@", this.UserAddrCountryIsoCode) %>
						<% if (IsAddress3Necessary(this.UserAddrCountryIsoCode)){ %><span class="necessary">*</span><span id="efo_sign_addr3"/><% } %>
					</th>
					<td>
						<% if (IsCountryTw(this.UserAddrCountryIsoCode)) { %>
							<asp:DropDownList runat="server" ID="ddlUserAddr3" AutoPostBack="true" DataTextField="Key" DataValueField="Value" Width="95"></asp:DropDownList>
						<% } else { %>
							<% SetMaxLength(this.WtbUserAddr3, "@@User.addr3.length_max@@"); %>
							<asp:TextBox id="tbUserAddr3" Runat="server" CssClass="addr2"></asp:TextBox><span class="notes">※全角入力</span>
							<asp:CustomValidator
								ID="cvUserAddr3"
								runat="Server"
								ControlToValidate="tbUserAddr3"
								ValidationGroup="UserRegist"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
						<% } %>
					</td>
				</tr>
				<tr>
					<%-- ビル・マンション名 --%>
					<th>
						<%: ReplaceTag("@@User.addr4.name@@", this.UserAddrCountryIsoCode) %>
						<% if (this.IsUserAddrJp == false) { %><span class="necessary">*</span><% } %>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserAddr4, "@@User.addr4.length_max@@"); %>
						<asp:TextBox id="tbUserAddr4" Runat="server" CssClass="addr2"></asp:TextBox><span class="notes">※全角入力</span>
						<asp:CustomValidator
							ID="cvUserAddr4"
							runat="Server"
							ControlToValidate="tbUserAddr4"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% if (this.IsUserAddrJp == false) { %>
				<tr>
					<%-- 州 --%>
					<th><%: ReplaceTag("@@User.addr5.name@@", this.UserAddrCountryIsoCode) %>
						<% if (this.IsUserAddrUs) { %><span class="necessary">*</span><%} %>
					</th>
					<td>
						<% if (this.IsUserAddrUs) { %>
						<asp:DropDownList runat="server" ID="ddlUserAddr5" ></asp:DropDownList>
							<asp:CustomValidator
								ID="cvUserAddr5"
								runat="Server"
								ControlToValidate="ddlUserAddr5"
								ValidationGroup="UserRegistGlobal"
								ValidateEmptyText="true"
								SetFocusOnError="true"
								ClientValidationFunction="ClientValidate"
								CssClass="error_inline" />
					<% } else { %>
						<asp:TextBox runat="server" ID="tbUserAddr5" ></asp:TextBox>
					<% } %>
					</td>
				</tr>
				<tr>
					<%-- 郵便番号（海外向け） --%>
					<th>
						<%: ReplaceTag("@@User.zip.name@@", this.UserAddrCountryIsoCode) %>
						<% if (this.IsUserAddrZipNecessary) { %><span class="necessary">*</span><% } %>
					</th>
					<td valign="middle">
						<asp:TextBox id="tbUserZipGlobal" Runat="server" MaxLength="30" Type="tel"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserZipGlobal"
							runat="Server"
							ControlToValidate="tbUserZipGlobal"
							ValidationGroup="UserRegistGlobal"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline"/>
					</td>
				</tr>
				<% } %>
				<% } %>
				<% if (Constants.DISPLAY_CORPORATION_ENABLED){ %>
				<tr>
					<%-- 企業名 --%>
					<th>
						<%: ReplaceTag("@@User.company_name.name@@") %>
						<span class="necessary"></span>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserCompanyName, "@@User.company_name.length_max@@"); %>
						<asp:TextBox id="tbUserCompanyName" Runat="server" CssClass="addr2"></asp:TextBox><span class="notes">※全角入力</span>
						<asp:CustomValidator
							ID="cvUserCompanyName"
							runat="Server"
							ControlToValidate="tbUserCompanyName"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<tr>
					<%-- 部署名 --%>
					<th>
						<%: ReplaceTag("@@User.company_post_name.name@@") %>
						<span class="necessary"></span>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserCompanyPostName, "@@User.company_post_name.length_max@@"); %>
						<asp:TextBox id="tbUserCompanyPostName" Runat="server" CssClass="addr2"></asp:TextBox><span class="notes">※全角入力</span>
						<asp:CustomValidator
							ID="cvUserCompanyPostName"
							runat="Server"
							ControlToValidate="tbUserCompanyPostName"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<%} %>
				<% if (this.IsUserAddrJp) { %>
				<tr>
					<%-- 電話番号 --%>
					<th><%: ReplaceTag("@@User.tel1.name@@") %><span class="necessary">*</span><span id="efo_sign_tel1"/></th>
					<td>
						<asp:TextBox id="tbUserTel1" Runat="server" MaxLength="6" CssClass="tel1" Type="tel"></asp:TextBox> -
						<asp:TextBox id="tbUserTel2" Runat="server" MaxLength="4" CssClass="tel2" Type="tel"></asp:TextBox> -
						<asp:TextBox id="tbUserTel3" Runat="server" MaxLength="4" CssClass="tel3" Type="tel"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserTel1"
							runat="Server"
							ControlToValidate="tbUserTel1"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserTel2"
							runat="Server"
							ControlToValidate="tbUserTel2"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserTel3"
							runat="Server"
							ControlToValidate="tbUserTel3"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<tr>
					<%-- 電話番号（予備） --%>
					<th><%: ReplaceTag("@@User.tel2.name@@") %></th>
					<td>
						<asp:TextBox id="tbUserTel2_1" Runat="server" MaxLength="6" CssClass="tel1" Type="tel"></asp:TextBox> -
						<asp:TextBox id="tbUserTel2_2" Runat="server" MaxLength="4" CssClass="tel2" Type="tel"></asp:TextBox> -
						<asp:TextBox id="tbUserTel2_3" Runat="server" MaxLength="4" CssClass="tel3" Type="tel"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserTel2_1"
							runat="Server"
							ControlToValidate="tbUserTel2_1"
							ValidationGroup="UserRegist"
							ValidateEmptyText="false"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserTel2_2"
							runat="Server"
							ControlToValidate="tbUserTel2_2"
							ValidationGroup="UserRegist"
							ValidateEmptyText="false"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
						<asp:CustomValidator
							ID="cvUserTel2_3"
							runat="Server"
							ControlToValidate="tbUserTel2_3"
							ValidationGroup="UserRegist"
							ValidateEmptyText="false"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } else { %>
				<tr>
					<%-- 電話番号 --%>
					<th>
						<%: ReplaceTag("@@User.tel1.name@@", this.UserAddrCountryIsoCode) %>
						<span class="necessary">*</span>
					</th>
					<td>
						<asp:TextBox id="tbUserTel1Global" Runat="server" MaxLength="16" Type="tel"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserTel1Global"
							runat="Server"
							ControlToValidate="tbUserTel1Global"
							ValidationGroup="UserRegistGlobal"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<tr>
					<%-- 電話番号（予備） --%>
					<th><%: ReplaceTag("@@User.tel2.name@@", this.UserAddrCountryIsoCode) %></th>
					<td>
						<asp:TextBox id="tbUserTel2Global" Runat="server" MaxLength="30" Type="tel"></asp:TextBox>
						<asp:CustomValidator
							ID="cvUserTel2Global"
							runat="Server"
							ControlToValidate="tbUserTel2Global"
							ValidationGroup="UserRegistGlobal"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
				<tr>
					<th>
						<%: ReplaceTag("@@User.mail_flg.name@@") %>
					</th>
					<td class="checkBox">
						<asp:CheckBox ID="cbUserMailFlg" Text=" 希望する " CssClass="checkBox" runat="server" />
					</td>
				</tr>
				<%-- ユーザー拡張項目　HasInput:true(入力画面)/false(確認画面)　HasRegist:true(新規登録)/false(登録編集) --%>
				<uc:BodyUserExtendRegist ID="ucBodyUserExtendRegist" runat="server" HasInput="true" HasRegist="true" />
			</table>
		<%-- ソーシャルログイン用 --%>
		<% if (Constants.SOCIAL_LOGIN_ENABLED) { %>
		<asp:HiddenField ID="hfSocialLoginJson" runat="server" />
		<% } %>
		</ContentTemplate>
		</asp:UpdatePanel>
		<%-- UPDATE PANELここまで --%>
		
		</div>
		
		<%if ((Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED == false) || this.IsVisible_UserPassword) { %>
		<div class="dvLoginInfo">
			<h3>ログイン情報</h3>
			<table cellspacing="0">
				<%if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED == false) { %>
				<tr>
					<th>
						<%-- ログインID --%>
						<%: ReplaceTag("@@User.login_id.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_login_id"/>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserLoginId, "@@User.login_id.length_max@@"); %>
						<% if (Constants.LOGIN_ID_USE_MAILADDRESS_ENABLED) tbUserLoginId.Attributes["Type"] = "email"; %>
						<asp:TextBox id="tbUserLoginId" Width="120" Runat="server"></asp:TextBox><span class="notes">※6～15桁</span>
						<asp:CustomValidator ID="cvUserLoginId" runat="Server"
							ControlToValidate="tbUserLoginId"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<%} %>
				<%-- ソーシャルログイン連携されている場合はパスワードスキップ --%>
				<%if (this.IsVisible_UserPassword){ %>
				<tr>
					<th>
						<%: ReplaceTag("@@User.password.name@@") %>
						<span class="necessary">*</span><span id="efo_sign_password" />
					</th>
					<td>
						<% SetMaxLength(this.WtbUserPassword, "@@User.password.length_max@@"); %>
						<asp:TextBox id="tbUserPassword" TextMode="Password" autocomplete="off" CssClass="password" Runat="server"></asp:TextBox><span class="notes">※半角英数字混合7～15文字</span>
						<asp:CustomValidator
							ID="cvUserPassword"
							runat="Server"
							ControlToValidate="tbUserPassword"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<tr>
					<th>
						<%: ReplaceTag("@@User.password.name@@") %>(確認用)
						<span class="necessary">*</span><span id="efo_sign_password_conf"/>
					</th>
					<td>
						<% SetMaxLength(this.WtbUserPasswordConf, "@@User.password.length_max@@"); %>
						<asp:TextBox id="tbUserPasswordConf" TextMode="Password" autocomplete="off" CssClass="password" Runat="server"></asp:TextBox><span class="notes">※半角英数字混合7～15文字</span>
						<asp:CustomValidator
							ID="cvUserPasswordConf"
							runat="Server"
							ControlToValidate="tbUserPasswordConf"
							ValidationGroup="UserRegist"
							ValidateEmptyText="true"
							SetFocusOnError="true"
							ClientValidationFunction="ClientValidate"
							CssClass="error_inline" />
					</td>
				</tr>
				<% } %>
			</table>
		</div>
		<% } %>

		<div class="dvUserBtnBox">
			<p>
				<span><a href="<%= WebSanitizer.HtmlEncode(Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_REGIST_REGULATION) %>" class="btn btn-large">戻る</a></span>
				<span><asp:LinkButton ID="lbConfirm" ValidationGroup="UserRegist" OnClientClick="return exec_submit();" runat="server" OnClick="lbConfirm_Click" class="btn btn-large btn-inverse">確認する</asp:LinkButton></span>
			</p>
		</div>
	</div>
</div>
<script type="text/javascript">
<!--
	bindEvent();

	<%-- UpdataPanelの更新時のみ処理を行う --%>
	function bodyPageLoad() {
		if (Sys.WebForms == null) return;
		var isAsyncPostback = Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack();
		if (isAsyncPostback) {
			bindEvent();
		}
	}

	<%-- イベントをバインドする --%>
	function bindEvent() {
		bindExecAutoKana();
		bindZipCodeSearch();
		<% if(Constants.GLOBAL_OPTION_ENABLE) { %>
		bindTwAddressSearch();
		<% } %>
	}

	<%-- 氏名（姓・名）の自動振り仮名変換のイベントをバインドする --%>
	function bindExecAutoKana() {
		execAutoKanaWithKanaType(
			$("#<%= tbUserName1.ClientID %>"),
			$("#<%= tbUserNameKana1.ClientID %>"),
			$("#<%= tbUserName2.ClientID %>"),
			$("#<%= tbUserNameKana2.ClientID %>"));
	}

	<%-- 郵便番号検索のイベントをバインドする --%>
	function bindZipCodeSearch() {
		$("#<%= tbUserZip2.ClientID %>").keyup(function (e) {
			if (isValidKeyCodeForKeyEvent(e.keyCode) == false) return;
			checkZipCodeLengthAndExecPostback(
				$("#<%= tbUserZip1.ClientID %>"),
				$("#<%= tbUserZip2.ClientID %>"),
				"<%= tbUserZip2.UniqueID %>",
				'<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_ZIPCODE_SEARCHER_GET_ADDR_JSON %>',
				'#addrSearchErrorMessage'
			);
		});
		$("#<%= lbSearchAddr.ClientID %>").on('click', function () {
			checkZipCodeLengthAndExecPostback(
				$("#<%= tbUserZip1.ClientID %>"),
				$("#<%= tbUserZip2.ClientID %>"),
				"<%= lbSearchAddr.UniqueID %>",
				'<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_ZIPCODE_SEARCHER_GET_ADDR_JSON %>',
				'#addrSearchErrorMessage'
			);
		});
	}

	$(document).on('click', '.search-result-layer-close', function () {
		closePopupAndLayer();
	});

	$(document).on('click', '.search-result-layer-addr', function () {
		bindSelectedAddr($('li.search-result-layer-addr').index(this));
	});

	<%-- 複数住所検索結果からの選択値を入力フォームにバインドする --%>
	function bindSelectedAddr(selectedIndex) {
		var selectedAddr = $('.search-result-layer-addrs li').eq(selectedIndex);
		$("#<%= ddlUserAddr1.ClientID %>").val(selectedAddr.find('.addr').text());
		$("#<%= tbUserAddr2.ClientID %>").val(selectedAddr.find('.city').text() + selectedAddr.find('.town').text());
		$("#<%= tbUserAddr3.ClientID %>").focus();

		closePopupAndLayer();
	}

	<%-- ソーシャルログイン用 --%>
	<% if (Constants.SOCIAL_LOGIN_ENABLED) { %>
	$(function () {
		var data = $('#<%= WhfSocialLoginJson.ClientID %>').val();
		var json = data ? JSON.parse(data) : {};
		<%-- メールアドレス補完 --%>
		var mails = json['email'];
		if (mails && mails.length > 0) {
			var email = mails[0]['email'];
			if ($('#<%= WtbUserMailAddr.ClientID %>').val().length === 0) $('#<%= WtbUserMailAddr.ClientID %>').val(email);
			if ($('#<%= WtbUserMailAddrConf.ClientID %>').val().length === 0) $('#<%= WtbUserMailAddrConf.ClientID %>').val(email);
		}
	});
	<% } %>

	<% if(Constants.GLOBAL_OPTION_ENABLE) { %>
	<%-- 台湾郵便番号取得関数 --%>
	function bindTwAddressSearch() {
		$('#<%= this.WddlUserAddr3.ClientID %>').change(function (e) {
			$('#<%= this.WtbUserZipGlobal.ClientID %>').val(
				$('#<%= this.WddlUserAddr3.ClientID %>').val().split('|')[0]);
		});
	}
	<% } %>
//-->
</script>
<%--▼▼Amazonウィジェット用スクリプト▼▼--%>
<script type="text/javascript">
	window.onAmazonLoginReady = function () {
		amazon.Login.setClientId('<%=Constants.PAYMENT_AMAZON_CLIENTID %>');
	};
	window.onAmazonPaymentsReady = function () {
		if ($('#addressBookWidgetDiv').length) showAddressBookWidget();
	};

	<%-- Amazonアドレス帳表示ウィジェット --%>
	function showAddressBookWidget() {
		new OffAmazonPayments.Widgets.AddressBook({
			sellerId: '<%=Constants.PAYMENT_AMAZON_SELLERID %>',
			onOrderReferenceCreate: function (orderReference) {
				var x = orderReference.getAmazonOrderReferenceId();
				$('#<%= this.WhfAmazonOrderRefID.ClientID %>').val(x);
			},
			onAddressSelect: function (orderReference) {
				var $addressBookErrorMessage = $('#addressBookErrorMessage');
				$addressBookErrorMessage.empty();
				getAmazonAddress(function (response) {
					var data = JSON.parse(response.d);
					if (data.Error) {
						$addressBookErrorMessage.html(data.Error);
						return;
					}
					$("#<%= tbUserTel1.ClientID %>").val(data.Input.Phone1);
					$("#<%= tbUserTel2.ClientID %>").val(data.Input.Phone2);
					$("#<%= tbUserTel3.ClientID %>").val(data.Input.Phone3);
				});
			},
			design: { designMode: 'responsive' },
			onError: function (error) {
				alert(error.getErrorMessage());
				location.href = "<%=Constants.PATH_ROOT + Constants.PAGE_FRONT_LOGIN %>";
			}
		}).bind("addressBookWidgetDiv");
	}

	<%-- Amazon住所取得関数 --%>
	function getAmazonAddress(callback) {
		$.ajax({
			type: "POST",
			url: "<%= Constants.PATH_ROOT + Constants.PAGE_FRONT_USER_REGIST_INPUT%>/GetAmazonAddress",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify({
				amazonOrderReferenceId: $('#<%= this.WhfAmazonOrderRefID.ClientID %>').val()
			}),
			success: callback
		});
	}
</script>
<script async="async" type="text/javascript" charset="utf-8" src="<%=Constants.PAYMENT_AMAZON_WIDGETSSCRIPT %>"></script>
<%-- ▲▲Amazonウィジェット用スクリプト▲▲ --%>

</asp:Content>