﻿<%--
=========================================================================================================
  Module      : メールマガジン解除入力画面(MailMagazineCancelInput.aspx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ page language="C#" masterpagefile="~/Form/Common/UserPage.master" autoeventwireup="true" inherits="Form_User_MailMagazineCancelInput, App_Web_mailmagazinecancelinput.aspx.b2a7112d" title="メールマガジン解除入力ページ" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="dvUserContents">
		<h2>メールマガジン解除</h2>
	<div id="dvMailMagazineCancelInput" class="unit">
		<p>
			メールマガジンを解除される方は、下記のフォームに必要事項をご入力の上、「解除する」ボタンをクリックして下さい。<br /><br /><ins><span class="necessary">*</span>は必須入力となります。</ins></p>
		<div class="dvMailMagazineCancel">
			<table cellspacing="0">
				<tr>
					<th>
						<%: ReplaceTag("@@User.mail_addr.name@@") %><span class="necessary">*</span>
					</th>
					<td>
						<asp:TextBox ID="tbMailAddr" Runat="server" CssClass="mailAddr" MaxLength="256" Type="email"></asp:TextBox>
					</td>
				</tr>
			</table>
		</div>
		<div class="dvUserBtnBox">
			<p>
				<span><a href="javascript:history.back();" class="btn btn-large">
					キャンセル</a></span>
				<span><asp:LinkButton ID="lbCancel" runat="server" OnClick="lbCancelClick" class="btn btn-large btn-inverse">
					解除する</asp:LinkButton></span>
			</p>
		</div>
	</div>
</div>
</asp:Content>