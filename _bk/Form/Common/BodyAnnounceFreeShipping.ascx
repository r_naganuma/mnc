﻿<%--
=========================================================================================================
  Module      : 送料無料案内出力コントローラ(BodyAnnounceFreeShipping.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2009 All Rights Reserved.
=========================================================================================================
--%>
<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_BodyAnnounceFreeShipping, App_Web_bodyannouncefreeshipping.ascx.2af06a88" %>
<%--

下記は保持用のダミー情報です。削除しないでください。
<%@ FileInfo LastChanged="最終更新者" %>

--%>
<%-- ▽編集可能領域：コンテンツ▽ --%>
<div class="dvAnnounceFreeShipping">あと<span class="defPrice"><%#: CurrencyManager.ToPrice(this.DifferenceFreeShippingPrice) %></span>以上ご購入されますと<span class="defPrice freeShipping">送料無料</span>となります。</div>
<%-- △編集可能領域△ --%>