﻿<%--
=========================================================================================================
  Module      : Line Payment Script(LinePaymentScript.ascx)
  ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2020 All Rights Reserved.
=========================================================================================================
--%>
<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_LinePaymentScript, App_Web_linepaymentscript.ascx.2af06a88" %>
<script type="text/javascript">
	var isLastItemCart = false;
	var confirmSuccess = false;
	var isAuthories = false;
	var currentIndex = 0;

	<%-- Execute order --%>
	function executeOrder() {
		completeButton.click();
	}

	<% if (Constants.PAYMENT_LINEPAY_OPTION_ENABLED) { %>
	<%-- Get Line Authority --%>
	function LineRequestPayment() {
		$.ajax({
			type: "POST",
			url: "<%= Constants.PATH_ROOT + this.ConfirmPageUrl %>/GetCartIndexUseLinePaymentNotConfirm",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			async: false,
			data: JSON.stringify({
				isLanding: ('<%# this.IsLandingCartPage %>' == 'True')
			}),
			success: function (response) {
				var data = JSON.parse(response.d);
				if (data.listIndex.length > 0) {
					for (var index = 0; index < data.listIndex.length; index++) {
						ExecRequestPayment(data.listIndex[index]);
						isLastItemCart = (index == (data.listIndex.length - 1));
						break;
					}

					paymentNeedSubmitted = true;
				} else {
					paymentNeedSubmitted = false;
				}
			}
		});
	}

	<%-- Exec Request Payment --%>
	function ExecRequestPayment(index) {
		currentIndex = index;
		$.ajax({
			type: "POST",
			url: "<%= Constants.PATH_ROOT + this.ConfirmPageUrl %>/RequestLinePayPayment",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify({
				index: index,
				isLanding: '<%# this.IsLandingCartPage %>' == 'True'
			}),
			async: false,
			success: function (response) {
				var url = JSON.parse(response.d).url;
				var success = JSON.parse(response.d).success;
				if (success) {
					var myWindow = window.open(url, "Line Authority", "width=700,height=546");
				} else {
					window.location.replace(url);
				}
			}
		});
	}

	<%-- Exec Request Payment from My page --%>
	function ExecRequestPaymentMyPage(orderId, paymentId) {
		isMyPage = true;
		$.ajax({
			type: "POST",
			url: "<%= Constants.PATH_ROOT + this.OrderHistoryDetailUrl %>/RequestLinePayPaymentFromMyPage",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify({
				orderId: orderId,
				paymentId: paymentId
			}),
			async: false,
			success: function (response) {
				var isSuccess = JSON.parse(response.d).success;
				if (isSuccess) {
					var myWindow = window.open(JSON.parse(response.d).url, "Line Authority", "width=700,height=546");
				}
			}
		});
	}

	<%-- Set Line Transaction Id To Cart --%>
	function SetLineTransactionIdToCart(tranId) {
		$.ajax({
			type: "POST",
			url: "<%= Constants.PATH_ROOT + this.ConfirmPageUrl %>/SetLineTransactionIdToCart",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			async: false,
			data: JSON.stringify({
				tranId: tranId,
				index: currentIndex,
				isLanding: ('<%# this.IsLandingCartPage %>' == 'True')
			}),
			success: function (response) {
				var result = JSON.parse(response.d).success;
				if (result) {
					confirmSuccess = true;
				}
			}
		});
	}
	<% } %>
</script>