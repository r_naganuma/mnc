﻿<%--
=========================================================================================================
  Module      : 検索結果レイヤー(SearchResultLayer.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2017 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" AutoEventWireup="true" %>

<div id="search-result-layer">
	<div class="search-result-layer-title">検索結果<span class="search-result-count"></span></div>
	<ul class="search-result-layer-addrs" style="margin-top:10px;">
	</ul>
	<p class="search-result-layer-close" style="padding-top:10px; margin: 0 auto 0 auto; margin-bottom: 20px; margin-top: 15px;">閉じる</p>
</div>