﻿<%--
=========================================================================================================
  Module      : レコメンド表示出力コントローラ(BodyRecommend.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2017 All Rights Reserved.
=========================================================================================================
--%>
<%@ control language="C#" autoeventwireup="true" inherits="Form_Common_BodyRecommend, App_Web_bodyrecommend.ascx.2af06a88" %>
<%--

下記のタグはファイル情報保持用です。削除しないでください。
タイトルタグはカスタムパーツのみ利用します。
<%@ Page Title="無名のパーツ" %>
<%@ FileInfo LastChanged="ｗ２ユーザー" %>

--%>
<%-- レコメンド商品投入リンク --%>
<asp:LinkButton id="lbAddItem" runat="server" OnClick="lbAddItem_Click" />
<%-- ▽編集可能領域：コンテンツ▽ --%>
<%-- レコメンド表示 --%>
<%= this.RecommendDisplay %>
<%-- △編集可能領域△ --%>