﻿<%--
=========================================================================================================
  Module      : コンビニ後払い注意書きユーザーコントロール(PaymentDescriptionCvsDef.ascx)
 ･･･････････････････････････････････････････････････････････････････････････････････････････････････････
  Copyright   : Copyright w2solution Co.,Ltd. 2017 All Rights Reserved.
=========================================================================================================
--%>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="BaseUserControl" %>

<%-- ヤマト後払い注意書き表示 --%>
<%if(Constants.PAYMENT_CVS_DEF_KBN == Constants.PaymentCvsDef.YamatoKa) { %>
	<strong>◆『クロネコ代金後払いサービス』の詳細</strong>
	<div>
		ご注文商品の配達完了を基にヤマトクレジットファイナンス株式会社から購入者様へ請求書をお届けいたします。請求書の記載事項に従って発行日から14日以内にお支払い下さい。
		主要なコンビニエンスストア・郵便局のいずれでもお支払いできます。
	</div>
	<strong>◆ご注意</strong>
	<div>
		代金後払いのご注文には、ヤマトクレジットファイナンス株式会社の提供するクロネコ代金後払いサービス規約が適用され、サービスの範囲内で個人情報を提供し、立替払い契約を行います。<br />
		ご利用限度額は累計残高で54,000円（税込）迄です。<span style="text-decoration: underline">短期間の内に繰り返し複数のご注文をされた場合、与信審査が通らない場合がございます。</span><br />
		詳細は下記「クロネコ代金後払いサービス」をクリックしてご確認ください。
	</div>
	<br />
	<div class="txtc" style="text-align: center">
		<a href="https://www.yamato-credit-finance.co.jp/afterpayment/afterpayment.html" target="_blank">
			<img src="https://www.yamato-credit-finance.co.jp/images/afterpayment/ban_ap_01.gif" width="250" height="50" alt="クロネコ代金後払いサービス" border="0" />
		</a>
	</div>
<% } %>

<%-- GMO後払い注意書き表示 --%>
<% if (Constants.PAYMENT_CVS_DEF_KBN == Constants.PaymentCvsDef.Gmo) { %>
	<strong>◆GMO後払い</strong>
<% } %>

<%-- Atodene後払い注意書き表示 --%>
<% if (Constants.PAYMENT_CVS_DEF_KBN == Constants.PaymentCvsDef.Atodene) { %>
	<strong>◆Atodene後払い</strong>
<% } %>