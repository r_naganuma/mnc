jQuery(function($){
  $(function(){
    $('.in_view').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            $(this).addClass('on');
        }
    });

    "use strict";
    // スマホ高さいっぱい
    // 最初に、ビューポートの高さを取得し、0.01を掛けて1%の値を算出して、vh単位の値を取得
    var vh = window.innerHeight * 0.01; // カスタム変数--vhの値をドキュメントのルートに設定
  
    document.documentElement.style.setProperty('--vh', "".concat(vh, "px")); // resizeイベントの取得
  
    window.addEventListener('resize', function () {
      // あとは上記と同じスクリプトを実行
      var vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', "".concat(vh, "px"));
    });

    $('a[href^="#"]').click(function(){
      var speed = 500;
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top - 120;
      $("html, body").animate({scrollTop:position}, speed, "swing");
      return false;
    });

    $(".mvArea_slide").slick({
      autoplay: true,
      autoplaySpeed: 3000,
      infinite: true,
      swipe: true,
      arrows: false,
      dots: true,
      fade: true,
      speed: 700,
      easing: "ease-in-out",
      pauseOnFocus: false,
      pauseOnHover: false,
      pauseOnDotsHover: false,
      responsive: [{
        breakpoint: 768,
        settings: {
          fade: false,
          centerMode: true,
          centerPadding: '20px'
        }
      }]
    });

    $(".promoArea_slide").slick({
      autoplay: false,
      infinite: false,
      swipe: true,
      arrows: true,
      dots: false,
      fade: false,
      speed: 700,
      slidesToShow: 3.25,
      slidesToScroll: 1,
      easing: "ease-in-out",
      prevArrow: '<div class="slide-arrow prev-arrow"><img src="./Contents/ImagesPkg/top/icn_prev.png" alt=""></div>',
      nextArrow: '<div class="slide-arrow next-arrow"><img src="./Contents/ImagesPkg/top/icn_next.png" alt=""></div>',
      responsive: [{
        breakpoint: 768,
        settings: {
          slidesToShow: 1.4
        }
      }]
    });

    $(".lineupArea_list--slide").slick({
      autoplay: false,
      infinite: true,
      swipe: true,
      arrows: true,
      dots: true,
      fade: false,
      speed: 700,
      slidesToShow: 3,
      slidesToScroll: 3,
      easing: "ease-in-out",
      prevArrow: '',
      nextArrow: '<div class="slide-arrow next-arrow"><img src="./Contents/ImagesPkg/top/icn_next_02.png" alt=""></div>',
      responsive: [{
        breakpoint: 768,
        settings: {
          slidesToShow: 1.57,
          slidesToScroll: 1
        }
      }]
    });

    //ヘッダー追従

    var startPos = 0,winScrollTop = 0;
    $(window).on('scroll',function(){
      winScrollTop = $(this).scrollTop();
      if (winScrollTop >= startPos) {
        if(winScrollTop >= 200){
          $('header').addClass('hide');
        }
      } else {
        $('header').removeClass('hide');
      }
      startPos = winScrollTop;
    });

    //メガメニュー
    $('.headerCts_main ul .menuList').hover(
      function() {
        $('.headerCts_main ul .menuList').removeClass("on");
        $(this).addClass("on");
      }
    );
    $('#wrap').hover(
      function() {
        $('.headerCts_main ul .menuList').removeClass("on");
      }
    );

    //SPメニュー
    $('.headerCts_sub--wrap .spLine').click(function(){
      setTimeout(function(){
        $('header').addClass('open');
      },200);
      $('header').addClass('active');
      $("#wrap").addClass('open');
      $('header').removeClass('noActive');
      $('body').css('overflow-y','hidden');
    });
    $('.headerInner_close').click(function(){
      setTimeout(function(){
        $('header').removeClass('open');
      },200);
      $('header').removeClass('active');
      $("#wrap").removeClass('open');
      $('header').addClass('noActive');
      $('body').css('overflow-y','auto');
    });
    $('.headerInner_cat--list p').click(function(){
      $(this).next("ul").slideToggle();
      $(this).parent(".headerInner_cat--list").toggleClass("on");
    });
    $('.headerCts_sub--wrap .spSearch').click(function(){
      $(".searchSp").css("top",headerMainHeight);
      $(".searchSp").toggleClass("on");
    });

    // top modal
    $.cookie('modal') == 'on'?$("#top .modalArea").hide():$("#top .modalArea").show();
    $(document).on('click',function(e) {
      if(!$(e.target).closest('#top .modalArea_inner').length) {
        // ターゲット要素の外側をクリックした時の操作
        $('#top .modalArea').fadeOut();
        $.cookie('modal', 'on', { expires: 1,path: '/' }); //cookieの保存
      }
    });
    $('#top .modalArea .closeIcn').click(function(){
      $('#top .modalArea').fadeOut();
      $.cookie('modal', 'on', { expires: 1,path: '/' }); //cookieの保存
    });
  });
  
  if($(window).width()<768){
    $("#top .linkArea").slick({
      autoplay: false,
      infinite: true,
      swipe: true,
      arrows: false,
      dots: true,
      fade: false,
      speed: 700,
      centerMode: true,
      centerPadding: '30px',
      easing: "ease-in-out"
    });
  }

  function mediaQueriesWin(){
    var headerMainHeight = $('header').innerHeight();
    $('body').css('padding-top',headerMainHeight);
  }
  
  // ページがリサイズされたら動かしたい場合の記述
  $(window).resize(function() {
    mediaQueriesWin();/* ドロップダウンの関数を呼ぶ*/
  });

  $(window).on('load',function(){
    mediaQueriesWin();/* ドロップダウンの関数を呼ぶ*/
  });
})